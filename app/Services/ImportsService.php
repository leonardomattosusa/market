<?php
namespace App\Services;

use App\Jobs\ImportProductListJob;
use App\Models\Market;
use App\Models\MarketServices;
use App\Models\Media;
use App\Models\Product;
use Log;
use DB;
use Storage;
use SimpleImage;
use File;

class ImportsService{

    public function firstImport($market_id){
        $market = Market::with('serviceClass')->find($market_id);

        if($market){
            $service_name = $market->serviceClass->class_path;
            if($service_name){
                $market_service = new $service_name();
                $market_service->importAllProducts();
            }
        }
    }

    public function updateSingleProductPrice($product){
        $market_service = MarketServices::where('market_id', $product->market_id)->first();
        if($market_service){
            $class_path = $market_service->class_path;
            $service = new $class_path();
            $products = $service->getSingleProductPriceUpdate($product);

            foreach($products as $p){
                Product::where('gtin_ean', $p['gtin_ean'])->where('market_id', $product->market_id)->update(['price' => $p['price']]);
            }

            Log::channel('cronjobs')->info('Update de preço do produto '.$product->id.' do mercado '.$market_service->market->name.' rodou com sucesso.');
        }
    }

    public function updateProductPrices($market_id){
        $market_service = MarketServices::where('market_id', $market_id)->first();
        if($market_service){
            $class_path = $market_service->class_path;
            $service = new $class_path();
            $products = $service->getProductPricesUpdate(date('Y-m-d 00:00:00', strtotime('-1 day')));

            $product_gtins=[];

            foreach($products as $p){
                $product_gtins[] = $p['gtin_ean'];
                Product::where('gtin_ean', $p['gtin_ean'])->where('market_id', $market_id)->update(['price' => $p['price']]);
            }

            Log::channel('cronjobs')->info('Update de preços dos produtos do mercado '.$market_service->market->name.' rodou com sucesso. '.count($products).' tiveram os seus preços atualizados.');
        }
    }

    public function updateProductOutOfStock($market_id){
        $market_service = MarketServices::where('market_id', $market_id)->first();
        if($market_service){
            $class_path = $market_service->class_path;
            $service = new $class_path();
            $products = $service->getProductsOutOfStock();

            $product_gtins=[];

            foreach($products as $p){
                $product_gtins[] = $p['gtin_ean'];
                Product::where('gtin_ean', $p['gtin_ean'])->where('market_id', $market_id)->where('disabled', 0)->update(['disabled' => 1]);
            }

            Log::channel('cronjobs')->info('Verificação de produtos que estão sem estoque do mercado '.$market_service->market->name.' rodou com sucesso. '.count($products).' foram desativados.');
        }
    }

    public function updateProductBackToStock($market_id){
        $market_service = MarketServices::where('market_id', $market_id)->first();
        if($market_service){
            $class_path = $market_service->class_path;
            $service = new $class_path();

            $array_out_of_stock_gtins_ean = [];
            $out_of_stock_gtins_ean = Product::select('gtin_ean')->where('market_id', $market_id)->where('disabled', 1)->get();
            foreach($out_of_stock_gtins_ean as $o){
                $array_out_of_stock_gtins_ean[] = $o->gtin_ean;
            }
            $products = $service->getProductsBackToStock($array_out_of_stock_gtins_ean);

            $product_gtins=[];

            foreach($products as $p){
                $product_gtins[] = $p['gtin_ean'];
                Product::where('gtin_ean', $p['gtin_ean'])->where('market_id', $market_id)->update(['disabled' => 0]);
            }

            Log::channel('cronjobs')->info('Verificação de produtos que voltaram ao estoque do mercado '.$market_service->market->name.' rodou com sucesso. '.count($products).' foram reativados.');
        }
    }

    //Import all products to a specific market
    public function saveProductsList($products_list=[], $market_id=null){
        try {
            foreach ($products_list as $index => $p){
                $products_list[$index]['market_id'] = $market_id;

                $base_market_product = Product::where('market_id', 1)->where('gtin_ean', $p['gtin_ean'])->first();
                if($base_market_product){
                    $this->copyBaseProduct($base_market_product->id, $market_id, $products_list[$index]['price']);
                }else{
                    Product::firstOrCreate($products_list[$index]);
                }
            }
        }catch (\Exception $e){
            Log::error('Error.', [$e]);
        }

        return response('Sucesso', 200);
    }

    //copy products that aren't registered in the base market
    public function importProductsToBaseMarket(){
        $products = DB::select("SELECT * FROM market.products where gtin_ean not in (select gtin_ean from market.products where market_id = 1) and weight_based = 0 and market_id <> 1 LIMIT 50");
        $products = $this->databaseFormatToArray($products);

        ImportProductListJob::dispatch($products, 1);
    }


    public function databaseFormatToArray($products){
        $converted = [];

        foreach($products as $p){
            $converted[] = [
                'name' => $p->name,
                'ncm' => $p->ncm,
                'gtin_ean' => $p->gtin_ean,
                'price' => $p->price,
                'description' => $p->description,
                'deliverable' => $p->deliverable,
                'weight_based' => $p->weight_based,
                'quantity_unit' => $p->quantity_unit,
            ];
        }

        return $converted;
    }

    //get product images that weren't imported yet
    //$product must be a valid Product instance
    public function importProductImage($product_id){
        $product = Product::find($product_id);
        if($product){
            try {
                $product_image = file_get_contents('https://cdn-cosmos.bluesoft.com.br/products/'.$product->gtin_ean);
            }catch(\Exception $exception){
                $product_image = null;
            }

            if($product_image != null){
                Media::where('model_type', 'App\\Models\\Product')->where('model_id', $product->id)->delete();

                $media = Media::create([
                    'model_type' => 'App\\Models\\Product',
                    'model_id' => $product->id,
                    'collection_name' => 'image',
                    'name' => $product->name,
                    'file_name' => $product->gtin_ean.'.jpg',
                    'mime_type' => 'image/jpeg',
                    'disk' => 'public',
                    'size' => 0
                ]);

                //STORE PRODUCT IMAGE
                Storage::disk('public')->put($media->id.'/'.$product->gtin_ean.'.jpg', $product_image);

                //CREATE ICON IMAGE
                $icon_path = $media->id.'/conversions/'.$product->gtin_ean.'-icon.jpg';
                Storage::disk('public')->put($icon_path, $product_image);
                $image = $this->createOverlayImage($icon_path, 100, 5);
                if($image != false){
                    $image->toFile(storage_path('app/public/'.$icon_path));
                }

                //CREATE THUMB IMAGE
                $thumb_path = $media->id.'/conversions/'.$product->gtin_ean.'-thumb.jpg';
                Storage::disk('public')->put($thumb_path, $product_image);
                $image = $this->createOverlayImage($thumb_path, 200, 10);
                if($image != false){
                    $image->toFile(storage_path('app/public/'.$thumb_path));
                }
            }

            $product->image_imported = 1;
            $product->save();
        }
    }


    public function createOverlayImage($image_url, $size, $padding=10){
        try {
            $overlay = new SimpleImage();
            $overlay->fromNew($size, $size, 'white');

            $image = new SimpleImage();
            $image->fromFile(Storage::disk('public')->url($image_url));
            $image_orientation = $image->getOrientation();

            if($image_orientation == 'landscape'){
                $image->resize($size-$padding, null);
            }elseif($image_orientation == 'portrait'){
                $image->resize(null, $size-$padding);
            }else{
                $image->resize($size-$padding, $size-$padding);
            }

            $overlay->overlay($image, 'center');

            return $overlay;
        }catch (\Exception $e){
            return false;
        }

    }

    public function copyBaseProduct($model_id, $market_id, $price = null){
        $model_product = Product::find($model_id);
        $product_exists = Product::where('gtin_ean', $model_product->gtin_ean)->where('market_id', $market_id)->first();

        if(!$product_exists){
            $input = $model_product->toArray();

            // Set market, new price and unset all unecessary data to copy.
            if($price){
                $input['price'] = $price;
            }
            $input['market_id'] = $market_id;
            unset($input['id'], $input['created_at'], $input['updated_at'], $input['market'], $input['media'], $input['has_media'], $input['custom_fields']);

            $product = Product::create($input);

            try {
                $photo = Media::where('model_type', 'App\Models\Product')->where('model_id', $model_product->id)->first();

                if($photo){
                    $data = $photo->toArray();
                    $data['model_id'] = $product->id;
                    $data['manipulations'] = [];
                    $data['custom_properties'] = (object)[];
                    $data['responsive_images'] = [];

                    unset($data['id'], $data['order_column'], $data['created_at'], $data['updated_at'], $data['url'], $data['thumb'], $data['icon'], $data['formated_size']);

                    $new_photo = Media::create($data);

                    // Limpa os campos que podem causar erros e copia as imagens no STORAGE
                    File::copyDirectory(storage_path('app/public/'.$photo->id), storage_path('app/public/'.$new_photo->id));
                }
            } catch(\Exception $e){

            }

            return $product;
        }
    }
}
?>