<?php
namespace App\Services\Markets;

use App\Jobs\ImportProductListJob;
use App\Services\ImportsService;
use DB;
use Config;

class MercadoFidellize{
    private $market_id = 30;

    private function setDatabaseConnection(){
        DB::purge("fidellizeDB");
        Config::set("database.connections.fidellizeDB",[
            'driver' => 'sqlsrv',
            'host' => '177.87.34.50',
            'port' => '1433',
            "database" => 'GESTAO',
            "username" => "ecommerce",
            "password" => 'U@#zuVJVtqiv'
        ]);
    }

    //import all products from the market database to the imarket database
    public function importAllProducts(){
        $raw_products = $this->getRawProducts();
        $converted = $this->convertProductList($raw_products);
        
        $converted_divided_in_fifty = array_chunk($converted, 50);

        foreach ($converted_divided_in_fifty as $subarray){
            ImportProductListJob::dispatch($subarray, $this->market_id);
        }
    }


    //get the products from the market server
    public function getRawProducts(){
        //$raw_products = [];

        $this->setDatabaseConnection();            
        $raw_products = DB::connection("fidellizeDB")->select("select VW_ECOMMERCE_PRODUTOS.NomeProduto, VW_ECOMMERCE_PRODUTOS_BARRAS.Barras, VW_ECOMMERCE_PRODUTOS.NCM, VW_ECOMMERCE_PRODUTOS_LOJAS.PrecoVenda, VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque from VW_ECOMMERCE_PRODUTOS RIGHT JOIN VW_ECOMMERCE_PRODUTOS_BARRAS
        on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_BARRAS.CodProduto 
        INNER JOIN VW_ECOMMERCE_PRODUTOS_LOJAS on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_LOJAS.CodProduto;");

        //dd($raw_products);

        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        //</market specific code>

        return $raw_products;
    }

    //convert the products to the correct list formatz
    public function convertProductList($raw_products){
        $converted = [];

        //each market will have a specific code to convert the products, change the code below
        //<market specific code>
        foreach($raw_products as $p){
            $converted[] = [
                    'name' => $p->NomeProduto,
                    'ncm' => $p->NCM,
                    'gtin_ean' => $p->Barras,
                    'price' => $p->PrecoVenda,
                    'description' => '',
                    'deliverable' => 1,
                    'last_stock_check' => $p->QtdEstoque
                ];
        }
        //</market specific code>
        //dd($converted);
        return $converted;
    }


    //get the products from the market server
    public function getProductPricesUpdate($date){
        //$raw_products = [];

        $this->setDatabaseConnection();
        $raw_products = DB::connection("fidellizeDB")->select("select VW_ECOMMERCE_PRODUTOS.NomeProduto, VW_ECOMMERCE_PRODUTOS_BARRAS.Barras, VW_ECOMMERCE_PRODUTOS.NCM, VW_ECOMMERCE_PRODUTOS_LOJAS.PrecoVenda, VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque from VW_ECOMMERCE_PRODUTOS RIGHT JOIN VW_ECOMMERCE_PRODUTOS_BARRAS
        on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_BARRAS.CodProduto 
        INNER JOIN VW_ECOMMERCE_PRODUTOS_LOJAS on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_LOJAS.CodProduto");
        //WHERE VW_ECOMMERCE_PRODUTOS.DataUltimaAlteracao >= ?;", [$date]);

        $converted_products = $this->convertProductList($raw_products);

        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        //</market specific code>

        return $converted_products;
    }

    //get the products from the market server
    public function getSingleProductPriceUpdate($product){
        //$raw_products = [];

        $this->setDatabaseConnection();
        $raw_products = DB::connection("fidellizeDB")->select("select VW_ECOMMERCE_PRODUTOS.NomeProduto, VW_ECOMMERCE_PRODUTOS_BARRAS.Barras, VW_ECOMMERCE_PRODUTOS.NCM, VW_ECOMMERCE_PRODUTOS_LOJAS.PrecoVenda, VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque from VW_ECOMMERCE_PRODUTOS RIGHT JOIN VW_ECOMMERCE_PRODUTOS_BARRAS
        on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_BARRAS.CodProduto 
        INNER JOIN VW_ECOMMERCE_PRODUTOS_LOJAS on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_LOJAS.CodProduto 
        WHERE VW_ECOMMERCE_PRODUTOS_BARRAS.Barras = ?;", [$product->gtin_ean]);

        $converted_products = $this->convertProductList($raw_products);

        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        //</market specific code>

        return $converted_products;
    }

    public function getProductsOutOfStock(){
        //$raw_products = [];

        $this->setDatabaseConnection();
        $raw_products = DB::connection("fidellizeDB")->select("select VW_ECOMMERCE_PRODUTOS.NomeProduto, VW_ECOMMERCE_PRODUTOS_BARRAS.Barras, VW_ECOMMERCE_PRODUTOS.NCM, VW_ECOMMERCE_PRODUTOS_LOJAS.PrecoVenda, VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque from VW_ECOMMERCE_PRODUTOS RIGHT JOIN VW_ECOMMERCE_PRODUTOS_BARRAS
        on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_BARRAS.CodProduto 
        INNER JOIN VW_ECOMMERCE_PRODUTOS_LOJAS on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_LOJAS.CodProduto 
        WHERE (VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque <= 0 OR VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque IS NULL);");

        $converted_products = $this->convertProductList($raw_products);

        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        //</market specific code>

        return $converted_products;
    }

    public function getProductsBackToStock($out_of_stock_gtins_ean){
        //$raw_products = [];

        $this->setDatabaseConnection();
        $raw_products = DB::connection("fidellizeDB")->select("select VW_ECOMMERCE_PRODUTOS.NomeProduto, VW_ECOMMERCE_PRODUTOS_BARRAS.Barras, VW_ECOMMERCE_PRODUTOS.NCM, VW_ECOMMERCE_PRODUTOS_LOJAS.PrecoVenda, VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque from VW_ECOMMERCE_PRODUTOS RIGHT JOIN VW_ECOMMERCE_PRODUTOS_BARRAS
        on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_BARRAS.CodProduto 
        INNER JOIN VW_ECOMMERCE_PRODUTOS_LOJAS on VW_ECOMMERCE_PRODUTOS.CodProduto = VW_ECOMMERCE_PRODUTOS_LOJAS.CodProduto 
        WHERE VW_ECOMMERCE_PRODUTOS_LOJAS.QtdEstoque > 0 AND VW_ECOMMERCE_PRODUTOS_BARRAS.Barras IN (?);", [implode(',', $out_of_stock_gtins_ean)]);

        $converted_products = $this->convertProductList($raw_products);

        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        //</market specific code>

        return $converted_products;
    }
}
