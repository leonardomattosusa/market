<?php
namespace App\Services\Markets;

use App\Jobs\ImportProductListJob;
use App\Models\Product;
use App\Services\ImportsService;
use DB;
use Config;

class MercadoDaAdi{
    private $market_id = 29;

    private function setDatabaseConnection(){
        DB::purge("adiDB");
        Config::set("database.connections.adiDB",[
            'driver' => 'mysql',
            'host' => '179.189.37.182',
            'port' => '33306',
            "database" => 'PSi_Adi',
            "username" => "adi",
            "password" => 'P$i-adi'
        ]);
    }


    //import all products from the market database to the imarket database
    public function importAllProducts(){
        ini_set('memory_limit','2048M');
        set_time_limit(0);
        $raw_products = $this->getRawProducts();
        $converted = $this->convertProductList($raw_products);
        $converted_divided_in_fifty = array_chunk($converted, 20);

        foreach ($converted_divided_in_fifty as $subarray){
            ImportProductListJob::dispatch($subarray, $this->market_id);
        }
    }


    //get the products from the market server
    public function getRawProducts(){
        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        $this->setDatabaseConnection();
        $raw_products = DB::connection("adiDB") -> select("select * FROM (
            SELECT prd_controle, prd_descricao, prd_class_fiscal, prd_alternativo, prv_cd_produto, prv_data, prv_valor, prd_unidade 
            FROM produto INNER JOIN produto_valor ON
            produto.prd_controle = produto_valor.prv_cd_produto
             ORDER BY prv_cd_produto ASC, prv_data 
           ) QUERY 
           group by prv_cd_produto
           ");
        //</market specific code>

        return $raw_products;
    }

    //convert the products to the correct list format
    public function convertProductList($raw_products){
        $converted = [];

        //each market will have a specific code to convert the products, change the code below
        //<market specific code>
        foreach($raw_products as $p){
            $converted[] = [
                    'name' => utf8_encode($p->prd_descricao),
                    'ncm' => $p->prd_class_fiscal,
                    'gtin_ean' => $p->prd_alternativo,
                    'price' => $p->prv_valor,
                    'description' => utf8_encode($p->prd_descricao),
                    'deliverable' => 1,
                    'weight_based' => ($p->prd_unidade == 'KG' || $p->prd_unidade == 'kg' ? 1 : 0),
                    'quantity_unit' => ($p->prd_unidade == 'KG' || $p->prd_unidade == 'kg' ? 'kg' : 'un'),
                ];
        }
        //</market specific code>

        return $converted;
    }

    //get the products from the market server
    public function getSingleProductPriceUpdate($product){
        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        $this->setDatabaseConnection();
        $raw_products = DB::connection("adiDB") -> select("select * FROM (
            SELECT prd_controle, prd_descricao, prd_class_fiscal, prd_alternativo, prv_cd_produto, prv_data, prv_valor, prd_unidade 
            FROM produto INNER JOIN produto_valor ON
            produto.prd_controle = produto_valor.prv_cd_produto
            WHERE prd_alternativo = ?
             ORDER BY prv_cd_produto ASC, prv_data  DESC
           ) QUERY 
           group by prv_cd_produto
           ", [$product->gtin_ean]);
        //</market specific code>

        $converted_products = $this->convertProductList($raw_products);

        return $converted_products;
    }

    public function getProductPricesUpdate($date){
        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        $this->setDatabaseConnection();
        $raw_products = DB::connection("adiDB") -> select("select * FROM (
            SELECT prd_controle, prd_descricao, prd_class_fiscal, prd_alternativo, prv_cd_produto, prv_data, prv_valor, prd_unidade 
            FROM produto INNER JOIN produto_valor ON
            produto.prd_controle = produto_valor.prv_cd_produto
            WHERE prv_data >= ?
             ORDER BY prv_cd_produto ASC, prv_data  DESC
           ) QUERY 
           group by prv_cd_produto
           ", [$date]);
        //</market specific code>

        $converted_products = $this->convertProductList($raw_products);

        return $converted_products;
    }

    public function updateAllProductPrices(){
        //each market will have a specific code to retrieve the product data from their services, change the code below
        //<market specific code>
        $this->setDatabaseConnection();
        $raw_products = DB::connection("adiDB") -> select("select * FROM (
            SELECT prd_controle, prd_descricao, prd_class_fiscal, prd_alternativo, prv_cd_produto, prv_data, prv_valor, prd_unidade 
            FROM produto INNER JOIN produto_valor ON
            produto.prd_controle = produto_valor.prv_cd_produto
             ORDER BY prv_cd_produto ASC, prv_data  DESC
           ) QUERY 
           group by prv_cd_produto
           ");
        //</market specific code>

        $converted_products = $this->convertProductList($raw_products);

        foreach($converted_products as $p){
            $product_gtins[] = $p['gtin_ean'];
            Product::where('gtin_ean', $p['gtin_ean'])->where('market_id', $this->market_id)->update(['price' => $p['price']]);
        }
    }
}
