<?php

namespace App\Criteria\Markets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NearCriteria.
 *
 * @package namespace App\Criteria\Markets;
 */
class NearCriteria implements CriteriaInterface
{

    /**
     * @var array
     */
    private $request;

    /**
     * NearCriteria constructor.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has(['myLon', 'myLat', 'areaLon', 'areaLat'])) {

            $myLat = $this->request->get('myLat');
            $myLon = $this->request->get('myLon');
            $areaLat = $this->request->get('areaLat');
            $areaLon = $this->request->get('areaLon');

            return $model->select(DB::raw("SQRT(
                POW(69.1 * (latitude - $myLat), 2) +
                POW(69.1 * ($myLon - longitude) * COS(latitude / 57.3), 2)) AS distance, SQRT(
                POW(69.1 * (latitude - $areaLat), 2) +
                POW(69.1 * ($areaLon - longitude) * COS(latitude / 57.3), 2))  AS area"), DB::raw('(SELECT CASE WHEN EXISTS(SELECT * FROM market_open_hours mop WHERE mop.market_id = markets.id AND mop.day = '.date('w').' AND TIME(mop.start) <= "'.date('H:i:s').'" AND TIME(mop.end) >= "'.date('H:i:s').'") THEN 0 ELSE 1 END) as closed'), DB::raw('(SELECT start FROM market_delivery_hours mdp WHERE mdp.market_id = markets.id AND mdp.day = '.date('w').') as early_delivery_hour'), DB::raw('(SELECT end FROM market_delivery_hours mdp WHERE mdp.market_id = markets.id AND mdp.day = '.date('w').') as late_delivery_hour'), 'markets.id', 'markets.identifier', 'markets.fr_id', 'markets.name', 'markets.fr_login', 'markets.fr_password', 'markets.email', 'markets.social_reason', 'markets.document', 'markets.description', 'markets.address', 'markets.street', 'markets.number', 'markets.district', 'markets.complement', 'markets.city', 'markets.state', 'markets.zipcode', 'markets.latitude', 'markets.longitude', 'markets.phone', 'markets.mobile', 'markets.information', 'markets.admin_commission', 'markets.delivery_fee', 'markets.free_delivery_above', 'markets.delivery_range', 'markets.default_tax', 'markets.available_for_delivery', 'markets.mercado_pago_token', 'markets.cash_on_delivery_method', 'markets.card_on_delivery_method', 'markets.local_method', 'markets.mercado_pago_method', 'markets.saldo_fr_method', 'markets.created_at', 'markets.updated_at')
                ->havingRaw('distance <= delivery_range')
                ->orderBy('closed')
                ->orderBy('area');
        } else {
            return $model->orderBy('closed');
        }
    }
}
