<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CreateOrderPaymentCashback
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if($event->order->productOrders->first()->product->market->fr_id){
            $data = [
                'token' => '987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788',
                'id_associado' => $event->order->user->fr_id,
                'id_empresa' => $event->order->productOrders->first()->product->market->fr_id,
                'valor' => $event->order->payment->price,
                'pedido' => $event->order->toArray()
            ];

            $client = new \GuzzleHttp\Client();
            $response = $client->post('https://fidelidaderemunerada.com.br/api/imarket/cashback/cadastrar', ['json' => $data]);

            $response_data = json_decode($response->getBody());

            if($response->getStatusCode() == 200 || $response->getStatusCode() == 201){
                $cashback = $response_data->cashback_amount;

                $event->order->payment->cashback_amount = $response_data->cashback_amount;
                $event->order->payment->save();

                if($event->order && $event->order->user->device_token && $response_data->cashback_amount > 0){
                    sendNotification('Oba! Você acaba de receber o cashback de sua compra!', 'O pagamento de seu pedido foi confirmado e você recebeu R$'.number_format($response_data->cashback_amount, 2, ',', '.').' de cashback! Clique aqui para visualizar o seu saldo.', [$event->order->user->device_token]);
                }
            }else{
                Log::error('O pagamento foi confirmado porém aconteceu algum erro ao gerar o cashback deste pedido. Gere o cashback manualmente através do seu painel FR ou em caso de dúvidas entre em contato com a equipe administrativa da Fidelidade Remunerada.');

                return redirect()->back()->with('error', 'O pagamento foi confirmado porém aconteceu algum erro ao gerar o cashback deste pedido. Gere o cashback manualmente através do seu painel FR ou em caso de dúvidas entre em contato com a equipe administrativa da Fidelidade Remunerada.');
            }
        }
    }
}
