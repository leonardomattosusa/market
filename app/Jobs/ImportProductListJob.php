<?php
namespace App\Jobs;

use App\Services\ImportsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportProductListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productList;
    protected $marketId;

    public function __construct($productList, $marketId)
    {
        $this->productList = $productList;
        $this->marketId = $marketId;
    }

    public function handle(ImportsService $service)
    {
        try {
            $service->saveProductsList($this->productList, $this->marketId);
        }catch (\Exception $e){

        }
    }
}