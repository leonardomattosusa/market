<?php
namespace App\Jobs;

use App\Models\MarketServices;
use App\Services\ImportsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DailyProductCheck implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $marketId;

    public function __construct($marketId)
    {
        $this->marketId = $marketId;
    }

    public function handle(ImportsService $service)
    {
        $market_service = MarketServices::where('market_id', $this->marketId)->first();
        if($market_service){
            if($market_service->check_prices_update == 1){
                $service->updateProductPrices($this->marketId);
            }
            if($market_service->check_out_of_stock == 1){
                $service->updateProductOutOfStock($this->marketId);
            }
            if($market_service->check_back_to_stock == 1){
                $service->updateProductBackToStock($this->marketId);
            }
        }
    }
}