<?php

namespace App\Http\Requests\Website;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string'
        ];
    }

    public function messages(){
        return [
            'email.required' => 'O campo de e-mail é obrigatório',
            'email.email' => 'O campo de e-mail deve ter o formato de email válido',
            'password.required' => 'O campo de senha é obrigatório',
            'password.string' => 'O campo de senha deve conter apenas caracteres válidos'
        ];
    }
}
