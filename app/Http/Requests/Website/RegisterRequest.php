<?php

namespace App\Http\Requests\Website;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'legal_type' => 'required|integer',
            'document' => 'required',
            'phone' => 'required',
            'password' => 'required|string|same:confirm_password',
            'confirm_password' => 'required|string',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'O nome é obrigatório',
            'name.string' => 'O nome não pode conter nenhum símbolo',
            'email.required' => 'O campo de e-mail é obrigatório',
            'email.email' => 'O campo de e-mail deve ter o formato de email válido',
            'legal_type.required' => 'O campo tipo de pessoa é obrigatório',
            'legal_type.integer' => 'O campo tipo de pessoa é obrigatório',
            'document.required' => 'O campo documento é obrigatório',
            'password.required' => 'O campo de senha é obrigatório',
            'phone.required' => 'O campo de telefone é obrigatório',
            'password.string' => 'O campo de senha deve conter apenas caracteres válidos',
            'password.same' => 'O campo de confirmação de senha deve ser igual à senha',
            'confirm_password.required' => 'O campo de confirmação de senha é obrigatório',
            'confirm_password.string' => 'O campo de confirmação de senha deve conter apenas caracteres válidos'
        ];
    }
}
