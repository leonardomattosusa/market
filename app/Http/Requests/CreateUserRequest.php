<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class CreateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        User::$rules['password'] = 'required|string|min:6';
        return User::$rules;
    }

    public function messages(){
        return [
            'password.required' => 'A senha é obrigatória.',
            'password.min' => 'A senha deve ter no mínimo :min caracteres.',
            'name.required' => 'O nome é obrigatório.',
            'name.max' => 'O nome pode ter no máximo :max caracteres.',
            'legal_type.integer' => 'O tipo de pessoa é inválido.',
            'document.max' => 'O documento pode ter no máximo :max caracteres.',
            'email.required' => 'O e-mail é obrigatório.',
            'email.max' => 'O e-mail pode ter no máximo :max caracteres.',
            'email.unique' => 'Este e-mail já está cadastrado em algum usuário.',
            'email.email' => 'O e-mail é inválido.'
        ];
    }
}
