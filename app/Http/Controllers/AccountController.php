<?php

namespace App\Http\Controllers;

use App\Jobs\DailyProductCheck;
use App\Models\MarketServices;
use App\Models\MonthlyPaymentLines;
use App\Models\MonthlyPayments;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MercadoPago\SDK as MpSDK;
use MercadoPago\Preference as MPPreference;
use MercadoPago\Item as MPItem;
use App\Services\Markets\MercadoFidellize as MercadoFidellize;

class AccountController extends Controller
{
    public function index(){
        $manager = auth()->user();
        $markets = $manager->markets;

        return view('account.index', compact('manager', 'markets'));
    }

    public function addMarket(){
        $manager = auth()->user();

        return view('account.add_market', compact('manager'));
    }

    public function monthlyPayments(){
        $manager = auth()->user();

        return view('account.monthly_payments', compact('manager'));
    }

    public function showMonthlyPayment(MonthlyPayments $mp){
        $manager = auth()->user();

        MpSDK::setAccessToken("TEST-1797338572705399-042215-459dec35630845624f3fb8ba676d20ed-134884091");

        $preference = new MPPreference();

        $item = new MPItem();
        $item->title = 'Pagamento da mensalidade ID #'.$mp->id.' no iMarket.';
        $item->quantity = 1;
        $item->unit_price = $mp->total_amount;

        $preference->notification_url = 'https://imarket.digital/api/mercado_pago/monthly_payments';
        $preference->items = [$item];
        $preference->external_reference = $mp->id;
        $preference->save();

        return view('account.show_monthly_payment', compact('manager', 'mp', 'preference'));
    }

    public function payMonthlyPayment(Request $request){

    }

    public function postAddMarket(Request $request){
        $manager = auth()->user();

        if(!$manager->lastMonthlyPayment){
            return redirect()->back()->with('error', 'Ainda não há nenhuma mensalidade gerada em sua conta. Aguarde até que sua conta seja ativada para adicionar mais mercados.');
        }

        $qty = $request->markets_quantity;

        // Get how many days there are until the next payment date
        $date = new Carbon(date('Y-m-d'));
        $date_2 = new Carbon(date('Y-m-d', strtotime($manager->lastMonthlyPayment->due_date)));
        $days_until_next_payment = $date->diffInDays($date_2);

        // All the math to get the markets cost and the first month cost
        $markets_amount = $qty * 3000;
        $monthly_amount = $qty * 590;
        $first_month_amount = $monthly_amount / 31 * $days_until_next_payment;

        // Add the new markets cost + monthly cost to the next monthly payment
        $manager->lastMonthlyPayment->total_amount += ($markets_amount + $first_month_amount);
        $manager->lastMonthlyPayment->markets_quantity += $qty;
        $manager->lastMonthlyPayment->save();

        // Add 2 lines for each new market bought (3000 market cost + first month)
        for($i = 0; $i < $qty; $i++){
            $new_line = new MonthlyPaymentLines();

            $new_line->monthly_payment_id = $manager->lastMonthlyPayment->id;
            $new_line->amount = 3000;
            $new_line->description = 'Valor único refente à adição de um novo mercado em sua conta';
            $new_line->type = 'new_market';

            $new_line->save();

            $new_line = new MonthlyPaymentLines();

            $new_line->monthly_payment_id = $manager->lastMonthlyPayment->id;
            $new_line->amount = $first_month_amount / $qty;
            $new_line->description = 'Valor mensal referente ao cadastro extra de um mercado.';
            $new_line->type = 'market';

            $new_line->save();
        }

        // Increase manager markets limit
        $manager->markets_limit += $qty;
        $manager->save();

        return redirect()->route('account.index')->with('success', 'Novos mercados adicionados com sucesso.');
    }

    public function getMarketDbInfo(Request $request){
        set_time_limit(0);
        //DailyProductCheck::dispatch(30);
        $market_service = MarketServices::where('market_id', 30)->first();
        if($market_service){
            if($market_service->check_prices_update == 1){
                $service->updateProductPrices(30);
            }
            if($market_service->check_out_of_stock == 1){
                $service->updateProductOutOfStock(30);
            }
            if($market_service->check_back_to_stock == 1){
                $service->updateProductBackToStock(30);
            }
        }

        return true;
    }
}
