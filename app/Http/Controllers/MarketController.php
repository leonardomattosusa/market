<?php
/**
 * File name: MarketController.php
 * Last modified: 2020.04.29 at 18:37:10
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Criteria\Markets\MarketsOfUserCriteria;
use App\Criteria\Users\DriversCriteria;
use App\Criteria\Users\ManagersCriteria;
use App\DataTables\MarketDataTable;
use App\Events\MarketChangedEvent;
use App\Http\Requests\CreateMarketRequest;
use App\Http\Requests\UpdateMarketRequest;
use App\Models\Market;
use App\Models\MarketDeliveryHours;
use App\Models\MarketOpenHours;
use App\Models\MarketWeightBasedGtinSettings;
use App\Repositories\CustomFieldRepository;
use App\Repositories\FieldRepository;
use App\Repositories\MarketRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Flash;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class MarketController extends Controller
{
    /** @var  MarketRepository */
    private $marketRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
     */
    private $uploadRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var FieldRepository
     */
    private $fieldRepository;

    public function __construct(MarketRepository $marketRepo, CustomFieldRepository $customFieldRepo, UploadRepository $uploadRepo, UserRepository $userRepo, FieldRepository $fieldRepository)
    {
        parent::__construct();
        $this->marketRepository = $marketRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->uploadRepository = $uploadRepo;
        $this->userRepository = $userRepo;
        $this->fieldRepository = $fieldRepository;
    }

    /**
     * Display a listing of the Market.
     *
     * @param MarketDataTable $marketDataTable
     * @return Response
     */
    public function index(MarketDataTable $marketDataTable)
    {
        return $marketDataTable->render('markets.index');
    }

    /**
     * Show the form for creating a new Market.
     *
     * @return Response
     */
    public function create()
    {

        $user = $this->userRepository->getByCriteria(new ManagersCriteria())->pluck('name', 'id');
        $drivers = $this->userRepository->getByCriteria(new DriversCriteria())->pluck('name', 'id');
        $field = $this->fieldRepository->pluck('name', 'id');
        $usersSelected = [];
        $driversSelected = [];
        $fieldsSelected = [];
        $hasCustomField = in_array($this->marketRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->marketRepository->model());
            $html = generateCustomField($customFields);
        }
        $percentage = [3 => '3%', 4 => '4%', 5 => '5%', 6 => '6%'];

        return view('markets.create')->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("drivers", $drivers)->with("usersSelected", $usersSelected)->with("driversSelected", $driversSelected)->with('field', $field)->with('fieldsSelected', $fieldsSelected)->with('percentage', $percentage);
    }

    /**
     * Store a newly created Market in storage.
     *
     * @param CreateMarketRequest $request
     *
     * @return Response
     */
    public function store(CreateMarketRequest $request)
    {
        $user_markets_count = auth()->user()->markets->count();
        $cnpj_data = null;

        if($user_markets_count >= auth()->user()->markets_limit){
            Flash::error('Você já atingiu o limite de mercados. Limite do seu plano atual: '.auth()->user()->markets_limit.' mercado(s).');

            return redirect(route('markets.index'));
        }

        if(!verifyCNPJ($request->document)){
            Flash::error('CNPJ inválido.');

            return redirect(route('markets.create'))->withInput();
        }else{
            try {
                $cnpj = preg_replace('/\D/', '', $request->document);

                $client = new Client();
                $response = $client->get('https://www.receitaws.com.br/v1/cnpj/' . $cnpj);

                if($response->getStatusCode() == 200){
                    $cnpj_data = json_decode($response->getBody());
                }
            }catch(\Exception $e){
                $cnpj_data = null;
            }
        }

        $market_verifier = $this->marketRepository->where('document', $request->document)->count();

        if($market_verifier > 0){
            Flash::error('Já existe um mercado cadastrado com este CNPJ.');

            return redirect(route('markets.create'))->withInput();
        }

        $input = $request->except('open_hours_type', 'delivery_hours_type');
        if (auth()->user()->hasRole('manager')) {
            $input['users'] = [auth()->id()];
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->marketRepository->model());
        try {
            $market = $this->marketRepository->create($input);
            $market->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($market, 'image');
            }

            $this->setupHours($request, $market);

            event(new MarketChangedEvent($market));

            if(env('APP_ENV') != 'local'){
                $this->exportToFR($market->fresh(), $cnpj_data);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully', ['operator' => __('lang.market')]));

        return redirect(route('markets.index'));
    }

    protected function setupHours($request, $market){
        $open_hours = [];
        $delivery_hours = [];

        /* SETUP MARKET OPEN HOURS ARRAY */
        if($request->open_hours_type == 'custom'){
            $market->early_open_hour = null;
            $market->late_open_hour = null;
            $market->save();

            // Custom open hours
            foreach($request->open_hours as $day => $hour){
                $start_hour = date('H:i:s', strtotime($this->format_hour($hour['start'])));
                $end_hour = date('H:i:s', strtotime($this->format_hour($hour['end'])));

                $open_hours[] = ['day' => $day, 'start' => $start_hour, 'end' => $end_hour];
            }
        }else{
            // Fixed open hours
            $start_hour = date('H:i:s', strtotime($this->format_hour($request->early_open_hour)));
            $end_hour = date('H:i:s', strtotime($this->format_hour($request->late_open_hour)));

            for ($i = 0; $i <= 6; $i++){
                $open_hours[] = ['day' => $i, 'start' => $start_hour, 'end' => $end_hour];
            }
        }

        /* SETUP MARKET DELIVERY HOURS ARRAY */
        if($request->delivery_hours_type == 'custom'){
            $market->early_delivery_hour = null;
            $market->late_delivery_hour = null;
            $market->save();

            // Custom delivery hours
            foreach($request->delivery_hours as $day => $hour){
                $start_hour = date('H:i:s', strtotime($this->format_hour($hour['start'])));
                $end_hour = date('H:i:s', strtotime($this->format_hour($hour['end'])));

                $delivery_hours[] = ['day' => $day, 'start' => $start_hour, 'end' => $end_hour];
            }
        }else{
            // Fixed delivery hours
            $start_hour = date('H:i:s', strtotime($this->format_hour($request->early_delivery_hour)));
            $end_hour = date('H:i:s', strtotime($this->format_hour($request->late_delivery_hour)));

            for ($i = 0; $i <= 6; $i++){
                $delivery_hours[] = ['day' => $i, 'start' => $start_hour, 'end' => $end_hour];
            }
        }

        // SAVE OPEN HOURS
        foreach($open_hours as $hour){
            $h = MarketOpenHours::firstOrNew(['market_id' => $market->id, 'day' => $hour['day']]);

            $h->start = $hour['start'];
            $h->end = $hour['end'];

            $h->save();
        }

        // SAVE DELIVERY HOURS
        foreach($delivery_hours as $hour){
            $h = MarketDeliveryHours::firstOrNew(['market_id' => $market->id, 'day' => $hour['day']]);

            $h->start = $hour['start'];
            $h->end = $hour['end'];

            $h->save();
        }
    }

    public function exportToFR($market, $cnpj_data = null){
        if($cnpj_data && $cnpj_data->qsa){
            $socios = implode(',', collect($cnpj_data->qsa)->pluck('nome')->toArray());
        }else{
            $socios = null;
        }

        $data = [
            'id_imarket' => $market->id,
            'id_categoria' => 1,
            'nome_fantasia' => $market->name,
            'razao_social' => $market->social_reason,
            'nome_socios' => $socios,
            'cpf_cnpj' => $market->document,
            'rg_ie' => null,
            'endereco' => $market->street,
            'numero' => $market->number,
            'bairro' => $market->district,
            'complemento' => $market->complement,
            'cidade' => $market->city,
            'estado' => $market->state,
            'cep' => $market->zipcode,
            'lat' => $market->latitude,
            'lng' => $market->longitude,
            'email' => $market->email,
            'telefone' => $market->phone ? $market->phone : $market->mobile,
            'login' => $market->fr_login,
            'password' => $market->fr_password,
            'tags' => 'mercado,supermercado',
            'status' => 'Pendente de aprovação',
            'imagem' => $market->getFirstMediaUrl('image', 'icon'),
            'limite_vendas' => 1000000,
            'dias_para_gerar_boleto' => 7,
            'mostrar_site' => 'sim',
            'cobrar_taxa_adesao' => 0,
            'data_proximo_boleto_assinatura' => null,
            'porcentagem_cashback' => $market->cashback_percentage,
            'id_cadastro' => auth()->user()->fr_id,
        ];

        $client = new Client();
        $response = $client->post('https://fidelidaderemunerada.com.br/api/imarket/convenios/cadastrar', ['json' => $data]);

        $response_data = json_decode($response->getBody());

        if($response->getStatusCode() == 200 || $response->getStatusCode() == 201){
            if($response_data->success){
                $market->fr_password = Hash::make($market->fr_password);
                $market->fr_id = $response_data->convenio->id;
                $market->save();

                return true;
            }else{
                $market->delete();

                return redirect()->back()->with('error', $response_data->message);
            }
        }else{
            $market->delete();

            return redirect()->back()->with('error', 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.');
        }
    }

    /**
     * Display the specified Market.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function show($id)
    {
        $this->marketRepository->pushCriteria(new MarketsOfUserCriteria(auth()->id()));
        $market = $this->marketRepository->findWithoutFail($id);

        if (empty($market)) {
            Flash::error('Market not found');

            return redirect(route('markets.index'));
        }

        return view('markets.show')->with('market', $market);
    }

    /**
     * Show the form for editing the specified Market.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function edit($id)
    {
        $this->marketRepository->pushCriteria(new MarketsOfUserCriteria(auth()->id()));
        $market = $this->marketRepository->findWithoutFail($id);

        if (empty($market)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.market')]));
            return redirect(route('markets.index'));
        }

        $user = $this->userRepository->getByCriteria(new ManagersCriteria())->pluck('name', 'id');
        $drivers = $this->userRepository->getByCriteria(new DriversCriteria())->pluck('name', 'id');
        $field = $this->fieldRepository->pluck('name', 'id');

        $oh = MarketOpenHours::where('market_id', $market->id)->get();
        $dh = MarketDeliveryHours::where('market_id', $market->id)->get();

        $open_hours = [];
        $delivery_hours = [];

        foreach($oh as $o){
            $open_hours[$o->day] = ['start' => $o->start, 'end' => $o->end];
        }

        foreach($dh as $d){
            $delivery_hours[$d->day] = ['start' => $d->start, 'end' => $d->end];
        }

        $usersSelected = $market->users()->pluck('users.id')->toArray();
        $driversSelected = $market->drivers()->pluck('users.id')->toArray();
        $fieldsSelected = $market->fields()->pluck('fields.id')->toArray();

        $customFieldsValues = $market->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->marketRepository->model());
        $hasCustomField = in_array($this->marketRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }
        $percentage = [3 => '3%', 4 => '4%', 5 => '5%', 6 => '6%'];

        return view('markets.edit')->with('open_hours', $open_hours)->with('delivery_hours', $delivery_hours)->with('market', $market)->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("drivers", $drivers)->with("usersSelected", $usersSelected)->with("driversSelected", $driversSelected)->with('field', $field)->with('fieldsSelected', $fieldsSelected)->with('percentage', $percentage);
    }

    /**
     * Update the specified Market in storage.
     *
     * @param int $id
     * @param UpdateMarketRequest $request
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update($id, UpdateMarketRequest $request)
    {
        $this->marketRepository->pushCriteria(new MarketsOfUserCriteria(auth()->id()));
        $market = $this->marketRepository->findWithoutFail($id);

        if($market->id != 24 && !verifyCNPJ($request->document)){
            Flash::error('CNPJ inválido.');

            return redirect('admin/markets/'.$id.'/edit')->withInput();
        }

        $market_verifier = Market::where('document', $request->document)->where('id', '!=', $id)->count();

        if($market->id != 24 && $market_verifier > 0){
            Flash::error('Já existe um mercado cadastrado com este CNPJ.');

            return redirect('admin/markets/'.$id.'/edit')->withInput();
        }

        if (empty($market)) {
            Flash::error('Market not found');
            return redirect(route('markets.index'));
        }

        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->marketRepository->model());

        try {
            $market = $this->marketRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($market, 'image');
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $market->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
            event(new MarketChangedEvent($market));

            if($market->fr_id){
                $this->exportToFR($market->fresh());
            }

            $this->setupHours($request, $market);
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.market')]));

        return redirect(route('markets.index'));
    }

    public function format_hour($hour){
        $model = '##:##:00';
        $hour = str_replace(':', '', $hour);

        for($i=0;$i<strlen($hour);$i++){
            $model[strpos($model,"#")] = $hour[$i];
        }

        $model = str_replace('#', '0', $model);

        return $model;
    }

    /**
     * Remove the specified Market from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function destroy($id)
    {
        if (!env('APP_DEMO', false)) {
            $this->marketRepository->pushCriteria(new MarketsOfUserCriteria(auth()->id()));
            $market = $this->marketRepository->findWithoutFail($id);

            if (empty($market)) {
                Flash::error('Market not found');

                return redirect(route('markets.index'));
            }

            $this->marketRepository->delete($id);

            Flash::success(__('lang.deleted_successfully', ['operator' => __('lang.market')]));
        } else {
            Flash::warning('This is only demo app you can\'t change this section ');
        }
        return redirect(route('markets.index'));
    }

    /**
     * Remove Media of Market
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $market = $this->marketRepository->findWithoutFail($input['id']);
        try {
            if ($market->hasMedia($input['collection'])) {
                $market->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function settings($id){
        $this->marketRepository->pushCriteria(new MarketsOfUserCriteria(auth()->id()));
        $market = $this->marketRepository->findWithoutFail($id);

        if(!$market){
            Flash::error('Você não tem permissão para acessar esta página.');

            return redirect()->back();
        }

        return view('markets.settings', compact('market'));
    }

    public function weightBasedGtinPost($id, Request $request){
        $this->marketRepository->pushCriteria(new MarketsOfUserCriteria(auth()->id()));
        $market = $this->marketRepository->findWithoutFail($id);

        if(!$market){
            Flash::error('Você não tem permissão para acessar esta página.');

            return redirect()->back();
        }

        if($request->use_weight_based_gtin == 1){
            $setting = MarketWeightBasedGtinSettings::withTrashed()->firstOrNew(['market_id' => $market->id]);

            $setting->size = $request->size;
            $setting->initial_digit = $request->initial_digit;
            $setting->product_first_digit = $request->product_first_digit;
            $setting->product_last_digit = $request->product_last_digit;
            $setting->value_type = $request->value_type;
            $setting->value_decimal_places = $request->value_decimal_places;
            $setting->value_first_digit = $request->value_first_digit;
            $setting->value_last_digit = $request->value_last_digit;
            $setting->deleted_at = null;

            if(!$setting->save()){
                Flash::error('Aconteceu algum erro inesperado ao atualizar suas configurações de código de barras na balança. Verifique os dados e tente novamente.');

                return redirect()->back();
            }
        }else{
            $setting = MarketWeightBasedGtinSettings::withTrashed()->where('market_id', $market->id)->first();

            if($setting){
                $setting->delete();
            }
        }

        Flash::success('Configurações atualizadas com sucesso.');

        return redirect()->back();
    }
}
