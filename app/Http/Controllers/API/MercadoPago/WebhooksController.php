<?php

namespace App\Http\Controllers\API\MercadoPago;

use App\Events\OrderPaymentConfirmed;
use App\Models\MonthlyPaymentLines;
use App\Models\MonthlyPayments;
use App\Models\Order;
use App\Models\PartialPayments;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\WebhookLogs;

use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MPSdk;
use MPPreference;
use MPItem;

class WebhooksController extends Controller
{
    public function payment($market_id, Request $request){
        $market = $this->getMarket($market_id);

        $log = WebhookLogs::create(['service' => 'mercado_pago', 'request' => json_encode($request->all())]);

        MPSdk::setAccessToken($market->mercado_pago_token);

        $merchant_order = null;

        if(isset($request->data_id)){
            $payment = Payment::find_by_id($request->data_id);
            // Get the payment and the corresponding merchant_order reported by the IPN.
            $merchant_order = MerchantOrder::find_by_id($payment->order->id);
        }else{
            switch($_GET["topic"]) {
                case "payment":
                    $payment = Payment::find_by_id($_GET["id"]);
                    // Get the payment and the corresponding merchant_order reported by the IPN.
                    $merchant_order = MerchantOrder::find_by_id($payment->order->id);
                    break;
                case "merchant_order":
                    $merchant_order = MerchantOrder::find_by_id($_GET["id"]);
                    break;
            }
        }

        if(isset($payment) && $payment->status == 'approved'){
            $response = $this->confirmOrderPayment($payment->external_reference);

            $log->response = $response;
            $log->save();
        }else{
            if(!$merchant_order){
                $log->response = json_encode(['success' => false, 'merchant_order' => $merchant_order]);
            }

            $paid_amount = 0;

            foreach ($merchant_order->payments as $payment) {
                if ($payment['status'] == 'approved'){
                    $paid_amount += $payment['transaction_amount'];
                }
            }

            if($paid_amount >= $merchant_order->total_amount) {
                $response = $this->confirmOrderPayment($merchant_order->external_reference);

                $log->response = $response;
                $log->save();
            }else{
                $log->response = json_encode(['success' => false, 'merchant_order' => $merchant_order]);
                $log->save();
            }
        }

        return response('Payment updated successfully.', 200);
    }

    public function monthly_payment(Request $request){
        $log = WebhookLogs::create(['service' => 'mercado_pago', 'request' => json_encode($request->all())]);

        MPSdk::setAccessToken("TEST-1797338572705399-042215-459dec35630845624f3fb8ba676d20ed-134884091");

        $merchant_order = null;

        if(isset($request->data_id)){
            $payment = Payment::find_by_id($request->data_id);
            // Get the payment and the corresponding merchant_order reported by the IPN.
            $merchant_order = MerchantOrder::find_by_id($payment->order->id);
        }else{
            switch($_GET["topic"]) {
                case "payment":
                    $payment = Payment::find_by_id($_GET["id"]);
                    // Get the payment and the corresponding merchant_order reported by the IPN.
                    $merchant_order = MerchantOrder::find_by_id($payment->order->id);
                    break;
                case "merchant_order":
                    $merchant_order = MerchantOrder::find_by_id($_GET["id"]);
                    break;
            }
        }

        if($payment && $payment->status == 'approved'){
            $response = $this->confirmMonthlyPayment($payment->external_reference);

            $log->response = $response;
            $log->save();
        }else{
            if(!$merchant_order){
                $log->response = json_encode(['success' => false, 'merchant_order' => $merchant_order]);
            }

            $paid_amount = 0;

            foreach ($merchant_order->payments as $payment) {
                if ($payment['status'] == 'approved'){
                    $paid_amount += $payment['transaction_amount'];
                }
            }

            if($paid_amount >= $merchant_order->total_amount) {
                $response = $this->confirmMonthlyPayment($payment->external_reference);

                $log->response = $response;
                $log->save();
            }else{
                $log->response = json_encode(['success' => false, 'merchant_order' => $merchant_order]);
                $log->save();
            }
        }

        return response('Payment updated successfully.', 200);
    }

    function confirmMonthlyPayment($mp_id){
        $mp = MonthlyPayments::find($mp_id);

        $mp->status = 'paid';
        $mp->save();

        // Activate the user and the markets again
        $mp->user->status = 'active';
        $mp->user->save();

        if($mp->user->markets){
            foreach($mp->user->markets as $market){
                $market->status = 'active';
                $market->save();
            }
        }

        $this->createNewMonthlyPayment($mp);
    }

    function createNewMonthlyPayment($mp){
        $user = User::find($mp->user_id);

        $total_amount = 890 + (($user->markets_limit - 1) * 590);

        $new_monthly_payment = new MonthlyPayments();

        $new_monthly_payment->user_id = $user->id;
        $new_monthly_payment->total_amount = $total_amount;
        $new_monthly_payment->due_date = date('Y-m-05', strtotime($mp->due_date.' first day of next month'));
        $new_monthly_payment->markets_quantity = $user->markets_limit;
        $new_monthly_payment->status = 'pending';

        $new_monthly_payment->save();

        $new_line = new MonthlyPaymentLines();
        $new_line->monthly_payment_id = $new_monthly_payment->id;
        $new_line->amount = 890;
        $new_line->description = 'Valor mensal referente à conta no iMarket com direito ao cadastro de um mercado.';
        $new_line->type = 'market';
        $new_line->save();

        for($i = 0; $i < ($user->markets_limit - 1); $i++){
            $new_line = new MonthlyPaymentLines();
            $new_line->monthly_payment_id = $new_monthly_payment->id;
            $new_line->amount = 590;
            $new_line->description = 'Valor mensal referente ao cadastro extra de um mercado.';
            $new_line->type = 'market';
            $new_line->save();
        }
    }

    function confirmOrderPayment($payment_hash){
        $order = $this->getOrderByHash($payment_hash);

        $data = [
            'api_token' => 'PivvPlsQWxPl1bB5KrbKNBuraJit0PrUZekQUgtLyTRuyBq921atFtoR1HuA',
            'order_status_id' => 2
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->put('https://imarket.digital/api/orders/'.$order->id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());
            PartialPayments::where('user_id', $order->user_id)->where('status', 'pending')->whereNull('order_id')->update(['order_id' => $order->id, 'status' => 'used']);

            event(new OrderPaymentConfirmed($order));

            return json_encode(['success' => $response->success, 'order' => $response->data]);
        }else{
            return json_encode(['success' => false, 'order' => $order]);
        }
    }

    function getMarket($market_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/markets/'.$market_id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }
        }else{
            return null;
        }
    }

    function getOrderByHash($payment_hash){
        $order = Order::where('payment_hash', $payment_hash)->first();

        return $order;


//        $client = new \GuzzleHttp\Client();
//        $response = $client->get('https://imarket.digital/api/orders/payment_hash/'.$payment_hash.'?with=payment;productOrders;deliveryAddress;orderStatus');
//
//        if($response->getStatusCode() == 200){
//            $response = json_decode($response->getBody());
//
//            if($response->success){
//                return $response->data;
//            }else{
//                return null;
//            }
//        }else{
//            return null;
//        }
    }
}
