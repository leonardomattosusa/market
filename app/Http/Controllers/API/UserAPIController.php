<?php
/**
 * File name: UserAPIController.php
 * Last modified: 2020.05.04 at 09:04:09
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\CustomFieldRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rule;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\DB;

class UserAPIController extends Controller
{
    private $userRepository;
    private $uploadRepository;
    private $roleRepository;
    private $customFieldRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, UploadRepository $uploadRepository, RoleRepository $roleRepository, CustomFieldRepository $customFieldRepo)
    {
        $this->userRepository = $userRepository;
        $this->uploadRepository = $uploadRepository;
        $this->roleRepository = $roleRepository;
        $this->customFieldRepository = $customFieldRepo;
    }

    function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $user = User::whereHas('roles', function ($q) {
                $q->where('roles.id', 4);
            })
                ->where('email', $request->input('email'))
                ->first();

            if ($user && Hash::check($request->input('password'), $user->password)) {
                auth()->login($user);

                // Authentication passed...
                $user = auth()->user();
                $user->device_token = $request->input('device_token', '');
                $user->save();

                // if($user->fr_id == null && $user->document != null){
                //     $fr_data = [
                //         'token' => '987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788',
                //         'user' => $user->fresh()
                //     ];

                //     $client = new \GuzzleHttp\Client();
                //     $response = $client->post('https://fidelidaderemunerada.com.br/api/imarket/linkar_associado', ['json' => $fr_data]);

                //     if($response->getStatusCode() == 200){
                //         $response = json_decode($response->getBody());
                //         $user->fr_id = $response->id;
                //         $user->save();
                //     }
                // }

                return $this->sendResponse($user, 'User retrieved successfully');
            }

            return $this->sendError('User not found: User:' . $user . ", password: " . Hash::check($request->input('password'), $user->password), 401);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 401);
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return
     */
    function register(Request $request)
    {
        try {
            $request->merge(['document' => preg_replace('/\D/', '', $request->input('document'))]);

            $alreadyRegisteredUser = User::where('email', $request->input('email'))->first();

            if ($alreadyRegisteredUser) {
                return $this->sendError('E-mail já cadastrado', 500);
            }

            $this->validate($request, [
                'name' => 'required',
                'email' => 'unique:users|email',
                'document' => 'unique:users|string',
                'password' => 'required',
            ]);

            $user = new User;
            $user->name = $request->input('name');
            $user->legal_type = $request->input('legal_type');
            $user->document = preg_replace('/\D/', '', $request->input('document'));
            $user->email = $request->input('email');
            $user->device_token = $request->input('device_token', '');
            $user->password = Hash::make($request->input('password'));
            $user->api_token = str_random(60);

            $validQrCode = null;

            if ($request->input('qrcode')) {
                $validQrCode = DB::select("select * from qrcodes where qrcode = ? AND assigned_to is null", [$request->input('qrcode')]);
                if ($validQrCode) {
                    $user->can_use_qrcode = 1;
                } else {
                    return $this->sendError('QRcode inválido', 500);
                }
            }

            $user->save();

            if ($validQrCode) {
                DB::table('qrcodes')
                    ->where('qrcode', $request->input('qrcode'))
                    ->update(['assigned_to' => $user->id]);
            }

            $defaultRoles = $this->roleRepository->findByField('default', '1');
            $defaultRoles = $defaultRoles->pluck('name')->toArray();
            $user->assignRole($defaultRoles);

            if (copy(public_path('images/avatar_default.png'), public_path('images/avatar_default_temp.png'))) {
                $user->addMedia(public_path('images/avatar_default_temp.png'))
                    ->withCustomProperties(['uuid' => bcrypt(str_random())])
                    ->toMediaCollection('avatar');
            }

            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $user->customFieldsValues()->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 401);
        }

        return $this->sendResponse($user->fresh(), 'User retrieved successfully');
    }

    function logout(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }
        try {
            auth()->logout();
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 401);
        }
        return $this->sendResponse($user['name'], 'User logout successfully');

    }

    function user(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();

        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        return $this->sendResponse($user, 'User retrieved successfully');
    }

    function settings(Request $request)
    {
        $settings = setting()->all();
        $settings = array_intersect_key(
            $settings,
            [
                'default_tax' => '',
                'default_currency' => '',
                'default_currency_decimal_digits' => '',
                'app_name' => '',
                'currency_right' => '',
                'enable_paypal' => '',
                'enable_stripe' => '',
                'enable_razorpay' => '',
                'main_color' => '',
                'main_dark_color' => '',
                'second_color' => '',
                'second_dark_color' => '',
                'accent_color' => '',
                'accent_dark_color' => '',
                'scaffold_dark_color' => '',
                'scaffold_color' => '',
                'google_maps_key' => '',
                'mobile_language' => '',
                'app_version' => '',
                'enable_version' => '',
                'distance_unit' => '',
            ]
        );

        if (!$settings) {
            return $this->sendError('Settings not found', 401);
        }

        return $this->sendResponse($settings, 'Settings retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param Request $request
     *
     */
    public function update($id, Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], 'User not found');
        }
        $input = $request->except(['password', 'api_token']);
        try {
            if ($request->has('device_token')) {
                $user = $this->userRepository->update($request->only('device_token'), $id);
            } else {
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
                $user = $this->userRepository->update($input, $id);

                foreach (getCustomFieldsValues($customFields, $request) as $value) {
                    $user->customFieldsValues()
                        ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
                }
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }

        return $this->sendResponse($user, __('lang.updated_successfully', ['operator' => __('lang.user')]));
    }

    function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return $this->sendResponse(true, 'Reset link was sent successfully');
        } else {
            return $this->sendError('Reset link not sent', 401);
        }
    }

    function qrcodes(Request $request)
    {
        $qrcodes = DB::select("select * from qrcodes");

        return $qrcodes;
    }

    function validateQrCode(Request $request)
    {
        $qrcode = $request->input('qrcode');
        $validQrCode = DB::select("select * from qrcodes where qrcode = ?", [$qrcode]);
        $user = User::where('api_token', $request->input('api_token'))->first();

        if (!$validQrCode) {
            return ["validated" => false, "message" => "QRCode inválido"];
        }

        if ($validQrCode[0]->campaign_id != null) {
            $alreadyUsed = DB::select("select * from campaign_qrcode_uses where campaign_id = ? AND user_id = ?", [$validQrCode[0]->campaign_id, $user->id]);

            if ($alreadyUsed) {
                return ["validated" => false, "message" => "Você já participou desta campanha de descontos"];
            }

            return ["percent" => $validQrCode[0]->percent, "freeDelivery" => true];
        }

        if (!$user->can_use_qrcode) {
            return ["validated" => false, "message" => "Usuário não pode usar QRCode"];
        }

        if ($validQrCode[0]->assigned_to != $user->id) {
            return ["validated" => false, "message" => "QRCode não atribuído ao usuário"];
        }

        return ["percent" => $validQrCode[0]->percent, "freeDelivery" => true];
    }

    function toggleCanUseQrCode(Request $request)
    {
        $user = User::find($request->input('user_id'));
        if ($user) {
            $user->can_use_qrcode = $request->input('can_use_qrcode');
            $user->save();
        }
    }
}
