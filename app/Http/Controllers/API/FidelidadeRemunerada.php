<?php
namespace App\Http\Controllers\API;

use App\Models\PartialPayments;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FidelidadeRemunerada extends Controller
{
    private $api_token = '987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788';

    public function partialPayment(Request $request){
        $api_token = $request->api_token;
        $user = User::where('api_token', $api_token)->first();
        if($user){
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://fidelidaderemunerada.com.br/api/imarket/associados/consultar_saldo?api_token='.$this->api_token.'&id_associado='.$user->fr_id);
            if($response->getStatusCode() == 200){
                $response = json_decode($response->getBody());
                $balance = $response->saldo;

                return view('website.partial_payment_fr', compact('balance'));
            }
        }else{
            return response('Usuário não encontrado.', 400);
        }
    }

    public function postPartialPayment(Request $request){
        $api_token = $request->api_token;
        $user = User::where('api_token', $api_token)->first();
        if($user){
            try {
                $client = new \GuzzleHttp\Client();
                $response = $client->post('https://fidelidaderemunerada.com.br/api/imarket/compras/pagar_parcialmente_com_saldo', ['json' => ['api_token' => $this->api_token, 'id_associado' => $user->fr_id, 'valor_compra' => $request->amount]]);
                if($response->getStatusCode() == 200){
                    $response = json_decode($response->getBody());
                    if($response->success == true){
                        PartialPayments::create([
                            'user_id' => $user->id,
                            'amount' => $request->amount,
                            'status' => 'pending',
                            'type' => 'cashback'
                        ]);
                        return redirect('checkout/partial_payment_fr?returnPage=paymentMethods&api_token='.$api_token)->with(['success' => 'Pagamento efetuado com sucesso, clique em voltar e pague o restante do valor com o método de pagamento que preferir.']);
                    }else{
                        return redirect('checkout/partial_payment_fr?returnPage=paymentMethods&api_token='.$api_token)->with(['error' => $response->message]);
                    }
                }else{
                    return redirect()->back()->with(['error' => $response->message]);
                }
            } catch(\Exception $e){
                return redirect()->back()->with(['error' => $e->getMessage()]);
            }
        }else{
            return redirect()->back()->with(['error' => 'Não foi possível efetuar o pagamento, tente novamente.']);
        }
    }
}