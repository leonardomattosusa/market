<?php

namespace App\Http\Controllers\API\Driver;

use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Events\OrderPaymentConfirmed;
use App\Models\Order;
use App\Models\PartialPayments;
use App\Repositories\CartRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderStatusRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\ProductOrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

class OrderAPIController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;
    /** @var  ProductOrderRepository */
    private $productOrderRepository;
    /** @var  CartRepository */
    private $cartRepository;
    /** @var  UserRepository */
    private $userRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;
    /** @var  NotificationRepository */
    private $notificationRepository;


    public function __construct(OrderRepository $orderRepo, CustomFieldRepository $customFieldRepo, UserRepository $userRepo, OrderStatusRepository $orderStatusRepo, NotificationRepository $notificationRepo, PaymentRepository $paymentRepo)
    {
        parent::__construct();
        $this->orderRepository = $orderRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->orderStatusRepository = $orderStatusRepo;
        $this->notificationRepository = $notificationRepo;
        $this->paymentRepository = $paymentRepo;
    }
    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        try {
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            Flash::error($e->getMessage());
        }

        if($request->createdBetween){
            $start_date = explode(';', $request->createdBetween)[0];
            $final_date = explode(';', $request->createdBetween)[1];

            $this->orderRepository->whereDate('created_at', '>=', $start_date)->whereDate('created_at', '<=', $final_date);
        }

        if($request->deliveredBetween){
            $start_date = explode(';', $request->createdBetween)[0];
            $final_date = explode(';', $request->createdBetween)[1];

            $this->orderRepository->whereDate('delivery_date', '>=', $start_date)->whereDate('delivery_date', '<=', $final_date);
        }

        $orders = $this->orderRepository->where('driver_id', $user->id)->get();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        /** @var Order $order */
        if (!empty($this->orderRepository)) {
            try {
                $this->orderRepository->pushCriteria(new RequestCriteria($request));
                $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            } catch (RepositoryException $e) {
                Flash::error($e->getMessage());
            }

            $this->orderRepository->where('driver_id', $user->id);
            $order = $this->orderRepository->findWithoutFail($id);
        }

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $return = $order->toArray();
        $return['already_paid_amount'] = PartialPayments::where('order_id', $order['id'])->sum('amount');

        return $this->sendResponse($return, 'Order retrieved successfully');


    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param string $payment_hash
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderByPaymentHash(Request $request, $id)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        /** @var Order $order */
        if (!empty($this->orderRepository)) {
            try {
                $this->orderRepository->pushCriteria(new RequestCriteria($request));
                $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            } catch (RepositoryException $e) {
                Flash::error($e->getMessage());
            }

            $this->orderRepository->where('driver_id', $user->id);
            $order = $this->orderRepository->findWithoutFail($id);
        }

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $return = $order->toArray();
        $return['already_paid_amount'] = PartialPayments::where('order_id', $order['id'])->sum('amount');

        return $this->sendResponse($return, 'Order retrieved successfully');
    }

    public function markAsDelivered(Request $request, $id){
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        $this->orderRepository->where('driver_id', $user->id);
        $order = $this->orderRepository->findWithoutFail($id);

        if(empty($order)){
            return $this->sendError('Order not found', 401);
        }

        try{
            $order->delivery_date = date('Y-m-d');
            $order->order_status_id = 6;
            $order->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        if($order && $order->user->device_token){
            try{
                sendNotification('Pedido entregue', 'Seu pedido foi entregue.', [$order->user->device_token]);
            }catch(\Exception $e){
                Log::error($e->getMessage());
            }
        }

        return $this->sendResponse($order->toArray(), 'Order delivered successfully');
    }

    public function markAsPaid(Request $request, $id){
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        $this->orderRepository->where('driver_id', $user->id);
        $order = $this->orderRepository->findWithoutFail($id);

        if(empty($order)){
            return $this->sendError('Order not found', 401);
        }

        try {
            if($order->payment->status != 'Pago'){
                $this->paymentRepository->markAsPaid($order);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), 'Order paid successfully');
    }
}
