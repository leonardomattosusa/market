<?php
/**
 * File name: OrderAPIController.php
 * Last modified: 2020.04.30 at 15:28:24
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Events\OrderChangedEvent;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\PartialPayments;
use App\Models\Product;
use App\Notifications\NewOrder;
use App\Notifications\StatusChangedOrder;
use App\Repositories\CartRepository;
use App\Repositories\ProductOrderRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use Braintree\Gateway;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Stripe\Stripe;
use Stripe\Token;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */
class OrderAPIController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;
    /** @var  ProductOrderRepository */
    private $productOrderRepository;
    /** @var  CartRepository */
    private $cartRepository;
    /** @var  UserRepository */
    private $userRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;
    /** @var  NotificationRepository */
    private $notificationRepository;

    /**
     * OrderAPIController constructor.
     * @param OrderRepository $orderRepo
     * @param ProductOrderRepository $productOrderRepository
     * @param CartRepository $cartRepo
     * @param PaymentRepository $paymentRepo
     * @param NotificationRepository $notificationRepo
     * @param UserRepository $userRepository
     */
    public function __construct(OrderRepository $orderRepo, ProductOrderRepository $productOrderRepository, CartRepository $cartRepo, PaymentRepository $paymentRepo, NotificationRepository $notificationRepo, UserRepository $userRepository)
    {
        $this->orderRepository = $orderRepo;
        $this->productOrderRepository = $productOrderRepository;
        $this->cartRepository = $cartRepo;
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $this->orderRepository->pushCriteria(new OrdersOfUserCriteria($request->user()->id));
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            Flash::error($e->getMessage());
        }

        $orders = $this->orderRepository->get();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var Order $order */
        if (!empty($this->orderRepository)) {
            try {
                $this->orderRepository->pushCriteria(new RequestCriteria($request));
                $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            } catch (RepositoryException $e) {
                Flash::error($e->getMessage());
            }
            $order = $this->orderRepository->findWithoutFail($id);
        }

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');


    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param string $payment_hash
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderByPaymentHash(Request $request, $payment_hash)
    {
        /** @var Order $order */
        if (!empty($this->orderRepository)) {
            try {
                $this->orderRepository->pushCriteria(new RequestCriteria($request));
                $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            } catch (RepositoryException $e) {
                Flash::error($e->getMessage());
            }
            $order = $this->orderRepository->findByField('payment_hash', $payment_hash)->first();
        }

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $payment = $request->only('payment');
        if (isset($payment['payment']) && $payment['payment']['method']) {
            if ($payment['payment']['method'] == "Credit Card (Stripe Gateway)") {
                return $this->stripPayment($request);
            } else {
                return $this->cashPayment($request);

            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    private function stripPayment(Request $request)
    {
        $input = $request->all();
        $amount = 0;
        try {
            $user = $this->userRepository->findWithoutFail($input['user_id']);
            if (empty($user)) {
                return $this->sendError('User not found');
            }
            $stripeToken = Token::create(
                array(
                    "card" => array(
                        "number" => $input['stripe_number'],
                        "exp_month" => $input['stripe_exp_month'],
                        "exp_year" => $input['stripe_exp_year'],
                        "cvc" => $input['stripe_cvc'],
                        "name" => $user->name,
                    )
                )
            );
            if ($stripeToken->created > 0) {
                if (!empty($input['delivery_address_id'])) {
                    $order = $this->orderRepository->create(
                        $request->only('user_id', 'order_status_id', 'tax', 'delivery_address_id', 'delivery_fee')
                    );
                } else {
                    $order = $this->orderRepository->create(
                        $request->only('user_id', 'order_status_id', 'tax', 'delivery_fee')
                    );
                }
                foreach ($input['products'] as $productOrder) {
                    $productOrder['order_id'] = $order->id;
                    $amount += $productOrder['price'] * $productOrder['quantity'];
                    $this->productOrderRepository->create($productOrder);
                }
                $amountWithTax = $amount + ($amount * $order->tax / 100);
                $charge = $user->charge((int) ($amountWithTax * 100), ['source' => $stripeToken]);
                $payment = $this->paymentRepository->create([
                    "user_id" => $input['user_id'],
                    "description" => trans("lang.payment_order_done"),
                    "price" => $amountWithTax,
                    "status" => $charge->status, // $charge->status
                    "method" => $input['payment']['method'],
                ]);
                $this->orderRepository->update(['payment_id' => $payment->id], $order->id);

                $this->cartRepository->deleteWhere(['user_id' => $order->user_id]);

                Notification::send($order->productOrders[0]->product->market->users, new NewOrder($order));
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    private function cashPayment(Request $request)
    {
        $input = $request->all();


        $amount = 0;
        try {
            if (!empty($input['delivery_address_id'])) {
                $request_data = $request->only('user_id', 'order_status_id', 'tax', 'delivery_address_id', 'delivery_fee', 'payment_hash', 'scheduled_delivery_date', 'delivery_change', 'hint');
            } else {
                return $this->sendError('Endereço não informado, selecione um endereço antes de enviar o pedido.');
            }

            $request_data['order_status_id'] = ($input['payment']['method'] == 'Mercado Pago') ? 1 : 2;
            $order = $this->orderRepository->create($request_data);

            if ($input['discount_code'] != null) {
                $discountsArray = DB::select("select * from qrcodes where qrcode = ?", [$input['discount_code']]);

                if ($discountsArray) {
                    if ($discountsArray[0]->campaign_id != null) {
                        $alreadyUsed = DB::select("select * from campaign_qrcode_uses where campaign_id = ? AND user_id = ?", [$discountsArray[0]->campaign_id, $input['user_id']]);

                        if (!$alreadyUsed) {
                            DB::table('campaign_qrcode_uses')->insert([
                                'campaign_id' => $discountsArray[0]->campaign_id,
                                'user_id' => $input['user_id']
                            ]);

                            $discount = $discountsArray[0];
                            $order->hint .= ' | QRCODE: ' . $input['discount_code'];
                            $order->qrcode_used = $discount->id;
                            $order->save();
                        }
                    } else {
                        if ($discountsArray[0]->assigned_to == $input['user_id']) {
                            $discount = $discountsArray[0];
                            $order->hint .= ' | QRCODE: ' . $input['discount_code'];
                            $order->qrcode_used = $discount->id;
                            $order->save();
                        }
                    }
                }
            }

            foreach ($input['products'] as $productOrder) {
                $productOrder['order_id'] = $order->id;

                if (isset($discount) && $productOrder['category_id'] != 19) {
                    if ($discount->percent) {
                        $productOrder['price'] = $productOrder['price'] - ($productOrder['price'] * $discount->percent / 100);
                    }

                    if ($discount->amount) {
                        $productOrder['price'] = $productOrder['price'] - $discount->amount;
                    }
                }

                $amount += $productOrder['price'] * $productOrder['quantity'];

                $product_order = $this->productOrderRepository->create($productOrder);
            }

            $amountWithTax = $amount;

            if ($order->productOrders[0]->product->market && (float) $order->productOrders[0]->product->market->free_delivery_above > (float) $amountWithTax && isset($discount) == false) {
                $amountWithTax += $order->delivery_fee;
            } else {
                $order->delivery_fee = 0;
                $order->save();
            }

            $already_paid_amount = PartialPayments::where('user_id', $order->user_id)->where('status', 'pending')->whereNull('order_id')->sum('amount');
            if ($already_paid_amount) {
                $amountWithTax -= $already_paid_amount;
            }

            switch ($input['payment']['method']) {
                case 'Mercado Pago':
                    $description = 'Pagamento através do Mercado Pago';
                    break;
                case 'Saldo FR':
                    $description = 'Pagamento com saldo FR';
                    break;
                case 'Dinheiro na entrega':
                    $description = 'Pagamento em dinheiro na entrega';
                    break;
                case 'Cartão na entrega':
                    $description = 'Pagamento com cartão na entrega';
                    break;
                case 'Pagar na retirada':
                    $description = 'Pagamento será efetuado ao retirar no mercado.';
                    break;
                default:
                    $description = 'Pagamento em dinheiro na entrega';
                    break;
            }

            $payment = $this->paymentRepository->create([
                "user_id" => $input['user_id'],
                "description" => $description,
                "price" => $amountWithTax,
                "status" => $input['payment']['method'] == 'Saldo FR' ? 'Pago' : 'Aguardando pagamento',
                "method" => $input['payment']['method'],
                "payment_date" => $input['payment']['method'] == 'Saldo FR' ? date('Y-m-d H:i:s') : null
            ]);

            $this->orderRepository->update(['payment_id' => $payment->id], $order->id);
            $this->cartRepository->deleteWhere(['user_id' => $order->user_id]);

            Notification::send($order->productOrders[0]->product->market->users, new NewOrder($order));
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }
        $input = $request->all();

        try {
            $order = $this->orderRepository->update($input, $id);
            if (isset($input['order_status_id']) && $input['order_status_id'] == 2 && !empty($order) && $order->payment->status != 'Pago') {
                $this->paymentRepository->markAsPaid($order);

                event(new OrderChangedEvent($order));
            }
            Notification::send([$order->user], new StatusChangedOrder($order));

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    public function checkGtinEan($id, Request $request)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            return $this->sendError('Pedido não encontrado');
        }

        $products_order = $order->productOrders->where('product.id', $request->product_id);

        if ($products_order->count() > 0) {
            foreach ($products_order as $product_order) {
                $product = $product_order->product;

                $settings = $product->market->weight_based_gtin_settings;
                if ($product->weight_based == 1 && $settings) {
                    // Calc local gtin size in the GTIN/EAN
                    $gtin_lenght = $settings->product_last_digit - $settings->product_first_digit + 1;
                    // Calc value size in the GTIN/EAN
                    $value_lenght = $settings->value_last_digit - $settings->value_first_digit + 1;

                    $gtin_ean = ltrim(substr($request->gtin_ean, ($settings->product_first_digit - 1), $gtin_lenght), '0');
                    $value = ltrim(substr($request->gtin_ean, ($settings->value_first_digit - 1), $value_lenght), '0');

                    if ((float) $value > 0) {
                        $value = (float) $value / 100;
                    }
                } else {
                    $gtin_ean = $request->gtin_ean;
                }

                if ($product->gtin_ean == $gtin_ean) {
                    if ($product->weight_based == 1 && $settings) {
                        if ($settings->value_type == 'price') {
                            $result = round($value * 1000 / $product_order->price);

                            $product_order->quantity = $result / 1000;
                        }
                        if ($settings->value_type == 'weight') {
                            $result = round($product_order->price * $value / 1000);

                            $product_order->quantity = number_format($result, 2);
                        }
                    }

                    $product_order->check_gtin_ean = 1;
                    $product_order->save();
                } else {
                    return $this->sendError('GTIN/EAN inválido', 500);
                }
            }

            return $this->sendResponse($product_order->toArray(), 'GTIN/EAN verificado com sucesso.');
        } else {
            return $this->sendError('Produto não encontrado');
        }
    }
}
