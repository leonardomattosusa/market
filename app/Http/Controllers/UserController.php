<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Events\UserRoleChangedEvent;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\CustomFieldRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use App\Repositories\MarketRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class UserController extends Controller
{
    /** @var  UserRepository */
    private $userRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    private $uploadRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    public function __construct(UserRepository $userRepo, RoleRepository $roleRepo, UploadRepository $uploadRepo, CustomFieldRepository $customFieldRepo, MarketRepository $marketRepo)
    {
        parent::__construct();
        $this->userRepository = $userRepo;
        $this->marketRepository = $marketRepo;
        $this->roleRepository = $roleRepo;
        $this->uploadRepository = $uploadRepo;
        $this->customFieldRepository = $customFieldRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('settings.users.index');
    }

    /**
     * Display a user profile.
     *
     * @param
     * @return Response
     */
    public function profile()
    {
        $user = $this->userRepository->findWithoutFail(auth()->id());
        unset($user->password);
        $customFields = false;
        $role = $this->roleRepository->pluck('name', 'name');
        $rolesSelected = $user->getRoleNames()->toArray();
        $customFieldsValues = $user->customFieldsValues()->with('customField')->get();
        //dd($customFieldsValues);
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
            $customFields = generateCustomField($customFields, $customFieldsValues);
        }
        return view('settings.users.profile', compact(['user', 'role', 'rolesSelected', 'customFields', 'customFieldsValues']));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        if(auth()->user()->hasRole('manager')){
            $role = $this->roleRepository->where('name', 'mercado')->pluck('name', 'name');
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $role = $this->roleRepository->whereIn('name', ['admin', 'manager'])->pluck('name', 'name');
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = [];
        $rolesSelected = [];
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
            $html = generateCustomField($customFields);
        }

        return view('settings.users.create')
            ->with("role", $role)
            ->with("market", $market)
            ->with("customFields", isset($html) ? $html : false)
            ->with("marketSelected", $marketSelected)
            ->with("rolesSelected", $rolesSelected);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        if(auth()->user()->hasRole('manager')){
            $markets_ids = auth()->user()->markets->pluck('id');
            $my_users_count = $this->userRepository->join('user_markets', 'user_markets.user_id', '=', 'users.id')->whereHas('roles', function($q){
                $q->where('id', 6);
            })->whereIn('user_markets.market_id', $markets_ids->toArray())->count();

            if($my_users_count >= auth()->user()->users_limit){
                Flash::error('Você já atingiu o limite de gerentes. Limite do seu plano atual: '.auth()->user()->users_limit.' gerente(s).');

                return redirect(route('users.index'));
            }

            $input['roles'] = ['mercado'];
        }else{
            $input['roles'] = isset($input['roles']) ? $input['roles'] : [];
        }

        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = str_random(60);

        try {
            $user = $this->userRepository->create($input);

            if(auth()->user()->hasRole('manager')){
                $user->markets()->sync($input['markets']);
            }

            $user->syncRoles($input['roles']);

            $user->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            }
            event(new UserRoleChangedEvent($user));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success('salvo com sucesso.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('settings.users.show')->with('user', $user);
    }

    public function loginAsUser(Request $request, $id)
    {
        $user = $this->userRepository->findWithoutFail($id);
        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }
        auth()->login($user, true);
        if (auth()->id() !== $user->id) {
            Flash::error('User not found');
        }
        return redirect(route('users.profile'));
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && $id != auth()->id()) {
            Flash::error('Permission denied');

            return redirect(route('users.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);
        unset($user->password);
        $html = false;

        if(auth()->user()->hasRole('manager')){
            $role = $this->roleRepository->where('name', 'mercado')->pluck('name', 'name');
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $role = $this->roleRepository->pluck('name', 'name');
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = $user->markets()->pluck('id')->toArray();
        $rolesSelected = $user->getRoleNames()->toArray();
        $customFieldsValues = $user->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('settings.users.edit')
            ->with('user', $user)
            ->with("role", $role)
            ->with("market", $market)
            ->with("marketSelected", $marketSelected)
            ->with("rolesSelected", $rolesSelected)
            ->with("customFields", $html);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && $id != auth()->id()) {
            Flash::error('Permission denied');
            return redirect(route('users.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input = $request->all();

        if(auth()->user()->hasRole('manager')){
            $input['roles'] = ['mercado'];
        }else{
            $input['roles'] = isset($input['roles']) ? $input['roles'] : [];
        }

        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = Hash::make($input['password']);
        }
        try {
            if($user->id == 178){
                unset($input['email']);
            }

            $user = $this->userRepository->update($input, $id);
            if (empty($user)) {
                Flash::error('User not found');
                return redirect(route('users.index'));
            }
            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            }

            if(auth()->user()->hasRole('manager')){
                $user->markets()->sync($input['markets']);
            }else{
                $user->syncRoles($input['roles']);
            }

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $user->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
            
            event(new UserRoleChangedEvent($user));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }


        Flash::success('User atualizado com sucesso.');

        return redirect()->back();

    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
            return redirect(route('users.index'));
        }
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deletado com sucesso.');

        return redirect(route('users.index'));
    }

    /**
     * Remove Media of User
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
        } else {
            if (auth()->user()->can('medias.delete')) {
                $input = $request->all();
                $user = $this->userRepository->findWithoutFail($input['id']);
                try {
                    if ($user->hasMedia($input['collection'])) {
                        $user->getFirstMedia($input['collection'])->delete();
                    }
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        }
    }

    public function changePassword(Request $request){
        $user = auth()->user();

        if(Hash::check($request->password, $user->password)){
            if($request->new_password != $request->confirm_new_password){
                return redirect()->back()->with('error', 'A nova senha e a confirmação da nova senha devem ser iguais.');
            }

            $user->password = Hash::make($request->new_password);
            $user->save();

            return redirect()->back()->with('success', 'Sua senha foi atualizada com sucesso.');
        }else{
            return redirect()->back()->with('error', 'A senha atual informada é inválida.');
        }
    }
}
