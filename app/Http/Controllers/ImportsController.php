<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Media;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Storage;
use SimpleImage;

class ImportsController extends Controller
{
    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    public function createOverlayImage($image_url, $size, $padding = 10)
    {
        try {
            $overlay = new SimpleImage();
            $overlay->fromNew($size, $size, 'white');

            $image = new SimpleImage();
            $image->fromFile(Storage::disk('public')->url($image_url));
            $image_orientation = $image->getOrientation();

            if ($image_orientation == 'landscape') {
                $image->resize($size - $padding, null);
            } elseif ($image_orientation == 'portrait') {
                $image->resize(null, $size - $padding);
            } else {
                $image->resize($size - $padding, $size - $padding);
            }

            $overlay->overlay($image, 'center');

            return $overlay;
        } catch (\Exception $e) {
            return false;
        }

    }


    public function transformCsv()
    {
        set_time_limit(0);

        $csvFile = public_path('produtos_imarket.csv');
        $csv_data = $this->readCSV($csvFile, array('delimiter' => ','));

        foreach ($csv_data as $index => $row) {
            $csv_data[$index] = $row;

            $product = Product::firstOrCreate([
                'gtin_ean' => $row[0],
                'market_id' => 30
            ], [
                'name' => $row[1],
                'price' => $row[2],
            ]);
        }

    }

    public function crawlImagesForDatabaseProducts()
    {
        ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 6.0)');
        set_time_limit(0);

        $products = Product::where('image_imported', 0)->where('gtin_ean', '!=', null)->where('market_id', 1)->limit(500)->get();

        foreach ($products as $index => $product) {
            try {
                $product_image = file_get_contents('https://cdn-cosmos.bluesoft.com.br/products/' . $product->gtin_ean);

                // print_r($product_image);
                // exit;
            } catch (\Exception $exception) {
                // throw $exception;
                $product_image = null;
            }

            if ($product_image != null) {
                Media::where('model_type', 'App\\Models\\Product')->where('model_id', $product->id)->delete();

                $media = Media::create([
                    'model_type' => 'App\\Models\\Product',
                    'model_id' => $product->id,
                    'collection_name' => 'image',
                    'name' => $product->name,
                    'file_name' => $product->gtin_ean . '.jpg',
                    'mime_type' => 'image/jpeg',
                    'disk' => 'public',
                    'size' => 0
                ]);

                //STORE PRODUCT IMAGE
                Storage::disk('public')->put($media->id . '/' . $product->gtin_ean . '.jpg', $product_image);

                //CREATE ICON IMAGE
                $icon_path = $media->id . '/conversions/' . $product->gtin_ean . '-icon.jpg';
                Storage::disk('public')->put($icon_path, $product_image);
                $image = $this->createOverlayImage($icon_path, 100, 5);
                if ($image != false) {
                    $image->toFile(storage_path('app/public/' . $icon_path));
                }

                //CREATE THUMB IMAGE
                $thumb_path = $media->id . '/conversions/' . $product->gtin_ean . '-thumb.jpg';
                Storage::disk('public')->put($thumb_path, $product_image);
                $image = $this->createOverlayImage($thumb_path, 200, 10);
                if ($image != false) {
                    $image->toFile(storage_path('app/public/' . $thumb_path));
                }

                echo "Product " . $product->name . "[" . $product->id . "] has a new image.<br>";
            }

            $product->image_imported = 1;
            $product->save();
        }
    }

}
