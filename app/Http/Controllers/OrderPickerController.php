<?php

namespace App\Http\Controllers;

use App\Criteria\Users\OrderPickerssCriteria;
use App\DataTables\OrderPickerDataTable;
use App\Events\UserRoleChangedEvent;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\OrderPickersRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\MarketRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class OrderPickerController extends Controller
{
    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;
    private $uploadRepository;

    public function __construct(CustomFieldRepository $customFieldRepo , UserRepository $userRepo, RoleRepository $roleRepo, UploadRepository $uploadRepo, MarketRepository $marketRepo)
    {
        parent::__construct();
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->roleRepository = $roleRepo;
        $this->uploadRepository = $uploadRepo;
        $this->marketRepository = $marketRepo;
    }

    /**
     * Display a listing of the OrderPickers.
     *
     * @param OrderPickerDataTable $orderPickerDataTable
     * @return Response
     */
    public function index(OrderPickerDataTable $orderPickerDataTable)
    {
        return $orderPickerDataTable->render('order_pickers.index');
    }

    /**
     * Show the form for creating a new OrderPickers.
     *
     * @return Response
     */
    public function create()
    {
        if(auth()->user()->hasRole('manager')){
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = [];
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
            $html = generateCustomField($customFields);
        }

        return view('order_pickers.create')
            ->with("market", $market)
            ->with("customFields", isset($html) ? $html : false)
            ->with("marketSelected", $marketSelected);
    }

    /**
     * Store a newly created OrderPickers in storage.
     *
     * @param CreateOrderPickersRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input['roles'] = ['order_picker'];
        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = str_random(60);

        try {
            $user = $this->userRepository->create($input);

            if(auth()->user()->hasRole('manager')){
                $user->markets()->sync($input['markets']);
            }

            $user->syncRoles($input['roles']);

            $user->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            }
            event(new UserRoleChangedEvent($user));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success('Separador salvo com sucesso.');

        return redirect(route('order_pickers.index'));
    }

    /**
     * Display the specified OrderPickers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Separador não encontrado.');

            return redirect(route('order_pickers.index'));
        }

        return view('order_pickers.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified OrderPickers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && !auth()->user()->hasRole('mercado') && $id != auth()->id()) {
            Flash::error('Você não tem essa permissão.');

            return redirect(route('order_pickers.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);
        unset($user->password);
        $html = false;

        if(auth()->user()->hasRole('manager')){
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = $user->markets()->pluck('id')->toArray();
        $customFieldsValues = $user->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        if (empty($user)) {
            Flash::error('Separador não encontrado.');

            return redirect(route('order_pickers.index'));
        }

        return view('order_pickers.edit')
            ->with('user', $user)
            ->with("market", $market)
            ->with("marketSelected", $marketSelected)
            ->with("customFields", $html);
    }

    /**
     * Update the specified OrderPickers in storage.
     *
     * @param  int              $id
     * @param UpdateOrderPickersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && !auth()->user()->hasRole('mercado') && $id != auth()->id()) {
            Flash::error('Você não tem permissão para alterar separadores.');
            return redirect(route('order_pickers.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Separador não encontrado');
            return redirect(route('order_pickers.index'));
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input = $request->all();

        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = Hash::make($input['password']);
        }

        try {
            $user = $this->userRepository->update($input, $id);
            if (empty($user)) {
                Flash::error('Separador não encontrado');
                return redirect(route('order_pickers.index'));
            }
            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            }

            $user->markets()->sync($input['markets']);

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $user->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }

            event(new UserRoleChangedEvent($user));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }


        Flash::success('Separador atualizado com sucesso.');

        return redirect()->back();
    }

    /**
     * Remove the specified OrderPickers from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order_picker = $this->userRepository->findWithoutFail($id);

        if (empty($order_picker)) {
            Flash::error('Separador não encontrado');

            return redirect(route('order_pickers.index'));
        }

        $this->userRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.order_picker')]));

        return redirect(route('order_pickers.index'));
    }

    /**
     * Remove Media of OrderPickers
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $user = $this->userRepository->findWithoutFail($input['id']);
        try {
            if($user->hasMedia($input['collection'])){
                $user->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function reports(Request $request)
    {
        $order_pickers = null;

        if (auth()->user()->hasRole('manager')) {
            $markets_ids = auth()->user()->markets->pluck('id')->toArray();

            $order_pickers = $this->userRepository->with('roles')->whereHas('roles', function($q){
                    $q->where('roles.id', 7);
                })
                ->join('user_markets', 'user_markets.user_id', '=', 'users.id')
                ->whereIn('user_markets.market_id', $markets_ids)
                ->where('users.id', '!=', auth()->id())
                ->select('users.*')
                ->groupBy('users.id')
                ->get();
        }else{
            $order_pickers = $this->userRepository->with('roles')->whereHas('roles', function($q){
                $q->where('roles.id', 7);
            })->select('users.*')->groupBy('users.id')->get();
        }

        return view('order_pickers.reports', compact('order_pickers'))->with('start_date', $request->start_date)->with('final_date', $request->final_date);
    }
}
