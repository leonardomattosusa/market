<?php
/**
 * File name: OrderController.php
 * Last modified: 2020.04.29 at 18:37:35
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Criteria\Users\ClientsCriteria;
use App\Criteria\Users\DriversCriteria;
use App\Criteria\Users\DriversOfMarketCriteria;
use App\DataTables\OrderDataTable;
use App\DataTables\ProductOrderDataTable;
use App\Events\OrderChangedEvent;
use App\Events\OrderPaymentConfirmed;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Driver;
use App\Models\Order;
use App\Models\PartialPayments;
use App\Models\Product;
use App\Models\ProductOrder;
use App\Models\User;
use App\Notifications\AssignedOrder;
use App\Notifications\StatusChangedOrder;
use App\Repositories\CustomFieldRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderStatusRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class OrderController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderStatusRepository
     */
    private $orderStatusRepository;
    /** @var  NotificationRepository */
    private $notificationRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(OrderRepository $orderRepo, CustomFieldRepository $customFieldRepo, UserRepository $userRepo
        , OrderStatusRepository $orderStatusRepo, NotificationRepository $notificationRepo, PaymentRepository $paymentRepo)
    {
        parent::__construct();
        $this->orderRepository = $orderRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->orderStatusRepository = $orderStatusRepo;
        $this->notificationRepository = $notificationRepo;
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param OrderDataTable $orderDataTable
     * @return Response
     */
    public function index(OrderDataTable $orderDataTable)
    {
        return $orderDataTable->render('orders.index');
    }

    public function list(OrderDataTable $orderDataTable)
    {
        if(in_array(request()->segment(4), ['pending','processing','delivered','canceled'])){
            return $orderDataTable->render('orders.index');
        }else{
            if (auth()->user()->hasRole('admin')) {
                $orders = Order::with("user")->with("orderStatus")->with('payment');
            } else if (auth()->user()->hasRole('manager') || auth()->user()->hasRole('mercado') || auth()->user()->hasRole('order_picker')) {
                $orders = Order::with("user")->with("orderStatus")->with('payment')
                    ->join("product_orders", "orders.id", "=", "product_orders.order_id")
                    ->join("products", "products.id", "=", "product_orders.product_id")
                    ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
                    ->where('user_markets.user_id', auth()->id())
                    ->groupBy('orders.id')
                    ->orderBy('scheduled_delivery_date')
                    ->select('orders.*');
            } else if (auth()->user()->hasRole('client')) {
                $orders = Order::with("user")->with("orderStatus")->with('payment')
                    ->where('orders.user_id', auth()->id())
                    ->groupBy('orders.id')
                    ->orderBy('scheduled_delivery_date')
                    ->select('orders.*');
            } else if (auth()->user()->hasRole('driver')) {
                $orders = Order::with("user")->with("orderStatus")->with('payment')
                    ->where('orders.driver_id', auth()->id())
                    ->groupBy('orders.id')
                    ->orderBy('scheduled_delivery_date')
                    ->select('orders.*');
            } else {
                $orders = Order::with("user")->with("orderStatus")->with('payment');
            }

            switch (request()->segment(4)){
                case 'ready_to_collect':
                    $orders = $orders->where('order_status_id', 4)->whereHas('payment', function($q){
                        $q->where('method', 'Pagar na retirada');
                    })->get();
                    break;
                case 'ready_to_delivery':
                    $orders = $orders->where('order_status_id', 4)->whereHas('payment', function($q){
                        $q->where('method', '!=', 'Pagar na retirada');
                    })->get();
                    break;
                case 'sent':
                    $orders = $orders->where('order_status_id', 5)->get();
                    break;
                default:
                    $orders = $orders->where('order_status_id', 4)->get();
                    break;
            }

            if (auth()->user()->hasRole('manager') || auth()->user()->hasRole('mercado') || auth()->user()->hasRole('order_picker')) {
                $markets_ids = auth()->user()->markets->pluck('id')->toArray();

                $drivers = User::with('roles')->whereHas('roles', function($q){
                        $q->where('roles.id', 5);
                    })
                    ->join('user_markets', 'user_markets.user_id', '=', 'users.id')
                    ->whereIn('user_markets.market_id', $markets_ids)
                    ->where('users.id', '!=', auth()->id())
                    ->select('users.*')
                    ->groupBy('users.id')
                    ->get();
            }else{
                $drivers = User::with('roles')->whereHas('roles', function($q){
                    $q->where('roles.id', 5);
                })->get();
            }

            return view('orders.index', compact('orders', 'drivers'));
        }
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        $user = $this->userRepository->getByCriteria(new ClientsCriteria())->pluck('name', 'id');
        $driver = $this->userRepository->getByCriteria(new DriversCriteria())->pluck('name', 'id');

        $orderStatus = $this->orderStatusRepository->pluck('status', 'id');

        $hasCustomField = in_array($this->orderRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('orders.create')->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("driver", $driver)->with("orderStatus", $orderStatus);
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
        try {
            $order = $this->orderRepository->create($input);
            $order->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     * @param ProductOrderDataTable $productOrderDataTable
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function show(ProductOrderDataTable $productOrderDataTable, $id)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);
        if (empty($order)) {
            return redirect(route('orders.list', ['pending']));
        }
        $subtotal = 0;

        foreach ($order->productOrders as $productOrder) {
            $subtotal += $productOrder->price * $productOrder->quantity;
        }

        $change = $order['delivery_change'];
        $total = $subtotal + $order['delivery_fee'];
        $total += ($total * $order['tax'] / 100);
        $productOrderDataTable->id = $id;

        $partial_payments_amount = PartialPayments::where('order_id', $id)->sum('amount');

        return $productOrderDataTable->render('orders.show', ["order" => $order, "total" => $total, "subtotal" => $subtotal, 'partial_payments_amount' => $partial_payments_amount, 'change' => $change]);
    }

    public function printPaymentReceipt($id)
    {

        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);
        if (empty($order)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

            return redirect(route('orders.index'));
        }
        if($order->payment->method != 'Saldo FR'){
            Flash::error('O comprovante de pagamento só pode ser gerado para pedidos pagos com Saldo FR');

            return redirect()->back();
        }

        return view('orders.print_payment_receipt', compact('order'));
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function edit($id)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);
        if (empty($order)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

            return redirect(route('orders.index'));
        }

        $market = $order->productOrders()->first();
        $market = isset($market) ? $market->product['market_id'] : 0;

        $user = $this->userRepository->getByCriteria(new ClientsCriteria())->pluck('name', 'id');
        $driver = $this->userRepository->getByCriteria(new DriversOfMarketCriteria($market))->pluck('name', 'id');
        $orderStatus = $this->orderStatusRepository->pluck('status', 'id');


        $customFieldsValues = $order->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
        $hasCustomField = in_array($this->orderRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('orders.edit')->with('order', $order)->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("driver", $driver)->with("orderStatus", $orderStatus);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $oldOrder = $this->orderRepository->findWithoutFail($id);
        if (empty($oldOrder)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));
            return redirect(route('orders.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->orderRepository->model());
        try {
//            if (isset($input['driver_id']) && $input['driver_id'] == "0") {
//                unset($input['driver_id']);
//            }
            $order = $this->orderRepository->update($input, $id);

            if (setting('enable_notifications', false)) {
                if ($input['order_status_id'] != $oldOrder->order_status_id) {
                    Notification::send([$order->user], new StatusChangedOrder($order));
                }

                if (isset($input['driver_id']) && ($input['driver_id'] != $oldOrder['driver_id'])) {
                    $driver = $this->userRepository->findWithoutFail($input['driver_id']);
                    if (!empty($driver)) {
                        Notification::send([$driver], new AssignedOrder($order));
                    }
                }
            }

            $this->paymentRepository->update([
                "status" => $input['status'],
            ], $order['payment_id']);

            event(new OrderChangedEvent($order));

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $order->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function destroy($id)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

            return redirect(route('orders.list', ['pending']));
        }

        $order->active = 0;
        $order->save();

        Flash::success('Pedido cancelado com sucesso.');

        return redirect(route('orders.list', ['pending']));
    }

    /**
     * Restore the specified Order from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function restore($id)
    {
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->where('orders.id', $id)->withTrashed()->first();

        if (empty($order)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.order')]));

            return redirect(route('orders.index'));
        }

        if($order->deleted_at != null){
            $order->restore();
        }

        $order->active = 1;
        $order->save();

        Flash::success(__('lang.restored_successfully', ['operator' => __('lang.order')]));

        return redirect()->back();
    }

    /**
     * Remove Media of Order
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->findWithoutFail($input['id']);
        try {
            if ($order->hasMedia($input['collection'])) {
                $order->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function changeStatus($id, $status, Request $request){
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);

        try {
            if($status == 6 && $order->payment->status != 'Pago'){
                Flash::error(__('lang.payment_needed_to_deliver'));

                return redirect()->back();
            }

            if($status == 4 && $order->productOrders->where('final_quantity_defined', 0)->count() > 0){
                Flash::error(__('lang.define_quantities_first'));

                return redirect()->back();
            }

            if($status == 4 && $order->productOrders->where('check_gtin_ean', 0)->count() > 0){
                Flash::error(__('lang.check_gtin_ean_first'));

                return redirect()->back();
            }

            if($status == 3){
                try{
                    $order->order_picker_id = auth()->id();
                    $order->preparing_start_date = date('Y-m-d H:i:s');
                    $order->save();

                    sendNotification('Pedido em preparo', 'Seu pedido está sendo preparado. A estimativa para a entrega de seu pedido é de '.$request->delivery_minutes.' minutos', [$order->user->device_token]);
                }catch(\Exception $e){
                    Log::error($e->getMessage());
                }
            }

            if($status == 4){
                try{
                    $order->preparing_end_date = date('Y-m-d H:i:s');

                    $datetime1 = date_create($order->preparing_start_date);
                    $datetime2 = date_create($order->preparing_end_date);
                    $interval = date_diff($datetime2, $datetime1);
                    $order->preparing_time_taken = $interval->format('%H:%I:%S');

                    $order->save();

                    if($order->payment->method == 'Coleta no local'){
                        sendNotification('Pedido pronto para coleta', 'Seu pedido está pronto para coleta.', [$order->user->device_token]);
                    }else{
                        sendNotification('Pedido separado', 'Seu pedido está separado e aguardando envio.', [$order->user->device_token]);
                    }
                }catch(\Exception $e){
                    Log::error($e->getMessage());
                }
            }

            $order->order_status_id = $status;
            $order->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.show', [$id]));
    }

    public function markAsSent(Request $request){
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));

        $devices = [];

        if(!$request->orders){
            Flash::error('Você tem que selecionar ao menos um pedido para marcar como à caminho.');

            return redirect()->back();
        }

        foreach($request->orders as $key => $id){
            $order = $this->orderRepository->findWithoutFail($id);

            $order->driver_id = $request->driver_id;
            $order->order_status_id = 5;
            $order->save();

            if($order->user->device_token){
                $devices[] = $order->user->device_token;
            }
        }

        if(!empty($devices)){
            try{
                foreach ($devices as $device){
                    sendNotification('Pedido enviado', 'Seu pedido está a caminho.', [$device]);
                }
            }catch(\Exception $e){
                Log::error($e->getMessage());
            }
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.list', ['sent']));
    }

    public function deliverOrder(Request $request, $id){
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));

        try{
            $order = $this->orderRepository->findWithoutFail($id);

            $order->delivery_date = date('Y-m-d');
            $order->order_status_id = 6;
            $order->save();

            if($order->payment->status != 'Pago'){
                $this->paymentRepository->markAsPaid($order);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        if($order && $order->user->device_token){
            try{
                sendNotification('Pedido entregue', 'Seu pedido foi entregue.', [$order->user->device_token]);
            }catch(\Exception $e){
                Log::error($e->getMessage());
            }
        }

        Flash::success('Entrega do pedido confirmada com sucesso.');

        return redirect(route('orders.list', ['ready_to_collect']));
    }

    public function markAsDelivered(Request $request){
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));

        $devices = [];

        if(!$request->orders){
            Flash::error('Você tem que selecionar ao menos um pedido para marcar como entregue.');

            return redirect()->back();
        }

        foreach($request->orders as $key => $id){
            $order = $this->orderRepository->findWithoutFail($id);

            $order->delivery_date = date('Y-m-d');
            $order->order_status_id = 6;
            $order->save();

            if($order->payment->status != 'Pago'){
                $this->paymentRepository->markAsPaid($order);
            }

            if($order->user->device_token){
                $devices[] = $order->user->device_token;
            }
        }

        if(!empty($devices)){
            try{
                foreach($devices as $device){
                    sendNotification('Pedido entregue', 'Seu pedido no iMarket foi entregue.', [$device]);
                }
            }catch(\Exception $e){
                Log::error($e->getMessage());
            }
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.list', ['delivered']));
    }

    public function markAsPaid($id){
        $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        $order = $this->orderRepository->findWithoutFail($id);

        try {
            $this->paymentRepository->markAsPaid($order);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.order')]));

        return redirect(route('orders.show', [$id]));
    }

    public function updateQuantities(Request $request, $id){
        $order = $this->orderRepository->find($id);

        if(!$order){
            Flash::error('Aconteceu algum erro inesperado. Tente novamente em alguns minutos.');
            return redirect()->back();
        }

        try {
            foreach($request->order_products as $id => $quantity){
                $product_order = ProductOrder::with('product')->find($id);


                if($product_order->product->quantity_unit == 'g'){
                    $quantity = $quantity / 1000;
                }

                if($quantity <= 0){
                    Flash::error('A quantidade digitada deve ser maior que zero.');
                    return redirect()->back();
                }

                $actual_price = $product_order->product->price * $product_order->quantity;
                $new_price = $product_order->product->price * $quantity;

                $product_order->quantity = $quantity;
                $product_order->final_quantity_defined = 1;
                $product_order->save();

                $product_order->order->payment->price -= $actual_price;
                $product_order->order->payment->price += $new_price;
                $product_order->order->payment->save();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        if($order && $order->user->device_token){
            try{
                $price = $order->payment->fresh()->price;

                sendNotification('Pedido foi revisado', 'Seu pedido foi revisado e está sendo preparado. O valor final do seu pedido é: R$ '.number_format($price, 2, ',', '.'), [$order->user->device_token]);
            }catch(\Exception $e){
                Log::error($e->getMessage());
            }
        }

        Flash::success('Peso final dos produtos confirmado com sucesso. O valor final do pedido é: R$'.number_format($order->payment->fresh()->price,2,',','.'));

        return redirect()->back();
    }

    public function addProductOrder($order_id, Request $request){
        $order = $this->orderRepository->find($order_id);

        if(!$order){
            Flash::error('Aconteceu algum erro inesperado. Tente novamente em alguns minutos.');
            return redirect()->back();
        }

        $product_id = $request->product_id;
        $quantity = $request->quantity;

        $product = Product::findOrFail($product_id);

        if($product->weight_based == 1){
            $quantity = $quantity / 1000;
        }

        if($quantity <= 0){
            Flash::error('A quantidade digitada deve ser maior que zero.');
            return redirect()->back();
        }

        if(ProductOrder::where('order_id', $order->id)->where('product_id', $product->id)->first()){
            Flash::error('Este produto já foi adicionado à este pedido.');
            return redirect()->back();
        }

        $product_order = ProductOrder::create([
            'price' => $product->price,
            'quantity' => $quantity,
            'final_quantity_defined' => 1,
            'product_id' => $product->id,
            'order_id' => $order->id,
            'check_gtin_ean' => 0
        ]);

        if($product_order){
            // Update order price removing the product
            $order->payment->price += ($product_order->product->price * $product_order->quantity);
            $order->payment->save();

            Flash::success('Produto adicionado com sucesso.');
        }else{
            Flash::error('Não foi possível adicionar este produto ao pedido. Tente novamente em alguns minutos.');
        }

        return redirect()->back();
    }

    public function removeProductOrder($order_id, $product_order_id, Request $request){
        $order = $this->orderRepository->find($order_id);

        if(!$order){
            Flash::error('Aconteceu algum erro inesperado. Tente novamente em alguns minutos.');
            return redirect()->back();
        }

        $product_order = ProductOrder::with('product')->where('order_id', $order->id)->find($product_order_id);

        $total_quantity = $order->productOrders->sum('quantity');

        if($total_quantity <= $product_order->quantity){
            Flash::error('Você não pode remover todos os produtos do pedido.');

            return redirect()->back();
        }

        if($product_order->delete()){
            // Update order price removing the product
            $order->payment->price -= ($product_order->product->price * $product_order->quantity);
            $order->payment->save();

            Flash::success('Produto removido com sucesso.');
        }else{
            Flash::error('Não foi possível remover este produto.');
        }

        return redirect()->back();
    }
}
