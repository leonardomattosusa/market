<?php

namespace App\Http\Controllers;

use App\DataTables\CustomerDataTable;
use App\Events\UserRoleChangedEvent;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\CustomFieldRepository;
use App\Repositories\MarketRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;
use Flash;

class CustomerController extends Controller
{
    /** @var  UserRepository */
    private $userRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    private $uploadRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    public function __construct(UserRepository $userRepo, RoleRepository $roleRepo, UploadRepository $uploadRepo,
                                CustomFieldRepository $customFieldRepo, MarketRepository $marketRepo)
    {
        parent::__construct();
        $this->userRepository = $userRepo;
        $this->marketRepository = $marketRepo;
        $this->roleRepository = $roleRepo;
        $this->uploadRepository = $uploadRepo;
        $this->customFieldRepository = $customFieldRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param CustomerDataTable $customerDataTable
     * @return Response
     */
    public function index(CustomerDataTable $customerDataTable)
    {
        return $customerDataTable->render('customers.index');
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && $id != auth()->id()) {
            Flash::error('Permission denied');

            return redirect(route('customers.index'));
        }

        $customer = $this->userRepository->findWithoutFail($id);
        unset($customer->password);
        $html = false;

        if(auth()->user()->hasRole('manager')){
            $role = $this->roleRepository->where('name', 'mercado')->pluck('name', 'name');
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $role = $this->roleRepository->pluck('name', 'name');
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = $customer->markets()->pluck('id')->toArray();
        $rolesSelected = $customer->getRoleNames()->toArray();
        $customFieldsValues = $customer->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        if (empty($customer)) {
            Flash::error('User not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')
            ->with('user', $customer)
            ->with("role", $role)
            ->with("market", $market)
            ->with("marketSelected", $marketSelected)
            ->with("rolesSelected", $rolesSelected)
            ->with("customFields", $html);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && $id != auth()->id()) {
            Flash::error('Permission denied');
            return redirect(route('customers.index'));
        }

        $customer = $this->userRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('User not found');
            return redirect(route('customers.index'));
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input = $request->all();

        if(auth()->user()->hasRole('manager')){
            $input['roles'] = ['mercado'];
        }else{
            $input['roles'] = isset($input['roles']) ? $input['roles'] : [];
        }

        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = Hash::make($input['password']);
        }
        try {
            $customer = $this->userRepository->update($input, $id);
            if (empty($customer)) {
                Flash::error('User not found');
                return redirect(route('customers.index'));
            }
            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($customer, 'avatar');
            }

            if(auth()->user()->hasRole('manager')){
                $customer->markets()->sync($input['markets']);
            }else{
                $customer->syncRoles($input['roles']);
            }

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $customer->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }

            event(new UserRoleChangedEvent($customer));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }


        Flash::success('User atualizado com sucesso.');

        return redirect()->back();

    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
            return redirect(route('customers.index'));
        }
        $customer = $this->userRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('User not found');

            return redirect(route('customers.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deletado com sucesso.');

        return redirect(route('customers.index'));
    }

    /**
     * Remove Media of User
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
        } else {
            if (auth()->user()->can('medias.delete')) {
                $input = $request->all();
                $customer = $this->userRepository->findWithoutFail($input['id']);
                try {
                    if ($customer->hasMedia($input['collection'])) {
                        $customer->getFirstMedia($input['collection'])->delete();
                    }
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        }
    }
}
