<?php

namespace App\Http\Controllers;

use App\Models\MonthlyPaymentLines;
use App\Models\MonthlyPayments;
use App\Models\User;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\MarketRepository;
use App\Repositories\UserRepository;
use App\Services\Markets\MercadoDaAdi;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /** @var  OrderRepository */
    private $orderRepository;


    /**
     * @var UserRepository
     */
    private $userRepository;

    /** @var  MarketRepository */
    private $marketRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(OrderRepository $orderRepo, UserRepository $userRepo, PaymentRepository $paymentRepo, MarketRepository $marketRepo)
    {
        parent::__construct();
        $this->orderRepository = $orderRepo;
        $this->userRepository = $userRepo;
        $this->marketRepository = $marketRepo;
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(auth()->user()->hasRole('admin')){
            $markets_ids = $this->marketRepository->where('id', '!=', 1)->get()->pluck('id');
            $markets_ids = $markets_ids ? $markets_ids->toArray() : null;
        }else{
            $markets_ids = auth()->user()->markets->pluck('id');
            $markets_ids = $markets_ids ? $markets_ids->toArray() : null;
        }

        $ordersCount = $this->orderRepository->join("product_orders", "orders.id", "=", "product_orders.order_id")
            ->join("products", "products.id", "=", "product_orders.product_id")
            ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
            ->join("payments", "orders.payment_id", "=", "payments.id")
            ->whereIn('user_markets.market_id', $markets_ids)
            ->where('payments.status', 'Pago')
            ->groupBy('orders.id');

        $membersCount = $this->userRepository->leftJoin("orders", "orders.user_id", "=", "users.id")
            ->leftJoin("product_orders", "product_orders.order_id", "=", "orders.id")
            ->leftJoin("products", "products.id", "=", "product_orders.product_id")
            ->whereIn('products.market_id', $markets_ids)
            ->whereHas('roles', function($q){
                $q->where('roles.id', 4);
            })
            ->select('users.*')
            ->groupBy('users.id');

        $markets = $this->marketRepository->whereIn('id', $markets_ids)->get();

        $earning = $this->paymentRepository->join("orders", "orders.payment_id", "=", "payments.id")
            ->join("product_orders", "orders.id", "=", "product_orders.order_id")
            ->join("products", "products.id", "=", "product_orders.product_id")
            ->whereIn('products.market_id', $markets_ids)
            ->where('payments.status', 'Pago')
            ->groupBy('payments.id')
            ->select('payments.*');

        if($request->period && $request->period == 'month'){
            $ordersCount->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime('first day of this month')))
                ->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime('last day of this month')));

            $membersCount->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime('first day of this month')))
                ->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime('last day of this month')));

            $earning->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime('first day of this month')))
                ->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime('last day of this month')));
        }

        $ordersCount = $ordersCount->get()->count();
        $membersCount = $membersCount->get()->count();
        $earning = $earning->get()->sum('price');

        $averageTicket = $earning;

        if($membersCount > 0){
            $averageTicket /= $membersCount;
        }

        $ajaxEarningUrl = route('payments.byMonth',['api_token' => auth()->user()->api_token, 'markets_ids' => $markets_ids]);

        return view('dashboard.index')
            ->with("ajaxEarningUrl", $ajaxEarningUrl)
            ->with("ordersCount", $ordersCount)
            ->with("averageTicket", $averageTicket)
            ->with("markets", $markets)
            ->with("membersCount", $membersCount)
            ->with("earning", $earning);
    }

    public function test(){
        set_time_limit(0);
        $service = new MercadoDaAdi();
        $service->updateAllProductPrices();
    }
}
