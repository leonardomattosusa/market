<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class CashRegisterController extends Controller
{
    public function index(Request $request){
        if($request->date){
            $date = date('Y-m-d', strtotime($request->date));
        }else{
            $date = date('Y-m-d');
        }

        $delivered_orders = Order::with("user")->with("orderStatus")->with('payment')
            ->join("product_orders", "orders.id", "=", "product_orders.order_id")
            ->join("products", "products.id", "=", "product_orders.product_id")
            ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
            ->join("payments", "orders.payment_id", "=", "payments.id")
            ->where('user_markets.user_id', auth()->id())
            ->where('payments.status', 'Pago')
            ->whereDate('orders.delivery_date', $date)
            ->whereIn('payments.method', ['Dinheiro na entrega', 'Cartão na entrega'])
            ->groupBy('orders.id')
            ->select('orders.*');

        $collected_orders = Order::with("user")->with("orderStatus")->with('payment')
            ->join("product_orders", "orders.id", "=", "product_orders.order_id")
            ->join("products", "products.id", "=", "product_orders.product_id")
            ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
            ->join("payments", "orders.payment_id", "=", "payments.id")
            ->where('user_markets.user_id', auth()->id())
            ->where('payments.status', 'Pago')
            ->whereDate('orders.delivery_date', $date)
            ->whereIn('payments.method', ['Pagar na retirada'])
            ->groupBy('orders.id')
            ->select('orders.*');

        $mp_orders = Order::with("user")->with("orderStatus")->with('payment')
            ->join("product_orders", "orders.id", "=", "product_orders.order_id")
            ->join("products", "products.id", "=", "product_orders.product_id")
            ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
            ->join("payments", "orders.payment_id", "=", "payments.id")
            ->where('user_markets.user_id', auth()->id())
            ->where('payments.status', 'Pago')
            ->whereDate('orders.delivery_date', $date)
            ->whereIn('payments.method', ['Mercado Pago'])
            ->groupBy('orders.id')
            ->select('orders.*');

        $fr_orders = Order::with("user")->with("orderStatus")->with('payment')
            ->join("product_orders", "orders.id", "=", "product_orders.order_id")
            ->join("products", "products.id", "=", "product_orders.product_id")
            ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
            ->join("payments", "orders.payment_id", "=", "payments.id")
            ->where('user_markets.user_id', auth()->id())
            ->where('payments.status', 'Pago')
            ->whereDate('payments.payment_date', $date)
            ->whereIn('payments.method', ['Saldo FR'])
            ->groupBy('orders.id')
            ->select('orders.*');

        if(auth()->user()->hasRole('order_picker')){
            $delivered_orders->where('order_picker_id', auth()->id());
            $collected_orders->where('order_picker_id', auth()->id());
            $mp_orders->where('order_picker_id', auth()->id());
            $fr_orders->where('order_picker_id', auth()->id());
        }

        $delivered_orders = $delivered_orders->get();
        $collected_orders = $collected_orders->get();
        $mp_orders = $mp_orders->get();
        $fr_orders = $fr_orders->get();

        return view('cash_register.index', compact('delivered_orders', 'collected_orders', 'mp_orders', 'fr_orders', 'date'));
    }
}
