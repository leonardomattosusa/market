<?php
/**
 * File name: ProductController.php
 * Last modified: 2020.04.29 at 18:37:35
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Criteria\Products\ProductsOfUserCriteria;
use App\DataTables\ProductDataTable;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Media;
use App\Models\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\MarketRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UploadRepository;
use App\Services\ImportsService;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Prettus\Validator\Exceptions\ValidatorException;

class ProductController extends Controller
{
    /** @var  ProductRepository */
    private $productRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
     */
    private $uploadRepository;
    /**
     * @var MarketRepository
     */
    private $marketRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        ProductRepository $productRepo,
        CustomFieldRepository $customFieldRepo,
        UploadRepository $uploadRepo
        ,
        MarketRepository $marketRepo
        ,
        CategoryRepository $categoryRepo
    ) {
        parent::__construct();
        $this->productRepository = $productRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->uploadRepository = $uploadRepo;
        $this->marketRepository = $marketRepo;
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    public function index(ProductDataTable $productDataTable)
    {
        return $productDataTable->render('products.index');
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        if ($request->gtin_ean) {
            $gtin_ean = $request->gtin_ean;
            $readonly_fields_on = false;

            if (auth()->user()->hasRole('admin')) {
                $already_imported = $this->productRepository->where('market_id', 1)->where('gtin_ean', $request->gtin_ean)->first();
            } else {
                $already_imported = $this->productRepository->whereIn('market_id', $this->marketRepository->myMarkets()->pluck('id')->toArray())->where('gtin_ean', $request->gtin_ean)->first();
            }
            $existing_product = $this->productRepository->where('market_id', 1)->where('validated', 1)->where('gtin_ean', $request->gtin_ean)->first();

            if ($already_imported) {
                return redirect('admin/products/' . $already_imported->id . '/edit')->with('info', 'Este produto já está cadastrado em seu mercado.');
            }

            if ($existing_product) {
                $new_product = $this->productRepository->copyBaseProduct($existing_product->id, $request->market_id);

                return redirect('admin/products/' . $new_product->id . '/edit')->with('info', 'Produto importado com sucesso.');
            }
        } else {
            return redirect('admin/products')->with('info', 'Aconteceu algum erro inesperado. Tente novamente.');
        }

        $category = $this->categoryRepository->pluck('name', 'id');

        if (auth()->user()->hasRole('admin')) {
            $market = $this->marketRepository->where('id', 1)->pluck('name', 'id');
        } else {
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }

        $weight_based = [0 => 'Não', 1 => 'Sim'];
        $validated = [0 => 'Não', 1 => 'Sim'];
        $deliverable = 1;

        $hasCustomField = in_array($this->productRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
            $html = generateCustomField($customFields);
        }

        return view('products.create')->with("customFields", isset($html) ? $html : false)->with("market", $market)->with("category", $category)->with("weight_based", $weight_based)->with("validated", $validated)->with("gtin_ean", $gtin_ean)->with("readonly_fields_on", $readonly_fields_on)->with("deliverable", $deliverable);
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();

        if ($input['weight_based'] == 1) {
            $input['quantity_unit'] = 'g';
        } else {
            $input['quantity_unit'] = 'un';
        }

        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
        try {
            $existing_product = $this->productRepository->where('market_id', 1)->where('validated', 1)->where('gtin_ean', $request->gtin_ean)->first();

            if ($existing_product) {
                Flash::error('Este GTIN/EAN já foi cadastrado em nosso sistema.');
                return redirect()->back()->withInput();
            }

            $input['name'] = strtoupper($input['name']);
            $input['validated'] = 1;

            $product = $this->productRepository->create($input);
            $product->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);

                if ($cacheUpload) {
                    $mediaItem = $cacheUpload->getMedia('image')->first();
                    $mediaItem->copy($product, 'image');
                } else {
                    $product->delete();

                    Flash::error('Aconteceu algum erro no upload da imagem deste produto. Tente novamente com uma imagem diferente.');
                    return redirect()->back()->withInput();
                }
            }

            if ($product->market_id != 1 && $product->weight_based != 1) {
                $this->productRepository->registerNewBaseProduct($product);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully', ['operator' => __('lang.product')]));

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function show($id)
    {
        $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Produto não encontrado.');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function edit($id)
    {
        $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
        $product = $this->productRepository->findWithoutFail($id);
        if (empty($product)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.product')]));
            return redirect(route('products.index'));
        }
        $category = $this->categoryRepository->pluck('name', 'id');

        $readonly_fields_on = false;

        if (auth()->user()->hasRole('admin')) {
            $market = $this->marketRepository->pluck('name', 'id');
        } else {
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');

            if ($product->weight_based == 0) {
                $readonly_fields_on = true;
            }
        }

        $weight_based = [0 => 'Não', 1 => 'Sim'];
        $validated = [0 => 'Não', 1 => 'Sim'];

        $customFieldsValues = $product->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
        $hasCustomField = in_array($this->productRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        $gtin_ean = $product->gtin_ean;
        $deliverable = $product->deliverable;

        return view('products.edit')->with('product', $product)->with("customFields", isset($html) ? $html : false)->with("market", $market)->with("category", $category)->with("weight_based", $weight_based)->with("validated", $validated)->with("gtin_ean", $gtin_ean)->with("readonly_fields_on", $readonly_fields_on)->with("deliverable", $deliverable);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update($id, UpdateProductRequest $request)
    {
        $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Produto não encontrado.');
            return redirect(route('products.index'));
        }

        if ($request->weight_based == 1) {
            $input = $request->all();
            $input['quantity_unit'] = 'g';
        } else {
            if (auth()->user()->hasRole('admin')) {
                $input = $request->except('gtin_ean');
            } else {
                $input = $request->except('gtin_ean', 'ncm', 'weight_based', 'capacity', 'unit', 'category_id', 'market_id');
            }

            $input['quantity_unit'] = 'un';
        }

        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
        try {
            $validate_and_update = 0;

            // Verifica se o produtos está sendo validado ou é um produto já validado sendo alterado, caso sim, deve aplicar as alterações pra todos os produtos do sistema.
            if (($product->validated == 0 && isset($input['validated']) && $input['validated'] == 1) || ($product->validated == 1 && (!isset($input['validated']) || $input['validated'] == 1))) {
                $validate_and_update = 1;
            }

            $input['name'] = strtoupper($input['name']);

            $product = $this->productRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                print_r('tem imagem');

                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                if ($cacheUpload) {
                    $mediaItem = $cacheUpload->getMedia('image')->first();

                    if ($mediaItem) {
                        Media::where('model_type', 'App\Models\Product')->where('model_id', $product->id)->delete();
                    }

                    $mediaItem->copy($product, 'image');
                } else {
                    Flash::error('Aconteceu algum erro no upload da imagem deste produto. Tente novamente com uma imagem diferente.');

                    return redirect()->back()->withInput();
                }
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $product->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }

            if ($product->market_id == 1 && $validate_and_update) {
                $this->productRepository->validateProductAndUpdate($product);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }


        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.product')]));

        if ($request->redirect_validated && $request->redirect_validated == 1) {
            return redirect(route('products.index', ['validated' => 'no']));
        } else {
            return redirect(route('products.index'));
        }
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (!env('APP_DEMO', false)) {
            $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
            $product = $this->productRepository->findWithoutFail($id);

            if (empty($product)) {
                Flash::error('Produto não encontrado.');

                return redirect(route('products.index'));
            }

            $this->productRepository->delete($id);

            Flash::success(__('lang.deleted_successfully', ['operator' => __('lang.product')]));

        } else {
            Flash::warning('This is only demo app you can\'t change this section ');
        }
        return redirect(route('products.index'));
    }

    /**
     * Remove Media of Product
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $product = $this->productRepository->findWithoutFail($input['id']);
        try {
            if ($product->hasMedia($input['collection'])) {
                $product->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function importFromDatabase($market_id, Request $request)
    {
        $products_query = Product::selectRaw('products.id, products.name, products.gtin_ean, products.ncm, (SELECT b.price FROM products b WHERE b.gtin_ean = products.gtin_ean AND b.market_id = ' . $market_id . ' LIMIT 1) as importedPrice')->where('market_id', 1)->where('validated', 1);
        $categories = $this->categoryRepository->all();

        if ($request->category && $request->category != 'all') {
            $products_query->where('category_id', $request->category);
        }
        if ($request->search && $request->search != "") {
            $products_query->where(function ($q) use ($request) {
                $q->where('name', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('ncm', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('gtin_ean', 'LIKE', '%' . $request->search . '%');
            });
        }

        $products = $products_query->paginate(50);

        return view('products.import_from_database', compact('products', 'categories', 'market_id'));
    }

    public function postImportFromDatabase($market_id, Request $request)
    {
        return $this->productRepository->copyBaseProduct($request->model_id, $market_id, $request->price);
    }

    public function reloadFromServer($id)
    {
        $product = Product::find($id);

        if ($product) {
            $service = new ImportsService();
            $service->updateSingleProductPrice($product);

            Flash::success(__('lang.updated_successfully', ['operator' => __('lang.product')]));

            return redirect()->back();
        }
    }

    public function updateAmounts()
    {
        return view('products.update_amounts');
    }

    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    public function postUpdateAmounts(Request $request)
    {
        set_time_limit(0);

        try {
            $csv_data = $this->readCSV($request->file('file'), array('delimiter' => ','));

            foreach ($csv_data as $index => $row) {
                $csv_data[$index] = $row;

                $product = Product::updateOrCreate([
                    'gtin_ean' => $row[0],
                    'market_id' => 1,
                ], [
                    'name' => $row[1],
                    'price' => number_format(str_replace(',', '.', trim($row[2])), 2),
                    'manually_disabled' => 0,
                ]);
            }

            return redirect()->back()->with('success', 'Produtos atualizados com sucesso.');
        } catch (\Exception $e) {
            dd($e);

            return redirect()->back()->with('error', 'Erro ao atualizar os produtos. Verifique se o arquivo está no formato correto.');
        }
    }
}
