<?php

namespace App\Http\Controllers;

use App\Criteria\Users\DriversCriteria;
use App\DataTables\DriverDataTable;
use App\Events\UserRoleChangedEvent;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\DriverRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\MarketRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class DriverController extends Controller
{
    /** @var  DriverRepository */
    private $driverRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
  * @var UserRepository
  */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;
    private $uploadRepository;

    public function __construct(DriverRepository $driverRepo, CustomFieldRepository $customFieldRepo , UserRepository $userRepo, RoleRepository $roleRepo, UploadRepository $uploadRepo, MarketRepository $marketRepo)
    {
        parent::__construct();
        $this->driverRepository = $driverRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->roleRepository = $roleRepo;
        $this->uploadRepository = $uploadRepo;
        $this->marketRepository = $marketRepo;
    }

    /**
     * Display a listing of the Driver.
     *
     * @param DriverDataTable $driverDataTable
     * @return Response
     */
    public function index(DriverDataTable $driverDataTable)
    {
        return $driverDataTable->render('drivers.index');
    }

    /**
     * Show the form for creating a new Driver.
     *
     * @return Response
     */
    public function create()
    {
        if(auth()->user()->hasRole('manager')){
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = [];
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
            $html = generateCustomField($customFields);
        }

        return view('drivers.create')
            ->with("market", $market)
            ->with("customFields", isset($html) ? $html : false)
            ->with("marketSelected", $marketSelected);
    }

    /**
     * Store a newly created Driver in storage.
     *
     * @param CreateDriverRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input['roles'] = ['driver'];
        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = str_random(60);

        try {
            $user = $this->userRepository->create($input);

            if(auth()->user()->hasRole('manager')){
                $user->markets()->sync($input['markets']);
            }

            $user->syncRoles($input['roles']);

            $user->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            }
            event(new UserRoleChangedEvent($user));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success('Entregador salvo com sucesso.');

        return redirect(route('drivers.index'));
    }

    /**
     * Display the specified Driver.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Entregador não encontrado.');

            return redirect(route('drivers.index'));
        }

        return view('drivers.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified Driver.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && $id != auth()->id()) {
            Flash::error('Você não tem essa permissão.');

            return redirect(route('drivers.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);
        unset($user->password);
        $html = false;

        if(auth()->user()->hasRole('manager')){
            $market = $this->marketRepository->myMarkets()->pluck('name', 'id');
        }else{
            $market = $this->marketRepository->pluck('name', 'id');
        }

        $marketSelected = $user->markets()->pluck('id')->toArray();
        $customFieldsValues = $user->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        if (empty($user)) {
            Flash::error('Entregador não encontrado.');

            return redirect(route('drivers.index'));
        }

        return view('drivers.edit')
            ->with('user', $user)
            ->with("market", $market)
            ->with("marketSelected", $marketSelected)
            ->with("customFields", $html);
    }

    /**
     * Update the specified Driver in storage.
     *
     * @param  int              $id
     * @param UpdateDriverRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        if ((!auth()->user()->hasRole('admin') && !auth()->user()->hasRole('manager')) && $id != auth()->id()) {
            Flash::error('Você não tem permissão para alterar entregadores.');
            return redirect(route('drivers.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Entregador não encontrado');
            return redirect(route('drivers.index'));
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input = $request->all();

        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = Hash::make($input['password']);
        }

        try {
            $user = $this->userRepository->update($input, $id);
            if (empty($user)) {
                Flash::error('Entregador não encontrado');
                return redirect(route('drivers.index'));
            }
            if (isset($input['avatar']) && $input['avatar']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            }

            $user->markets()->sync($input['markets']);

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $user->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }

            event(new UserRoleChangedEvent($user));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }


        Flash::success('Entregador atualizado com sucesso.');

        return redirect()->back();
    }

    /**
     * Remove the specified Driver from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $driver = $this->userRepository->findWithoutFail($id);

        if (empty($driver)) {
            Flash::error('Entregador não encontrad');

            return redirect(route('drivers.index'));
        }

        $this->userRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.driver')]));

        return redirect(route('drivers.index'));
    }

        /**
     * Remove Media of Driver
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $user = $this->userRepository->findWithoutFail($input['id']);
        try {
            if($user->hasMedia($input['collection'])){
                $user->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function reports(Request $request)
    {
        $start_date = $request->start_date ? date('Y-m-d', strtotime($request->start_date)) : '0000-00-00';
        $final_date = $request->final_date ? date('Y-m-d', strtotime($request->final_date)) : '0000-00-00';

        if (auth()->user()->hasRole('manager')) {
            $markets_ids = auth()->user()->markets->pluck('id')->toArray();

            $drivers = User::with('roles')->whereHas('roles', function($q){
                    $q->where('roles.id', 5);
                })
                ->join('user_markets', 'user_markets.user_id', '=', 'users.id')
                ->whereIn('user_markets.market_id', $markets_ids)
                ->where('users.id', '!=', auth()->id())
                ->selectRaw(DB::raw('(SELECT count(id) FROM orders WHERE driver_id = users.id AND order_status_id = 6) as orders_count, (SELECT concat(count(orders.id),",",sum(payments.price)) FROM orders LEFT JOIN payments ON orders.payment_id = payments.id WHERE orders.driver_id = users.id AND orders.order_status_id = 6 AND DATE(orders.created_at) >= "'.$start_date.'" AND DATE(orders.created_at) <= "'.$final_date.'") as results, users.*'))
                ->groupBy('users.id')
                ->get();
        }else{
            $drivers = User::with('roles')->whereHas('roles', function($q){
                    $q->where('roles.id', 5);
                })
                ->selectRaw(DB::raw('(SELECT count(id) FROM orders WHERE driver_id = users.id AND order_status_id = 6) as orders_count, (SELECT concat(count(orders.id),",",sum(payments.price)) FROM orders LEFT JOIN payments ON orders.payment_id = payments.id WHERE orders.driver_id = users.id AND orders.order_status_id = 6 AND DATE(orders.created_at) >= "'.$start_date.'" AND DATE(orders.created_at) <= "'.$final_date.'") as results, users.*'))
                ->get();
        }

        return view('drivers.reports', compact('drivers'))->with('start_date', $request->start_date)->with('final_date', $request->final_date);
    }
}
