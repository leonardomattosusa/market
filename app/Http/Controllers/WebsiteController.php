<?php

namespace App\Http\Controllers;

use App\Models\PartialPayments;
use App\Models\PasswordResets;
use Illuminate\Http\Request;

use App\Http\Requests\Website\LoginRequest;
use App\Http\Requests\Website\RegisterRequest;
use App\Http\Requests\Website\UpdateUserRequest;

use App\Website\User;
use App\Website\Market;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use View;

use MPSdk;
use MPPreference;
use MPItem;
use Flash;

class WebsiteController extends Controller
{
    protected $user, $market, $cart_products, $favorites, $cep, $lat, $lng;
    protected $userLoggedIn = false;

    public function __construct(){
        $this->middleware(function($request, $next){
            if(session('user')){
                $user = new User(session('user'));

                if($user){
                    $this->userLoggedIn = true;
                    $this->user = $user;
                    $this->cart_products = $user->cart_products;
                    $this->favorites = $user->favorites;
                }
            }
            if(session('market_id')){
                $market = new Market(session('market_id'));

                if($market){
                    $this->market = $market;
                }
            }

            if(session('cep') && session('lat') && session('lng')) {
                $this->cep = session('cep');
                $this->lat = session('lat');
                $this->lng = session('lng');
            }

            View::share('userLoggedIn', $this->userLoggedIn);
            View::share('cart_products', $this->cart_products);
            View::share('market', $this->market);
            View::share('favorites', $this->favorites);
            View::share('cep', $this->cep);

            return $next($request);
        });
    }

    public function index(){
        return view('website.index');
    }

    public function saveLocalization(Request $request){
        $zipcode = $request->zipcode;

        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://maps.google.com/maps/api/geocode/json?address='.$zipcode.'&sensor=false&key=AIzaSyBakbheodl_aPGBYGxhsnnWVFX5HuNM174');

        if($response->getStatusCode() != 200){
            return redirect('markets-list');
        }else {
            $response = json_decode($response->getBody());

            session(['cep' => $zipcode, 'lat' => $response->results[0]->geometry->location->lat, 'lng' => $response->results[0]->geometry->location->lng]);
        }

        return redirect('markets-list');
    }

    public function changeMarket($market_id = null){
        session(['market_id' => $market_id]);

        if($market_id == null){
            return redirect('markets-list');
        }else{
            return redirect('market/'.$market_id);

        }
    }

    public function marketsList(){
        if(!$this->lat || !$this->lng){
            return redirect('/');
        }

        $lat = $this->lat;
        $lng = $this->lng;

        return view('website.markets-list', compact('lat', 'lng'));
    }

    public function market($market_id, $category_id = 0){
        return view('website.market', compact('market_id', 'category_id'));
    }

    public function cartClearMessage(){
        if(!$this->userLoggedIn){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return view('cart-clear-message');
    }

    public function cart(){
        return view('website.cart');
    }

    public function cartClear(){

    }

    public function checkout(){
        if(!$this->userLoggedIn){
            return redirect('login_register');
        }
        if(!$this->cart_products){
            return redirect('cart');
        }

        $user = $this->user;
        $user_addresses = $user->getAddresses();

        return view('website.checkout', compact('user_addresses', 'user'));
    }

    public function loginRegister(){
        if($this->userLoggedIn){
            if($this->user->market_id){
                return redirect('/change-market/' . $this->user->market_id);
            }else{
                return redirect('/markets-list');
            }
        }

        return view('website.login-register');
    }

    public function login(LoginRequest $request){
        $credentials = $request->all();

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/login', ['json' => $credentials]);

        if($response->getStatusCode() != 200){
            return ['success' => false, 'message' => 'Credenciais inválidas, verifique os dados digitados e tente novamente.'];
        }else{
            $response = json_decode($response->getBody());
            if($response && $response->success){
                session(['user' => $response->data]);

                return ['success' => true];
            }

            return ['success' => false, 'message' => 'Credenciais inválidas, verifique os dados digitados e tente novamente.'];
        }
    }

    public function register(RegisterRequest $request){
        $user = $request->all();

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/register', ['json' => $user]);

        if($response->getStatusCode() == 401){
           return ['success' => false, 'message' => 'O e-mail ou o documento já está cadastrado em nosso sistema.'];
        }else{
            $response = json_decode($response->getBody());

            if($response->success){
                session(['user' => $response->data]);

                return ['success' => true, 'fr_id' => $response->data->fr_id];
            }

            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado em sua requisição. Tente novamente em alguns minutos.'];
        }
    }

    public function updateUser(UpdateUserRequest $request){
        return $this->user->update($request->only('name'));
    }

    public function profile(){
        $user = $this->user;
        $orders = $user->orders();
        $addresses = $user->getAddresses();

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return view('website.profile', compact('user', 'orders', 'addresses'));
    }

    public function favorites(){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        $favorites = $user->favorites();

        return view('website.favorites', compact('user', 'favorites'));
    }

    public function logout(){
        session(['user' => null]);

        return redirect('login_register');
    }

    public function register_address(Request $request){
        if($this->userLoggedIn){
            $data = [
                'zipcode' => $request->zipcode,
                'street' => $request->street,
                'number' => $request->number,
                'district' => $request->district,
                'complement' => $request->complement,
                'state_initials' => $request->state_initials,
                'city' => $request->city,
                'description' => $request->description,
                'address' => $request->street.', '.$request->number.', '.$request->complement.', '.$request->city.'-'.$request->state_initials.' CEP: '.$request->zipcode,
                'user_id' => $this->user->id
            ];

            return $this->user->registerAddress($data);
        }else{
            return ['success' => 'false', 'message' => 'Sua sessão expirou, efetue o login novamente.'];
        }
    }

    public function delete_address($address_id, Request $request){
        if($this->userLoggedIn){
            return $this->user->deleteAddress($address_id);
        }else{
            return ['success' => 'false', 'message' => 'Sua sessão expirou, efetue o login novamente.'];
        }
    }

    public function update_address($address_id, Request $request){
        if($this->userLoggedIn){
            $data = [
                'zipcode' => $request->zipcode,
                'street' => $request->street,
                'number' => $request->number,
                'district' => $request->district,
                'complement' => $request->complement,
                'state_initials' => $request->state_initials,
                'city' => $request->city,
                'description' => $request->description,
                'address' => $request->street.', '.$request->number.', '.$request->complement.', '.$request->city.'-'.$request->state_initials.' CEP: '.$request->zipcode,
                'user_id' => $this->user->id,
                'is_default' => true
            ];

            return $this->user->updateAddress($data, $address_id);
        }else{
            return ['success' => 'false', 'message' => 'Sua sessão expirou, efetue o login novamente.'];
        }
    }

    public function addProductToFavorites(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->addToFavorites($request->product_id);
    }

    public function removeProductFromFavorites(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->removeFromFavorites($request->product_id);
    }

    public function addProductToCart(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->addToCart($request->product_id, $request->market_id, $request->quantity);
    }

    public function mercadopago_app($api_token, $delivery_address_id, $delivery_date=null){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/user', ['json' => ['api_token' => $api_token]]);
        }catch(\Exception $e){
            return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente efetuar o login novamente.']);
        }

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            if($data->success == true){
                $user = $data->data;
                session(['user' => $user]);
                $checkout_token = md5(uniqid());
                $this->user = new User($user);

                $cart_products = $this->user->cart_products();
                if(count($cart_products) > 0){
                    $delivery_address = $this->user->getDeliveryAddress($delivery_address_id);
                    if(isset($delivery_address->user_id) && $delivery_address->user_id == $this->user->id){
                        if(isset($cart_products[0]->product->market->mercado_pago_token)){
                            $market_mercadopago_token = $cart_products[0]->product->market->mercado_pago_token;
                        }else{
                            return view('website/mercadopago_error')->with(['error_message' => 'Erro ao recuperar conta Mercado Pago deste estabelecimento, por favor selecione outra forma de pagamento.']);
                        }

                        $already_paid_amount = PartialPayments::where('user_id', $user->id)->where('status', 'pending')->sum('amount');
                        $delivery_fee = $cart_products[0]->product->market->delivery_fee;
                        $products_total = 0;
                        $mp_products;
                        $market = $cart_products[0]->product->market;
                        MpSDK::setAccessToken($market_mercadopago_token);
                        $preference = new MPPreference();

                        foreach($cart_products as $prod){
                            $products_total += ($prod->quantity * $prod->product->price);

                            $item = new MPItem();
                            $item->title = $prod->product->name;
                            $item->quantity = $prod->quantity;
                            $item->unit_price = $prod->product->price;
                            $mp_products[] = $item;
                        }

                        if($delivery_fee > 0){
                            $item = new MPItem();
                            $item->title = 'Taxa de entrega';
                            $item->quantity = 1;
                            $item->unit_price = $delivery_fee;
                            $mp_products[] = $item;
                        }

                        if($already_paid_amount > 0){
                            $item = new MPItem();
                            $item->title = 'Desconto';
                            $item->quantity = 1;
                            if($already_paid_amount < ($delivery_fee + $products_total)){
                                $item->unit_price = (abs($already_paid_amount) * -1);
                            }else{
                                return view('website/mercadopago_error')->with(['error_message' => 'Você possui '.$already_paid_amount.' já pago e está tentando pagar uma compra no valor de '.($delivery_fee + $products_total).'. Não é possível pagar esta compra via Mercado Pago, selecione "Dinheiro na entrega".']);
                            }
                            $mp_products[] = $item;
                        }

                        $preference->notification_url = url('api/mercado_pago/notifications/'.$market->id);
                        $preference->external_reference = $checkout_token;
                        $preference->items = $mp_products;
                        $preference->payment_methods = array(
                            "excluded_payment_types" => array(
                                array("id" => "ticket")
                            )
                        );
                        $preference->binary_mode = true;
                        $preference->save();

                        $create_order_return = $this->user->createOrder($delivery_address_id, 'mercado_pago', $checkout_token, $delivery_date);
                        if($create_order_return['success'] == true){
                            return view('website/mercadopago', compact('cart_products', 'delivery_address', 'delivery_fee', 'products_total', 'preference', 'already_paid_amount'));
                        }else{
                            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível criar seu pedido, clique em voltar e tente novamente.']);
                        }
                    }else{
                        return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente novamente.']);
                    }
                }
            }else{
                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
            }
        }else{
            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
        }
    }

    public function mercadopago_app_order($api_token, $order_id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/user', ['json' => ['api_token' => $api_token]]);
        }catch(\Exception $e){
            return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente efetuar o login novamente.']);
        }

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            if($data->success == true){
                $user = $data->data;
                session(['user' => $user]);
                $this->user = new User($user);
                $order = $this->user->getOrder($order_id);

                if($order['success']){
                    $payment = $order['data']->payment;
                    $order_amount = $payment->price;
                    $market = isset($order['data']->product_orders[0]->product->market) ? $order['data']->product_orders[0]->product->market : null;

                    if($market){
                        $market_mercadopago_token = $market->mercado_pago_token;
                        MpSDK::setAccessToken($market_mercadopago_token);
                        $mp_products;
                        $preference = new MPPreference();

                        $item = new MPItem();
                        $item->title = 'Pagamento de pedido #'.$order['data']->id.' iMarket.';
                        $item->quantity = 1;
                        $item->unit_price = $order_amount;
                        $mp_products[] = $item;

                        $already_paid_amount = PartialPayments::where('user_id', $user->id)->where('status', 'pending')->sum('amount');
                        if($already_paid_amount > 0){
                            $item = new MPItem();
                            $item->title = 'Desconto';
                            $item->quantity = 1;
                            if($already_paid_amount < ($order_amount)){
                                $item->unit_price = (abs($already_paid_amount) * -1);
                            }else{
                                return view('website/mercadopago_error')->with(['error_message' => 'Você possui '.$already_paid_amount.' já pago e está tentando pagar uma compra no valor de '.($order_amount).'. Não é possível pagar esta compra via Mercado Pago, selecione "Dinheiro na entrega".']);
                            }
                            $mp_products[] = $item;
                        }

                        $preference->notification_url = url('api/mercado_pago/notifications/'.$market->id);
                        $preference->items = $mp_products;
                        $preference->external_reference = $order['data']->payment_hash;
                        $preference->payment_methods = array(
                            "excluded_payment_types" => array(
                                array("id" => "ticket")
                            )
                        );
                        $preference->binary_mode = true;
                        $preference->save();

                        $total = $order_amount;

                        return view('website/mercadopago_order', compact('total', 'preference', 'already_paid_amount'));
                    }else{
                        return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
                    }
                }else{
                    return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
                }
            }else{
                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
            }
        }else{
            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
        }
    }

    public function saldofr_app($api_token, $delivery_address_id, $delivery_date=null){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/user', ['json' => ['api_token' => $api_token]]);
        }catch(\Exception $e){
            return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente efetuar o login novamente.']);
        }

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            if($data->success == true){
                $user = $data->data;
                session(['user' => $user]);
                $checkout_token = md5(uniqid());
                $this->user = new User($user);

                $cart_products = $this->user->cart_products();
                if(count($cart_products) > 0){
                    $delivery_address = $this->user->getDeliveryAddress($delivery_address_id);
                    if(isset($delivery_address->user_id) && $delivery_address->user_id == $this->user->id){
                        $delivery_fee = $cart_products[0]->product->market->delivery_fee;
                        $products_total = 0;
                        $market = $cart_products[0]->product->market;

                        foreach($cart_products as $prod){
                            $products_total += ($prod->quantity * $prod->product->price);
                        }

                        try {
                            $client_fr = new \GuzzleHttp\Client();
                            $response_fr = $client_fr->get('https://fidelidaderemunerada.com.br/api/imarket/associados/consultar_saldo?api_token=987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788&id_associado='.$this->user->fr_id);
                        }catch(\Exception $e){
                            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível recuperar o saldo disponível Fidelidade Remunerada.']);
                        }

                        if($response_fr->getStatusCode() == 200){
                            $data_fr = json_decode($response_fr->getBody());
                            $current_fr_balance = $data_fr->saldo;
                        }else{
                            $current_fr_balance = 0;
                        }

                        $purchase_total = $products_total + $delivery_fee;
                        if($purchase_total <= $current_fr_balance){
                            return view('website/saldofr', compact('cart_products', 'delivery_address', 'delivery_fee', 'products_total', 'api_token', 'delivery_date'));
                        }else{
                            if($current_fr_balance > 0){
                                return redirect('checkout/partial_payment_fr?api_token='.$api_token);
                            }else{
                                return view('website/mercadopago_error')->with(['error_message' => 'Saldo insuficiente.']);
                            }
                        }
                    }else{
                        return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente novamente.']);
                    }
                }
            }else{
                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
            }
        }else{
            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
        }
    }

    public function post_saldofr_app($api_token, $delivery_address_id, $delivery_date=null){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/user', ['json' => ['api_token' => $api_token]]);
        }catch(\Exception $e){
            return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente efetuar o login novamente.']);
        }

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            if($data->success == true){
                $user = $data->data;
                session(['user' => $user]);
                $checkout_token = md5(uniqid());
                $this->user = new User($user);

                $cart_products = $this->user->cart_products();
                if(count($cart_products) > 0){
                    $delivery_address = $this->user->getDeliveryAddress($delivery_address_id);
                    if(isset($delivery_address->user_id) && $delivery_address->user_id == $this->user->id){
                        $delivery_fee = $cart_products[0]->product->market->delivery_fee;
                        $products_total = 0;
                        $market = $cart_products[0]->product->market;

                        foreach($cart_products as $prod){
                            $products_total += ($prod->quantity * $prod->product->price);
                        }

                        try {
                            $client_fr = new \GuzzleHttp\Client();
                            $response_fr = $client_fr->get('https://fidelidaderemunerada.com.br/api/imarket/associados/consultar_saldo?api_token=987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788&id_associado='.$this->user->fr_id);
                        }catch(\Exception $e){
                            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível recuperar o saldo disponível Fidelidade Remunerada.']);
                        }

                        if($response_fr->getStatusCode() == 200){
                            $data_fr = json_decode($response_fr->getBody());
                            $current_fr_balance = $data_fr->saldo;
                        }else{
                            $current_fr_balance = 0;
                        }

                        $purchase_total = $products_total + $delivery_fee;
                        if($purchase_total <= $current_fr_balance){
                            try {
                                $client_purchase = new \GuzzleHttp\Client();
                                $response_fr = $client_purchase->get('https://fidelidaderemunerada.com.br/api/imarket/compras/pagar_com_saldo?api_token=987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788&id_associado='.$this->user->fr_id.'&valor_compra='.$purchase_total);
                            }catch(\Exception $e){
                                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível pagar a compra com saldo Fidelidade Remunerada.'.$e->getMessage()]);
                            }

                            $create_order_return = $this->user->createOrder($delivery_address_id, 'saldo_fr', $checkout_token, $delivery_date);
                            $order_id = $create_order_return['data']->id;

                            if($create_order_return['success'] == true){
                                return view('website/payment_success');
                            }else{
                                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível criar seu pedido, clique em voltar e tente novamente.']);
                            }
                        }else{
                            return view('website/mercadopago_error')->with(['error_message' => 'Saldo insuficiente.']);
                        }
                    }else{
                        return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente novamente.']);
                    }
                }
            }else{
                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
            }
        }else{
            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
        }
    }

    public function callback_mercadopago(Request $request){

    }

    public function removeProductFromCart(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->removeFromCart($request->cart_id);
    }

    function hasProductsOnCart(){
        if($this->userLoggedIn && $this->cart_products && count($this->cart_products) > 0){
            return true;
        }

        return false;
    }

    public function order($order_id, Request $request){
        if($request->preference_id){
            return redirect('order/'.$order_id);
        }
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        try{
            $order = $user->getOrder($order_id);
        }catch (\Exception $e){
            return redirect()->back()->with('error', 'Você não pode acessar essa página.');
        }

        $payment = $order['data']->payment;
        $order_amount = $payment->price;
        $market = isset($order['data']->product_orders[0]->product->market) ? $order['data']->product_orders[0]->product->market : null;

        if($market){
            if($market->mercado_pago_method && $market->mercado_pago_token){
                $market_mercadopago_token = $market->mercado_pago_token;

                MpSDK::setAccessToken($market_mercadopago_token);
                $mp_products;
                $preference = new MPPreference();

                $item = new MPItem();
                $item->title = 'Pagamento de pedido #'.$order['data']->id.' iMarket.';
                $item->quantity = 1;
                $item->unit_price = $order_amount;
                $mp_products[] = $item;

                $preference->notification_url = url('api/mercado_pago/notifications/'.$market->id);
                $preference->items = $mp_products;
                $preference->external_reference = $order['data']->payment_hash;
                $preference->payment_methods = array(
                    "excluded_payment_types" => array(
                        array("id" => "ticket")
                    )
                );
                $preference->binary_mode = true;
                $preference->save();
            }else{
                $preference = null;
            }

            $total = $order_amount;
            $order = $order['data'];

            return view('website.order', compact('user','order', 'total', 'preference'));
        }else{
            return redirect('/profile')->with('error', 'Não foi possível requisitar os dados ao servidor. Tente novamente.');
        }
    }

    public function createOrder(Request $request){
        $user = $this->user;

        if(!$user){
            return ['success' => false, 'message' => 'Você precisa efetuar login para concluir seu pedido.'];
        }

        /*if(!$request->accept_terms){
            return ['success' => false, 'Você precisa aceitar os termos e condições de uso do iMarket para concluir sua compra.'];
        }*/

        if(!$request->payment_method){
            return ['success' => false, 'message' => 'Você precisa selecionar um método de pagamento para conluir sua compra.'];
        }

        if(!$request->address_id){
            return ['success' => false, 'message' => 'Você precisa selecionar um endereço de entrega para conluir sua compra.'];
        }

        if($this->market->closed){
            return ['success' => false, 'message' => $this->market->closed.'Este mercado está fechado. O mercado deve estar aberto para concluir seu pedido.'];
        }

        if($request->schedule == 'yes'){
            if($request->schedule_day == 'today'){
                $day = date('Y-m-d');
            }else{
                $day = date('Y-m-d', strtotime('tomorrow'));
            }

            $schedule_hour = $request->schedule_hour . ':00';
            $schedule_hour_1 = $request->schedule_hour;
            $schedule_hour_2 = date('H:i', strtotime($request->schedule_hour.' +30 minutes'));
            $schedule_hour_2 = ($schedule_hour_2 == '00:00') ? '24:00' : $schedule_hour_2;

            if($request->schedule_day == 'today' && date('H:i', strtotime('+45 min')) > $request->schedule_hour){
                return ['success' => false, 'message' => 'Horário de agendamento inválido.'];
            }

            if(date('H:i', strtotime($schedule_hour_1)) < date('H:i', strtotime($this->market->early_delivery_hour)) || $schedule_hour_2 > date('H:i', strtotime($this->market->late_delivery_hour))){
                return ['success' => false, 'message' => 'O mercado escolhido não efetua entregas neste horário.'];
            }

            $delivery_date = date('Y-m-d H:i:s', strtotime($day.' '.$schedule_hour));
        }else{
            $delivery_date = null;
        }

        return $user->createOrder($request->address_id, $request->payment_method, null, $delivery_date, $request->delivery_change);
    }

    public function fidelidadeRemunerada(){
        $user = $this->user;

        if(!$user){
            return redirect('login_register');
        }

        $fr_user = $user->getFRUser();

        if(!$fr_user || !$fr_user['success']){
            return redirect('logout');
        }

        $fr_user = $fr_user['user'];

        return view('website.fidelidade_remunerada',compact('user', 'fr_user'));
    }

    public function forgotPassword(){
        return view('website.forgot-password');
    }

    public function sendPasswordRecoveryEmail(Request $request){
        $email = $request->email;

        try{
            $client = new \GuzzleHttp\Client();
            $response = $client->post('https://imarket.digital/api/send_reset_link_email', ['json' => ['email' => $email]]);

            return response(['success' => true], 200);
        } catch(\Exception $e){
            return $e->getMessage();

            return response(['success' => false], 500);
        }
    }

    public function passwordReset($token, Request $request){
        $email = $request->email;
        $password_reset = PasswordResets::where('email', $email)->first();

        if(Hash::check($token, $password_reset->token) && date('Y-m-d', strtotime($password_reset->created_at)) >= date('Y-m-d', strtotime('-60 minutes'))){
            $user = $password_reset->user;

            return view('website.password-recovery', compact('user', 'token', 'email'));
        }else{
            return redirect('login_register')->with('error', 'Este link de recuperação de senha já expirou.');
        }
    }

    public function resetPassword($token, Request $request){
        $email = $request->email;
        $password_reset = PasswordResets::where('email', $email)->first();

        if(Hash::check($token, $password_reset->token) && date('Y-m-d', strtotime($password_reset->created_at)) >= date('Y-m-d', strtotime('-60 minutes'))){
            $user = $password_reset->user;
            $user->password = Hash::make($request->password);
            $user->save();

            Flash::success('Sua senha foi redefinida com sucesso. Você já pode fazer login com sua nova senha.');

            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 500);
        }
    }
}
