<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MonthlyPayments extends Model
{
    use SoftDeletes;

    public $table = 'monthly_payments';
    public $guarded = [];
    public $timestamps = true;

    public function getStatusHtmlAttribute(){
        switch ($this->status){
            case 'pending':
                return '<span class="badge badge-warning text-white">Pendente</span>';
                break;
            case 'paid':
                return '<span class="badge badge-success text-white">Pago</span>';
                break;
            case 'canceled':
                return '<span class="badge badge-secondary text-white">Cancelado</span>';
                break;
            default:
                return '<span class="badge badge-warning text-white">Pendente</span>';
                break;
        }
    }

    public function lines(){
        return $this->hasMany(MonthlyPaymentLines::class, 'monthly_payment_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
