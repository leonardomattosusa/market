<?php
namespace App\Models;
use Eloquent as Model;

class MarketServices extends Model
{
    public $table = 'market_services';

    public function market(){
        return $this->belongsTo(\App\Models\Market::class, 'market_id', 'id');
    }
}
