<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketOpenHours extends Model
{
    use SoftDeletes;

    public $table = 'market_open_hours';
    public $guarded = [];
}
