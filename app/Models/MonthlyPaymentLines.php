<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MonthlyPaymentLines extends Model
{
    use SoftDeletes;

    public $table = 'monthly_payment_lines';
    public $guarded = [];
    public $timestamps = true;
}
