<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketWeightBasedGtinSettings extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'market_weight_based_gtin_settings';
}
