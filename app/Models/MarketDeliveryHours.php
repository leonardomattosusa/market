<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketDeliveryHours extends Model
{
    use SoftDeletes;

    public $table = 'market_delivery_hours';
    public $guarded = [];
}
