<?php
/**
 * File name: Market.php
 * Last modified: 2020.04.28 at 21:59:34
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\DB;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Class Market
 * @package App\Models
 * @version August 29, 2019, 9:38 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Product
 * @property \Illuminate\Database\Eloquent\Collection Gallery
 * @property \Illuminate\Database\Eloquent\Collection MarketsReview
 * @property \Illuminate\Database\Eloquent\Collection[] fields
 * @property \Illuminate\Database\Eloquent\Collection User
 * @property \Illuminate\Database\Eloquent\Collection[] Market
 * @property string name
 * @property string description
 * @property string address
 * @property string latitude
 * @property string longitude
 * @property string phone
 * @property string mobile
 * @property string information
 * @property double admin_commission
 * @property double delivery_fee
 * @property double default_tax
 * @property double delivery_range
 * @property boolean available_for_delivery
 * @property boolean closed
 */
class Market extends Model implements HasMedia
{
    use HasMediaTrait {
        getFirstMediaUrl as protected getFirstMediaUrlTrait;
    }

    public $table = 'markets';

    public $fillable = [
        'identifier',
        'fr_id',
        'fr_login',
        'fr_password',
        'name',
        'email',
        'description',
        'document',
        'social_reason',
        'street',
        'number',
        'district',
        'complement',
        'city',
        'state',
        'zipcode',
        'address',
        'latitude',
        'longitude',
        'phone',
        'mobile',
        'admin_commission',
        'delivery_fee',
        'free_delivery_above',
        'early_open_hour',
        'late_open_hour',
        'early_delivery_hour',
        'late_delivery_hour',
        'default_tax',
        'delivery_range',
        'available_for_delivery',
        'closed',
        'information',
        'cash_on_delivery_method',
        'card_on_delivery_method',
        'local_method',
        'mercado_pago_method',
        'saldo_fr_method',
        'mercado_pago_token',
        'cashback_percentage',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'identifier' => 'string',
        'fr_id' => 'integer',
        'fr_login' => 'string',
        'fr_password' => 'string',
        'name' => 'string',
        'email' => 'string',
        'description' => 'string',
        'document' => 'string',
        'social_reason' => 'string',
        'street' => 'string',
        'number' => 'string',
        'district' => 'string',
        'complement' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zipcode' => 'string',
        'image' => 'string',
        'address' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'phone' => 'string',
        'mobile' => 'string',
        'admin_commission' =>'double',
        'delivery_fee'=>'double',
        'free_delivery_above' => 'double',
        'early_open_hour' => 'string',
        'late_open_hour' => 'string',
        'early_delivery_hour' => 'string',
        'late_delivery_hour' => 'string',
        'default_tax'=>'double',
        'delivery_range'=>'double',
        'available_for_delivery'=>'boolean',
        'closed'=>'boolean',
        'information' => 'string',
        'cash_on_delivery_method' => 'integer',
        'card_on_delivery_method' => 'integer',
        'local_method' => 'integer',
        'mercado_pago_method' => 'integer',
        'saldo_fr_method' => 'integer',
        'mercado_pago_token' => 'string',
        'cashback_percentage' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $adminRules = [
        'name' => 'required',
        'description' => 'required',
        'document' => 'required',
        'delivery_fee' => 'nullable|numeric|min:0',
        'longitude' => 'required|numeric',
        'latitude' => 'required|numeric',
        'admin_commission' => 'numeric|min:0',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $managerRules = [
        'name' => 'required',
        'description' => 'required',
        'document' => 'required',
        'delivery_fee' => 'nullable|numeric|min:0',
        'longitude' => 'required|numeric',
        'latitude' => 'required|numeric',
    ];

    /**
     * New Attributes
     *
     * @var array
     */
    protected $appends = [
        'custom_fields',
        'has_media',
        'rate'
        
    ];

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_FILL, 500, 500)
            ->sharpen(10)
            ->nonQueued();

        $this->addMediaConversion('icon')
            ->fit(Manipulations::FIT_FILL, 300, 300)
            ->sharpen(10)
            ->nonQueued();
    }

    public function customFieldsValues()
    {
        return $this->morphMany('App\Models\CustomFieldValue', 'customizable');
    }

    /**
     * to generate media url in case of fallback will
     * return the file type icon
     * @param string $conversion
     * @return string url
     */
    public function getFirstMediaUrl($collectionName = 'default',$conversion = '')
    {
        $url = $this->getFirstMediaUrlTrait($collectionName);
        $array = explode('.', $url);
        $extension = strtolower(end($array));
        if (in_array($extension,config('medialibrary.extensions_has_thumb'))) {
            return asset($this->getFirstMediaUrlTrait($collectionName,$conversion));
        }else{
            return asset(config('medialibrary.icons_folder').'/'.$extension.'.png');
        }
    }

    public function getCustomFieldsAttribute()
    {
        $hasCustomField = in_array(static::class,setting('custom_field_models',[]));
        if (!$hasCustomField){
            return [];
        }
        $array = $this->customFieldsValues()
            ->join('custom_fields','custom_fields.id','=','custom_field_values.custom_field_id')
            ->where('custom_fields.in_table','=',true)
            ->get()->toArray();

        return convertToAssoc($array,'name');
    }

    /**
     * Add Media to api results
     * @return bool
     */
    public function getHasMediaAttribute()
    {
        return $this->hasMedia('image') ? true : false;
    }

    /**
     * Add Media to api results
     * @return bool
     */
    public function getRateAttribute()
    {
        return $this->marketReviews()->select(DB::raw('round(AVG(market_reviews.rate),1) as rate'))->first('rate')->rate;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(\App\Models\Product::class, 'market_id');
    }

    public function weight_based_gtin_settings()
    {
        return $this->hasOne(\App\Models\MarketWeightBasedGtinSettings::class, 'market_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function galleries()
    {
        return $this->hasMany(\App\Models\Gallery::class, 'market_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function marketReviews()
    {
        return $this->hasMany(\App\Models\MarketReview::class, 'market_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'user_markets');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function drivers()
    {
        return $this->belongsToMany(\App\Models\User::class, 'driver_markets');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function fields()
    {
        return $this->belongsToMany(\App\Models\Field::class, 'market_fields');
    }

    public function getEarlyDeliveryHourAttribute($value)
    {
        return date('H:i', strtotime($value));
    }

    public function getLateDeliveryHourAttribute($value)
    {
        return date('H:i', strtotime($value));
    }

    public function serviceClass(){
        return $this->hasOne(\App\Models\MarketServices::class, 'market_id');
    }
}
