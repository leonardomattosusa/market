<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebhookLogs extends Model
{
    public $table = 'webhook_logs';
    public $timestamps = true;

    protected $guarded = [];
}
