<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model
{
    public $table = 'password_resets';

    public $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
