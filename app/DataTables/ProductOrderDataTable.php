<?php

namespace App\DataTables;

use App\Models\CustomField;
use App\Models\ProductOrder;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ProductOrderDataTable extends DataTable
{
    /**
     * custom fields columns
     * @var array
     */
    public static $customFields = [];
    public $id = 0;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $columns = array_column($this->getColumns(), 'data');
        $dataTable = $dataTable
            ->editColumn('updated_at', function ($product_order) {
                return getDateColumn($product_order, 'updated_at');
            })
            ->editColumn('product.name', function ($productOrder) {
                if($productOrder->product->weight_based == 1 && $productOrder->final_quantity_defined == 0) {
                    return getMediaColumn($productOrder->product, 'image').' <a href="' . route('products.edit', $productOrder->product_id) . '" target="_blank">' . $productOrder->product->name . '</a> <span class="text-warning" style="font-size: 1.3rem" tooltip="true" title="Você precisa definir o peso/quantidade final deste produto para atualizar o preço ao cliente final."><i class="fas fa-exclamation-circle"></i></span>';
                }else{
                    return getMediaColumn($productOrder->product, 'image').' <a href="' . route('products.edit', $productOrder->product_id) . '" target="_blank">' . $productOrder->product->name . '</a>';
                }
            })
            ->editColumn('quantity', function ($productOrder) {
                if($productOrder->final_quantity_defined){
                    $qty = $productOrder->quantity;

                    if($productOrder->product->quantity_unit == 'g'){
                        $qty *= 1000;
                    }

                    return $qty.$productOrder->product->quantity_unit;
                }else{
                    if($productOrder->product->weight_based == 1) {
                        return '<div class="input-group" style="width: 120px"><input type="text" name="order_products['.$productOrder->id.']" onchange="autoUpdatePrice(this)" qty_type="weight" price="'.number_format($productOrder->price,2).'" class="form-control bg-warning d-inline text-right" style="color:white !important" placeholder="Peso" value="'.($productOrder->quantity * 1000).'"><div class="input-group-append"><button type="button" class="btn btn-sm" onclick="clearInputBG(this)"><i class="fas fa-check"></i></button></div> <span class="mt-2 ml-1">'.$productOrder->product->quantity_unit.'</span></div>';
                    }else{
                        return '<div class="input-group" style="width: 120px"><input type="text" name="order_products['.$productOrder->id.']" onchange="autoUpdatePrice(this)" qty_type="un" price="'.number_format($productOrder->price,2).'" class="form-control bg-warning d-inline text-right" style="color:white !important" placeholder="Qtd" value="'.($productOrder->quantity).'"><div class="input-group-append"><button type="button" class="btn btn-sm" onclick="clearInputBG(this)"><i class="fas fa-check"></i></button></div> <span class="mt-2 ml-1">un</span></div>';
                    }
                }
            })
            ->editColumn('price', function ($productOrder) {
                if($productOrder->product->weight_based == 1) {
                    return '<span class="product-price-div">R$'.number_format($productOrder->price * $productOrder->quantity,2,',','.').'</span><sup><i class="fas fa-question-circle text-info" tooltip="true" title="R$ '.number_format($productOrder->price,2,',','.').'/kg"></i></sup>';
                }else{
                    return '<span class="product-price-div">R$'.number_format($productOrder->price * $productOrder->quantity,2,',','.').'</span><sup><i class="fas fa-question-circle text-info" tooltip="true" title="R$ '.number_format($productOrder->price,2,',','.').'/un"></i></sup>';
                }
            })
            ->editColumn('gtin_ean', function ($productOrder) {
                if($productOrder->check_gtin_ean == 0){
                    return '<div class="input-group">
                                <input type="text" class="form-control" placeholder="GTIN/EAN: ' . $productOrder->product->gtin_ean . '" product_id="' . $productOrder->product_id . '" onkeyup="gtinEanKeyPress(event, this)">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-success" onclick="gtinEanButtonClick(this)"><i class="fa fa-check"></i></button>
                                </div>
                            </div>';
                }else{
                    return '<div class="input-group">
                                <input type="text" class="form-control bg-success text-white" placeholder="GTIN/EAN: ' . $productOrder->product->gtin_ean . '" product_id="' . $productOrder->product_id . '" value="'.$productOrder->product->gtin_ean.'" readonly>
                            </div>';
                }
            })
            ->editColumn('action', function($productOrder){
                if($productOrder->order->order_status_id == 3){
                    return '<button type="button" class="btn btn-danger m-0" tooltip="true" title="Remover produto" data-toggle="modal" data-target="#modal-remove-product" product_order_id="'.$productOrder->id.'" onclick="removeProductOrder(this)"><i class="fas fa-fw fa-times"></i></button>';
                }else{
                    return '<button type="button" class="btn btn-danger disabled m-0" tooltip="true" title="Você não pode mais remover nenhum produto do pedido."><i class="fas fa-fw fa-times"></i></button>';

                }
            })
            ->rawColumns(array_merge($columns))
            ->skipPaging();

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductOrder $model)
    {
        return $model->newQuery()->with("product")
            ->leftJoin('products', 'product_orders.product_id', '=', 'products.id')
            ->where('product_orders.order_id', $this->id)
            ->select('product_orders.*')
            ->orderBy('products.weight_based')
            ->orderBy('product_orders.id', 'desc');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'dom' => 'rt',
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            [
                'data' => 'product.name',
                'title' => trans('lang.product_order_product_id'),
                'orderable' => false,
                'searchable' => false,

            ],
            /*[
                'data' => 'options',
                'title' => trans('lang.product_order_options'),
                'searchable' => false,
                'orderable' => false,
            ],*/
            [
                'data' => 'price',
                'title' => trans('lang.product_order_price'),
                'orderable' => false,

            ],
            [
                'data' => 'quantity',
                'title' => trans('lang.product_order_quantity'),
                'orderable' => false,

            ],
            [
                'data' => 'gtin_ean',
                'title' => trans('lang.gtin_ean'),
                'orderable' => false,

            ],
            [
                'data' => 'action',
                'title' => 'Remover',
                'class' => 'text-center',
                'orderable' => false,

            ]
        ];

        $hasCustomField = in_array(ProductOrder::class, setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFieldsCollection = CustomField::where('custom_field_model', ProductOrder::class)->where('in_table', '=', true)->get();
            foreach ($customFieldsCollection as $key => $field) {
                array_splice($columns, $field->order - 1, 0, [[
                    'data' => 'custom_fields.' . $field->name . '.view',
                    'title' => trans('lang.product_order_' . $field->name),
                    'orderable' => false,
                    'searchable' => false,
                ]]);
            }
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'product_ordersdatatable_' . time();
    }

    /**
     * Export PDF using DOMPDF
     * @return mixed
     */
    public function pdf()
    {
        $data = $this->getDataForPrint();
        $pdf = PDF::loadView($this->printPreview, compact('data'));
        return $pdf->download($this->filename() . '.pdf');
    }
}