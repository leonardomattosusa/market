<?php

namespace App\DataTables;

use App\Models\CustomField;
use App\Models\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Barryvdh\DomPDF\Facade as PDF;

class CustomerDataTable extends DataTable
{

    /**
     * @var array
     */
    public static $customFields = [];
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $columns = array_column($this->getColumns(), 'data');
        return $dataTable
            ->editColumn('qrcode', function ($user) {
                return '<input type="checkbox" onclick="toggleQrcode(this)" value="' . $user->id . '" ' . ($user->can_use_qrcode == 1 ? 'checked' : '') . '>';
            })
            ->editColumn('updated_at', function ($user) {
                return getDateColumn($user, 'updated_at');
            })
            ->editColumn('email', function ($user) {
                return getEmailColumn($user, 'email');
            })
            ->editColumn('phone', function ($user) {
                return ($user->customFields && $user->customFields['phone']) ? $user->customFields['phone']['value'] : '';
            })
            ->editColumn('avatar', function ($user) {
                return getMediaColumn($user, 'avatar', 'img-circle elevation-2');
            })
            ->addColumn('action', 'customers.datatables_actions')
            ->rawColumns(array_merge($columns, ['action']));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        if (auth()->user()->hasRole('manager')) {
            $markets_ids = auth()->user()->markets->pluck('id')->toArray();

            return $model->newQuery()->with('roles')
                ->join("orders", "orders.user_id", "=", "users.id")
                ->join("product_orders", "orders.id", "=", "product_orders.order_id")
                ->join("products", "products.id", "=", "product_orders.product_id")
                ->whereHas('roles', function ($q) {
                    $q->where('id', 4);
                })
                ->whereIn('products.market_id', $markets_ids)
                ->groupBy('users.id')
                ->select('users.*');
        } else {
            return $model->newQuery()->with('roles')->whereHas('roles', function ($q) {
                $q->where('id', 4);
            });
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false, 'responsivePriority' => '100'])
            ->parameters(
                array_merge(
                    config('datatables-buttons.parameters'),
                    [
                        'language' => json_decode(
                            file_get_contents(
                                base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                            ),
                            true
                        )
                    ]
                )
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        // TODO custom element generator
        $columns = [
            [
                'data' => 'qrcode',
                'title' => 'Permitir QRcode',
                'orderable' => false,
                'searchable' => false,

            ],
            [
                'data' => 'avatar',
                'title' => trans('lang.user_avatar'),
                'orderable' => false,
                'searchable' => false,

            ],
            [
                'data' => 'name',
                'title' => trans('lang.user_name'),

            ],
            [
                'data' => 'email',
                'title' => trans('lang.user_email'),

            ],
            [
                'data' => 'phone',
                'title' => trans('lang.user_phone'),

            ],
            [
                'data' => 'updated_at',
                'title' => trans('lang.user_updated_at'),
                'searchable' => false,
            ]
        ];

        // TODO custom element generator
        $hasCustomField = in_array(User::class, setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFieldsCollection = CustomField::where('custom_field_model', User::class)->where('in_table', '=', true)->get();
            foreach ($customFieldsCollection as $key => $field) {
                array_splice($columns, $field->order - 1, 0, [
                    [
                        'data' => 'custom_fields.' . $field->name . '.view',
                        'title' => trans('lang.user_' . $field->name),
                        'orderable' => false,
                        'searchable' => false,
                    ]
                ]);
            }
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }

    /**
     * Export PDF using DOMPDF
     * @return mixed
     */
    public function pdf()
    {
        $data = $this->getDataForPrint();
        $pdf = PDF::loadView($this->printPreview, compact('data'));
        return $pdf->download($this->filename() . '.pdf');
    }
}