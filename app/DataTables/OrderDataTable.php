<?php
/**
 * File name: OrderDataTable.php
 * Last modified: 2020.04.29 at 14:48:08
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\DataTables;

use App\Models\CustomField;
use App\Models\Order;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * custom fields columns
     * @var array
     */
    public static $customFields = [];

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $columns = array_column($this->getColumns(), 'data');
        $dataTable = $dataTable
            ->editColumn('orders.id', function ($order) {
                if($order->order_status_id == 2 && $order->created_at < date('Y-m-d H:i:s', strtotime('-15 minutes'))){
                    return "<span class='text-danger'><b>#".$order->id.'</b></span>';
                }else{
                    return "#".$order->id;
                }
            })
            ->editColumn('payment.status', function ($order) {
                return getPayment($order->payment,'status');
            })
            ->editColumn('scheduled_delivery_date', function ($order) {
                if($order->scheduled_delivery_date){
                    return date('d/m/Y H:i', strtotime($order->scheduled_delivery_date));
                }else{
                    return 'Agora';
                }
            })
            ->filterColumn('orders.scheduled_delivery_date', function($query, $keyword){
                $query->whereRaw("DATE_FORMAT(orders.scheduled_delivery_date,'%d/%m/%Y %H:%i') like ?", ["%$keyword%"]);
            })
            ->editColumn('orders.created_at', function ($order) {
                return date('d/m/Y', strtotime($order->created_at));
            })
            ->filterColumn('orders.created_at', function($query, $keyword){
                $query->whereRaw("DATE_FORMAT(orders.created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', 'orders.datatables_actions')
            ->rawColumns(array_merge($columns, ['action']));

        return $dataTable;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            [
                'data' => 'orders.id',
                'title' => trans('lang.order_id'),
            ],
            [
                'data' => 'scheduled_delivery_date',
                'title' => trans('lang.scheduled_delivery_date'),
            ],
            [
                'data' => 'user.name',
                'name' => 'user.name',
                'title' => trans('lang.order_user_id'),

            ],
            [
                'data' => 'order_status.status',
                'name' => 'orderStatus.status',
                'title' => trans('lang.order_order_status_id'),

            ],
            [
                'data' => 'payment.method',
                'name' => 'payment.method',
                'title' => trans('lang.payment_method'),

            ],
            [
                'data' => 'payment.status',
                'name' => 'payment.status',
                'title' => trans('lang.payment_status'),

            ],
            [
                'data' => 'orders.created_at',
                'name' => 'created_at',
                'title' => 'Data',

            ],
        ];

        $hasCustomField = in_array(Order::class, setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFieldsCollection = CustomField::where('custom_field_model', Order::class)->where('in_table', '=', true)->get();
            foreach ($customFieldsCollection as $key => $field) {
                array_splice($columns, $field->order - 1, 0, [[
                    'data' => 'custom_fields.' . $field->name . '.view',
                    'title' => trans('lang.order_' . $field->name),
                    'orderable' => false,
                    'searchable' => false,
                ]]);
            }
        }
        return $columns;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        if (auth()->user()->hasRole('admin')) {
            $query = $model->newQuery()->with("user")->with("orderStatus")->with('payment')->select('orders.*');
        } else if (auth()->user()->hasRole('manager') || auth()->user()->hasRole('mercado') || auth()->user()->hasRole('order_picker')) {
            $query = $model->newQuery()->with("user")->with("orderStatus")->with('payment')
                ->join("product_orders", "orders.id", "=", "product_orders.order_id")
                ->join("products", "products.id", "=", "product_orders.product_id")
                ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
                ->where('user_markets.user_id', auth()->id())
                ->groupBy('orders.id')
                ->select('orders.*');
        } else if (auth()->user()->hasRole('client')) {
            $query = $model->newQuery()->with("user")->with("orderStatus")->with('payment')
                ->where('orders.user_id', auth()->id())
                ->groupBy('orders.id')
                ->select('orders.*');
        } else if (auth()->user()->hasRole('driver')) {
            $query = $model->newQuery()->with("user")->with("orderStatus")->with('payment')
                ->where('orders.driver_id', auth()->id())
                ->groupBy('orders.id')
                ->select('orders.*');
        } else {
            $query = $model->newQuery()->with("user")->with("orderStatus")->with('payment')->select('orders.*');
        }

        if(request()->segment(4)){
            switch (request()->segment(4)){
                case 'pending':
                    $query->whereIn('orders.order_status_id', [2])->where('active', 1);
                    break;
                case 'processing':
                    $query->whereIn('orders.order_status_id', [3])->where('active', 1);
                    break;
                case 'ready_to_delivery':
                    $query->whereIn('orders.order_status_id', [4])->where('active', 1);
                    break;
                case 'sent':
                    $query->whereIn('orders.order_status_id', [5])->where('active', 1);
                    break;
                case 'delivered':
                    $query->whereIn('orders.order_status_id', [6])->where('active', 1);
                    break;
                case 'canceled':
                    $query->where(function($q){
                        $q->whereNotNull('deleted_at')->orWhere('active', 0);
                    })->withTrashed();
                    break;
            }
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false, 'responsivePriority' => '100'])
            ->parameters(array_merge(
                [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true),
                    'order' => [ [0, 'desc'] ],
                ],
                config('datatables-buttons.parameters')
            ));
    }

    /**
     * Export PDF using DOMPDF
     * @return mixed
     */
    public function pdf()
    {
        $data = $this->getDataForPrint();
        $pdf = PDF::loadView($this->printPreview, compact('data'));
        return $pdf->download($this->filename() . '.pdf');
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ordersdatatable_' . time();
    }
}