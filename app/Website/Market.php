<?php

namespace App\Website;

class Market{
    public $id, $name, $delivery_fee, $mercado_pago_token, $closed, $free_delivery_above, $early_delivery_hour, $late_delivery_hour, $photo;

    public function __construct($market_id){
        $market = $this->find($market_id);

        $this->id = $market->id;
        $this->name = $market->name;
        $this->delivery_fee = $market->delivery_fee;
        $this->mercado_pago_token = $market->mercado_pago_token;
        $this->closed = $market->closed;
        $this->free_delivery_above = $market->free_delivery_above;
        $this->early_delivery_hour = $market->early_delivery_hour;
        $this->late_delivery_hour = $market->late_delivery_hour;
        $this->photo = $market->has_media ? $market->media[0]->url : 'https://imarket.digital/images/image_default.png';
        $this->cash_on_delivery_method = $market->cash_on_delivery_method;
        $this->card_on_delivery_method = $market->card_on_delivery_method;
        $this->local_method = $market->local_method;
        $this->mercado_pago_method = $market->mercado_pago_method;
        $this->saldo_fr_method = $market->saldo_fr_method;
    }

    protected function find($market_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/markets/'.$market_id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                session(['market_id' => null]);
            }
        }else{
            session(['market_id' => null]);
        }
    }
}
