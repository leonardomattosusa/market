<?php

namespace App\Website;

use Illuminate\Support\Str;

class User{
    public $id, $fr_id, $name, $email, $legal_type, $document, $addresses, $favorites, $cart_products, $market, $market_id;
    protected $api_token;

    public function __construct($user){
        $this->id = $user->id;
        $this->fr_id = $user->fr_id;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->api_token = $user->api_token;
        $this->legal_type = $user->legal_type;
        $this->document = $user->document;
        $this->market_id = null;

        $this->cart_products = $this->cart_products();
        $this->favorites = $this->favorites();

        $cart_collection = collect($this->cart_products);

        if($cart_collection->count() > 0){
            $market_id = $cart_collection->first()->product->market_id;

            $this->market_id = $market_id;
            $this->market = $this->getMarket($market_id);
        }
    }

    public function getFRUser(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://fidelidaderemunerada.com.br/api/associados/'.$this->fr_id.'?token=987ALJB98SD9A8HJ8UH@90C09Q8W8KL980H8G7ZX788');

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            return ['success' => true, 'message' => 'Usuário encontrado com sucesso.', 'user' => $response];
        }else{
            return ['success' => false, 'message' => 'Usuário correspondente na fidelidade remunerada não encontrado.'];
        }
    }

    public function getMarket($market_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/markets/'.$market_id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado em sua requisição. Tente novamente em alguns minutos.'];
        }
    }

    public function update($data){
        $data['api_token'] = $this->api_token;

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/users/'.$this->id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                session(['user' => $response->data]);

                return ['success' => true, 'message' => 'Dados atualizados com sucesso.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado em sua requisição. Tente novamente em alguns minutos.'];
        }
    }

    public function cart_products(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/carts?with=product&api_token='.$this->api_token.'&search=user_id:'.$this->id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function addToFavorites($product_id){
        $data = [
            'api_token' => $this->api_token,
            'user_id' => $this->id,
            'product_id' => (int)$product_id
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/favorites', ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => true, 'message' => 'Produto adicionado aos favoritos com sucesso.'];
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function removeFromFavorites($product_id){
        $data = [
            'api_token' => $this->api_token
        ];

        $favorite = collect($this->favorites)->where('product_id', $product_id)->first();

        if(!$favorite){
            return ['success' => true, 'message' => 'Produto removido dos favoritos com sucesso.'];
        }

        $client = new \GuzzleHttp\Client();
        $response = $client->delete('https://imarket.digital/api/favorites/'.$favorite->id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => true, 'message' => 'Produto removido dos favoritos com sucesso.'];
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function favorites(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/favorites?with=product&api_token='.$this->api_token.'&search=user_id:'.$this->id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function removeFromCart($product_on_cart_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->delete('https://imarket.digital/api/carts/'.$product_on_cart_id, ['json' => ['api_token' => $this->api_token]]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => true, 'message' => 'Produto removido do carrinho com sucesso.', 'cart' => $this->cart_products()];
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function updateCartProduct($product_on_cart_id, $quantity){
        if($quantity == 0){
            return $this->removeFromCart($product_on_cart_id);
        }

        $cart = [
            'quantity' => $quantity,
            'api_token' => $this->api_token
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->put('https://imarket.digital/api/carts/'.$product_on_cart_id, ['json' => $cart]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => true, 'message' => 'Carrinho atualizado com sucesso.', 'cart' => $this->cart_products()];
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function addToCart($product_id, $market_id, $quantity){
        $actual_cart = collect($this->cart_products());
        $product_on_cart = $actual_cart->where('product_id', $product_id)->first();
        $validMarket = ($actual_cart->count() > 0 && !$actual_cart->where('product.market_id', $market_id)->first()) ? false : true;

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/products/'.$product_id.'?api_token='.$this->api_token);
            $product = collect(json_decode($response->getBody())->data);

            if($product && $product['weight_based'] == 1){
                $quantity /= 1000;
            }
        }catch(\Exception $e){

        }

        if(!$validMarket){
            return ['success' => false, 'message' => 'Você deve esvaziar o seu carrinho para comprar em outro mercado.', 'invalid_market' => true];
        }

        // Caso o produto já esteja no carrinho não pode adicionar denovo
        if($product_on_cart){
            return $this->updateCartProduct($product_on_cart->id, $quantity);
        }

        if($quantity > 0) {
            $cart = [
                'user_id' => $this->id,
                'product_id' => $product_id,
                'quantity' => $quantity,
                'api_token' => $this->api_token
            ];

            $client = new \GuzzleHttp\Client();
            $response = $client->post('https://imarket.digital/api/carts', ['json' => $cart]);

            if($response->getStatusCode() == 200){
                $response = json_decode($response->getBody());

                if($response->success){
                    return ['success' => true, 'message' => 'Produto adicionado ao carrinho com sucesso.', 'cart' => $this->cart_products()];
                }else{
                    return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
                }
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Você não pode adicionar o produto ao carrinho sem selecionar a quantidade.'];
        }
    }

    public function getAddress($address_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/delivery_addresses/'. $address_id .'?api_token='.$this->api_token);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro ao identificar o endereço de entrega. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro ao identificar o endereço de entrega. Tente novamente em alguns minutos.'];
        }
    }

    public function getAddresses(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/delivery_addresses?search=user_id:'.$this->id.'&api_token='.$this->api_token);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                 return $response->data;
            }else{
                session(['api_token' => null]);
            }
        }else{
            return ['success' => false, 'message' => 'Erro ao recuperar endereços do cliente.'];
        }
    }

    public function deleteAddress($address_id){
        $data = [
            'user_id' => $this->id,
            'api_token' => $this->api_token
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->delete('https://imarket.digital/api/delivery_addresses/'.$address_id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return redirect()->back()->with('success', 'Endereço excluído com sucesso.');
            }else{
                return redirect()->back()->with('error', 'Erro ao cadastrar um novo endereço. Tente novamente em alguns minutos.');
            }
        }else{
            return redirect()->back()->with('error', 'Erro ao cadastrar um novo endereço. Tente novamente em alguns minutos.');
        }
    }

    public function updateAddress($data, $address_id){
        $data['api_token'] = $this->api_token;

        $client = new \GuzzleHttp\Client();
        $response = $client->put('https://imarket.digital/api/delivery_addresses/'.$address_id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => true, 'message' => 'Endereço atualizado com sucesso.', 'address' => $response->data];
            }else{
                return ['success' => false, 'message' => 'Erro ao cadastrar um novo endereço. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Erro ao cadastrar um novo endereço. Tente novamente em alguns minutos.'];
        }
    }

    public function registerAddress($data){
        $data['api_token'] = $this->api_token;

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/delivery_addresses', ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => true, 'message' => 'Novo endereço cadastrado com sucesso.', 'address' => $response->data];
            }else{
                return ['success' => false, 'message' => 'Erro ao cadastrar um novo endereço. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Erro ao cadastrar um novo endereço. Tente novamente em alguns minutos.'];
        }
    }

    public function getDeliveryAddress($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/delivery_addresses/'.$id.'?api_token='.$this->api_token);

            if($response->getStatusCode() == 200){
                $response = json_decode($response->getBody());

                if($response->success){
                    return $response->data;
                }else{
                    session(['api_token' => null]);
                }
            }else{
                return ['success' => false, 'message' => 'Erro ao recuperar endereço do cliente.'];
            }
        }catch (\Exception $e){
            return ['success' => false, 'message' => 'Erro ao recuperar endereço do cliente.'];
        }
    }

    public function orders(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/orders?with=payment;orderStatus&api_token='.$this->api_token.'&search=user_id:'.$this->id.'&orderBy=created_at&sortedBy=desc');

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function getOrder($order_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/orders/'.$order_id.'?with=payment;productOrders;deliveryAddress;orderStatus&api_token='.$this->api_token.'&search=user_id:'.$this->id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success'=> true, 'message' => 'Pedido encontrado', 'data' => $response->data];
            }else{
                return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function createOrder($address_id, $payment_method, $payment_hash=null, $delivery_date=null, $delivery_change=0){
        if($payment_hash == null){
            $payment_hash = $this->id.Str::random(15);
        }

        switch ($payment_method){
            case 'mercado_pago':
                $written_payment_method = 'Mercado Pago';
                break;
            case 'saldo_fr':
                $written_payment_method = 'Saldo FR';
                break;
            case 'cash':
                $written_payment_method = 'Dinheiro na entrega';
                break;
            case 'card':
                $written_payment_method = 'Cartão na entrega';
                break;
            case 'local':
                $written_payment_method = 'Pagar na retirada';
                break;
            default:
                $written_payment_method = 'Dinheiro na entrega';
                break;
        }

        if(collect($this->cart_products)->where('product.deliverable', 0)->count() > 0){
            $written_payment_method = 'Pagar na retirada';
        }

        if($delivery_change == null || $delivery_change == 0){
            $delivery_change = 0;
        }

        $data = [
            'api_token' => $this->api_token,
            'user_id' => $this->id,
            'order_status_id' => ($payment_method == 'mercado_pago') ? 1 : 2,
            'tax' => 0,
            'hint' => null,
            'active' => 1,
            'driver_id' => null,
            'delivery_address_id' => (int)$address_id,
            'scheduled_delivery_date' => $delivery_date != null ? date('Y-m-d H:i:s', strtotime($delivery_date)) : null,
            'payment_hash' => $payment_hash,
            'delivery_change' => $delivery_change,
            'payment' => [
                'method' => $written_payment_method
            ],
            'products' => []
        ];

        $order_amount = 0;

        foreach ($this->cart_products as $p){
            $data['products'][] = [
                'product_id' => $p->product_id,
                'quantity' => $p->quantity,
                'price' => $p->product->price
            ];

            $order_amount += $p->product->price * $p->quantity;
        }

        if($this->market->free_delivery_above == null || $this->market->free_delivery_above <= 0 || $order_amount < $this->market->free_delivery_above){
            $data['delivery_fee'] = $this->market->delivery_fee;
        }else{
            $data['delivery_fee'] = 0;
        }

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/orders', ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                session(['payment_hash' => $payment_hash]);
                session(['order_id' => $response->data->id]);

                return ['success' => true, 'message' => 'Pedido cadastrado com sucesso', 'data' => $response->data];
            }else{
                return ['success' => false, 'message' => 'Erro ao criar o pedido do cliente.'];
            }
        }else{
            return ['success' => false, 'message' => 'Erro ao criar o pedido do cliente.'];
        }
    }

    /*protected function find($api_token){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/user?api_token='.$this->api_token);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                $this->user = $response->data;
            }else{
                session(['api_token' => null]);
            }
        }else{
            session(['api_token' => null]);
        }
    }*/
}