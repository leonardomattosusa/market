<?php

namespace App\Console;

use App\Jobs\DailyProductCheck;
use App\Models\MarketServices;
use App\Models\Media;
use App\Models\MonthlyPaymentLines;
use App\Models\MonthlyPayments;
use App\Models\Product;
use App\Models\User;
use App\Services\ImportsService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Storage;
use SimpleImage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //import products that aren't registered on the base market yet
        // $schedule->call(function(){
        //     set_time_limit(60);
        //     $service = new ImportsService();
        //     try {
        //         $service->importProductsToBaseMarket();
        //     }catch(\Exception $e){
        //         Log::error('Erro.', [$e]);
        //         Log::channel('cronjobs')->error('Erro ao processar importação de produtos para o mercado base. Verifique o canal de logs');
        //     }

        // })->everyMinute();

        // $schedule->call(function () {
        //     set_time_limit(0);
        //     $service = new ImportsService();
        //     $products = Product::where('image_imported', 0)->where('gtin_ean', '!=', null)->where('market_id', 1)->where('validated', 0)->limit(300)->get();

        //     foreach ($products as  $index => $product){
        //         $service->importProductImage($product->id);
        //     }
        // })->everyFifteenMinutes();

        // Create the first monthly payment
        // $schedule->call(function(){
        //     $users = User::whereHas('roles', function($q){
        //         $q->where('roles.id', 3);
        //     })->doesntHave('lastMonthlyPayment')->get();

        //     foreach($users as $user){
        //         $total_amount = 890 + (($user->markets_limit - 1) * 590);

        //         $new_monthly_payment = new MonthlyPayments();

        //         $new_monthly_payment->user_id = $user->id;
        //         $new_monthly_payment->total_amount = $total_amount;
        //         $new_monthly_payment->due_date = date('Y-m-05', strtotime('first day of next month'));
        //         $new_monthly_payment->markets_quantity = $user->markets_limit;
        //         $new_monthly_payment->status = 'pending';

        //         $new_monthly_payment->save();

        //         $new_line = new MonthlyPaymentLines();
        //         $new_line->monthly_payment_id = $new_monthly_payment->id;
        //         $new_line->amount = 890;
        //         $new_line->description = 'Valor mensal referente à conta no iMarket com direito ao cadastro de um mercado.';
        //         $new_line->type = 'market';
        //         $new_line->save();

        //         for($i = 0; $i < ($user->markets_limit - 1); $i++){
        //             $new_line = new MonthlyPaymentLines();
        //             $new_line->monthly_payment_id = $new_monthly_payment->id;
        //             $new_line->amount = 590;
        //             $new_line->description = 'Valor mensal referente ao cadastro extra de um mercado.';
        //             $new_line->type = 'market';
        //             $new_line->save();
        //         }
        //     }
        // })->hourly();


        // $schedule->call(function(){
        //     $due_mps = MonthlyPayments::where('status', 'pending')->whereDate('due_date', '<', date('Y-m-d'))->get();
        //     foreach($due_mps as $mp){
        //         $markets = $mp->user->markets;
        //         if($markets){
        //             foreach($markets as $market){
        //                 $market->status = 'inactive';
        //                 $market->save();
        //             }
        //         }
        //         $mp->user->status = 'inactive';
        //         $mp->user->save();
        //     }
        // })->daily();

        $schedule->call(function(){
            $market_services = MarketServices::all();
            foreach ($market_services as $m){
                try {
                    DailyProductCheck::dispatch($m->market_id);
                }catch (\Exeption $e){
                    Log::error('Erro.', [$e]);
                    Log::channel('cronjobs')->error('Erro ao processar verificação de produtos diária do mercado '.$m->market->name.'. Verifique o canal de logs.');
                }
            }
        })->dailyAt('02:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
