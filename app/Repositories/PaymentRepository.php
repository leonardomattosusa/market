<?php

namespace App\Repositories;

use App\Events\OrderPaymentConfirmed;
use App\Models\PartialPayments;
use App\Models\Payment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaymentRepository
 * @package App\Repositories
 * @version August 29, 2019, 9:39 pm UTC
 *
 * @method Payment findWithoutFail($id, $columns = ['*'])
 * @method Payment find($id, $columns = ['*'])
 * @method Payment first($columns = ['*'])
*/
class PaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'price',
        'description',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Payment::class;
    }

    public function markAsPaid($order){
        PartialPayments::where('user_id', $order->user_id)->where('status', 'pending')->update([
            'order_id' => $order->id,
            'status' => 'used'
        ]);

        $this->update([
            "status" => 'Pago',
            "payment_date" => date('Y-m-d H:i:s')
        ], $order->payment_id);

        if($order->order_status_id == 1){
            $order->order_status_id = 2;
            $order->save();
        }

        event(new OrderPaymentConfirmed($order));
    }
}
