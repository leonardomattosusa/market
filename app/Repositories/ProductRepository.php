<?php

namespace App\Repositories;

use App\Models\Media;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use Storage;
use SimpleImage;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version August 29, 2019, 9:38 pm UTC
 *
 * @method Product findWithoutFail($id, $columns = ['*'])
 * @method Product find($id, $columns = ['*'])
 * @method Product first($columns = ['*'])
 */
class ProductRepository extends BaseRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price',
        'discount_price',
        'description',
        'ingredients',
        'capacity',
        'package_items_count',
        'unit',
        'featured',
        'market_id',
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }

    /**
     * get my products
     **/
    public function myProducts()
    {
        return Product::join("user_markets", "user_markets.market_id", "=", "products.market_id")
                      ->where('user_markets.user_id', auth()->id())
                      ->get();
    }

    public function parseWhere($where){
        $result = [];

        if (stripos($where, ';') || stripos($where, ':')) {
            $values = explode(';', $where);
            foreach ($values as $value) {
                $s = explode(':', $value);

                $result[$s[0]] = $s[1];
            }
        }

        return $result;
    }

    public function copyBaseProduct($model_id, $market_id, $price = null){
        $model_product = Product::find($model_id);
        $product_exists = Product::where('gtin_ean', $model_product->gtin_ean)->where('market_id', $market_id)->first();

        if(!$product_exists){
            $input = $model_product->toArray();

            // Set market, new price and unset all unecessary data to copy.
            if($price){
                $input['price'] = $price;
            }
            $input['market_id'] = $market_id;
            unset($input['id'], $input['created_at'], $input['updated_at'], $input['market'], $input['media'], $input['has_media'], $input['custom_fields']);

            $product = Product::create($input);

            try {
                $photo = Media::where('model_type', 'App\Models\Product')->where('model_id', $model_product->id)->first();

                if($photo){
                    $data = $photo->toArray();
                    $data['model_id'] = $product->id;
                    $data['manipulations'] = [];
                    $data['custom_properties'] = (object)[];
                    $data['responsive_images'] = [];

                    unset($data['id'], $data['order_column'], $data['created_at'], $data['updated_at'], $data['url'], $data['thumb'], $data['icon'], $data['formated_size']);

                    $new_photo = Media::create($data);

                    // Limpa os campos que podem causar erros e copia as imagens no STORAGE
                    File::copyDirectory(storage_path('app/public/'.$photo->id), storage_path('app/public/'.$new_photo->id));
                }
            } catch(\Exception $e){

            }

            return $product;
        }
    }

    public function registerNewBaseProduct($product){
        $product_exists = Product::where('gtin_ean', $product->gtin_ean)->where('market_id', 1)->first();

        if(!$product_exists){
            $input = $product->toArray();

            // Set market, new price and unset all unecessary data to copy.
            $input['market_id'] = 1;
            $input['validated'] = 0;
            unset($input['id'], $input['created_at'], $input['updated_at'], $input['market'], $input['media'], $input['has_media'], $input['custom_fields']);

            $new_product = Product::create($input);

            $photo = Media::where('model_type', 'App\Models\Product')->where('model_id', $product->id)->first();

            try {
                if($photo){
                    $data = $photo->toArray();
                    $data['model_id'] = $new_product->id;
                    $data['manipulations'] = [];
                    $data['custom_properties'] = (object)[];
                    $data['responsive_images'] = [];

                    unset($data['id'], $data['order_column'], $data['created_at'], $data['updated_at'], $data['url'], $data['thumb'], $data['icon'], $data['formated_size']);

                    $new_photo = Media::create($data);

                    // Limpa os campos que podem causar erros e copia as imagens no STORAGE
                    File::copyDirectory(storage_path('app/public/'.$photo->id), storage_path('app/public/'.$new_photo->id));
                }

            } catch(\Exception $e){

            }

            return $new_product;
        }
    }

    public function validateProductAndUpdate($product){
        $product->validated = 1;
        $product->save();

        $photo = Media::where('model_type', 'App\Models\Product')->where('model_id', $product->id)->first();

        $existing_products = Product::where('gtin_ean', $product->gtin_ean)->where('market_id', '!=', 1)->get();

        try {
            foreach($existing_products as $existing_product){
                $existing_product->name = $product->name;
                $existing_product->ncm = $product->ncm;
                $existing_product->description = $product->description;
                $existing_product->package_items_count = $product->package_items_count;
                $existing_product->category_id = $product->category_id;
                $existing_product->validated = 1;

                $existing_product->save();

                Media::where('model_type', 'App\Models\Product')->where('model_id', $existing_product->id)->delete();

                if($photo){
                    $data = $photo->toArray();
                    $data['model_id'] = $existing_product->id;
                    $data['manipulations'] = [];
                    $data['custom_properties'] = (object)[];
                    $data['responsive_images'] = [];

                    unset($data['id'], $data['order_column'], $data['created_at'], $data['updated_at'], $data['url'], $data['thumb'], $data['icon'], $data['formated_size']);

                    $new_photo = Media::create($data);

                    if(File::exists(storage_path('app/public/'.$new_photo->id))){
                        File::deleteDirectory(storage_path('app/public/'.$new_photo->id));
                    }

                    // Limpa os campos que podem causar erros e copia as imagens no STORAGE
                    File::copyDirectory(storage_path('app/public/'.$photo->id), storage_path('app/public/'.$new_photo->id));
                }
            }
        } catch(\Exception $e){
            Log::error('Erro ao atualizar produto após a atualização no mercado base #'.$product->id, [$e]);
        }
    }
}
