<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMarketsAddAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('markets', function (Blueprint $table) {
            $table->string('identifier')->nullable()->after('id');
            $table->string('fr_login')->nullable()->after('name');
            $table->string('fr_password')->nullable()->after('fr_login');
            $table->string('email')->nullable()->after('fr_password');
            $table->string('social_reason')->nullable()->after('email');
            $table->string('street')->nullable()->after('address');
            $table->string('number')->nullable()->after('street');
            $table->string('district')->nullable()->after('number');
            $table->string('complement')->nullable()->after('district');
            $table->string('city')->nullable()->after('complement');
            $table->string('state')->nullable()->after('city');
            $table->string('zipcode')->nullable()->after('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('markets', function (Blueprint $table) {
            $table->dropColumn('identifier');
            $table->dropColumn('fr_login');
            $table->dropColumn('fr_password');
            $table->dropColumn('email');
            $table->dropColumn('social_reason');
            $table->dropColumn('street');
            $table->dropColumn('number');
            $table->dropColumn('district');
            $table->dropColumn('complement');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('zipcode');
        });
    }
}
