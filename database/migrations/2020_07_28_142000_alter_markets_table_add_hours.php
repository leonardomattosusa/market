<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMarketsTableAddHours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('markets', function (Blueprint $table) {
            $table->time('early_open_hour', 0)->default('08:00:00')->after('free_delivery_above');
            $table->time('late_open_hour', 0)->default('18:00:00')->after('early_open_hour');
            $table->time('early_delivery_hour', 0)->default('08:00:00')->after('late_open_hour');
            $table->time('late_delivery_hour', 0)->default('18:00:00')->after('early_delivery_hour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('markets', function (Blueprint $table) {
            $table->dropColumn('early_open_hour');
            $table->dropColumn('late_open_hour');
            $table->dropColumn('early_delivery_hour');
            $table->dropColumn('late_delivery_hour');
        });
    }
}
