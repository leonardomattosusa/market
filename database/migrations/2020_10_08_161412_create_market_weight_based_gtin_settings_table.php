<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketWeightBasedGtinSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_weight_based_gtin_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('market_id');
            $table->bigInteger('size');
            $table->char('initial_digit', 2);
            $table->char('product_first_digit', 2);
            $table->char('product_last_digit', 2);
            $table->enum('value_type', ['price', 'weight'])->default('price');
            $table->char('value_decimal_places', 2);
            $table->char('value_first_digit', 2);
            $table->char('value_last_digit', 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_weight_based_gtin_settings');
    }
}
