<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMethodsToMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('markets', function (Blueprint $table) {
            $table->tinyInteger('cash_on_delivery_method')->default(1)->after('available_for_delivery');
            $table->tinyInteger('card_on_delivery_method')->default(1)->after('cash_on_delivery_method');
            $table->tinyInteger('local_method')->default(1)->after('card_on_delivery_method');
            $table->tinyInteger('mercado_pago_method')->default(1)->after('local_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('markets', function (Blueprint $table) {
            $table->dropColumn('cash_on_delivery_method');
            $table->dropColumn('card_on_delivery_method');
            $table->dropColumn('local_method');
            $table->dropColumn('mercado_pago_method');
        });
    }
}
