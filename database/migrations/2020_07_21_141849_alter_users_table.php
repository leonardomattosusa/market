<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('markets_limit')->default(1)->after('paypal_email');
            $table->integer('users_limit')->default(1)->after('markets_limit');
            $table->integer('cashback_percentage')->default(3)->after('users_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('markets_limit');
            $table->dropColumn('users_limit');
            $table->dropColumn('cashback_percentage');
        });
    }
}
