<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTableAddPreparingDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('preparing_start_date')->nullable()->after('scheduled_delivery_date');
            $table->dateTime('preparing_end_date')->nullable()->after('preparing_start_date');
            $table->time('preparing_time_taken')->nullable()->after('preparing_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('preparing_start_date');
            $table->dropColumn('preparing_end_date');
            $table->dropColumn('preparing_time_taken');
        });
    }
}
