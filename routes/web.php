<?php
/**
 * File name: web.php
 * Last modified: 2020.06.07 at 07:02:57
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

use Maatwebsite\Excel\Row;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('teste', 'AccountController@getMarketDbInfo');//Rota de teste para o codigo de importação

Route::get('', 'WebsiteController@index');
Route::get('login_register', 'WebsiteController@loginRegister');
Route::get('forgot_password', 'WebsiteController@forgotPassword');
Route::get('markets-list', 'WebsiteController@marketsList');
Route::get('change-market/{market?}', 'WebsiteController@changeMarket');
Route::get('market/{market}', 'WebsiteController@market');
Route::get('market/{market}/categories/{category}', 'WebsiteController@market');
Route::get('cart', 'WebsiteController@cart');
Route::get('checkout', 'WebsiteController@checkout');
Route::get('logout', 'WebsiteController@logout');
Route::get('fidelidade-remunerada', 'WebsiteController@fidelidadeRemunerada');
Route::get('password/reset/{token}', 'WebsiteController@passwordReset')->name('auth.password.reset');
Route::post('password/reset/{token}', 'WebsiteController@resetPassword');

Route::get('profile', 'WebsiteController@profile');
Route::get('favorites', 'WebsiteController@favorites');
Route::get('order/{order}', 'WebsiteController@order');

Route::post('save-localization', 'WebsiteController@saveLocalization');
Route::post('login', 'WebsiteController@login');
Route::post('update_user', 'WebsiteController@updateUser');
Route::post('register', 'WebsiteController@register');
Route::post('register_address', 'WebsiteController@register_address');
Route::post('update_address/{address_id}', 'WebsiteController@update_address');
Route::post('add_to_favorites', 'WebsiteController@addProductToFavorites');
Route::post('remove_from_favorites', 'WebsiteController@removeProductFromFavorites');
Route::post('add_product_to_cart', 'WebsiteController@addProductToCart');
Route::post('remove_product_from_cart', 'WebsiteController@removeProductFromCart');
Route::post('create_order', 'WebsiteController@createOrder');
Route::post('password_recovery', 'WebsiteController@sendPasswordRecoveryEmail');

Route::get('checkout/mercadopago/callback/{status}', 'WebsiteController@callback_mercadopago')->name('checkout_mercadopago_callback');
Route::get('checkout/mercadopago/{api_token}/{delivery_address_id}/{delivery_date?}', 'WebsiteController@mercadopago_app')->name('checkout_mercadopago_app');
Route::get('checkout/mercadopago_order/{api_token}/{order_id}', 'WebsiteController@mercadopago_app_order')->name('checkout_mercadopago_app_order');
Route::get('checkout/partial_payment_fr', 'API\FidelidadeRemunerada@partialPayment');
Route::post('checkout/partial_payment_fr', 'API\FidelidadeRemunerada@postPartialPayment');


Route::get('checkout/saldofr/{api_token}/{delivery_address_id}/{delivery_date?}', 'WebsiteController@saldofr_app')->name('checkout_saldofr_app');
Route::post('pay/saldofr/{api_token}/{delivery_address_id}/{delivery_date?}', 'WebsiteController@post_saldofr_app')->name('post_checkout_saldofr_app');

Route::get('storage/app/public/{id}/{conversion}/{filename?}', 'UploadController@storage');

Route::get('import_products', 'ImportsController@transformCsv');
Route::get('import_images_database_products', 'ImportsController@crawlImagesForDatabaseProducts');

Route::prefix('admin')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::get('/', 'DashboardController@index')->name('dashboard');

        Route::post('uploads/store', 'UploadController@store')->name('medias.create');
        Route::get('users/profile', 'UserController@profile')->name('users.profile');
        Route::post('users/change_password', 'UserController@changePassword')->name('users.change_password');
        Route::post('users/remove-media', 'UserController@removeMedia');
        Route::resource('users', 'UserController');
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('dashboard/test', 'DashboardController@test')->name('dashboard.test');

        Route::group(['middleware' => ['permission:medias']], function () {
            Route::get('uploads/all/{collection?}', 'UploadController@all');
            Route::get('uploads/collectionsNames', 'UploadController@collectionsNames');
            Route::post('uploads/clear', 'UploadController@clear')->name('medias.delete');
            Route::get('medias', 'UploadController@index')->name('medias');
            Route::get('uploads/clear-all', 'UploadController@clearAll');
        });

        Route::group(['middleware' => ['permission:permissions.index']], function () {
            Route::get('permissions/role-has-permission', 'PermissionController@roleHasPermission');
            Route::get('permissions/refresh-permissions', 'PermissionController@refreshPermissions');
        });
        Route::group(['middleware' => ['permission:permissions.index']], function () {
            Route::post('permissions/give-permission-to-role', 'PermissionController@givePermissionToRole');
            Route::post('permissions/revoke-permission-to-role', 'PermissionController@revokePermissionToRole');
        });

        Route::group(['middleware' => ['permission:app-settings']], function () {
            Route::prefix('settings')->group(function () {
                Route::resource('permissions', 'PermissionController');
                Route::resource('roles', 'RoleController');
                Route::resource('customFields', 'CustomFieldController');
                Route::resource('currencies', 'CurrencyController')->except([
                    'show'
                ]);
                Route::get('users/login-as-user/{id}', 'UserController@loginAsUser')->name('users.login-as-user');
                Route::patch('update', 'AppSettingController@update');
                Route::patch('translate', 'AppSettingController@translate');
                Route::get('sync-translation', 'AppSettingController@syncTranslation');
                Route::get('clear-cache', 'AppSettingController@clearCache');
                Route::get('check-update', 'AppSettingController@checkForUpdates');
                // disable special character and number in route params
                Route::get('/{type?}/{tab?}', 'AppSettingController@index')
                    ->where('type', '[A-Za-z]*')->where('tab', '[A-Za-z]*')->name('app-settings');
            });
        });

        Route::post('fields/remove-media', 'FieldController@removeMedia');
        Route::resource('fields', 'FieldController')->except([
            'show'
        ]);

        Route::post('markets/remove-media', 'MarketController@removeMedia');
        Route::resource('markets', 'MarketController')->except([
            'show'
        ]);
        Route::get('markets/{id}/settings', 'MarketController@settings')->name('markets.settings');
        Route::post('markets/{id}/settings/weight_based_gtin', 'MarketController@weightBasedGtinPost')->name('markets.settings.weight_based_gtin.post');

        Route::resource('customers', 'CustomerController');

        Route::post('categories/remove-media', 'CategoryController@removeMedia');
        Route::resource('categories', 'CategoryController')->except([
            'show'
        ]);

        Route::resource('faqCategories', 'FaqCategoryController')->except([
            'show'
        ]);

        Route::resource('orderStatuses', 'OrderStatusController');

        Route::get('products/update_amounts', 'ProductController@updateAmounts');
        Route::post('products/update_amounts', 'ProductController@postUpdateAmounts');
        Route::get('products/import_from_database/{market_id}', 'ProductController@importFromDatabase')->name('products.import_from_database');
        Route::get('products/reload_from_server/{product_id}', 'ProductController@reloadFromServer')->name('products.reload_from_server');
        Route::post('products/import_from_database/{market_id}', 'ProductController@postImportFromDatabase')->name('products.import_from_database.post');
        Route::post('products/remove-media', 'ProductController@removeMedia');
        Route::resource('products', 'ProductController')->except([
            'show'
        ]);

        Route::post('galleries/remove-media', 'GalleryController@removeMedia');
        Route::resource('galleries', 'GalleryController')->except([
            'show'
        ]);

        Route::resource('productReviews', 'ProductReviewController')->except([
            'show'
        ]);

        Route::post('options/remove-media', 'OptionController@removeMedia');


        Route::resource('payments', 'PaymentController')->except([
            'create',
            'store',
            'edit',
            'destroy'
        ]);
        ;

        Route::resource('faqs', 'FaqController')->except([
            'show'
        ]);
        Route::resource('marketReviews', 'MarketReviewController')->except([
            'show'
        ]);

        Route::resource('favorites', 'FavoriteController')->except([
            'show'
        ]);

        Route::get('orders/list/{type}', 'OrderController@list')->name('orders.list');
        Route::post('orders/{order_id}/add_product', 'OrderController@addProductOrder')->name('orders.add_product');
        Route::post('orders/{order_id}/remove_product/{product_id}', 'OrderController@removeProductOrder')->name('orders.remove_product');
        Route::post('orders/mark_as_sent', 'OrderController@markAsSent')->name('orders.mark_as_sent');
        Route::post('orders/mark_as_delivered', 'OrderController@markAsDelivered')->name('orders.mark_as_delivered');
        Route::get('orders/{order_id}/mark_as_delivered', 'OrderController@deliverOrder')->name('orders.deliver');
        Route::get('orders/{order_id}/mark_as_paid', 'OrderController@markAsPaid')->name('orders.mark_as_paid');
        Route::get('orders/{order_id}/change_status/{status}', 'OrderController@changeStatus')->name('orders.change_status');
        Route::get('orders/{order_id}/print_payment_receipt', 'OrderController@printPaymentReceipt')->name('orders.print_payment_receipt');
        Route::put('orders/{order_id}/restore', 'OrderController@restore')->name('orders.restore');
        Route::put('orders/{order_id}/update_quantities', 'OrderController@updateQuantities')->name('orders.update_quantities');
        Route::post('orders/{order_id}/add_new_product', 'OrderController@addProductOrder')->name('orders.add_new_product');
        Route::resource('orders', 'OrderController');

        Route::resource('notifications', 'NotificationController')->except([
            'create',
            'store',
            'update',
            'edit',
        ]);
        ;

        Route::resource('carts', 'CartController')->except([
            'show',
            'store',
            'create'
        ]);
        Route::resource('deliveryAddresses', 'DeliveryAddressController')->except([
            'show'
        ]);

        Route::get('drivers/reports', 'DriverController@reports')->name('drivers.reports');
        Route::post('drivers/remove-media', 'DriverController@removeMedia');
        Route::resource('drivers', 'DriverController');

        Route::get('order_pickers/reports', 'OrderPickerController@reports')->name('order_pickers.reports');
        Route::post('order_pickers/remove-media', 'OrderPickerController@removeMedia');
        Route::resource('order_pickers', 'OrderPickerController');

        Route::resource('earnings', 'EarningController')->except([
            'show',
            'edit',
            'update'
        ]);

        Route::resource('driversPayouts', 'DriversPayoutController')->except([
            'show',
            'edit',
            'update'
        ]);

        Route::resource('marketsPayouts', 'MarketsPayoutController')->except([
            'show',
            'edit',
            'update'
        ]);

        Route::resource('optionGroups', 'OptionGroupController')->except([
            'show'
        ]);

        Route::post('options/remove-media', 'OptionController@removeMedia');

        Route::resource('options', 'OptionController')->except([
            'show'
        ]);

        Route::get('account', 'AccountController@index')->name('account.index');
        Route::get('account/add_market', 'AccountController@addMarket')->name('account.add_market');
        Route::post('account/add_market', 'AccountController@postAddMarket')->name('account.add_market.post');
        Route::get('account/monthly_payments', 'AccountController@monthlyPayments')->name('account.monthly_payments');
        Route::get('account/show_monthly_payment/{mp}', 'AccountController@showMonthlyPayment')->name('account.show_monthly_payment');
        Route::post('account/pay_monthly_payment/{mp}', 'AccountController@payMonthlyPayment')->name('account.pay_monthly_payment');



        Route::get('cash_register', 'CashRegisterController@index')->name('cash_register.index');
    });

    Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');
    Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');
    Auth::routes(['password.reset' => false]);

    Route::get('payments/failed', 'PayPalController@index')->name('payments.failed');
    Route::get('payments/razorpay/checkout', 'RazorPayController@checkout');
    Route::post('payments/razorpay/pay-success/{userId}/{deliveryAddressId?}', 'RazorPayController@paySuccess');
    Route::get('payments/razorpay', 'RazorPayController@index');

    Route::get('payments/paypal/express-checkout', 'PayPalController@getExpressCheckout')->name('paypal.express-checkout');
    Route::get('payments/paypal/express-checkout-success', 'PayPalController@getExpressCheckoutSuccess');
    Route::get('payments/paypal', 'PayPalController@index')->name('paypal.index');

    Route::get('firebase/sw-js', 'AppSettingController@initFirebase');


});

