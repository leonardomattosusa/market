<?php
$secret = "PXKmZP4y1GFQAq8J"; // Set this to a secret known only by your Git provider and your server

// Validate the webhook
$payload = file_get_contents('php://input');
$signature = $_SERVER['HTTP_X_HUB_SIGNATURE'];

if ($signature) {
    $hash = "sha256=" . hash_hmac('sha256', $payload, $secret);
    if (hash_equals($signature, $hash)) {
        // Signature is valid, execute deployment script
        shell_exec("/var/www/html/deploy.sh");
        http_response_code(200);
        echo "Deployment successful.";
    } else {
        http_response_code(403);
        echo "Invalid signature.";
    }
} else {
    http_response_code(400);
    echo "Signature header not found.";
}
?>