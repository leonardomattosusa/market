@extends('layouts.app')

@push('css_lib')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/css/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/css/datetimepicker/bootstrap-datetimepicker.min.css') }}">
@endpush

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header content-header{{setting('fixed_header')}}">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-6">
                    <h1>Caixa</h1>
                </div>
                <div class="col-6">
                    <div class="d-flex justify-content-end">
                        <div class="d-flex align-items-center mr-2 font-weight-bold">
                            Data:
                        </div>
                        <div style="width: 120px">
                            <form method="GET" action="cash_register">
                                <input type="text" class="form-control datepicker" placeholder="Data do caixa" name="date" value="{{ $date ? date('d-m-Y', strtotime($date)) : date('d-m-Y') }}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-3 col-12">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>R$ {{ number_format($delivered_orders->sum('payment.price'),2,',','.') }}</h3>

                        <p>Total em entregas</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-md-3 col-12">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner text-white">
                        <h3>R$ {{ number_format($collected_orders->sum('payment.price'),2,',','.') }}</h3>

                        <p>Total retirado no local</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-md-3 col-12">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>R$ {{ number_format($mp_orders->sum('payment.price'),2,',','.') }}</h3>

                        <p>Total no Mercado Pago</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-md-3 col-12">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner text-white">
                        <h3>R$ {{ number_format($fr_orders->sum('payment.price'),2,',','.') }}</h3>

                        <p>Total no Saldo FR</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border bg-success">
                        <h4 class="card-title text-white">Pedidos pagos na entrega</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Método</th>
                                <th>Valor</th>
                                <th>Frete</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($delivered_orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{ $order->payment->method }}</td>
                                    <td>R${{ number_format($order->payment->price - $order->delivery_fee,2,',','.') }}</td>
                                    <td>R${{ number_format($order->delivery_fee,2,',','.') }}</td>
                                    <td>
                                        <a href="{{ route('orders.show', $order->id) }}" class="btn btn-sm" tooltip="true" title="Detalhes"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Nenhum pedido disponível.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border bg-success">
                        <h4 class="card-title text-white">Pedidos pagos na retirada</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Valor</th>
                                <th>Frete</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($collected_orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->name }}</td>>
                                    <td>R${{ number_format($order->payment->price - $order->delivery_fee,2,',','.') }}</td>
                                    <td>R${{ number_format($order->delivery_fee,2,',','.') }}</td>
                                    <td>
                                        <a href="{{ route('orders.show', $order->id) }}" class="btn bnt-primary btn-sm" tooltip="true" title="Detalhes"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Nenhum pedido disponível.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border bg-primary">
                        <h4 class="card-title text-white">Pedidos pagos no MercadoPago</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Valor</th>
                                <th>Frete</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($mp_orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>R${{ number_format($order->payment->price - $order->delivery_fee,2,',','.') }}</td>
                                    <td>R${{ number_format($order->delivery_fee,2,',','.') }}</td>
                                    <td>
                                        <a href="{{ route('orders.show', $order->id) }}" class="btn bnt-primary btn-sm" tooltip="true" title="Detalhes"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Nenhum pedido disponível.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border bg-primary">
                        <h4 class="card-title text-white">Pedidos pagos com Saldo FR</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Valor</th>
                                <th>Frete</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($fr_orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>R${{ number_format($order->payment->price - $order->delivery_fee,2,',','.') }}</td>
                                    <td>R${{ number_format($order->delivery_fee,2,',','.') }}</td>
                                    <td>
                                        <a href="{{ route('orders.show', $order->id) }}" class="btn bnt-primary btn-sm" tooltip="true" title="Detalhes"><i class="fas fa-eye"></i></a>
                                        <a href="{{ route('orders.print_payment_receipt', $order->id) }}" class="btn btn-sm" target="_blank" tooltip="true" title="Imprimir comprovante de pagamentos"><i class="fas fa-receipt"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Nenhum pedido disponível.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/js/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js/datetimepicker/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
        $('.datepicker').datetimepicker({
            useCurrent: false,
            format: 'DD-MM-YYYY',
            locale: 'pt-BR',
            maxDate: '{{ date('Y-m-d') }}'
        });

        $('.datepicker').on('dp.change', function(e){
            $("form").submit();
        })
    </script>
@endpush