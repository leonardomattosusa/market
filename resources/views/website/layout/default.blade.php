<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iMarket @yield('title')</title>
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS
    ============================================ -->
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Elegent CSS -->
    <link href="{{ asset('assets/css/elegent.min.css') }}" rel="stylesheet">

    <!-- Plugins CSS -->
    <link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet">

    <!-- Helper CSS -->
    <link href="{{ asset('assets/css/helper.css') }}" rel="stylesheet">

    <!-- Main CSS -->
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">

    <!-- Modernizer JS -->
    <script src="{{ asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" type="text/css" />

    @yield('stylesheets')

    <style>
        #loading {
            display: flex;
            position: fixed;
            top: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background-color: white;
            z-index: 9999;
            justify-content: center;
            align-items: center;
        }

        #loading .fa{
            color: #00891b;
            font-size: 10rem;
        }

        .swal2-styled.swal2-confirm{
            background-color: #00891b !important;
            border-color: #007215 !important;
        }
    </style>
</head>

<body>
<div id="loading">
    <i class="fa fa-spin fa-spinner"></i>
</div>

<header>
    <div class="header-top pt-10 pb-10 pt-lg-10 pb-lg-10 pt-md-10 pb-md-10">
        <div class="container">
            <div class="row">
                <div class="offset-sm-6 col-lg-6 col-md-6 col-sm-6 col-xs-12  text-center text-sm-right">
                    <div class="header-top-menu">
                        <ul id="logged_nav" {{ ($userLoggedIn) ? '' : 'style=display:none' }}>
                            <li><a href="/profile">Minha Conta</a></li>
                            <li><a href="/favorites">Favoritos</a></li>
                        </ul>
                        <ul id="new_user_nav" {{ ($userLoggedIn) ? 'style=display:none' : '' }}>
                            <li><a href="/admin">LOGIN CONVENIADOS</a></li>
                            <li><a href="/login_register">Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-bottom header-bottom-one header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-lg-left text-md-center text-sm-center">
                    <div class="logo mt-15 mb-15">
                        <a href="javascript:void(0)">
                            <img src="{{ asset('images/imarket_logo.png') }}" class="img-fluid" alt="" style="height: 90px; max-width: 200px">
                        </a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="menubar-top d-flex justify-content-md-between align-items-center flex-sm-wrap flex-md-wrap flex-lg-nowrap mt-sm-15">
                        @if($market)
                            <div class="d-flex flex-grow-1">
                                <div style="position:relative; float:left; height: 80px; width: 80px; display: inline">
                                    <div style="background: url({{ $market->photo }}); background-size: 100%; background-position: center; background-repeat: no-repeat; width: 100%; height: 100%"></div>
                                </div>
                                <div class="align-self-center ml-2">
                                    <a href="{{ '/market/'.$market->id }}"><h4 class="market-header-title d-inline">{{ $market->name }}</h4> @if(request()->segment(1) != 'market' || request()->segment(2) != $market->id)<i class="fa fa-arrow-circle-left primary-color"></i>@endif</a>
                                    <small class="d-block">Entrega das {{ date('H:i', strtotime($market->early_delivery_hour)) }} às {{ date('H:i', strtotime($market->late_delivery_hour)) }}</small>
                                    <a href="/change-market" class="d-block market-header-button">Selecionar outro mercado</a>
                                </div>
                            </div>
                        @else
                            <div class="d-flex flex-grow-1">
                                <div class="align-self-center ml-2">
                                    Mostrando mercados para o CEP {{ $cep }}. <div class="d-block"><a href="/markets-list" class="btn-link">Lista de Mercados</a><a href="/" class="ml-2 btn-link">Alterar CEP </a></div>
                                </div>
                            </div>
                        @endif
                        <div class="shopping-cart" id="shopping-cart">
                            <a href="/cart">
                                <div class="cart-icon d-inline-block">
                                    <span class="icon_bag_alt"></span>
                                </div>
                                <div class="cart-info d-inline-block">
                                    <p>
                                        Carrinho<span id="cart-totals"></span>
                                    </p>
                                </div>
                            </a>

                            <div class="cart-floating-box" id="cart-floating-box">
                                @if($userLoggedIn)
                                    <div class="cart-items" id="cart-products-container">

                                    </div>
                                    <div class="cart-calculation">
                                        <div class="calculation-details">
                                            <p class="total">Subtotal <span id="cart-subtotal">R$0,00</span></p>
                                        </div>
                                        <div class="floating-cart-btn text-center">
                                            <a href="/checkout">Checkout</a>
                                            <a href="/cart">Ver Carrinho</a>
                                        </div>
                                    </div>
                                @else
                                    <div class="jumbotron mb-0 py-3">
                                        Você precisa estar logado para adicionar produtos ao seu carrinho. Clique <a href="/login_register" class="btn-link">aqui</a> para efetuar login.
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile-menu d-block d-lg-none"></div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<footer>
    <div class="copyright-section pt-35 pb-35">
        <div class="container">
            <div class="row align-items-md-center align-items-sm-center">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center text-md-left">
                    <div class="copyright-segment">
                        <p>
                            <a href="#">Política de Privacidade</a>
                            <span class="separator">|</span>
                            <a href="#">Condições de uso</a>
                        </p>
                        <p class="copyright-text">&copy; {{ date('Y') }} <a href="/">iMarket</a>. All Rights Reserved</p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
                    <div class="payment-info text-center text-md-right">
                        <p>Pagamento online através da plataforma <img src="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.7.0/mercadopago/logo__large@2x.png" class="img-fluid" style="max-height:40px" alt=""></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade quick-view-modal-container" id="quick-view-modal-container" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header pb-0 pt-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body pt-0" id="product-modal-details-container">

                </div>
            </div>
        </div>
    </div>
    <!--=======  End of copyright section  =======-->
</footer>

<!--=====  End of Footer  ======-->

<!--=============================================
=            Quick view modal         =
=============================================-->

<div style="display:none">
    @include('website.widgets.market-card')
    @include('website.widgets.category-card')
    @include('website.widgets.product-card')
    @include('website.widgets.product-modal-details')
    @include('website.widgets.cart-product')
    @include('website.widgets.favorite-card')
</div>

<a href="#" class="scroll-top"></a>

<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>

<!-- Popper JS -->
<script src="{{ asset('assets/js/popper.min.js') }}"></script>

<!-- Bootstrap JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<!-- Plugins JS -->
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/js-templates.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>

<!-- CART PRODUCTS JS -->
<script>
    var market = {!! json_encode($market) !!};
    var lat = {!! (isset($lat)) ? json_encode($lat) : 0 !!};
    var lng = {!! (isset($lng)) ? json_encode($lng) : 0 !!};
    var category_id = null;
    var actual_page = 1;

    @if($userLoggedIn)
        var favorites = {!! json_encode(collect($favorites)->pluck('product_id')->toArray()) !!};
        var cart_products = {!! json_encode($cart_products) !!}
    @else
        var cart_products = {};
        var favorites = {};
    @endif

    $(".decimal").maskMoney({ thousands : '' });
</script>
<script src="{{ asset('assets/js/custom.js') }}"></script>

@yield('scripts')

@if(session('success'))
    <script type="text/javascript">
        Swal.fire("Sucesso", "{{ session('success') }}", 'success');
    </script>
@endif
@if(session('error'))
    <script type="text/javascript">
        Swal.fire("Erro", "{{ session('error') }}", 'error');
    </script>
@endif
@if(session('warning'))
    <script type="text/javascript">
        Swal.fire("Atenção!", "{{ session('warning') }}", 'warning');
    </script>
@endif
@if(session('info'))
    <script type="text/javascript">
        Swal.fire("Info:", "{{ session('info') }}");
    </script>
@endif

@if($errors->any())
    <?php
    $error_list = "<ul>";

    foreach($errors->all() as $error){
        $error_list .= "<li>".$error.'</li>';
    }

    $error_list .= "</ul>";
    ?>

    <script type="text/javascript">
        Swal.fire("Validation error", "{!! $errors->first() !!}", 'error');
    </script>
@endif

<script src="{{ asset('assets/js/main.js') }}"></script>
</body>

</html>
