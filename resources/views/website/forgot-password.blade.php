@extends('website.layout.default')

@section('content')
    <div class="page-content mb-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6 offset-lg-3 mb-30">
                    <!-- Login Form s-->
                    <form action="javascript:(recoverPassword())">
                        <div class="login-form">
                            <h4 class="login-title">Recuperar Senha</h4>
                            <div class="mb-2">
                                <span class="text-danger" id="recover-password-container"></span>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-12 mb-20">
                                    <label>Endereço de e-mail*</label>
                                    <input class="mb-0" id="email-input" type="email" placeholder="Endereço de email">
                                </div>
                                <div class="col-md-8">
                                    <button class="register-button mt-0" id="recover-button">Recuperar senha</button>
                                </div>
                                <div class="col-md-4 mt-10 mb-20 text-rigth">
                                    <a href="/login_register">Voltar para login</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function recoverPassword(){
            let email = $("#email-input").val();
            $("#recover-button").html('<i class="fa fa-spin fa-spinner"></i>');

            $.ajax({
                url: '/password_recovery',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN' : $("meta[name='csrf-token']").attr('content')
                },
                data: {email : email},
                success: function(response){
                    $("#recover-button").html('Recuperar senha');

                    if(response.success){
                        $("#recover-password-container").removeClass('text-danger').addClass('text-success');
                        $("#recover-password-container").html('Você receberá um e-mail com o link de recuperação de senha em alguns minutos.');
                    }else{
                        console.log(response);

                        $("#recover-password-container").removeClass('text-success').addClass('text-danger');
                        $("#recover-password-container").html('Este email não está cadastrado em nosso sistema.');
                    }
                },
                error: function(response){
                    console.log(response);

                    $("#recover-button").html('Recuperar senha');
                    $("#recover-password-container").removeClass('text-success').addClass('text-danger');
                    $("#recover-password-container").html('Este email não está cadastrado em nosso sistema.');
                }
            });
        }
    </script>
@endsection
