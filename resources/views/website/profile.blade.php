@extends('website.layout.default')

@section('content')
    <div class="my-account-section section position-relative mb-50 fix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-3 col-12">
                            <div class="myaccount-tab-menu nav" role="tablist">
                                <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i> Conta</a>
                                <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Meus Pedidos</a>
                                {{--<a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Métodos de Pagamento</a>--}}
                                <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> Meus endereços</a>

                                <a href="/logout"><i class="fa fa-sign-out"></i> Sair</a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-12">
                            <div class="tab-content" id="myaccountContent">
                                <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Conta</h3>

                                        <div class="welcome mb-4">
                                            <p>Olá, <strong>{{ $user->name }}</strong>, você pode atualizar seus dados básicos através do formulário abaixo:</p>
                                        </div>

                                        <div class="account-details-form">
                                            <form action="javascript:(updateUser())">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <small class="text-danger" id="update-user-error-container"></small>
                                                    </div>
                                                    <div class="col-md-12 col-12 mb-20">
                                                        <label>Nome</label>
                                                        <input class="mb-0" id="name-input" type="text" placeholder="Nome completo" value="{{ $user->name }}">
                                                    </div>
                                                    <div class="col-md-12 mb-20">
                                                        <label>Endereço de email*</label>
                                                        <input class="mb-0" id="email-input" type="email" placeholder="Endereço de e-mail" value="{{ $user->email }}" disabled>
                                                    </div>
                                                    <div class="col-md-6 col-12 mb-20">
                                                        <label>Tipo de pessoa</label>
                                                        <select class="mb-0" id="legal-type-select" onchange="changeUserType()" disabled>
                                                            <option value="1" {{ ($user->legal_type == 1) ? 'selected' : '' }}>Pessoa física</option>
                                                            <option value="2" {{ ($user->legal_type == 2) ? 'selected' : '' }}>Pessoa jurídica</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 col-12 mb-20">
                                                        <label id="document-label">CPF</label>
                                                        <input class="mb-0" id="document-input" type="text" placeholder="Documento de identificação" value="{{ $user->document }}" disabled>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="text-center">
                                                            <button class="register-button mt-0" id="update-user-button">Atualizar Dados</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="orders" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Meus pedidos</h3>

                                        <div class="myaccount-table table-responsive text-center">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>Nome</th>
                                                    <th>Data</th>
                                                    <th>Status</th>
                                                    <th>Total</th>
                                                    <th>Ações</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                    @forelse($orders as $order)
                                                        <tr>
                                                            <td>Pedido #{{ $order->id }}</td>
                                                            <td>{{ date('d/m/Y', strtotime($order->created_at)) }}</td>
                                                            <td>{{ $order->order_status->status }}</td>
                                                            <td>R${{ number_format($order->payment->price, 2, ',', '.') }}</td>
                                                            <td><a href="/order/{{ $order->id }}" class="btn">Ver</a></td>
                                                        </tr>
                                                    @empty
                                                        <tr>
                                                            <td colspan="6">Nenhum pedido realizado ainda.</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Meus endereços</h3>

                                        <div id="addresses_container">
                                            <div class="row" id="addresses_div">
                                                @forelse($addresses as $address)
                                                    <div class="col-lg-4 col-md-6 col-12 mb-4" id="address_{{ $address->id }}">
                                                        <address>
                                                            <p><strong>{{ $address->description }}</strong></p>
                                                            <p>{{ $address->street }}, {{ $address->number }}<br>
                                                                {{ $address->district }}, {{ $address->complement }}</p>
                                                            <p>{{ $address->city }}/{{ $address->state_initials }}</p>
                                                        </address>

                                                        <a href="#!" onclick="getAddressModal({{ $address->id }})" class="btn d-inline-block edit-address-btn"><i class="fa fa-edit"></i>Editar</a>
                                                    </div>
                                                @empty
                                                    <div class="col">
                                                        <p>Nenhum endereço cadastrado ainda. Você pode cadastrar novos endereços ao realizar uma compra.</p>
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" role="dialog" tabindex="-1" id="address_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="javascript:(updateAddress())" class="checkout-form" id="address_form">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body">
                        <div class="row" id="new_address">
                            <div class="col-12 mb-10">
                                <label>Identificação do endereço*</label>
                                <input type="text" name="description" id="description" placeholder="Ex: Casa, Trabalho, etc" required>
                            </div>

                            <div class="col-12 mb-10">
                                <label>CEP*</label>
                                <input type="text" class="cep_mask" name="zipcode" id="cep" placeholder="00000-000" required>
                            </div>

                            <div class="col-md-8 col-12 mb-10">
                                <label>Logradouro*</label>
                                <input type="text" placeholder="Logradouro" name="street" id="logradouro" required>
                            </div>

                            <div class="col-md-4 col-12 mb-10">
                                <label>Número*</label>
                                <input type="text" placeholder="Número" name="number" id="numero" required>
                            </div>

                            <div class="col-md-6 col-12">
                                <label>Bairro*</label>
                                <input type="text" placeholder="Bairro" name="district" id="bairro" required>
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Complemento</label>
                                <input type="text" placeholder="Complemento" name="complement" id="complemento">
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Estado*</label>
                                <select name="state_initials" id="uf">
                                    <option value="">Selecione</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Cidade*</label>
                                <input type="text" placeholder="Cidade" name="city" id="cidade" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12">
                            <button type="submit" class="place-order float-right mt-2" id="updateAddressButton">Atualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var addresses = {!! json_encode($addresses) !!}

        function updateUser(){
            let result = getUserData()
            let button_text = $("#update-user-button").html();

            if(result.status == 'error'){
                showUpdateUserError(result.message);
            }else{
                let user_data = result.user;

                $.ajax({
                    url: "/update_user",
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    data: user_data,
                    beforeSend: function(response){
                        $("#update-user-error-container").empty();
                        $("#update-user-button").html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success){
                            toastr.success('Dados atualizados com sucesso');
                        }else{
                            showUpdateUserError(response.message);
                        }
                    },
                    error: function(response){
                        console.log(response);
                        if(response.success){
                            toastr.success('Dados atualizados com sucesso');
                        }else{
                            response = response.responseJSON;

                            showValidationUserErrors(response.errors);
                        }
                    },
                    complete: function(){
                        $("#update-user-button").html(button_text);
                    }
                })
            }
        }

        function showUpdateUserError(){
            $("#update-user-error-container").text(message);
        }

        function showValidationUserErrors(errors){
            let message = '';

            $.each(errors, function(index, error){
                message += error[0] + '<br>';
            });

            $("#update-user-error-container").html(message);
        }

        function getUserData(){
            let user = new Object();

            user.name = $("#name-input").val();
            user.legal_type = $("#legal-type-select").val();
            user.document = $("#document-input").val().replace(/\D/g,'');

            if((user.legal_type == 1 && user.document.length != 11) || (user.legal_type == 2 && user.document.length != 14)){
                return {'status' : 'error', 'message' : 'Documento inválido.'};
            }

            return {'status' : 'success', 'user' : user};
        }

        function getAddressModal(address_id){
            let address = addresses.find(address => address.id === address_id);

            $("#description").val(address.description);
            $("#cep").val(address.zipcode);
            $("#logradouro").val(address.street);
            $("#numero").val(address.number);
            $("#bairro").val(address.district);
            $("#complemento").val(address.complement);
            $("#uf").val(address.state_initials);
            $("#cidade").val(address.city);
            $("#address_form").attr('action', 'javascript:(updateAddress('+address.id+'))');

            $("#address_modal").modal('show');
        }

        $('.cep_mask').on('focusout', function(){
            var cep = $(this).val()
            var script = document.createElement('script')
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=callback_cep'
            document.body.appendChild(script)
        })

        function callback_cep(conteudo) {
            if (!("erro" in conteudo)) {
                //Atualiza os campos com os valores.
                if(conteudo.logradouro){
                    document.getElementById('logradouro').value=(conteudo.logradouro);
                }
                if(conteudo.bairro){
                    document.getElementById('bairro').value=(conteudo.bairro);
                }
                if(conteudo.localidade){
                    document.getElementById('cidade').value=(conteudo.localidade);
                }
                if(conteudo.uf){
                    document.getElementById('uf').value=(conteudo.uf);
                }
            }
        }

        function updateAddress(address_id){
            let address = addresses.find(address => address.id === address_id);

            $.ajax({
                url: '/update_address/' + address_id,
                headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                data: $('#address_form').serialize(),
                method: 'POST',
                beforeSend(){
                    setButtonState('#updateAddressButton', 'inactive')
                },
                success(data){
                    setButtonState('#updateAddressButton', 'active')

                    if(data.success == true){
                        $("#address_modal").modal('hide');
                        $("#address_"+address_id).html('<div class="text-center"><i class="fa fa-spin fa-spinner" style="font-size:1.4"></i></div>');
                        $('#addresses_container').load('/profile #addresses_div');

                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
                error(data){
                    setButtonState('#updateAddressButton', 'active')
                }
            })

            return false
        }

        function setButtonState(selector, state){
            if(state == 'inactive'){
                $(selector).html('<i class="fa fa-spin fa-spinner"></i>')
                $(selector).prop('disabled', true)
            }
            if(state == 'active'){
                $(selector).html('Atualizar')
                $(selector).prop('disabled', false)
            }
        }
    </script>
@endsection
