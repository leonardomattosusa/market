@extends('website.layout.default')

@section('title', '- Listagem de Mercados')

@section('stylesheets')
    <style>
        .market-button{
            width: 100%;
            display: flex;
            justify-content: center;
        }

        .market-button a button{
            position: relative;
            background: none;
            border: none;
            background-color: #00891b;
            color: #ffffff;
            width: 150px;
            height: 40px;
            border-radius: 5px;
        }

        .market-button a button:hover{
            background-color: #007215;
        }
    </style>
@endsection

@section('content')
    <div class="slider shop-page-container mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-sm-35 mb-xs-35">
                    <div class="shop-header mb-35">
                        <div class="row">
                            {{--<div class="col-lg-4 col-md-4 col-sm-12 d-flex align-items-center">
                                <div class="sort-by-dropdown d-flex align-items-center mb-xs-10">
                                    <p class="mr-10">Ordenar por: </p>
                                    <select name="sort-by" id="sort-by" class="nice-select">
                                        <option value="0">Popularidade</option>
                                        <option value="0">Preço: Menor ao Maior</option>
                                        <option value="0">Preço: Maior ao Menor</option>
                                    </select>
                                </div>
                            </div>--}}
                            <div class="col-lg-12 col-md-12 col-sm-12 d-flex flex-column flex-sm-row justify-content-between align-items-left align-items-sm-center">
                                <div class="sort-by-dropdown d-flex align-items-center mb-xs-10">
                                    <div class="header-advance-search">
                                        <form action="javascript:(searchMarkets(this))">
                                            <input type="text" class="search-market" placeholder="Procurar por um mercado" style="min-width: 220px" minlength="3">
                                            <button class="search-market-button"><span class="fa fa-check"></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-slider-wrapper bg-white shadow">
                        <div class="tab-slider-container" id="markets-list">

                        </div>
                    </div>

                    {{--<div class="pagination-container">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pagination-content text-center">
                                        <ul>
                                            <li><a class="active" href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        getMarkets();
    </script>
@endsection
