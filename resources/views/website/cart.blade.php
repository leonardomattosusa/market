@extends('website.layout.default')

@section('title', '- Carrinho')

@section('stylesheets')
    <style>
        .btn-cart-qty{
            display: inline;
            background: none;
            border: none;
            font-size: 0.8rem;
            background-color: #00891b;
            padding: 5px 7px;
            border-radius: 50%;
        }

        .btn-cart-qty:hover{
            background-color: #5D8802;
        }
    </style>
@endsection

@section('content')
    <div class="page-section section mb-50">
        <div class="container">
            @if($market && $market->free_delivery_above > 0)
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success" id="free_delivery_alert">
                        Este mercado oferece frete gratuíto para compras acima de R$ {{ number_format($market->free_delivery_above, 2, ',', '.') }}.
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-12">
                    @php($subtotal = 0)
                    <form action="javascript:(createOrder())">
                        <div class="cart-table table-responsive mb-40">
                            @if($userLoggedIn)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="pro-thumbnail">Foto</th>
                                    <th class="pro-title">Produto</th>
                                    <th class="pro-price">Preço</th>
                                    <th class="pro-quantity">Qtd</th>
                                    <th class="pro-subtotal">Total</th>
                                    <th class="pro-remove">Remover</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($cart_products as $p)
                                        @php($subtotal += ($p->quantity * $p->product->price))
                                        <tr class="cart-item-{{ $p->id }}">
                                            <td class="pro-thumbnail"><a href="#"><img src="{{ $p->product->has_media ? $p->product->media[0]->url : 'https://imarket.digital/images/image_default.png' }}" class="img-fluid" alt="{{ $p->product->name }}" style="width:50px"></a></td>
                                            <td class="pro-title"><a href="#">{{ $p->product->name }}</a></td>
                                            <td class="pro-price"><span>R${{ number_format($p->product->price, 2, ',', '.') }}</span></td>
                                            <td class="pro-quantity cart-buttons">
                                                @if($p->product->weight_based == 1)
                                                    <input type="text" class="form-control d-inline" style="width: 80px" value="{{ ($p->quantity * 1000) }}"> <span style="font-size: 1.2rem">g</span>
                                                @else
                                                    <div class="pro-qty">
                                                        <input type="text" value="{{ $p->quantity }}">
                                                    </div>
                                                @endif
                                                <div class="btn-cart-qty">
                                                    <a href="#!" onclick="addProductToCart(this)" product_id="{{ $p->product_id }}" market_id="{{ $p->product->market_id }}"><i class="fa fa-check text-white"></i></a>
                                                </div>
                                            </td>
                                            <td class="pro-subtotal"><span>R${{ number_format($p->quantity * $p->product->price, 2, ',', '.') }}</span></td>
                                            <td class="pro-remove">
                                                <a href="#" cart_id="{{ $p->id }}" onclick="removeProductFromCart(this, '{!! '.cart-item-'.$p->id !!}')"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="7">Nenhum produto adicionar ao carrinho ainda.</td></tr>
                                    @endforelse
                                </tbody>
                            </table>
                            @else
                                <div class="m-2 jumbotron py-4">
                                    <h4>Você precisa estar logado para adicionar produtos ao seu carrinho. Clique <a href="/login-register'" class="btn-link">aqui</a> para efetuar login.</h4>
                                </div>
                            @endif
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-lg-6 col-12">
                            {{--<div class="discount-coupon">
                                <h4>Utilizar cupom de desconto</h4>
                                <form action="#">
                                    <div class="row">
                                        <div class="col-md-6 col-12 mb-25">
                                            <input type="text" placeholder="Código do cupom">
                                        </div>
                                        <div class="col-md-6 col-12 mb-25">
                                            <input type="submit" value="Aplicar">
                                        </div>
                                    </div>
                                </form>
                            </div>--}}
                        </div>

                        <div class="col-lg-6 col-12 d-flex">
                            <div class="cart-summary">
                                @php($total = $subtotal)
                                <div class="cart-summary-wrap">
                                    <h4>Resumo do Carrinho</h4>
                                    <p>Subtotal <span>R${{ number_format($subtotal, 2, ',', '.') }}</span></p>
                                    @if($market)
                                        @if($market->free_delivery_above > $total)
                                            <p id="shipping-amount">Frete <span>R${{ number_format($market->delivery_fee, 2, ',', '.') }}</span></p>
                                            @php($total += $market->delivery_fee)
                                        @else
                                            <p id="shipping-amount">Frete <span>Gratuíto</span></p>
                                        @endif
                                        <h2>Total <span>R${{ number_format($total, 2, ',', '.') }}</span></h2>
                                    @else
                                        <h2>Total <span>R${{ number_format($total, 2, ',', '.') }}</span></h2>
                                    @endif
                                </div>
                                <div class="cart-summary-button">
                                    <a href="/checkout" class="checkout-btn">Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function createOrder(){

        }

        function calculateShipping(){
            var zipcode = $("#zipcode").val().replace(/\D/g,'');
            $("#zipcode-error-container").empty();

            if(zipcode.length == 8){
                let address = new Object();

                $.getJSON('https://viacep.com.br/ws/' + zipcode + '/json/', function (data) {
                    address.zipcode = data.cep;
                    address.district = data.bairro;
                    address.number = data.numero;
                    address.street = data.logradouro;
                    address.city = data.localidade;
                    address.state = data.uf;
                });
            }else{
                $("#zipcode-error-container").text('O CEP deve conter 8 dígitos.');
            }
        }
    </script>
@endsection
