<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>iMarket</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- FontAwesome CSS -->
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #F5F5F5;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .content{
            text-align: center;
        }

        .button {
            position: relative;
            -ms-flex-preferred-size: 50%;
            flex-basis: 50%;
            background-color: #ffffff;
            height: 50px;
            border: 1px solid #e4e4e4;
            padding-right: 55px;
            border-radius: 50px;
        }

        .button input{
            border: none;
            width: 95%;
            margin-top: 12px;
            margin-left: 15px;
            color: #a4a4a4;
        }

        .button button{
            position: absolute;
            right: 5px;
            top: 5px;
            background: none;
            border: none;
            background-color: #00891b;
            color: #ffffff;
            width: 40px;
            height: 40px;
            border-radius: 50%;
        }

        .button button:hover{
            background-color: #007215;
        }
    </style>
</head>
<body>

<div class="container my-0 py-0">
    <div class="text-right">
        <a href="/admin" class="btn btn-link">LOGIN CONVENIADOS</a>
    </div>
</div>

<div class="flex-center position-ref full-height">
    <div class="content" style="margin-top: -25px">
        <img src="{{ asset('images/imarket_logo.png') }}" class="img-fluid mb-4" style="max-height:120px" alt="iMarket">
        <form action="/save-localization" method="POST">
            @csrf
            <div class="button">
                <input type="text" class="cep_mask" placeholder="Digite o seu CEP" id="cep" name="zipcode" required>
                <button><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
</div>

<!-- jQuery JS -->
<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>

<!-- Popper JS -->
<script src="{{ asset('assets/js/popper.min.js') }}"></script>

<!-- Bootstrap JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<!-- Plugins JS -->
<script src="{{ asset('assets/js/plugins.js') }}"></script>

<!-- Google Maps -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAn5r2AqwLe4U5vrZMjtQg90fJnNtK0Zro"></script>
<script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<script>
    $(".cep_mask").mask('00000-000');

    var latitude, longitude;

    function getLocation(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        }else{
            alert("O seu navegador não suporta Geolocalização.");
        }
    }

    function showPosition(position){
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        $.ajax({
            url: 'https://maps.google.com/maps/api/geocode/json?address='+latitude+','+longitude+'&sensor=false&key=AIzaSyBakbheodl_aPGBYGxhsnnWVFX5HuNM174',
            method: 'GET',
            success: function(response){
                $.each(response.results[0].address_components, function(index, component) {
                    let cep = component.long_name.replace(/\D+/g, '');

                    if(cep.length == 8){
                        $("#cep").val(component.long_name);
                    }
                });
            },
            error: function(response){
               console.log(response);
            }
        });
    }

    function showError(error){
        switch(error.code){
            case error.PERMISSION_DENIED:
                alert("Usuário rejeitou a solicitação de Geolocalização.")
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Localização indisponível.")
                break;
            case error.TIMEOUT:
                alert("A requisição expirou.")
                break;
            case error.UNKNOWN_ERROR:
                alert("Algum erro desconhecido aconteceu.")
                break;
        }
    }

    getLocation();
</script>
</body>
</html>
