@extends('website.layout.default')

@section('stylesheets')
    <style>
        button.mercadopago-button {
            font-size: 1.1rem;
        }
    </style>
@endsection

@section('content')
    <div class="my-account-section section position-relative mb-50 fix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Pedido #{{ $order->id }}
                            <span class="float-right"> <strong>Criado em:</strong> {{ date('d/m/Y', strtotime($order->created_at)) }}</span>
                        </div>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-sm-6">
                                    @if($preference && $order->order_status_id == 1 && $order->productOrders[0]->product->market->mercado_pago_method == 1 && $order->payment_hash)
                                        <div class="d-flex align-items-center mt-2">
                                            <i class="fa fa-exclamation-circle text-info mr-10" style="font-size: 60px"></i> <h4 class="d-inline">Pagamento pendente através do MercadoPago</h4>
                                        </div>
                                        <div class="mt-2">
                                            <div class="float-right">
                                                <form method="GET" action="/order/{{ $order->id }}">
                                                    <script src="https://www.mercadopago.com.br/integrations/v1/web-payment-checkout.js" data-preference-id="{{ $preference->id }}" data-button-label="Pagar pedido"></script>
                                                </form>
                                            </div>
                                        </div>
                                    @else
                                        <div class="d-flex align-items-center mt-2">
                                            <i class="fa fa-check-circle text-success mr-10" style="font-size: 60px"></i> <h4 class="d-inline">{{ $order->order_status->status }}</h4>
                                        </div>
                                        <p>Você pode acompanhar o status de seu pedido através desta página. Basta recarregar a página para acompanhar as mudanças de status de seu pedido.</p>
                                    @endif
                                </div>

                                <div class="col-sm-6">
                                    <h6 class="mb-3">Endereço de Entrega:</h6>
                                    <div>
                                        <strong>{{ $order->delivery_address->description }}</strong>
                                    </div>
                                    <div>{{ $order->delivery_address->street }}, {{ $order->delivery_address->number }}</div>
                                    <div>{{ $order->delivery_address->district }}</div>
                                    @if($order->delivery_address->complement)
                                        <div>{{ $order->delivery_address->complement }}</div>
                                    @endif
                                    <div>{{ $order->delivery_address->city }}/{{ $order->delivery_address->state_initials }}</div>
                                </div>
                                @if($order->payment->cashback_amount && $order->payment->cashback_amount > 0)
                                    <div class="col-12">
                                        <span class="badge badge-success float-right" style="font-size: 0.85rem; background-color:#F47A35">Cashback FR: R${{ number_format($order->payment->cashback_amount,2,',','.') }}</span>
                                    </div>
                                @endif
                            </div>

                            <div class="table-responsive-sm">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th class="center">Qtd</th>
                                        <th>Produto</th>
                                        <th class="right" style="width: 150px">Valor unitário</th>
                                        <th class="right" style="width: 150px">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($subtotal = 0)
                                    @forelse($order->product_orders as $p)
                                        @php($subtotal += ($p->product->price * $p->quantity))
                                        <tr>
                                            <td class="center">{{ ($p->product->weight_based) ? ($p->quantity * 1000).'g' : $p->quantity.'un' }}</td>
                                            <td class="center">{{ $p->product->name }}</td>
                                            <td class="right">R${{ number_format($p->product->price, 2, ',', '.') }}</td>
                                            <td class="right">R${{ number_format($p->product->price * $p->quantity, 2, ',', '.') }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">Nenhum produto registrado neste pedido.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-5">

                                </div>

                                <div class="col-lg-4 col-sm-5 ml-auto">
                                    <table class="table table-clear">
                                        <tbody>
                                        <tr>
                                            <td class="left">
                                                <strong>Subtotal</strong>
                                            </td>
                                            <td class="right">R${{ number_format($subtotal, 2, ',', '.') }}</td>
                                        </tr>
                                        {{--<tr>
                                            <td class="left">
                                                <strong>Discount (20%)</strong>
                                            </td>
                                            <td class="right">$1,699,40</td>
                                        </tr>--}}
                                        {{--<tr>
                                            <td class="left">
                                                <strong>VAT (10%)</strong>
                                            </td>
                                            <td class="right">$679,76</td>
                                        </tr>--}}
                                        <tr>
                                            <td class="left">
                                                <strong>Taxa de Entrega</strong>
                                            </td>
                                            <td class="right">R${{ number_format($order->product_orders[0]->product->market->delivery_fee, 2, ',', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Total</strong>
                                            </td>
                                            <td class="right">
                                                <strong>R${{ number_format($order->payment->price, 2, ',', '.') }}</strong>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection