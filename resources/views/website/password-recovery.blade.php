@extends('website.layout.default')

@section('content')
    <div class="page-content mb-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6 offset-lg-3 mb-30">
                    <!-- Login Form s-->
                    <form action="javascript:(recoverPassword())">
                        <div class="login-form">
                            <h4 class="login-title">Redefinir Senha</h4>
                            <div class="mb-2">
                                <span class="text-danger" id="recover-password-container"></span>
                            </div>

                            <div class="row">
                                <input type="hidden" id="token-input" value="{{ $token }}">
                                <input type="hidden" id="email-input" value="{{ $email }}">
                                <div class="col-md-12 col-12 mb-20">
                                    <label>Senha*</label>
                                    <input class="mb-0" id="password-input" type="password" placeholder="Digite sua nova senha" readonly>
                                </div>
                                <div class="col-md-12 col-12 mb-20">
                                    <label>Confirmação de Senha*</label>
                                    <input class="mb-0" id="password-confirmation-input" type="password" placeholder="Confirme sua nova senha" readonly>
                                </div>
                                <div class="col-md-8">
                                    <button class="register-button mt-0" id="recover-button">Redefinir senha</button>
                                </div>
                                <div class="col-md-4 mt-10 mb-20 text-rigth">
                                    <a href="/login_register">Voltar para login</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('input').removeAttr('readonly');

        function recoverPassword(){
            let password = $("#password-input").val();
            let password_confirmation = $("#password-confirmation-input").val();
            let token = $("#token-input").val();
            let email = $("#email-input").val();

            if(password != password_confirmation){
                $("#recover-password-container").removeClass('text-success').addClass('text-danger');
                $("#recover-password-container").html('A senha e a confirmação da senha devem ser iguais.');
            }

            $.ajax({
                url: '/password/reset/' + token,
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                },
                data: {email : email, password : password},
                success: function(response){
                    if(response.success){
                        $("#recover-password-container").removeClass('text-danger').addClass('text-success');
                        $("#recover-password-container").html('Senha redefinida com sucesso.');

                        window.location.href = '/login_register';
                    }else{
                        console.log(response);

                        $("#recover-password-container").removeClass('text-success').addClass('text-danger');
                        $("#recover-password-container").html('Aconteceu algum erro inesperado ao redefinir sua senha. Tente novamente em alguns minutos.');
                    }
                },
                error: function(response){
                    console.log(response);

                    $("#recover-password-container").removeClass('text-success').addClass('text-danger');
                    $("#recover-password-container").html('Aconteceu algum erro inesperado ao redefinir sua senha. Tente novamente em alguns minutos.');
                }
            })
        }
    </script>
@endsection
