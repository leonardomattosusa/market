@extends('website.layout.default')

@section('content')
    <div class="page-content mb-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6 mb-30">
                    <!-- Login Form s-->
                    <form action="javascript:(loginUser())">
                        <div class="login-form">
                            <h4 class="login-title">Login</h4>
                            <span class="text-danger" id="login-error-container"></span>

                            <div class="row">
                                <div class="col-md-12 col-12 mb-20">
                                    <label>Endereço de e-mail*</label>
                                    <input class="mb-0" id="login-email-input" type="email" placeholder="Endereço de email">
                                </div>
                                <div class="col-12 mb-20">
                                    <label>Senha</label>
                                    <input class="mb-0" id="login-password-input" type="password" placeholder="Senha">
                                </div>
                                <div class="col-md-8">
                                    <button class="register-button mt-0" id="login-button">Login</button>
                                </div>
                                <div class="col-md-4 mt-10 mb-20 text-rigth">
                                    <a href="/forgot_password">Esqueceu a senha?</a>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6">
                    <form action="javascript:(registerUser())">
                        <div class="login-form">
                            <h4 class="login-title">Cadastro</h4>
                            <span class="text-danger" id="register-error-container"></span>

                            <div class="row">
                                <div class="col-md-12 col-12 mb-20">
                                    <label>Nome</label>
                                    <input class="mb-0" id="name-input" type="text" placeholder="Nome completo" readonly>
                                </div>
                                <div class="col-md-12 mb-20">
                                    <label>Endereço de email*</label>
                                    <input class="mb-0" id="email-input" type="email" placeholder="Endereço de e-mail" readonly>
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                    <label>Tipo de pessoa</label>
                                    <select class="mb-0" id="legal-type-select" onchange="changeUserType()" readonly>
                                        <option value="1">Pessoa física</option>
                                        <option value="2">Pessoa jurídica</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                    <label id="document-label">CPF</label>
                                    <input class="mb-0" id="document-input" type="text" placeholder="Documento de identificação" readonly>
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                    <label id="document-label">Telefone</label>
                                    <input class="mb-0" type="text" name="phone" id="phone-input" placeholder="Número de telefone" readonly>
                                </div>
                                <div class="col-md-6 mb-20">
                                    <label>Senha</label>
                                    <input class="mb-0" id="password-input" type="password" placeholder="Senha" readonly>
                                </div>
                                <div class="col-md-6 mb-20">
                                    <label>Confirmar Senha</label>
                                    <input class="mb-0" id="confirm-password-input" type="password" placeholder="Confirme sua senha" readonly>
                                </div>
                                <div class="col-12">
                                    <div class="text-center">
                                        <button class="register-button mt-0" id="register-button">Concluir Cadastro</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("[readonly]").removeAttr('readonly');
        })

        function loginUser(){
            let result = getLoginData();

            if(result.status == 'error'){
                showRegisterError(result.message);
            }else{
                let credentials = result.credentials;

                $.ajax({
                    url: "/login",
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    data: credentials,
                    beforeSend: function(response){
                        $("#login-error-container").empty();
                        $("#login-button").html('<i class="fa fa-spin fa-spinner"></i>');
                        $("#login-button").attr('disabled', true);
                    },
                    success: function(response){
                        if(response.success){
                            window.location.reload();
                        }else{
                            showLoginError(response.message);
                        }
                    },
                    error: function(response){
                        if(response.success){
                            window.location.reload();
                        }else{
                            response = response.responseJSON;
                            console.log(response);

                            showValidationLoginErrors(response.errors);
                        }
                    }
                })
            }
        }

        function registerUser(){
            let result = getUserData();

            if(result.status == 'error'){
                showRegisterError(result.message);
            }else{
                let user_data = result.user;

                $.ajax({
                    url: "/register",
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    data: user_data,
                    beforeSend: function(response){
                        $("#register-error-container").empty();
                        $("#register-button").html('<i class="fa fa-spin fa-spinner"></i>');
                        $("#register-button").attr('disabled', true);
                    },
                    success: function(response){
                        if(response.success){
                            window.location.href = '/fidelidade-remunerada';
                        }else{
                            showRegisterError(response.message);
                        }
                    },
                    error: function(response){
                        if(response.success){
                            window.location.href = '/fidelidade-remunerada';
                        }else{
                            console.log(response);
                            response = response.responseJSON;

                            showValidationRegisterErrors(response.errors);
                        }
                    }
                })
            }
        }

        function showLoginError(message){
            $("#login-error-container").text(message);

            $("#login-button").html('Login');
            $("#login-button").removeAttr('disabled');
        }

        function showValidationLoginErrors(errors){
            let message = '';

            $.each(errors, function(index, error){
                message += error[0] + '<br>';
            });

            $("#login-error-container").html(message);
            $("#login-button").html('Login');
            $("#login-button").removeAttr('disabled');
        }

        function showRegisterError(message){
            $("#register-error-container").text(message);

            $("#register-button").html('Concluir Cadastro');
            $("#register-button").removeAttr('disabled');
        }

        function showValidationRegisterErrors(errors){
            let message = '';

            $.each(errors, function(index, error){
                message += error[0] + '<br>';
            });

            $("#register-error-container").html(message);
            $("#register-button").html('Concluir Cadastro');
            $("#register-button").removeAttr('disabled');
        }

        function getLoginData(){
            let credentials = new Object();

            credentials.email = $("#login-email-input").val();
            credentials.password = $("#login-password-input").val();

            return {'status' : 'success', 'credentials' : credentials};
        }

        function getUserData(){
            let user = new Object();

            user.name = $("#name-input").val();
            user.email = $("#email-input").val();
            user.legal_type = $("#legal-type-select").val();
            user.document = $("#document-input").val().replace(/\D/g,'');
            user.phone = $("#phone-input").val();
            user.password = $("#password-input").val();
            user.confirm_password = $("#confirm-password-input").val();

            if((user.legal_type == 1 && user.document.length != 11) || (user.legal_type == 2 && user.document.length != 14)){
                return {'status' : 'error', 'message' : 'Documento inválido.'};
            }

            if(user.password != user.confirm_password){
                return {'status' : 'error', 'message' : 'A senha e a confirmação de senha devem ser iguais.'};
            }

            return {'status' : 'success', 'user' : user};
        }
    </script>
@endsection
