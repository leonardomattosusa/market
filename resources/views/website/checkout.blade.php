@extends('website.layout.default')

@section('title', '- Checkout')

@section('stylesheets')
    <style>
        input[disabled], select[disabled]{
            background-color: #DDDDDD;
        }

        .new-address-button{
            margin-top: 10px;
            width: 250px;
            border-radius: 50px;
            height: 36px;
            border: none;
            line-height: 24px;
            padding: 6px 20px;
            float: left;
            font-weight: 400;
            text-transform: uppercase;
            color: #ffffff;
            background-color: #00891b;
        }

        .new-address-button:hover {
            background-color: #007215;
        }

        .checkout-title{
            text-transform: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="page-section section mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="javascript:(createOrder())" class="checkout-form">
                        <div class="row row-40" id="checkout-div">
                            <div class="col-lg-7 mb-10">
                                <div id="billing-form" class="mb-40">
                                    <h4 class="checkout-title">Meus dados</h4>
                                    <div class="row mb-20">
                                        <div class="col-12">
                                            <small class="text-danger" id="update-user-error-container"></small>
                                        </div>
                                        <div class="col-md-12 col-12 mb-20">
                                            <label>Nome</label>
                                            <input class="mb-0" id="name-input" type="text" placeholder="Nome completo" value="{{ $user->name }}" disabled>
                                        </div>
                                        <div class="col-md-12 mb-20">
                                            <label>Endereço de email*</label>
                                            <input class="mb-0" id="email-input" type="email" placeholder="Endereço de e-mail" value="{{ $user->email }}" disabled>
                                        </div>
                                        <div class="col-md-6 col-12 mb-20">
                                            <label>Tipo de pessoa</label>
                                            <select class="mb-0" id="legal-type-select" onchange="changeUserType()" disabled>
                                                <option value="1" {{ ($user->legal_type == 1) ? 'selected' : '' }}>Pessoa física</option>
                                                <option value="2" {{ ($user->legal_type == 2) ? 'selected' : '' }}>Pessoa jurídica</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12 mb-20">
                                            <label id="document-label">CPF</label>
                                            <input class="mb-0" id="document-input" type="text" placeholder="Documento de identificação" value="{{ $user->document }}" disabled>
                                        </div>
                                    </div>
                                    <div id="addresses_container">
                                        <div id="addresses_div">
                                            <h4 class="checkout-title">Endereço de entrega</h4>
                                            @forelse($user_addresses as $add)
                                                <div class="row mx-1">
                                                    <div class="col-12 pt-3 pb-2 mb-2" style="background-color: #ddd">
                                                        <div class="single-method">
                                                            <input type="radio" id="shipping_address_{{ $add->id }}" class="shipping_address" name="shipping_address" value="{{ $add->id }}" {{ $add->is_default == 1 ? 'checked' : '' }}>
                                                            <label for="shipping_address_{{ $add->id }}"><h4 class="font-weight-bold">{{ $add->description ? $add->description : "Sem nome" }}</h4></label>
                                                            <p data-method="{{ $add->id }}" style="display: {{ $add->is_default == 1 ? 'block' : 'none' }}">{{ $add->address }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-center">Nenhum endereço cadastrado</p>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <button type="button" class="new-address-button" data-toggle="modal" data-target="#address_modal">Cadastrar novo endereço</button>
                                        </div>
                                    </div>

                                    <h4 class="checkout-title mt-4">Quando você deseja receber/coletar este pedido?</h4>
                                    <div class="row">
                                        <div class="col-6 offset-3">
                                            <select name="schedule" id="schedule" onchange="scheduleOption(this)">
                                                <option value="no">O mais breve possível</option>
                                                <option value="yes">Agendar horário</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" id="schedule_div">
                                        <div class="col-4 offset-2">
                                            <select name="schedule_day" id="schedule_day" onchange="updateMinutes(this)">
                                                @if(date('H:i') >= $market->early_delivery_hour && date('H:i', strtotime('+75 minutes')) <= $market->late_delivery_hour)
                                                <option value="today">Hoje</option>
                                                @endif
                                                <option value="tomorrow">Amanhã</option>
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <select name="schedule_hour" id="schedule_hour">
                                                @php
                                                    $hours = 0;
                                                    $minutes = 0;
                                                @endphp

                                                @for($i = 47; $i > 0; $i--)
                                                    @php
                                                        $minutes += 30;

                                                        if($minutes == 60){
                                                            $hours++;
                                                            $minutes = 0;
                                                        }
                                                        $hour_minute = str_pad($hours,2, 0,STR_PAD_LEFT) . ':' . str_pad($minutes,2, 0,STR_PAD_LEFT);
                                                        $hour_minute_2 = date('H:i', strtotime(str_pad($hours,2, 0,STR_PAD_LEFT) . ':' . str_pad($minutes,2, 0,STR_PAD_LEFT).' +30 minutes'));
                                                        $hour_minute_2 = ($hour_minute_2 == '00:00') ? '24:00' : $hour_minute_2;
                                                    @endphp

                                                    @if(date('H:i', strtotime($hour_minute)) >= date('H:i', strtotime($market->early_delivery_hour)) && $hour_minute_2 <= date('H:i', strtotime($market->late_delivery_hour)))
                                                        @if(date('H:i') >= $market->early_delivery_hour && date('H:i', strtotime('+75 minutes')) <= $market->late_delivery_hour)
                                                            <option value="{{ $hour_minute }}" {{ date('H:i', strtotime('+45 minutes')) > date('H:i', strtotime($hour_minute)) ? 'disabled' : '' }}>{{ $hour_minute }} - {{ $hour_minute_2 }}</option>
                                                        @else
                                                            <option value="{{ $hour_minute }}">{{ $hour_minute }} - {{ $hour_minute_2 }}</option>
                                                        @endif
                                                    @endif
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-2">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5">
                                <div class="row">
                                    <div class="col-12 mb-60">
                                        <h4 class="checkout-title">Carrinho</h4>
                                        <div class="checkout-cart-total">
                                            <h4>Produto <span>Total</span></h4>
                                            <ul>
                                                @php($subtotal = 0)
                                                @forelse($cart_products as $p)
                                                    @php($subtotal += $p->product->price * $p->quantity)
                                                    <li>{{ $p->product->name }} - {{ ($p->product->weight_based) ? ($p->quantity * 1000).'g' : $p->quantity.'un' }} <span>R${{ number_format(($p->product->price * $p->quantity), 2,',','.') }}</span></li>
                                                @empty
                                                    <li>Nenhum produto adicionado ao carrinho.</li>
                                                @endforelse
                                            </ul>

                                            <p>Subtotal <span>R${{ number_format($subtotal, 2,',','.') }}</span></p>
                                            @if($market->free_delivery_above > $subtotal)
                                                <p>Frete <span>R${{ number_format($market->delivery_fee, 2,',','.') }}</span></p>
                                                @php($subtotal += $market->delivery_fee)
                                            @else
                                                <p>Frete <span class="text-success">Gratuíto</span></p>
                                            @endif
                                            <h4>Valor total <span>R${{ number_format($subtotal, 2,',','.') }}</span></h4>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <h4 class="checkout-title">Método de pagamento</h4>
                                        <div class="checkout-payment-method">
                                            @if($market->local_method)
                                            <div class="single-method">
                                                <input type="radio" id="payment_local" class="payment_method" name="payment_method" value="local" checked>
                                                <label for="payment_local">Retirar no local</label>
                                                <p data-method="local">Efetue o pagamento com dinheiro ou cartão ao coletar o seu pedido no mercado.</p>
                                            </div>
                                            @endif
                                            @if(collect($cart_products)->where('product.deliverable', 0)->count() == 0)
                                                @if($market->cash_on_delivery_method)
                                                <div class="single-method">
                                                    <input type="radio" id="payment_cash" class="payment_method" name="payment_method" value="cash">
                                                    <label for="payment_cash">Dinheiro na entrega</label>
                                                    <p data-method="cash">Efetue o pagamento com dinheiro na entrega do pedido.</p>
                                                </div>
                                                @endif
                                                @if($market->card_on_delivery_method)
                                                <div class="single-method">
                                                    <input type="radio" id="payment_card" class="payment_method" name="payment_method" value="card">
                                                    <label for="payment_card">Cartão na entrega</label>
                                                    <p data-method="card">Efetue o pagamento com cartão de crédito ou débito na entrega do pedido.</p>
                                                </div>
                                                @endif
                                                @if(collect($cart_products)->where('product.weight_based', 1)->count() <= 0)
                                                    @if($market->mercado_pago_method && $market->mercado_pago_token)
                                                    <div class="single-method">
                                                        <input type="radio" id="payment_mercado_pago" class="payment_method" name="payment_method" value="mercado_pago">
                                                        <label for="payment_mercado_pago">Mercado Pago</label>
                                                        <p data-method="mercado_pago">Efetue o pagamento com cartão de crédito através da plataforma de pagamento online MercadoPago.</p>
                                                    </div>
                                                    @endif
                                                @else
                                                    <span class="text-danger text-justify">
                                                        Seu pedido possui produtos que necessitam de pesagem. Os pesos dos produtos e o valor total de seu pedido podem ter uma pequena variação após a revisão do mercado.
                                                        @if($market->mercado_pago_token)
                                                            O pagamento com o Mercado Pago está desabilitado para este pedido.
                                                        @endif
                                                    </span>
                                                @endif
                                            @else
                                                <span class="text-danger text-justify">
                                                    Algum dos produtos em seu carrinho não pode ser entregue e seu pedido deve ser coletado no mercado.
                                                </span>
                                            @endif
                                            {{--<div class="single-method mt-30">
                                                <input type="checkbox" name="accept_terms" id="accept_terms">
                                                <label for="accept_terms" style="text-transform: none; color: #222222">Li e aceito os <a href="#!">termos de contrato</a></label>
                                            </div>--}}

                                            <div id="delivery_change_div" style="display:none">
                                                <label class="mb-0">Troco para</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend input-group-text px-2" style="font-size: 0.9rem; border-radius: 0px; border-color: gray">
                                                        R$
                                                    </div>
                                                    <input type="text" id="delivery_change" class="form-control decimal px-2" placeholder="Informe o valor com que pagará o pedido">
                                                </div>
                                            </div>
                                        </div>

                                        <button class="place-order float-right" id="create-order-button">Prosseguir</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" role="dialog" tabindex="-1" id="address_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form onsubmit="return registerAddress()" class="checkout-form" id="registerAddressForm">
                    <div class="modal-header">
                        <h4 class="modal-title">Cadastrar novo endereço</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="new_address">
                            <div class="col-12 mb-10">
                                <label>Identificação do endereço*</label>
                                <input type="text" name="description" placeholder="Ex: Casa, Trabalho, etc" required>
                            </div>

                            <div class="col-12 mb-10">
                                <label>CEP*</label>
                                <input type="text" class="cep_mask" name="zipcode" placeholder="00000-000" required>
                            </div>

                            <div class="col-md-8 col-12 mb-10">
                                <label>Logradouro*</label>
                                <input type="text" placeholder="Logradouro" name="street" id="logradouro" required>
                            </div>

                            <div class="col-md-4 col-12 mb-10">
                                <label>Número*</label>
                                <input type="text" placeholder="Número" name="number" required>
                            </div>

                            <div class="col-md-6 col-12">
                                <label>Bairro*</label>
                                <input type="text" placeholder="Bairro" name="district" id="bairro" required>
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Complemento</label>
                                <input type="text" placeholder="Complemento" name="complement">
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Estado*</label>
                                <select name="state_initials" id="uf">
                                    <option value="">Selecione</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Cidade*</label>
                                <input type="text" placeholder="Cidade" name="city" id="cidade" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12">
                            <button type="submit" class="place-order float-right mt-2" id="regiterAddressButton">Cadastrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.cep_mask').on('focusout', function(){
            var cep = $(this).val()
            var script = document.createElement('script')
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=callback_cep'
            document.body.appendChild(script)
        })

        $(".payment_method").on('change', function(){
            if($(this).val() == 'cash'){
                $("#delivery_change_div").show();
                $("#delivery_change").attr('required', 'required');
            }else{
                $("#delivery_change_div").hide();
                $("#delivery_change").removeAttr('required');
                $("#delivery_change").val('');
            }
        });

        function callback_cep(conteudo) {
            if (!("erro" in conteudo)) {
                //Atualiza os campos com os valores.
                document.getElementById('logradouro').value=(conteudo.logradouro);
                document.getElementById('bairro').value=(conteudo.bairro);
                document.getElementById('cidade').value=(conteudo.localidade);
                document.getElementById('uf').value=(conteudo.uf);
            }
        }

        function registerAddress(){
            $.ajax({
                url: '/register_address',
                headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                data: $('#registerAddressForm').serialize(),
                method: 'POST',
                beforeSend(){
                    setButtonState('#regiterAddressButton', 'inactive')
                },
                success(data){
                    setButtonState('#regiterAddressButton', 'active')

                    if(data.success == true){
                        $('#addresses_container').load('/checkout #addresses_div', function(){
                            setRadioDescription();
                        });

                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
                error(data){
                    setButtonState('#regiterAddressButton', 'active')
                },
                complete: function(){
                    $("#addresses_container").html('<h4>Atualizando endereços...</h4>');
                    $('#address_modal').modal('hide');
                }
            })

            return false
        }

        function setButtonState(selector, state){
            if(state == 'inactive'){
                $(selector).html('<i class="fa fa-spin fa-spinner"></i>');
                $(selector).prop('disabled', true);
            }
            if(state == 'active'){
                $(selector).html('Cadastrar');
                $(selector).prop('disabled', false);
            }
        }

        function createOrder(){
            /*let accept_terms = $("#accept_terms").is(':checked');*/
            let address_id = $(".shipping_address:checked").val();
            let payment_method = $(".payment_method:checked").val();
            let schedule = $("#schedule").val();
            let schedule_day = $("#schedule_day").val();
            let schedule_hour = $("#schedule_hour").val();
            let delivery_change = $("#delivery_change").val();

            /*if(!accept_terms){
                toastr.error('Você precisa aceitar os termos e condições de contrato para concluir o seu pedido.');
                return false;
            }*/
            if(!payment_method){
                toastr.error('Você precisa selecionar um método de pagamento para concluir o seu pedido.');
                return false;
            }
            if(!address_id){
                toastr.error('Você precisa selecionar um endereço para concluir o seu pedido.');
                return false;
            }

            if(payment_method == 'cash' && (!delivery_change || delivery_change <= 0)){
                toastr.error('Informe o valor que será pago em dinheiro para calcularmos o troco que será enviado através do entregador.');
                return false;
            }

            $.ajax({
                url: '/create_order',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN' : $("meta[name='csrf-token']").attr('content')
                },
                data: {payment_method : payment_method, address_id : address_id, schedule : schedule, schedule_day : schedule_day, schedule_hour : schedule_hour, delivery_change: delivery_change},
                beforeSend: function(){
                    $("#create-order-button").html('<i class="fa fa-spin fa-spinner"></i>');
                    $("#create-order-button").attr('disabled', true);
                },
                success: function(response){
                    if(response.success){
                        window.location.href = '/order/' + response.data.id;
                    }else{
                        console.log(response);
                        toastr.error(response.message);
                    }
                },
                error: function(response){
                    if(response.success){
                        window.location.href = '/order/' + response.data.id;
                    }else{
                        console.log(response);
                        toastr.error(response.message);
                    }
                },
                complete: function(){
                    $("#create-order-button").html('Prosseguir');
                    $("#create-order-button").removeAttr('disabled');
                }
            });
        }

        function updateMinutes(obj){
            let day = $(obj).val();

            if(day == 'today'){
                $("#schedule_hour option").each(function(index, option){
                    let date = moment(new Date()).add(30, 'm').toDate();
                    console.log(date);
                    return false;
                    let current_hour = (date.getHours())+':'+date.getMinutes()+':00';
                    let hour = $(option).val()+':00';

                    if(current_hour > hour){
                        $(option).attr('disabled', true);
                    }
                });
            }

            if(day == 'tomorrow'){
                $("#schedule_hour option").removeAttr('disabled');
            }
        }

        function scheduleOption(obj){
            let schedule = $(obj).val();

            if(schedule == 'yes'){
                $("#schedule_div").show();
            }else{
                $("#schedule_div").hide();
            }
        }

        scheduleOption();
    </script>
@endsection
