@extends('website.layout.default')

@section('stylesheets')
    <style>
        .image a div{
            min-height: 200px !important;
        }
    </style>
@endsection

@section('content')
    <div class="slider tab-slider mb-35">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h3>Meus favoritos</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-slider-wrapper bg-white shadow" style="padding: 0px 20px">
                        <div class="row" id="products-list">
                            {{--Products will be printed here--}}
                            @if(!$favorites || empty($favorites))
                                <h4 class="py-4 pl-4 mb-0">Nenhum produto adicionado à sua lista de favoritos.</h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        products = {!! json_encode($favorites) !!};

        $.each(products, function(index, result){
            result = prepareFavoriteObject(result);

            createElementFromTemplate($("#favorite-template"), result, $("#products-list"));
        });

        function prepareFavoriteObject(result){
            result.name = result.product.name;
            result.link = window.location.origin + '/product/' + result.product.id;
            result.product.media[0] ? result.photo = result.product.media[0].url : undefined;
            result.market_name = result.product.market.name;
            result.market_link = window.location.origin + '/market/' + market.id;
            result.price = result.product.price.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            result.discount_price = result.product.discount_price.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            if(favorites.length > 0){
                let favorite_product = favorites.find(favorites => favorites === result.product_id);

                result.favoriteClass = favorite_product ? 'favorite-product' : null;
            }else{
                result.favoriteClass = null;
            }

            return result;
        }
    </script>
@endsection
