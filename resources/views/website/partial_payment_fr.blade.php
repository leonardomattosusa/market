<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pagamento parcial com Saldo de Cashback</title>
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- FontAwesome CSS -->
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">

    <style>
        .mercadopago-button{
            background-color: #28A745 !important;
            width: 100% !important;
            padding: 10px !important;
        }
    </style>
</head>
<body>
<div class="container h-100">
    <div class="row h-100">
        <div class="col-12 py-3 h-100">
            <div class="h-100 d-flex flex-column">
                <div class="flex-grow-1">
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @else
                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        <h3 class="mb-4">Pagamento com cashback</h3>
                        <h5>Seu saldo de cashback atual: R$ {{ number_format($balance, 2, ',', '.') }}</h5>
                        <form action="{{ url('checkout/partial_payment_fr') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="api_token" value="{{ Request::input('api_token') }}">
                            <div class="form-group">
                                <label for="">Qual valor você deseja pagar?</label>
                                <input class="form-control decimal" type="text" placeholder="0.00" name="amount">
                            </div>
                            <button class="btn btn-success" style="width:100%">Pagar</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery JS -->
<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.maskmoney.min.js') }}"></script>
<script>
    $(".decimal").maskMoney();
</script>
</body>
</html>
