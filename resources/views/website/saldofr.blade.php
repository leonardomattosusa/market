<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pagamento com Saldo FR</title>
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- FontAwesome CSS -->
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">

    <style>
        .mercadopago-button{
            background-color: #28A745 !important;
            width: 100% !important;
            padding: 10px !important;
        }
    </style>
</head>
<body>
<div class="container h-100">
    <div class="row h-100">
        <div class="col-12 py-3 h-100">
            <div class="h-100 d-flex flex-column">
                <div class="flex-grow-1">
                    <h5>Resumo da sua compra</h5>
                    <hr>
                    <h6>Produtos</h6>
                    <ul class="list-unstyled bg-light p-2">
                        @forelse($cart_products as $prod)
                            <li>- {{ $prod->quantity }}x {{ $prod->product->name }}</li>
                        @empty
                            <li>Nenhum produto em seu carrinho</li>
                        @endforelse
                    </ul>
                    <h6>Endereço de entrega</h6>
                    <div class="bg-light p-2">
                        <p class="font-weight-bold">{{ $delivery_address->description }}</p>
                        <p>{{ $delivery_address->address }}</p>
                    </div>
                    <h6 class="mt-2">Valor a pagar</h6>
                    <div class="bg-light text-right">
                        <table class="table">
                            <tr>
                                <td><h6>Entrega</h6></td>
                                <td class="text-right"><h6>R$ {{ number_format($delivery_fee, 2, ',', '.') }}</h6></td>
                            </tr>
                            <tr>
                                <td><h6>Produtos</h6></td>
                                <td class="text-right"><h6>R$ {{ number_format($products_total, 2, ',', '.') }}</h6></td>
                            </tr>
                            <tr>
                                <td><h6>Total</h6></td>
                                <td class="text-right"><h6>R$ {{ number_format($delivery_fee+$products_total, 2, ',', '.') }}</h6></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <form action="{{ url('/pay/saldofr/'.$api_token.'/'.$delivery_address->id.($delivery_date != null ? '/'.$delivery_date : '')) }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-success pull-right" type="submit">Pagar com saldo FR</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- jQuery JS -->
<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>
