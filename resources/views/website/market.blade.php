@extends('website.layout.default')

@section('title', '')

@section('stylesheets')
    <style>
        .image a div{
            min-height: 200px !important;
        }

        .pagination-content ul li{
            margin-right: 3px;
        }

        .category-slider-container .slick-slide .category-image img{
            height: 135px;
        }
    </style>
@endsection

@section('content')
<div class="slider category-slider mb-35">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="shop-header mb-35">
                    <div class="row">
                        {{--<div class="col-lg-4 col-md-4 col-sm-12 d-flex align-items-center">
                            <div class="sort-by-dropdown d-flex align-items-center mb-xs-10">
                                <p class="mr-10">Ordenar por: </p>
                                <select name="sort-by" id="sort-by" class="nice-select">
                                    <option value="0">Popularidade</option>
                                    <option value="0">Preço: Menor ao Maior</option>
                                    <option value="0">Preço: Maior ao Menor</option>
                                </select>
                            </div>
                        </div>--}}
                        <div class="col-lg-12 col-md-12 col-sm-12 d-flex flex-column flex-sm-row justify-content-between justify-content-sm-center align-items-left align-items-sm-center">
                            <div class="sort-by-dropdown d-flex flex-md-grow-1 flex-grow-0 justify-content-md-start justify-content-center align-items-center mb-xs-10">
                                <div class="header-advance-search">
                                    <form action="javascript:(searchProducts(this))">
                                        <input type="text" class="search-product" placeholder="Pesquisar por um produto" style="min-width: 220px" minlength="3">
                                        <button class="search-product-button">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>

                            <p class="result-show-message text-center text-md-right">Mostrando página <span class="page-count">0</span> de <span class="page-total-count">0</span>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success" id="free_delivery_alert" style="display: none">
                    Este mercado oferece frete gratuíto para compras acima de <span id="free_delivery_span"></span>.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Categorias</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="category-slider-container bg-white shadow" id="categories-list">
                    <div class="single-category" style="max-width: 250px">
                        <div class="category-image">
                            <a href="javascript:void(0)" class="category_link" category_id="0" onclick="changeCategory(this)" title="Todos os Produtos" {{ request()->segment('3') == 'categories' && request()->segment('4') == 0 ? 'class=highlighted' : '' }}>
                                <img src="https://imarket.digital/cesta_compras_thumb.jpg" class="img-fluid">
                            </a>
                        </div>
                        <div class="category-title">
                            <h3>
                                <a href="javascript:void(0)" category_id="0" onclick="changeCategory(this)">Todos os Produtos</a>
                            </h3>
                        </div>
                    </div>

                    {{--Categories will be printed here--}}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="slider tab-slider mb-35" id="featured-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Destaques</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-slider-wrapper bg-white shadow" style="padding: 0px 20px">
                    <div class="row" id="featured-list">
                        {{--Featured products will be printed here--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="slider tab-slider mb-35">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Produtos</h3>
                </div>
            </div>
        </div>
        <div class="row mb-15">
            <div class="col-lg-12">
                <div class="tab-slider-wrapper bg-white shadow" style="padding: 0px 20px">
                    <div class="row" id="products-list">
                        {{--Products will be printed here--}}
                    </div>
                </div>
                <div class="pagination-content text-center mt-20" id="pagination-container">
                    <ul id="pagination-numbers">

                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-12">
                <p class="result-show-message text-center text-md-right">Mostrando página <span class="page-count">0</span> de <span class="page-total-count">0</span>.</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var categories, products;

        if(market && market.free_delivery_above > 0){
            $("#free_delivery_alert").show();
            $("#free_delivery_span").html(market.free_delivery_above.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));
        }

        // Listagem de categorias
        $.ajax({
            url: 'https://imarket.digital/api/markets/' + market.id + '/categories',
            beforeSend: function(){
                $("#categories-list").hide();
            },
            success: function(response){
                $("#categories-list").show();

                if(response.success){
                    categories = response.data;

                    $.each(response.data, function(index, result){
                        console.log(result.media[0])
                        result.media[0] ? result.photo = result.media[0].thumb : undefined;

                        createElementFromTemplate($("#category-template"), result, $("#categories-list"));
                    });
                }
            },
            complete: function(response){
                setCategoriesSlider();
            }
        });

        $.ajax({
            url: 'https://imarket.digital/api/products/?with=category&search=market_id:' + market.id + ';featured:1&searchJoin=and',
            beforeSend: function(){
                $("#featured-list").html('<div class="d-flex w-100 my-4 justify-content-center"><i class="fa fa-spin fa-spinner" style="font-size: 4rem"></i></div>');
            },
            success: function(response){
                $("#featured-list").empty();

                if(response.success){
                    if(response.data.length > 0){
                        $.each(response.data, function(index, result){
                            result = prepareProductObject(result);

                            createElementFromTemplate($("#product-template"), result, $("#featured-list"));
                        });
                    }else{
                        $("#featured-container").remove();
                    }
                }
            },
            complete: function(){
                productModalSetEvent();
            }
        });

        getMarketProducts();
    </script>
@endsection
