<div class="row" id="product-modal-details-template">
    <div class="col-lg-5 col-md-6 col-xs-12">
        <div class="product-image-slider">
            <div class="single-product-img img-full">
                <img src="#" data-attributes="src" data-variables="photo" class="img-fluid" alt="">
            </div>
        </div>
    </div>
    <div class="col-lg-7 col-md-6 col-xs-12">
        <div class="product-feature-details">
            <h2 class="product-title mb-15" data-attributes="text" data-variables="name"></h2>

            <h2 class="product-price mb-15">
                <span class="main-price" data-attributes="text_not_null" data-variables="discount_price"></span>
                <span class="discounted-price" data-attributes="text" data-variables="price"></span>
            </h2>

            <p class="product-description mb-10" data-attributes="html" data-variables="description"></p>
            <div class="mb-20" data-attributes="html" data-variables="capacity_div">
            </div>
            <span class="weight-price" style="display:none"></span>
            <div class="cart-buttons mb-20">
                <div class="d-inline" data-attributes="html" data-variables="qty_selector">
                    <div class="pro-qty mr-10"><input type="text" class="quantity" data-attributes="value,price" data-variables="quantity,price" price onkeyup="updatePrice(this)" value="1"></div>
                </div>
                <div class="add-to-cart-btn">
                    <a href="#!" onclick="addProductToCart(this)" product_id="#" market_id="#" data-attributes="product_id,market_id,html" data-variables="id,market_id,addToCartButtonText"><i class="fa fa-shopping-cart"></i> Adicionar ao Carrinho</a>
                </div>
            </div>

            {{--<div class="social-share-buttons">
                <h3>share this product</h3>
                <ul>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                </ul>
            </div>--}}
        </div>
    </div>
</div>