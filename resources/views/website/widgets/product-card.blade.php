<div class="col-lg-3 col-md-6 col-xs-12" id="product-template">
    <div class="gf-product tab-slider-sub-product">
        <div class="image">
            <a href="javascript:void(0)" product_id="#" data-toggle="modal" data-target="#quick-view-modal-container" data-attributes="product_id" data-variables="id">
                <div data-attributes="style-image" data-variables="photo" class="img-fluid"></div>
            </a>
            <div class="product-hover-icons">
                {{--<a class="active" href="#" data-tooltip="Adicionar ao carrinho"> <span class="icon_cart_alt"></span></a>
                <a href="#" data-tooltip="Adicionar à lista de desejos"> <span class="icon_heart_alt"></span> </a>
                <a href="#" data-tooltip="Compare"> <span class="arrow_left-right_alt"></span> </a>--}}
                <a href="javascript:void(0)" data-tooltip="Adicionar aos Favoritos" product_id="#" data-attributes="product_id,class" data-variables="id,favoriteClass" onclick="addToFavorites(this)"><span class="icon_heart"></span> </a>
                <a href="javascript:void(0)" data-tooltip="Adicionar ao Carrinho " product_id="#" data-toggle="modal" data-target="#quick-view-modal-container" data-attributes="product_id" data-variables="id"><span class="icon_cart_alt"></span> </a>
            </div>
        </div>
        <div class="product-content">
            <div class="product-categories">
                <a href="javascript:void(0)" data-attributes="text,href" data-variables="category_name,category_link"></a>
            </div>
            <h3 class="product-title"><a href="#!" product_id="#" data-toggle="modal" data-target="#quick-view-modal-container" data-attributes="product_id,text" data-variables="id,name"></a></h3>
            <div class="price-box">
                <span class="main-price" data-attributes="text_not_null" data-variables="discount_price"></span>
                <span class="discounted-price" data-attributes="text" data-variables="price"></span>
            </div>
        </div>
    </div>
</div>
