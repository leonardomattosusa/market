<div class="single-category" id="category-template">
    <div class="category-image">
        <a href="javascript:void(0)" class="category_link" category_id data-attributes="category_id,title" data-variables="id,name" onclick="changeCategory(this)" title="">
            <img src="#!" data-attributes="src" data-variables="photo" class="img-fluid">
        </a>
    </div>
    <div class="category-title">
        <h3>
            <a href="javascript:void(0)" category_id data-attributes="category_id,text" data-variables="id,name" onclick="changeCategory(this)"></a>
        </h3>
    </div>
</div>
