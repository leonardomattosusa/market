<div id="market-template">
    <div class="single-tab-slider-item">
        <div class="gf-product tab-slider-sub-product">
            <div class="image align-items-center">
                <span style="position: absolute; top: 8px; left: 8px; font-size: 1rem" data-attributes="class,text" data-variables="closed,closed_text">Aberto</span>
                <a href="#!" data-attributes="href" data-variables="link">
                    <div data-attributes="style-image" data-variables="photo" class="img-fluid"></div>
                </a>
            </div>
            <div class="product-content d-flex flex-column flex-grow-1 justify-content-between">
                <h3 class="product-title">
                    <a href="single-product.html" data-attributes="href,text" data-variables="link,name"></a>
                    <small class="d-block" style="font-weight: 200; font-size: 0.8rem">Entrega das <span data-attributes="time-text" data-variables="early_delivery_hour"></span> às <span data-attributes="time-text" data-variables="late_delivery_hour"></span></small>
                </h3>
                <p class="product-description" data-attributes="html" data-variables="description"></p>

                <div class="market-button">
                    <a href="#!" data-attributes="href" data-variables="link"><button>Acessar mercado</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
