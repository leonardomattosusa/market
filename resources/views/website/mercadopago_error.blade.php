<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pagamento via Mercado Pago</title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="container h-100">
    <div class="row h-100">
        <div class="col-12 py-3 h-100">
            <div class="alert alert-danger">
                {{ $error_message }}
            </div>
        </div>
    </div>
</div>
<!-- jQuery JS -->
<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>
