<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pagamento via Mercado Pago</title>
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- FontAwesome CSS -->
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">

    <style>
        .mercadopago-button{
            background-color: #28A745 !important;
            width: 100% !important;
            padding: 10px !important;
        }
    </style>
</head>
<body>
<div class="container h-100">
    <div class="row h-100">
        <div class="col-12 py-3 h-100">
            <div class="h-100 d-flex flex-column">
                <div class="flex-grow-1">
                    <img src="{{ asset('assets/images/mercado-pago-logo.png') }}" alt="" style="width:50%">
                    <hr>
                    <h6 class="mt-2">Valor a pagar</h6>
                    <div class="bg-light text-right">
                        <table class="table">
                            <tr>
                                <td><h6>Valor do pedido</h6></td>
                                <td class="text-right"><h6>R$ {{ number_format($total, 2, ',', '.') }}</h6></td>
                            </tr>
                            @if($already_paid_amount)
                                <tr>
                                    <td><h6>Valor já pago</h6></td>
                                    <td class="text-right"><h6>R$ {{ number_format($already_paid_amount, 2, ',', '.') }}</h6></td>
                                </tr>
                            @endif
                            <tr>
                                <td><h6>Total</h6></td>
                                <td class="text-right"><h6>R$ {{ number_format($total - $already_paid_amount, 2, ',', '.') }}</h6></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <form action="/teste" method="POST">
                    <script src="https://www.mercadopago.com.br/integrations/v1/web-payment-checkout.js" data-preference-id="{{ $preference->id }}"></script>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- jQuery JS -->
<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>
