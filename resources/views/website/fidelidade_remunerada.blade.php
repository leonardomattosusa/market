@extends('website.layout.default')

@section('stylesheets')
    <style>
        h3{
            font-size: 32px;
            color: #111;
            text-transform: uppercase;
            text-align: center;
            font-weight: 700;
            position: relative;
            padding-bottom: 15px;
        }

        h3::before {
            content: '';
            position: absolute;
            display: block;
            width: 120px;
            height: 1px;
            background: #ddd;
            bottom: 1px;
            left: calc(50% - 60px);
        }

        h3::after {
            content: '';
            position: absolute;
            display: block;
            width: 40px;
            height: 3px;
            background: rgb(244, 122, 53);
            bottom: 0;
            left: calc(50% - 20px);
        }
    </style>
@endsection

@section('content')
    <div class="my-account-section section position-relative mb-50 fix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body" id="message-container">
                            <div class="row">
                                <div class="col-12">
                                    <div style="position:absolute; float:left; left: 20px;">
                                        <a href="/markets-list">
                                            <i class="fa fa-arrow-circle-left text-warning" style="font-size: 2rem"></i>
                                        </a>
                                    </div>
                                    <div class="text-center mb-4">
                                        <img src="https://fidelidaderemunerada.com.br/site/img/logo.png" class="img-fluid">
                                    </div>
                                    <h3 class="text-center">Cashback</h3>
                                    @if($fr_user->login == $fr_user->documento)
                                        <p>Ao cadastrar-se no iMarket, você recebeu um cadastro grátis na Fidelidade Remunerada, agora você recebe cashback de até 4% por compras realizadas no iMarket e em milhares de empresas conveniadas pelo Brasil.</p>
                                        <p>O seu login é <b>o seu CPF</b>, ao pagar suas compras em estabelecimentos conveniados solicite o lançamento do cashback e informe o seu CPF.</p>
                                    @else
                                        <p>Sua conta na Fidelidade Remunerada foi integrada com o iMarket automaticamente através do seu CPF, agora você recebe cashback de até 4% por compras realizadas no iMarket e em milhares de empresas conveniadas pelo Brasil.</p>
                                        <p>O seu login é <b>{{ $fr_user->login }}</b>, ao pagar suas compras em estabelecimentos conveniados solicite o lançamento do cashback e informe o seu login.</p>
                                    @endif
                                </div>
                                <div class="col-lg-4 offset-lg-4 col-12">
                                    <img src="https://fidelidaderemunerada.com.br/site/img/consumidora.png" class="img-fluid my-4">
                                </div>
                                <div class="col-12">
                                    <p class="text-center">Baixe o aplicativo Fidelidade Remunerada e encontre os estabelecimentos conveniados próximos de você.</p>
                                </div>
                                <div class="col-lg-3 col-md-2 col-12"></div>
                                <div class="col-lg-3 col-md-4 col-12">
                                    <a href="https://apps.apple.com/us/app/fidelidade-remunerada-bank/id1490190768?l=pt" target="_blank">
                                        <img src="https://fidelidaderemunerada.com.br/site/img/app_store.png" class="img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-12">
                                    <a href="https://play.google.com/store/apps/details?id=br.com.fidelidaderemunerada.idez&hl=pt_BR" target="_blank">
                                        <img src="https://fidelidaderemunerada.com.br/site/img/play_store.webp" class="img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-2 col-12"></div>
                                <div class="col-12 mt-4">
                                    <div class="text-center">
                                        <a href="/markets-list" class="btn btn-warning text-white">Continuar comprando</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection