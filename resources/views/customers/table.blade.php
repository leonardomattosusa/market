@push('css_lib')
    @include('layouts.datatables_css')
@endpush

{!! $dataTable->table(['width' => '100%']) !!}

@push('scripts_lib')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}

    <script>
        function toggleQrcode(checkbox) {
            fetch('/api/users/can-use-qrcode/toggle', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({
                    user_id: checkbox.value,
                    can_use_qrcode: checkbox.checked
                })
            });
        }
    </script>
@endpush