<div class='btn-group btn-group-sm'>
    <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.user_show')}}" href="{{ route('users.show', $id) }}" class='btn btn-link'>
    <i class="fa fa-eye"></i> </a>
    {{--<a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.user_edit')}}" href="{{ route('users.edit', $id) }}" class='btn btn-link'>
        <i class="fa fa-edit"></i>
    </a>--}}
</div>