@extends('layouts.app')

@php
    if($manager->lastMonthlyPayment){
        $year = date('Y', strtotime($manager->lastMonthlyPayment->due_date));
        $month = date('m', strtotime($manager->lastMonthlyPayment->due_date));
        $day = date('d', strtotime($manager->lastMonthlyPayment->due_date));
    }else{
        if(date('d') < 5){
            $year = date('Y');
            $month = date('m');
            $day = '05';
        }else{
            $year = date('Y');
            $month = date('m', strtotime('first day of next month'));
            $day = '05';
        }
    }
@endphp

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header content-header{{setting('fixed_header')}}">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1>Minha Conta</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <form method="POST" action="{{ route('account.add_market.post') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h4 class="mb-0">Aumentar meu limite de cadastro de mercados</h4>
                        </div>
                        <div class="card-body">
                            <p>Atualmente você pode cadastrar {{ $manager->markets_limit }} mercados em sua conta. Aumente o limite de cadastro de mercados de sua conta e utilize o iMarket em toda a sua franquia!</p>
                            <p>Cada mercado adicional custa <span class="badge badge-success" style="font-size: 0.9rem">R$0</span> de pagamento imediato e um adicional de <span class="badge badge-success" style="font-size: 0.9rem">R$590,00</span> em suas mensalidades.</p>
                            <hr>
                            <div class="row">
                                <div class="d-flex w-100 justify-content-center">
                                    <div class="d-flex flex-nowrap flex-column mr-3" style="width: 300px">
                                        <label class="text-center">Quantos mercados você deseja adicionar à sua conta?</label>
                                        <div class="d-flex justify-content-center">
                                            <input type="number" class="form-control d-inline" style="width: 60px" name="markets_quantity" id="markets_quantity" value="1" min="1" onchange="updatePlanPrice()">
                                        </div>
                                    </div>
                                    <div class="d-flex flex-nowrap flex-column align-items-center justify-content-center ml-3">
                                        <b>Valor adicional na próxima mensalidade ({{ $day }}/{{ $month }})</b>
                                        <div><span class="text-success d-inline" id="initial_amount" style="font-size: 1.3rem">R$0 + R$590,00</span><sup class="d-inline"> <i class="far fa-question-circle text-danger" tooltip="true" title="Valor referente aos dias de uso do período atual até a próxima mensalidade"></i></sup></div>
                                        <b>Valor adicional nas mensalidades subsequentes</b>
                                        <span class="text-success" id="monthly_amount" style="font-size: 1.3rem">R$590,00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 mt-5">
                                <p>Ao gerar o pedido de adição de mercados à sua conta você concorda com os <a href="#!">Termos e Condições do iMarket</a>.</p>
                                <div class="float-right">
                                    <a href="{{ route('account.index') }}" class="btn btn-default">
                                        <i class="fas fa-undo"></i> Voltar
                                    </a>
                                    <button class="btn btn-primary">
                                        <i class="fas fa-check"></i> Gerar pedido
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $("#markets_quantity").focus();
        $("[tooltip='true']").tooltip({boundary: 'window'});

        function getDaysToNextPayment() {
            dt1 = new Date('{!! date('Y-m-d H:i:s') !!}');
            dt2 = new Date('{!! $year !!}-{!! $month !!}-{!! $day !!} 00:00:00');

            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
        }

        updatePlanPrice();

        function updatePlanPrice(){
            var qty = $("#markets_quantity").val();
            var market_amount = 0 * qty;
            var monthly_amount = 590 * qty;

            var days_to_next_payment = getDaysToNextPayment();
            var first_monthly_amount = (monthly_amount / 31) * days_to_next_payment;

            $("#initial_amount").text(market_amount.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}) + ' + ' + first_monthly_amount.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}));
            $("#monthly_amount").text(monthly_amount.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}));
        }
    </script>
@endpush