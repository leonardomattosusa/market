@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header content-header{{setting('fixed_header')}}">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1>Detalhes da mensalidade <span class="float-right">{!! $mp->status_html !!}</span></h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border">
                        <h4 class="mb-0">Detalhes da mensalidade</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table border-bottom">
                            <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($mp->lines as $line)
                                    <tr>
                                        <td>{{ $line->description }}</td>
                                        <td>R${{ number_format($line->amount, 2, ',', '.') }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">Detalhes da mensalidade indisponíveis.</td>
                                    </tr>
                                @endforelse
                                <tr class="text-success" style="font-size: 1.2rem">
                                    <th class="text-right">Valor total:</th>
                                    <td>R${{ number_format($mp->total_amount, 2, ',', '.') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-body">
                        <div class="w-100">
                            <div class="float-right">
                                <a href="{{ route('account.index') }}" class="btn btn-default"><i class="fas fa-undo"></i> Voltar</a>
                                @if($mp->status == 'pending')
                                    @if(date('m') == date('m', strtotime($mp->due_date)))
                                        <form method="GET" action="/order/{{ $mp->id }}" class="d-inline">
                                            <script src="https://www.mercadopago.com.br/integrations/v1/web-payment-checkout.js" data-preference-id="{{ $preference->id }}" data-button-label="Efetuar pagamento"></script>
                                        </form>
                                    @else
                                        <button class="btn btn-secondary" tooltip="true" title="O pagamento ficará disponível a partir do dia primeiro.">Efetuar pagamento</button>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $("#markets_quantity").focus();
        $("[tooltip='true']").tooltip({boundary: 'window'});
    </script>
@endpush