@extends('layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header content-header{{setting('fixed_header')}}">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1>Minha Conta</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-4 col-12">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ $manager->markets_limit }}</h3>

                        <p>Limite de mercados <br><br></p>
                    </div>
                    <div class="icon pt-2">
                        <i class="fa fa-shopping-basket"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-md-4 col-12">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner text-white">
                        <h3>30/08/2020</h3>

                        <p>Vencimento da proxima <br>mensalidade</p>
                    </div>
                    <div class="icon pt-2">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-md-4 col-12">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>R$ 2070,00</h3>

                        <p>Valor da próxima <br>mensalidade</p>
                    </div>
                    <div class="icon pt-2">
                        <i class="fa fa-money"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border">
                        <h4>Meus mercados</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table border-bottom">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Identificador</th>
                                    <th>Endereço</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($i = 0; $i < $manager->markets_limit; $i++)
                                    @php($market = isset($markets[$i]) ? $markets[$i] : null)
                                    @if($market)
                                        <tr>
                                            <td class="text-center">{!! getMediaColumn($market, 'image') !!}</td>
                                            <td>{{ $market->identifier }}</td>
                                            <td>{{ $market->address }}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td class="text-center">
                                                <a href="{{ url('admin/markets/create') }}" class="text-success"><i class="fa fa-plus-square py-3" style="font-size: 1rem"></i></a>
                                            </td>
                                            <td colspan="3"><a href="{{ url('admin/markets/create') }}" class="text-success">Espaço livre para o cadastro de um mercado</a></td>
                                        </tr>
                                    @endif
                                @endfor
                            </tbody>
                        </table>
                        <a href="{{ route('account.add_market') }}" class="btn btn-link float-right mb-2 mr-2">Adicionar mais mercados</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border">
                        <h4>Mensalidades</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table border-bottom">
                            <thead>
                            <tr>
                                <th style="width: 120px" class="text-center">Status</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                <th style="width: 120px" class="text-center">Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($manager->monthlyPayments->take(3) as $mp)
                                    <tr class="{{ ($mp->status == 'paid') ? 'text-success' : '' }}">
                                        <td class="text-center">
                                            {!! $mp->status_html !!}
                                        </td>
                                        <td>R$ {{ number_format($mp->total_amount,2,',','.') }}</td>
                                        <td>{{ date('d/m/Y', strtotime($mp->due_date)) }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('account.show_monthly_payment', $mp->id) }}" class="btn btn-link btn-sm" tooltip="true" title="Visualizar mensalidade"><i class="fas fa-eye {{ ($mp->status == 'paid') ? 'text-success' : 'text-dark' }}"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">Nenhuma mensalidade gerada ainda.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <a href="{{ route('account.monthly_payments') }}"  class="btn btn-link float-right mb-2 mr-2">Ver todas mensalidades</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection