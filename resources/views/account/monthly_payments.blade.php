@extends('layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header content-header{{setting('fixed_header')}}">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1>Todas mensalidades</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header no-border">
                        <h4>Mensalidades</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table border-bottom">
                            <thead>
                            <tr>
                                <th style="width: 120px" class="text-center">Status</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                <th style="width: 120px" class="text-center"">Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($manager->monthlyPayments as $mp)
                                    <tr class="text-warning">
                                        <td class="text-center">
                                            {!! $mp->status_html !!}
                                        </td>
                                        <td>R$ {{ number_format($mp->total_amount,2,',','.') }}</td>
                                        <td>{{ date('d/m/Y', strtotime($mp->due_date)) }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('account.show_monthly_payment', $mp->id) }}" class="btn btn-link btn-sm" tooltip="true" title="Visualizar mensalidade"><i class="fas fa-eye text-warning"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">Nenhuma mensalidade gerada ainda.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <a href="{{ route('account.index') }}" class="btn btn-default float-right mb-2 mr-2"><i class="fas fa-undo"></i> Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection