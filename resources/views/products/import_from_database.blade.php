<html>
<head>
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">

    <style>
        .table tr td{
            vertical-align: middle;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <form method="GET">
            <div class="d-flex my-2">
                <b class="mr-2" style="vertical-align: middle; margin-top: 3px">Categoria:</b>
                <select id="category" name="category" class="form-control form-control-sm" style="width: 180px">
                    <option value="all">Todas categorias</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ (request()->category == $category->id) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
                <b class="mx-2" style="vertical-align: middle; margin-top: 3px">Pesquisa:</b>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text px-2 pb-2" id="basic-addon1"><i class="fa fa-search"></i></span>
                    </div>
                    <input type="text" name="search" id="search" class="form-control form-control-sm" placeholder="Pesquisar produto (Nome, NCM, GTIN/EAN)" value="{{ request()->search }}">
                </div>
                <button class="btn btn-sm btn-primary ml-2">Pesquisar</button>
                <a href="{{ url('admin/products') }}" class="btn btn-sm btn-secondary ml-2">Voltar</a>
            </div>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-sm table-compressed">
            <thead>
            <tr>
                <th class="text-center" style="width:5%">Importado</th>
                <th style="width:1%" class="text-center">Imagem</th>
                <th style="width:25%">Nome</th>
                <th style="width:25%">NCM</th>
                <th style="width:24%">GTIN/EAN</th>
                <th style="width:20%">Preço</th>
            </tr>
            </thead>
            <tbody>
            @forelse($products as $product)
                <tr class="product-line" id="product_{{ $product->id }}" model_id="{{ $product->id }}">
                    <td class="text-center"><input type="checkbox" name="products[{{ $product->id }}][active]" value="1" tabindex="-1" {{ ($product->importedPrice && $product->importedPrice > 0) ? 'checked disabled' : '' }}></td>
                    <td class="text-center">
                        {!! getSmallMediaColumn($product, 'image') !!}
                    </td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->ncm }}</td>
                    <td>{{ $product->gtin_ean }}</td>
                    <td class="pr-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text px-1 py-0" id="basic-addon1">
                                   <small>R$</small>
                                </span>
                            </div>
                            <input type="text" name="products[{{ $product->id }}][price]" class="form-control form-control-sm decimal" placeholder="Preço de venda do produto" value="{{ ($product->importedPrice && $product->importedPrice > 0) ? number_format($product->importedPrice,2) : '' }}" {{ ($product->importedPrice && $product->importedPrice > 0) ? 'disabled' : '' }} onkeyup="priceKeyPress(event, this)">
                            <div class="input-group-append">
                                <button class="btn {{ ($product->importedPrice && $product->importedPrice > 0) ? 'btn-secondary' : 'btn-success' }} btn-sm" onclick="updatePrice(this)"><i class="fa fa-check"></i></button>
                            </div>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">Nenhum produto de nossos produtos atende estas especificações.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="float-right mr-2">
            {{ $products->appends(request()->except('page'))->render() }}
        </div>
    </div>

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js') }}"></script>

    <script>
        $(document.body).delegate('[type="checkbox"]', 'click', function(e) {
            e.preventDefault();
        });

        $(".decimal").maskMoney({ thousands : '' });

        function priceKeyPress(event, obj){
            if(event.keyCode == 13){
                updatePrice($(obj));
            }
        }

        function updatePrice(obj){
            let line = $(obj).closest('tr');
            let price = parseFloat($(line).find('[type="text"]').val());
            let model_id = $(line).attr("model_id");
            let imported = $(line).find('[type="checkbox"]').is(':checked');

            if(!imported && price > 0){
                $.ajax({
                    url : '{{ url("admin/products/import_from_database/".$market_id) }}',
                    headers: {
                        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                    },
                    method: 'POST',
                    data: {model_id : model_id, price : price},
                    success: function(response){
                        console.log(response);
                    },
                    error: function(response){
                        console.log(response);
                    }
                })

                $(line).find('[type="checkbox"]').prop('checked', true).prop('disabled', true);
                $(line).find('[type="text"]').prop('readonly', true);
                $(line).find('button').removeClass('btn-success').addClass('btn-secondary');
            }
        }
    </script>
</body>
</html>