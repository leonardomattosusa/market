@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{trans('lang.product_plural')}} <small>{{trans('lang.product_desc')}}</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
          <li class="breadcrumb-item active"><a href="{!! route('products.index') !!}">{{trans('lang.product_plural')}}</a>
          </li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
  <div class="card">
    <div class="card-header">
      <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
        <li class="nav-item">
          <a class="nav-link" href="{!! route('products.index') !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.product_table')}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{!! route('products.create') !!}"><i class="fa fa-plus mr-2"></i>Atualizar valores de produtos</a>
        </li>
      </ul>
    </div>
    <div class="card-body">
      <form method="POST" action="/admin/products/update_amounts" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-12">
            @if(session('success'))
              <p class="text-success">{{ session('success') }}</p>
            @endif
            @if(session('error'))
              <p class="text-danger">{{ session('error') }}</p>
            @endif
          </div>

          <div class="form-group col-12">
            <label for="file">Selecione o arquivo de atualização dos valores</label><br>
            <small>Arquivo deve ser no formato .csv e conter as seguintes colunas: GTIN_EAN, NOME, PREÇO, o arquivo não deve ter um cabeçalho, a primeira linha já deve conter os dados dos produtos. Caso não exista um produto com o GTIN/EAN informado, um novo produto será criado.</small><br>
            <input type="file" name="file" id="file" class="form-control">
          </div>

          <!-- Back Field -->
          <div class="form-group col-12 text-right">
            <button type="submit" class="btn btn-primary mr-2"> Importar dados</button>
            <a href="{!! route('products.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.back')}}</a>
          </div>
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
@endsection
