@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{trans('lang.product_plural')}} <small>{{trans('lang.product_desc')}}</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('products.index') !!}">{{trans('lang.product_plural')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{trans('lang.product_table')}}</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-12">
                    @php($markets = auth()->user()->markets)
                    @if($markets->count() > 0)
                        @if($markets->count()> 1)
                            <a href="#!" data-toggle="modal" data-target="#modal-choose-market" class="btn btn-primary btn-sm float-right">Importar produtos da base de dados</a>

                            <div class="modal fade" role="dialog" tabindex="-1" id="modal-choose-market">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title">Escolha o mercado</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>Abaixo escolha o mercado para qual os produtos serão importados da base de dados do iMarket.</p>
                                            <select id="market_id" class="form-control">
                                                @foreach($markets as $market)
                                                    <option value="{{ $market->id }}">{{ $market->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="confirm_market" class="btn btn-primary">Confirmar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <a href="{{ route('products.import_from_database', $markets->first()->id) }}" class="btn btn-primary btn-sm float-right">Importar produtos da base de dados</a>
                        @endif
                    @endif
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.product_table')}}</a>
                    </li>
                    @can('products.create')
                        <li class="nav-item">
                            <a class="nav-link" href="#!" data-toggle="modal" data-target="#create-product-modal"><i class="fa fa-plus mr-2"></i>{{trans('lang.product_create')}}</a>
                        </li>
                    @endcan
                    @include('layouts.right_toolbar', compact('dataTable'))
                </ul>
            </div>
            <div class="card-body">
                <form action="">
                    <div class="row mb-4">
                        @if($markets && count($markets) > 1)
                        <div class="col-5">
                            <div class="text-right">
                                <label for="" class="control-label mt-1">Selecione um mercado</label>
                            </div>
                        </div>
                        <div class="col-3">
                            <select name="market_id" id="market_id_input" class="form-control" onchange="submitForm(this)">
                                @foreach(auth()->user()->markets as $market)
                                    <option value="{{ $market->id }}" {{ request()->market_id == $market->id ? 'selected' : '' }}>{{ $market->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @else
                            <div class="col-8"></div>
                        @endif
                        <div class="col-4">
                            <div class="float-right">
                                <a href="/admin/products/update_amounts" class="btn btn-primary mr-2"><i class="fas fa-dollar-sign mr-1"></i> Atualizar  valores</a>
                                <button class="btn btn-primary"><i class="fas fa-sync mr-1"></i> Forçar atualização</button>
                            </div>
                        </div>
                    </div>
                </form>
                @include('products.table')
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" role="dialog" tabindex="-1" id="create-product-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ url('admin/products/create') }}">
                    <div class="modal-header">
                        <h3 class="modal-title">Cadastrar novo produto</h3>
                    </div>
                    <div class="modal-body">
                        @if(auth()->user()->hasRole('admin'))
                            <p>Você está cadastrando um novo produto no MERCADO BASE. Informe o GTIN/EAN do produto para prosseguir para o cadastro.</p>
                            <div class="form-group">
                                <input type="text" class="form-control" name="gtin_ean" id="create_product_gtin_ean" placeholder="Digite o GTIN/EAN do produto" required>
                            </div>
                            <input type="hidden" name="market_id" value="1">
                        @else
                            @if($markets->count() > 0)
                                <p>Informe o GTIN/EAN do produto para prosseguir para o cadastro. Caso o produto já esteja cadastrado em nosso sistema o importaremos para seu mercado automaticamente.</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="gtin_ean" id="create_product_gtin_ean" placeholder="Digite o GTIN/EAN do produto" required>
                                </div>
                                <div class="form-group">
                                    <label for="market_id">Mercado</label>
                                    <select id="market_id" name="market_id" class="form-control" required>
                                        @foreach($markets as $market)
                                            <option value="{{ $market->id }}">{{ $market->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @else
                                <div class="alert alert-warning">
                                    Você deve ter pelo menos um mercado cadastrado para começar a cadastrar novos produtos.
                                </div>
                            @endif
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Prosseguir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('#confirm_market').on('click', function(){
                var market_id = $('#market_id').val();

                $(this).prop('disabled', true);
                $(this).html('<i class="fa fa-spin fa-spinner"></i>')

                window.location.href = '/admin/products/import_from_database/' + market_id;
            });

        })

        function submitForm(obj){
            $(obj).closest('form').submit();
        }
    </script>
@endpush

