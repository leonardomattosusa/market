@extends('layouts.app')

@push('css_lib')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/css/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/css/datetimepicker/bootstrap-datetimepicker.min.css') }}">
@endpush

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{trans('lang.driver_plural')}} <small>{{trans('lang.driver_reports_desc')}}</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
                        <li class="breadcrumb-item active">{{trans('lang.reports')}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="content">
        <div class="card">
            <div class="card-body">
                <h3>Relatório de desempenho</h3>
                <p>Selecione um período para gerar o relatório com a contagem e valores totais das entregas de cada entregador cadastrado.</p>
                <hr>
                <form method="GET" action="{{ route('drivers.reports') }}">
                    <div class="row">
                        <div class="form-group col-lg-5 col-4">
                            <label for="start-date" class="control-label">Data inicial</label>
                            <input type="text" class="form-control datepicker" name="start_date" id="start-date" placeholder="Data inicial" value="{{ $start_date }}" autocomplete="off">
                        </div>
                        <div class="form-group col-lg-5 col-4">
                            <label for="final-date" class="control-label">Data final</label>
                            <input type="text" class="form-control datepicker" name="final_date" id="final-date" placeholder="Data inicial" value="{{ $final_date }}" autocomplete="off">
                        </div>
                        <div class="form-group col-lg-2 col-4">
                            <label class="control-label d-block">&nbsp;</label>
                            <button class="btn btn-primary btn-block">Pesquisar</button>
                        </div>
                    </div>
                </form>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Entregador</th>
                            <th>Total de pedidos</th>
                            <th>Pedidos no periodo selecionado</th>
                            <th>Valor no periodo selecionado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($drivers as $driver)
                        @php
                            $result_count = $driver->results ? (int)explode(',', $driver->results)[0] : 0;
                            $result_price = $driver->results ? (float)explode(',', $driver->results)[1] : 0;
                        @endphp
                            <tr>
                                <td>{{ $driver->name }}</td>
                                <td>{{ $driver->orders_count }}</td>
                                <td>{{ $result_count }}</td>
                                <td>R${{ number_format($result_price,2,',','.') }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Nenhum entregador cadastrado.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/js/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js/datetimepicker/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
        $('#start-date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $('#final-date').datetimepicker({
            useCurrent: false,
            format: 'DD-MM-YYYY'
        });

        $("#start-date").on("dp.change", function (e) {
            $('#final-date').data("DateTimePicker").minDate(e.date);
        });
        $("#final-date").on("dp.change", function (e) {
            $('#start-date').data("DateTimePicker").maxDate(e.date);
        });
    </script>
@endpush