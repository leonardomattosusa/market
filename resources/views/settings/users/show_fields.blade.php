<div class="col-6">
    <!-- Name Field -->
    <div class="form-group row">
        {!! Form::label('name', 'Nome:', ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            <p>{!! $user->name !!}</p>
        </div>
    </div>

    <!-- Email Field -->
    <div class="form-group row">
        {!! Form::label('email', 'Email:', ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            <p>{!! $user->email !!}</p>
        </div>
    </div>
</div>
<div class="col-6">
    <!-- Email Field -->
    <div class="form-group row">
        {!! Form::label('document', 'Doc. Identificação:', ['class' => 'col-4 control-label text-right']) !!}
        <div class="col-8">
            <p>{!! $user->document !!}</p>
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('phone', 'Telefone:', ['class' => 'col-4 control-label text-right']) !!}
        <div class="col-8">
            <p>{!! ($user->customFields && $user->customFields['phone']) ? $user->customFields['phone']['value'] : '' !!}</p>
        </div>
    </div>

</div>
<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Data</th>
            <th>Valor</th>
        </tr>
        </thead>
        <tbody>
        @forelse($user->orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ date('d/m/Y', strtotime($order->created_at)) }}</td>
                <td>R$ {{ number_format($order->payment->price,2,',','.') }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Nenhum pedido realizado ainda</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>

