@if($customFields)
    <h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div class="column col-md-6 col-12 pr-4">
    <div class="form-group row ">
        {!! Form::label('identifier', trans("lang.market_identifier"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('identifier', null,  ['class' => 'form-control','placeholder'=>  trans("lang.market_identifier_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_identifier_help") }}
            </div>
        </div>
    </div>

    <div class="form-group row ">
        {!! Form::label('document', trans("lang.market_cnpj"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('document', null,  ['class' => 'form-control cnpj','placeholder'=>  trans("lang.market_cnpj_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_cnpj_help") }}
            </div>
            <small class="text-danger cnpj_error" style="display:none">Documento inválido.</small>
        </div>
    </div>

    <!-- Name Field -->
    <div class="form-group row ">
        {!! Form::label('name', trans("lang.market_name"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('name', null,  ['class' => 'form-control','id' => 'name', 'placeholder'=>  trans("lang.market_name_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_name_help") }}
            </div>
        </div>
    </div>

    <!-- Name Field -->
    <div class="form-group row ">
        {!! Form::label('social_reason', trans("lang.market_social_reason"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('social_reason', null,  ['class' => 'form-control','id' => 'social_reason', 'placeholder'=>  trans("lang.market_social_reason_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_social_reason_help") }}
            </div>
        </div>
    </div>

    <div class="form-group row ">
        {!! Form::label('email', trans("lang.market_email"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::email('email', null,  ['class' => 'form-control','id' => 'email', 'placeholder'=>  trans("lang.market_email_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_email_help") }}
            </div>
        </div>
    </div>

    @if(!request()->segment('4') || request()->segment('4') != 'edit')
        <div class="form-group row ">
            {!! Form::label('fr_login', trans("lang.market_fr_login"), ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                {!! Form::text('fr_login', null,  ['class' => 'form-control','placeholder'=>  trans("lang.market_fr_login_placeholder")]) !!}
                <div class="form-text text-muted">
                    {{ trans("lang.market_fr_login_help") }}
                </div>
            </div>
        </div>

        <div class="form-group row ">
            {!! Form::label('fr_password', trans("lang.market_fr_password"), ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                {!! Form::password('fr_password', ['class' => 'form-control','placeholder'=>  trans("lang.market_fr_password_placeholder")]) !!}
                <div class="form-text text-muted">
                    {{ trans("lang.market_fr_password_help") }}
                </div>
                <p class="text-danger">
                    Caso seu convênio já esteja cadastrado na Fidelidade Remunerada o seu login e senha serão mantidos.
                </p>
            </div>
        </div>
    @endif

    @hasanyrole('admin|manager')
    <!-- Users Field -->
    {{-- <div class="form-group row ">
        {!! Form::label('drivers[]', trans("lang.market_drivers"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('drivers[]', $drivers, $driversSelected, ['class' => 'select2 form-control' , 'multiple'=>'multiple']) !!}
            <div class="form-text text-muted">{{ trans("lang.market_drivers_help") }}</div>
        </div>
    </div> --}}
    <!-- delivery_fee Field -->
    <div class="form-group row ">
        {!! Form::label('cashback_percentage', trans("lang.cashback_percentage"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('cashback_percentage', $percentage, null, ['class' => 'form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.cashback_percentage_help") }}</div>
        </div>
    </div>

    <div class="form-group row ">
        {!! Form::label('delivery_fee', trans("lang.market_delivery_fee"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('delivery_fee', null, ['class' => 'form-control decimal','step'=>'any', 'placeholder'=>  trans("lang.market_delivery_fee_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_delivery_fee_help") }}
            </div>
        </div>
    </div>

    <div class="form-group row ">
        {!! Form::label('free_delivery_above', trans("lang.market_free_delivery_above"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('free_delivery_above', null, ['class' => 'form-control decimal','step'=>'any', 'placeholder'=>  trans("lang.market_free_delivery_above_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_free_delivery_above_help") }}
            </div>
        </div>
    </div>

    @prepend('scripts')
        @parent

        <script>
            $('.cnpj').on('focusout', function(){
                let cnpj = $(this).val().replace(/[^\d]+/g,'');

                if(validateCNPJ(cnpj)){
                    $.ajax({
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                        },
                        dataType: 'jsonp',
                        cors: true ,
                        contentType:'application/json',
                        secure: true,
                        method: 'GET',
                        url: 'https://www.receitaws.com.br/v1/cnpj/'+cnpj,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader ("Authorization", "Basic " + btoa(""));
                        },
                        success(data){
                            if(data.status != 'ERROR'){
                                if(data.fantasia && data.fantasia != ''){
                                    $('#name').val(data.fantasia)
                                }

                                if(data.nome && data.nome != ''){
                                    $('#social_reason').val(data.nome)
                                }

                                if(data.email && data.email != ''){
                                    $('#email').val(data.email)
                                }
                                if(data.telefone && data.telefone != ''){
                                    $('#phone').val(data.telefone)
                                }
                                if(data.cep && data.cep != ''){
                                    $('#zipcode').val(data.cep.replace(/\./g, ''))
                                }
                                if(data.logradouro && data.logradouro != ''){
                                    $('#street').val(data.logradouro)
                                }
                                if(data.numero && data.numero != ''){
                                    $('#number').val(data.numero)
                                }
                                if(data.complemento && data.complemento != ''){
                                    $('#complement').val(data.complemento)
                                }
                                if(data.bairro && data.bairro != ''){
                                    $('#district').val(data.bairro)
                                }
                                if(data.municipio && data.municipio != ''){
                                    $('#city').val(data.municipio)
                                }
                                if(data.uf && data.uf != ''){
                                    $('#state').val(data.uf)
                                }

                                getAddress();
                            }
                        }
                    });

                    $(".cnpj_error").hide();
                }else{
                    $(".cnpj_error").show();
                }
            });
        </script>
    @endprepend

    <!-- delivery_range Field -->
    <div class="form-group row ">
        {!! Form::label('delivery_range', trans("lang.market_delivery_range"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('delivery_range', null, ['class' => 'form-control decimal', 'step'=>'any', 'placeholder'=>  trans("lang.market_delivery_range_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_delivery_range_help") }}
            </div>
        </div>
    </div>

    <!-- default_tax Field -->
    {{--<div class="form-group row ">
        {!! Form::label('default_tax', trans("lang.market_default_tax"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('default_tax', null,  ['class' => 'form-control decimal', 'step'=>'any','placeholder'=>  trans("lang.market_default_tax_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_default_tax_help") }}
            </div>
        </div>
    </div>--}}

    @endhasrole

    <!-- Phone Field -->
    <div class="form-group row ">
        {!! Form::label('phone', trans("lang.market_phone"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('phone', null,  ['class' => 'form-control phone','id' => 'phone', 'placeholder'=>  trans("lang.market_phone_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_phone_help") }}
            </div>
        </div>
    </div>

    <!-- Mobile Field -->
    <div class="form-group row ">
        {!! Form::label('mobile', trans("lang.market_mobile"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('mobile', null,  ['class' => 'form-control phone','placeholder'=>  trans("lang.market_mobile_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_mobile_help") }}
            </div>
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('description', trans("lang.market_description"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=> trans("lang.market_description_placeholder")  ]) !!}
            <div class="form-text text-muted">{{ trans("lang.market_description_help") }}</div>
        </div>
    </div>

    <div class="form-group row ">
        {!! Form::label('zipcode', trans("lang.market_zipcode"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('zipcode', null,  ['class' => 'form-control cep','id' => 'zipcode','placeholder'=>  trans("lang.market_zipcode_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_zipcode_help") }}
            </div>
        </div>
    </div>

    <div class="form-group row ">
        {!! Form::label('street', trans("lang.market_street"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-6">
            {!! Form::text('street', null,  ['class' => 'form-control','id' => 'street','placeholder'=>  trans("lang.market_street_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_street_help") }}
            </div>
        </div>
        <div class="col-3">
            {!! Form::text('number', null,  ['class' => 'form-control','id' => 'number','placeholder'=>  trans("lang.market_number_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_number_help") }}
            </div>
        </div>
    </div>
    <div class="form-group row ">
        {!! Form::label('district', trans("lang.market_district"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('district', null,  ['class' => 'form-control','id' => 'district','placeholder'=>  trans("lang.market_district_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_district_help") }}
            </div>
        </div>
    </div>
    <div class="form-group row ">
        {!! Form::label('complement', trans("lang.market_complement"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('complement', null,  ['class' => 'form-control','id' => 'complement','placeholder'=>  trans("lang.market_complement_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_complement_help") }}
            </div>
        </div>
    </div>
    <div class="form-group row ">
        {!! Form::label('city', trans("lang.market_city"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-6">
            {!! Form::text('city', null,  ['class' => 'form-control','id' => 'city','placeholder'=>  trans("lang.market_city_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_city_help") }}
            </div>
        </div>
        <div class="col-3">
            {!! Form::select('state', [
                    'AC' => 'AC',
                    'AL' => 'AL',
                    'AP' => 'AP',
                    'AM' => 'AM',
                    'BA' => 'BA',
                    'CE' => 'CE',
                    'DF' => 'DF',
                    'ES' => 'ES',
                    'GO' => 'GO',
                    'MA' => 'MA',
                    'MT' => 'MT',
                    'MS' => 'MS',
                    'MG' => 'MG',
                    'PA' => 'PA',
                    'PB' => 'PB',
                    'PR' => 'PR',
                    'PE' => 'PE',
                    'PI' => 'PI',
                    'RJ' => 'RJ',
                    'RN' => 'RN',
                    'RS' => 'RS',
                    'RO' => 'RO',
                    'RR' => 'RR',
                    'SC' => 'SC',
                    'SP' => 'SP',
                    'SE' => 'SE',
                    'TO' => 'TO',
                ],null,  ['class' => 'form-control','id' => 'state','placeholder'=>  trans("lang.market_state_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_state_help") }}
            </div>
        </div>
    </div>

    <!-- Address Field -->
    {!! Form::hidden('address', null,  ['class' => 'form-control','id' => 'address','placeholder'=>  trans("lang.market_address_placeholder"), 'onfocusout' => 'getAddress()']) !!}

    {{--<div class="form-group row ">
        {!! Form::label('address', trans("lang.market_address"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('address', null,  ['class' => 'form-control','id' => 'address','placeholder'=>  trans("lang.market_address_placeholder"), 'onfocusout' => 'getAddress()']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.market_address_help") }}
            </div>
        </div>
    </div>--}}

    <!-- 'Boolean closed Field' -->
    {{--<div class="form-group row ">
        {!! Form::label('closed', trans("lang.market_closed"),['class' => 'col-3 control-label text-right']) !!}
        <div class="checkbox icheck">
            <label class="col-9 ml-2 form-check-inline">
                {!! Form::hidden('closed', 0) !!}
                {!! Form::checkbox('closed', 1, null) !!}
            </label>
        </div>
    </div>--}}

    <!-- 'Boolean available_for_delivery Field' -->
    {!! Form::hidden('available_for_delivery', 1) !!}
    {{--<div class="form-group row ">
        {!! Form::label('available_for_delivery', trans("lang.market_available_for_delivery"),['class' => 'col-3 control-label text-right']) !!}
        <div class="checkbox icheck">
            <label class="col-9 ml-2 form-check-inline">
                {!! Form::checkbox('available_for_delivery', 1, null) !!}
            </label>
        </div>
    </div>--}}
</div>
<div class="column col-md-6 col-12 pl-4">

    <!-- Image Field -->
    <div class="form-group">
        {!! Form::label('image', trans("lang.market_image"), ['class' => 'control-label text-right']) !!}
        <div>
            <div style="width: 100%" class="dropzone image" id="image" data-field="image">
                <input type="hidden" name="image">
            </div>
            <a href="#loadMediaModal" data-dropzone="image" data-toggle="modal" data-target="#mediaModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">{{ trans('lang.media_select')}}</a>
            <div class="form-text text-muted w-50">
                {{ trans("lang.market_image_help") }}
            </div>
            <div class="d-block mt-3">
                <span class="file-size-error text-danger" style="display:none">A imagem é inválida. O tamanho máximo de upload é de 2MB.</span>
            </div>
        </div>
    </div>
    @prepend('scripts')
        <script type="text/javascript">
            var var15671147011688676454ble = '';
            @if(isset($market) && $market->hasMedia('image'))
                var15671147011688676454ble = {
                name: "{!! $market->getFirstMedia('image')->name !!}",
                size: "{!! $market->getFirstMedia('image')->size !!}",
                type: "{!! $market->getFirstMedia('image')->mime_type !!}",
                collection_name: "{!! $market->getFirstMedia('image')->collection_name !!}"
            };
                    @endif
            var dz_var15671147011688676454ble = $(".dropzone.image").dropzone({
                    url: "{!!url('admin/uploads/store')!!}",
                    addRemoveLinks: true,
                    maxFiles: 1,
                    maxFileSize: 2,
                    init: function () {
                        @if(isset($market) && $market->hasMedia('image'))
                        dzInit(this, var15671147011688676454ble, '{!! url($market->getFirstMediaUrl('image','thumb')) !!}')
                        @endif

                        this.on("error", function(file, message) {
                            $('.file-size-error').show();
                            this.removeFile(file);
                        });
                    },
                    accept: function (file, done) {
                        dzAccept(file, done, this.element, "{!!config('medialibrary.icons_folder')!!}");
                    },
                    sending: function (file, xhr, formData) {
                        $('.file-size-error').hide();

                        dzSending(this, file, formData, '{!! csrf_token() !!}');
                    },
                    maxfilesexceeded: function (file) {
                        dz_var15671147011688676454ble[0].mockFile = '';
                        dzMaxfile(this, file);
                    },
                    complete: function (file) {
                        dzComplete(this, file, var15671147011688676454ble, dz_var15671147011688676454ble[0].mockFile);
                        dz_var15671147011688676454ble[0].mockFile = file;
                    },
                    removedfile: function (file) {
                        dzRemoveFile(
                            file, var15671147011688676454ble, '{!! url("admin/markets/remove-media") !!}',
                            'image', '{!! isset($market) ? $market->id : 0 !!}', '{!! url("admin/uploads/clear") !!}', '{!! csrf_token() !!}'
                        );
                    }
                });
            dz_var15671147011688676454ble[0].mockFile = var15671147011688676454ble;
            dropzoneFields['image'] = dz_var15671147011688676454ble;
        </script>
    @endprepend

    <!-- Information Field -->
    <div class="form-group">
        {!! Form::label('information', trans("lang.market_information"), ['class' => 'control-label text-right']) !!}
        <div>
            {!! Form::textarea('information', null, ['class' => 'form-control','placeholder'=>
             trans("lang.market_information_placeholder")  ]) !!}
            <div class="form-text text-muted">{{ trans("lang.market_information_help") }}</div>
        </div>
    </div>

    <hr class="mt-5">

    <h4 class="mb-4">Métodos de Pagamento</h4>

    <!-- 'Boolean Featured Field' -->
    <div class="form-group">
        <div class="checkbox icheck">
            <label class="ml-2 form-check-inline">
                {!! Form::hidden('cash_on_delivery_method', 0) !!}
                {!! Form::checkbox('cash_on_delivery_method', 1, null) !!}
            </label>
            {{ trans("lang.cash_on_delivery") }}
        </div>
    </div>
    <!-- 'Boolean Featured Field' -->
    <div class="form-group">
        <div class="checkbox icheck">
            <label class="ml-2 form-check-inline">
                {!! Form::hidden('card_on_delivery_method', 0) !!}
                {!! Form::checkbox('card_on_delivery_method', 1, null) !!}
            </label>
            {{ trans("lang.card_on_delivery") }}
        </div>
    </div>
    <!-- 'Boolean Featured Field' -->
    <div class="form-group">
        <div class="checkbox icheck">
            <label class="ml-2 form-check-inline">
                {!! Form::hidden('local_method', 0) !!}
                {!! Form::checkbox('local_method', 1, null) !!}
            </label>
            {{ trans("lang.local_method") }}
        </div>
    </div>
    <!-- 'Boolean Featured Field' -->
    <div class="form-group">
        <div class="checkbox icheck">
            <label class="ml-2 form-check-inline">
                {!! Form::hidden('mercado_pago_method', 0) !!}
                {!! Form::checkbox('mercado_pago_method', 1, null) !!}
            </label>
            {{ trans("lang.mercado_pago") }}
        </div>
    </div>


    <div class="form-group mt-2">
        {!! Form::label('mercado_pago_token', trans("lang.mercado_pago_token"), ['class' => 'control-label text-right']) !!}
        <div>
            {!! Form::text('mercado_pago_token', null,  ['class' => 'form-control','placeholder'=>  trans("lang.mercado_pago_token_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.mercado_pago_token_help") }} <a href="https://www.mercadopago.com.br/developers/panel/credentials" target="_blank">aqui</a>.
            </div>
        </div>
    </div>

    <hr class="mt-5">

    <h4 class="mb-4">Horários de funcionamento</h4>

    <div class="hours-div">
        <div class="form-group">
            <label>Tipo</label>
            <select class="form-control hour_type_select" name="open_hours_type" onchange="changeHourType(this)">
                <option value="fixed" {{ isset($market) && $market->early_open_hour != '00:00:00' ? 'selected' : '' }}>Fixo</option>
                <option value="custom" {{ isset($market) && $market->early_open_hour == '00:00:00' ? 'selected' : '' }}>Personalizado</option>
            </select>
        </div>

        <div class="fixed-hours">
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Mercado abre às</label>
                        <input type="text" class="form-control hour-mask" name="early_open_hour" value="{{ isset($market) && $market->early_open_hour }}" placeholder="Horário do abertura do mercado">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Mercado fecha às</label>
                        <input type="text" class="form-control hour-mask" name="late_open_hour" value="{{ isset($market) && $market->late_open_hour }}" placeholder="Horário de fechamento do mercado">
                    </div>
                </div>
            </div>
        </div>

        <div class="custom-hours" style="display:none">
            <div class="form-group">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Dia</th>
                            <th>Abre às</th>
                            <th>Fecha às</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Domingo</td>
                            <td>{!! Form::text('open_hours[0][start]', (isset($open_hours[0]) && $open_hours[0]['start'] != '00:00:00') ? $open_hours[0]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[0][end]', (isset($open_hours[0]) && $open_hours[0]['end'] != '00:00:00') ? $open_hours[0]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                        <tr>
                            <td>Segunda</td>
                            <td>{!! Form::text('open_hours[1][start]', (isset($open_hours[1]) && $open_hours[1]['start'] != '00:00:00') ? $open_hours[1]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[1][end]', (isset($open_hours[1]) && $open_hours[1]['end'] != '00:00:00') ? $open_hours[1]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                        <tr>
                            <td>Terça</td>
                            <td>{!! Form::text('open_hours[2][start]', (isset($open_hours[2]) && $open_hours[2]['start'] != '00:00:00') ? $open_hours[2]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[2][end]', (isset($open_hours[2]) && $open_hours[2]['end'] != '00:00:00') ? $open_hours[2]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                        <tr>
                            <td>Quarta</td>
                            <td>{!! Form::text('open_hours[3][start]', (isset($open_hours[3]) && $open_hours[3]['start'] != '00:00:00') ? $open_hours[3]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[3][end]', (isset($open_hours[3]) && $open_hours[3]['end'] != '00:00:00') ? $open_hours[3]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                        <tr>
                            <td>Quinta</td>
                            <td>{!! Form::text('open_hours[4][start]', (isset($open_hours[4]) && $open_hours[4]['start'] != '00:00:00') ? $open_hours[4]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[4][end]', (isset($open_hours[4]) && $open_hours[4]['end'] != '00:00:00') ? $open_hours[4]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                        <tr>
                            <td>Sexta</td>
                            <td>{!! Form::text('open_hours[5][start]', (isset($open_hours[5]) && $open_hours[5]['start'] != '00:00:00') ? $open_hours[5]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[5][end]', (isset($open_hours[5]) && $open_hours[5]['end'] != '00:00:00') ? $open_hours[5]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                        <tr>
                            <td>Sábado</td>
                            <td>{!! Form::text('open_hours[6][start]', (isset($open_hours[6]) && $open_hours[6]['start'] != '00:00:00') ? $open_hours[6]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                            <td>{!! Form::text('open_hours[6][end]', (isset($open_hours[6]) && $open_hours[6]['end'] != '00:00:00') ? $open_hours[6]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <hr class="mt-5">

    <h4 class="mb-4">Horários de entrega</h4>

    <div class="hours-div">
        <div class="form-group">
            <label>Tipo</label>
            <select class="form-control hour_type_select" name="delivery_hours_type" onchange="changeHourType(this)">
                <option value="fixed" {{ isset($market) && $market->early_delivery_hour != '00:00' ? 'selected' : '' }}>Fixo</option>
                <option value="custom" {{ isset($market) && $market->early_delivery_hour == '00:00' ? 'selected' : '' }}>Personalizado</option>
            </select>
        </div>

        <div class="fixed-hours">
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Entrega inicia às</label>
                        <input type="text" class="form-control hour-mask" name="early_delivery_hour" value="{{ isset($market) && $market->early_delivery_hour }}" placeholder="Horário do início das entregas">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Entrega finaliza às</label>
                        <input type="text" class="form-control hour-mask" name="late_delivery_hour" value="{{ isset($market) && $market->late_delivery_hour }}" placeholder="Horário do fim das entregas">
                    </div>
                </div>
            </div>
        </div>

        <div class="custom-hours" style="display:none">
            <div class="form-group">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Dia</th>
                        <th>Abre às</th>
                        <th>Fecha às</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Domingo</td>
                        <td>{!! Form::text('delivery_hours[0][start]', (isset($delivery_hours[0]) && $delivery_hours[0]['start'] != '00:00:00') ? $delivery_hours[0]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[0][end]', (isset($delivery_hours[0]) && $delivery_hours[0]['end'] != '00:00:00') ? $delivery_hours[0]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    <tr>
                        <td>Segunda</td>
                        <td>{!! Form::text('delivery_hours[1][start]', (isset($delivery_hours[1]) && $delivery_hours[1]['start'] != '00:00:00') ? $delivery_hours[1]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[1][end]', (isset($delivery_hours[1]) && $delivery_hours[1]['end'] != '00:00:00') ? $delivery_hours[1]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    <tr>
                        <td>Terça</td>
                        <td>{!! Form::text('delivery_hours[2][start]', (isset($delivery_hours[2]) && $delivery_hours[2]['start'] != '00:00:00') ? $delivery_hours[2]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[2][end]', (isset($delivery_hours[2]) && $delivery_hours[2]['end'] != '00:00:00') ? $delivery_hours[2]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    <tr>
                        <td>Quarta</td>
                        <td>{!! Form::text('delivery_hours[3][start]', (isset($delivery_hours[3]) && $delivery_hours[3]['start'] != '00:00:00') ? $delivery_hours[3]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[3][end]', (isset($delivery_hours[3]) && $delivery_hours[3]['end'] != '00:00:00') ? $delivery_hours[3]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    <tr>
                        <td>Quinta</td>
                        <td>{!! Form::text('delivery_hours[4][start]', (isset($delivery_hours[4]) && $delivery_hours[4]['start'] != '00:00:00') ? $delivery_hours[4]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[4][end]', (isset($delivery_hours[4]) && $delivery_hours[4]['end'] != '00:00:00') ? $delivery_hours[4]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    <tr>
                        <td>Sexta</td>
                        <td>{!! Form::text('delivery_hours[5][start]', (isset($delivery_hours[5]) && $delivery_hours[5]['start'] != '00:00:00') ? $delivery_hours[5]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[5][end]', (isset($delivery_hours[5]) && $delivery_hours[5]['end'] != '00:00:00') ? $delivery_hours[5]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    <tr>
                        <td>Sábado</td>
                        <td>{!! Form::text('delivery_hours[6][start]', (isset($delivery_hours[6]) && $delivery_hours[6]['start'] != '00:00:00') ? $delivery_hours[6]['start'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.early_delivery_hour_placeholder")]) !!}</td>
                        <td>{!! Form::text('delivery_hours[6][end]', (isset($delivery_hours[6]) && $delivery_hours[6]['end'] != '00:00:00') ? $delivery_hours[6]['end'] : null, ['class' => 'form-control hour-mask','step'=>'any', 'placeholder'=>  trans("lang.late_delivery_hour_placeholder")]) !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="d-block border-top mt-2">
        <label class="control-label mt-2">Selecione no mapa a localização exata de seu estabelecimento.</label>
    </div>
</div>
<div class="col-md-12">
    <div id="map" class="mb-2"></div>
</div>
<div class="col-6">
    <!-- Latitude Field -->
    <div class="form-group">
        {!! Form::text('latitude', null,  ['class' => 'form-control','id' => 'lat','placeholder'=>  trans("lang.market_latitude_placeholder"), 'readonly' => true]) !!}
        <div class="form-text text-muted">
            {{ trans("lang.market_latitude_help") }}
        </div>
    </div>
</div>
<div class="col-6">
    <!-- Longitude Field -->
    <div class="form-group">
        {!! Form::text('longitude', null,  ['class' => 'form-control','id' => 'lng','placeholder'=>  trans("lang.market_longitude_placeholder"), 'readonly' => true]) !!}
        <div class="form-text text-muted">
            {{ trans("lang.market_longitude_help") }}
        </div>
    </div>
</div>

@hasrole('admin')
<div class="col-12 custom-field-container">
    <h5 class="col-12 pb-4">{!! trans('lang.admin_area') !!}</h5>
    <div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
        <!-- Users Field -->
        <div class="form-group row ">
            {!! Form::label('users[]', trans("lang.market_users"),['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                {!! Form::select('users[]', $user, $usersSelected, ['class' => 'select2 form-control' , 'multiple'=>'multiple']) !!}
                <div class="form-text text-muted">{{ trans("lang.market_users_help") }}</div>
            </div>
        </div>
    </div>
    <div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
        <!-- fields Field -->
        <div class="form-group row ">
            {!! Form::label('fields[]', trans("lang.market_fields"),['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                {!! Form::select('fields[]', $field, $fieldsSelected, ['class' => 'select2 form-control' , 'multiple'=>'multiple']) !!}
                <div class="form-text text-muted">{{ trans("lang.market_fields_help") }}</div>
            </div>
        </div>

        <!-- admin_commission Field -->
        {{--<div class="form-group row ">
            {!! Form::label('admin_commission', trans("lang.market_admin_commission"), ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                {!! Form::number('admin_commission', null,  ['class' => 'form-control', 'step'=>'any', 'placeholder'=>  trans("lang.market_admin_commission_placeholder")]) !!}
                <div class="form-text text-muted">
                    {{ trans("lang.market_admin_commission_help") }}
                </div>
            </div>
        </div>--}}
    </div>
</div>
@endhasrole

@if($customFields)
    <div class="clearfix"></div>
    <div class="col-12 custom-field-container">
        <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
        {!! $customFields !!}
    </div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
    <button type="submit" class="btn btn-{{setting('theme_color')}}"><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.market')}}</button>
    <a href="{!! route('markets.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
