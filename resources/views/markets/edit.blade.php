@extends('layouts.app')
@push('css_lib')
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
<!-- select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
{{--dropzone--}}
<link rel="stylesheet" href="{{asset('plugins/dropzone/bootstrap.min.css')}}">

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBakbheodl_aPGBYGxhsnnWVFX5HuNM174"></script>
<script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>

<style>
  #map {
    width: 100%;
    height: 480px;
  }
</style>
@endpush
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{trans('lang.market_plural')}} <small>{{trans('lang.market_desc')}}</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{!! route('markets.index') !!}">{{trans('lang.market_plural')}}</a>
          </li>
          <li class="breadcrumb-item active">{{trans('lang.market_edit')}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
  <div class="clearfix"></div>
  @include('flash::message')
  @include('adminlte-templates::common.errors')
  <div class="clearfix"></div>
  <div class="card">
    <div class="card-header">
      <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
        @can('markets.index')
        <li class="nav-item">
          <a class="nav-link" href="{!! route('markets.index') !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.market_table')}}</a>
        </li>
        @endcan
        @can('markets.create')
        <li class="nav-item">
          <a class="nav-link" href="{!! route('markets.create') !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.market_create')}}</a>
        </li>
        @endcan
        <li class="nav-item">
          <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-pencil mr-2"></i>{{trans('lang.market_edit')}}</a>
        </li>
      </ul>
    </div>
    <div class="card-body">
      {!! Form::model($market, ['route' => ['markets.update', $market->id], 'method' => 'patch']) !!}
      <div class="row">
        @include('markets.fields')
      </div>
      {!! Form::close() !!}
      <div class="clearfix"></div>
    </div>
  </div>
</div>
@include('layouts.media_modal')
@endsection
@push('scripts_lib')
<!-- iCheck -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<!-- select2 -->
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
{{--dropzone--}}
<script src="{{asset('plugins/dropzone/dropzone.js')}}"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var dropzoneFields = [];

    var locationPicker = new locationPicker('map', {
      setCurrentPosition: true,
      lat: parseFloat({{ old('latitude', $market->latitude) }}),
      lng: parseFloat({{ old('longitude', $market->longitude) }})
    }, {
      zoom: 18
    });

    google.maps.event.addListener(locationPicker.map, 'idle', function (event) {
      // Get current location and show it in HTML
      var location = locationPicker.getMarkerPosition();

      $('#lat').val(location.lat)
      $('#lng').val(location.lng)
    });

    function setAddress(){
      let street = $("#street").val();
      let number = $("#number").val();
      let district = $("#district").val();
      let complement = $("#complement").val();
      let city = $("#city").val();
      let state = $("#state").val();
      let zipcode = $("#zipcode").val();

      $("#address").val(street + ', ' + number + ', ' + district + ' ' + city + '/' + state + ' '+ zipcode);
    }

    function getAddress(){
      setAddress();

      let endereco = $("#address").val();

      if(endereco != '' && endereco != null){
        let zoom = 18;
        endereco += ', Brasil';

        codeAddress(endereco, zoom);
      }else{
        return false;
      }
    }

    function codeAddress(address, zoom) {
      geocoder = new google.maps.Geocoder();
      geocoder.geocode({
        'address': address
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          locationPicker.map.setCenter({lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng()});
          locationPicker.map.setZoom(zoom);
        }
      });
    }
</script>
@endpush