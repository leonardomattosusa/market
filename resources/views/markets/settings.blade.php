@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{trans('lang.market_plural')}} <small>{{trans('lang.market_desc')}}</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('markets.index') !!}">{{trans('lang.market_plural')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{trans('lang.market_table')}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    @can('markets.index')
                        <li class="nav-item">
                            <a class="nav-link" href="{!! route('markets.index') !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.market_table')}}</a>
                        </li>
                    @endcan
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-pencil mr-2"></i>{{trans('lang.market_settings')}}</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <h4>Configurar código de barras da balança</h4>
                <div class="alert alert-info">Os códigos de barras gerados para produtos à granel são gerados pela balança conforme o peso do produto. Para que o iMarket consiga identificar corretamente o produto conforme o código de barras na hora da leitura de GTIN/EAN você precisa preencher as configurações abaixo conforme o código de barras gerado pelo sistema utilizado em seu mercado.</div>

                <div class="row">
                    @php
                        if($market->weight_based_gtin_settings){
                            $size = old('size', $market->weight_based_gtin_settings->size);
                            $initial_digit = old('initial_digit', $market->weight_based_gtin_settings->initial_digit);
                            $product_first_digit = old('product_first_digit', $market->weight_based_gtin_settings->product_first_digit);
                            $product_last_digit = old('product_last_digit', $market->weight_based_gtin_settings->product_last_digit);
                            $value_type = old('value_type', $market->weight_based_gtin_settings->value_type);
                            $value_decimal_places = old('value_decimal_places', $market->weight_based_gtin_settings->value_decimal_places);
                            $value_first_digit = old('value_first_digit', $market->weight_based_gtin_settings->value_first_digit);
                            $value_last_digit = old('value_last_digit', $market->weight_based_gtin_settings->value_last_digit);
                        }else{
                            $size = old('size', 13);
                            $initial_digit = old('initial_digit', 2);
                            $product_first_digit = old('product_first_digit', 2);
                            $product_last_digit = old('product_last_digit', 5);
                            $value_type = old('value_type', 'price');
                            $value_decimal_places = old('value_decimal_places', 2);
                            $value_first_digit = old('value_first_digit', 6);
                            $value_last_digit = old('value_last_digit', 12);
                        }
                    @endphp
                    <div class="col-lg-8 col-12">
                        <form method="POST" action="{{ route('markets.settings.weight_based_gtin.post', $market->id) }}">
                            @csrf
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" style="width: 20px; height:20px" name="use_weight_based_gtin" id="exampleCheck1" value="1" {{ $market->weight_based_gtin_settings ? 'checked' : '' }}>
                                <label class="form-check-label font-weight-bold mt-1 ml-2" for="exampleCheck1">Ler códigos de barras gerado pela balança</label>
                            </div>
                            <div class="form-group mt-2">
                                <input type="number" name="size" class="form-control d-inline" style="width: 60px" value="{{ $size }}" min="1" max="15" required> Tamanho total dos códigos de barras gerados pela balança.
                            </div>
                            <div class="form-group mt-4">
                                <input type="number" name="initial_digit" class="form-control d-inline" style="width: 60px" value="{{ $initial_digit }}" min="1" max="15" required> Dígito inicial dos códigos de barras gerados pela balança.
                            </div>
                            <div class="form-group mt-2">
                                O código do produto inicia no dígito
                                <input type="number" name="product_first_digit" class="form-control d-inline" style="width: 60px" value="{{ $product_first_digit }}" min="1" max="15" required>
                                e termina no dígito
                                <input type="number" name="product_last_digit" class="form-control d-inline" style="width: 60px" value="{{ $product_last_digit }}" min="1" max="15" required> do código de barras.
                            </div>
                            <div class="form-group mt-2">
                                O
                                <select name="value_type" class="form-control d-inline" style="width: 100px" required>
                                    <option value="price" {{ $value_type == 'price' ? 'selected' : '' }}>Valor</option>
                                    <option value="weight" {{ $value_type == 'weight' ? 'selected' : '' }}>Peso</option>
                                </select>
                                possui <input type="number" name="value_decimal_places" class="form-control d-inline" style="width: 60px" value="{{ $value_decimal_places }}" min="1" max="15" required>
                                casas decimais e inicia no dígito <input type="number" name="value_first_digit" class="form-control d-inline" style="width: 60px" value="{{ $value_first_digit }}" min="1" max="15" required>
                                e termina no dígito <input type="number" name="value_last_digit" class="form-control d-inline" style="width: 60px" value="{{ $value_last_digit }}" min="1" max="15" required> do código de barras.
                            </div>
                            <div class="form-group mt-2 text-center">
                                <button class="btn btn-primary">Confirmar alterações</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 col-12 align-items-center">
                        <p>Exemplo de produto à granel cadastrado com o código 70 e pesado na balança:</p>
                        <img src="{{ asset('assets/images/weight_gtin_example.png') }}">
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

