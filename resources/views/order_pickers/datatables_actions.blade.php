<div class='btn-group btn-group-sm'>
  {{--@can('order_pickers.show')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.view_details')}}" href="{{ route('order_pickers.show', $id) }}" class='btn btn-link'>
    <i class="fa fa-eye"></i>
  </a>
  @endcan--}}

  @can('order_pickers.edit')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.order_picker_edit')}}" href="{{ route('order_pickers.edit', $id) }}" class='btn btn-link'>
    <i class="fa fa-edit"></i>
  </a>
  @endcan

  @can('order_pickers.destroy')
{!! Form::open(['route' => ['order_pickers.destroy', $id], 'method' => 'delete']) !!}
  {!! Form::button('<i class="fa fa-trash"></i>', [
  'type' => 'submit',
  'class' => 'btn btn-link text-danger',
  'onclick' => "return confirm('Você tem certeza?')"
  ]) !!}
{!! Form::close() !!}
  @endcan
</div>
