@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
 # {{ $greeting }}
 @else
@if ($level == 'error')
# @lang('Ops!')
@else
# @lang('Olá!')
@endif
@endif

{{-- Intro Lines --}}
Você está recebendo este e-mail porque nós recebemos um pedido de redefinição de senha para sua conta.

{{-- Action Button --}}
@isset($actionText)
<?php
switch ($level) {
case 'success':
$color = 'green';
break;
case 'error':
$color = 'red';
break;
default:
$color = 'blue';
}
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
Redefinir a senha
@endcomponent
@endisset

{{-- Outro Lines --}}
O link de redefinição de senha vai expirar em 60 minutos. Caso você não tenha solicitado a redefinição de senha, ignore este e-mail.

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Atenciosamente'),<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
@lang(
"Se você estiver tendo problemas clicando no botão de resetar a senha, copie e cole a seguinte URL\n".
'no seu navegador: [:actionURL](:actionURL)',
[
'actionText' => 'Redefinir a senha',
'actionURL' => $actionUrl
]
)
@endcomponent
@endisset
@endcomponent
