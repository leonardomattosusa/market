<div class='btn-group btn-group-sm'>
  @if(!request()->segment(4) || request()->segment(4) != 'canceled')

    @can('orders.show')
    <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.view_details')}}" href="{{ route('orders.show', $id) }}" class='btn btn-link'>
      <i class="fa fa-eye"></i>
    </a>
    @endcan

    @can('orders.edit')
    <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.order_edit')}}" href="{{ route('orders.edit', $id) }}" class='btn btn-link'>
      <i class="fa fa-edit"></i>
    </a>
    @endcan

  @endif

  @can('orders.destroy')
    @if(request()->segment(4) && request()->segment(4) == 'pending')
      {!! Form::open(['route' => ['orders.destroy', $id], 'method' => 'delete']) !!}
        {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-link text-danger',
        'onclick' => "return confirm('Você tem certeza que deseja cancelar este pedido?')"
        ]) !!}
      {!! Form::close() !!}
    @elseif(request()->segment(4) == 'canceled')
        {!! Form::open(['route' => ['orders.restore', $id], 'method' => 'put']) !!}
        {!! Form::button('<i class="fa fa-undo"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-link text-success',
        'onclick' => "return confirm('Você tem certeza que deseja restaurar este pedido?')"
        ]) !!}
        {!! Form::close() !!}
    @endif
  @endcan
</div>
