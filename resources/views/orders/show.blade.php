@extends('layouts.app')

@push('css_lib')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
@endpush

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{trans('lang.order_plural')}} <small>{{trans('lang.order_desc')}}</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('orders.index') !!}">{{trans('lang.order_plural')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{trans('lang.order')}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="card">
            <div class="card-header d-print-none">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route('orders.index') !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.order_table')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.order')}}</a>
                    </li>
                    <div class="ml-auto d-inline-flex">
                        <li class="nav-item">
                            <a class="nav-link pt-1" id="printOrder" href="#"><i class="fa fa-print"></i> {{trans('lang.print')}}</a>
                        </li>
                    </div>
                    @if($order->order_status_id == 3)
                    <div class="d-inline-flex">
                        <li class="nav-item mb-1 ml-2">
                            <i class="fas fa-clock"></i> <span id="clock"><i class="fas fa-spin fa-spinner"></i></span>
                        </li>
                    </div>
                    @endif
                </ul>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('orders.update_quantities', $order->id) }}" onkeydown="return event.key != 'Enter';">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        @include('orders.show_fields')
                    </div>
                    @if($order->order_status_id == 2)
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>A listagem de produtos do pedido ficará disponível ao iniciar o preparo do pedido.</td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        @if($order->order_status_id == 3)
                            <div class="float-right mb-2">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-add-product" onclick="addProductOrder()"><i class="fas fa-plus"></i> Adicionar produto</button>
                            </div>
                        @endif

                        @include('product_orders.table')
                    @endif
                    <div class="row">
                        <div class="col-5 offset-7">
                            <div class="table-responsive table-light">
                                <table class="table">
                                    <tbody>
                                    @if($change > 0)
                                        <tr>
                                            <th class="text-right text-danger">{{trans('lang.order_delivery_change')}}</th>
                                            <td>{!! getPrice($change) !!}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th class="text-right">{{trans('lang.order_subtotal')}}</th>
                                        <td>{!! getPrice($subtotal) !!}</td>
                                    </tr>
                                    {{--<tr>
                                      <th class="text-right">{{trans('lang.order_tax')}} ({!!$order->tax!!}%) </th>
                                      <td>{!! getPrice($subtotal * $order->tax/100)!!}</td>
                                    </tr>--}}
                                    <tr>
                                        <th class="text-right">{{trans('lang.order_delivery_fee')}}</th>
                                        <td>{!! getPrice($order['delivery_fee'])!!}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{trans('lang.paid_with_fr_credit')}}</th>
                                        <td>{!! getPrice($partial_payments_amount)!!}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{trans('lang.order_total')}}</th>
                                        <td>{!!getPrice($total)!!}</td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="modal fade" id="modal-confirm-weight-changes" role="dialog" tabindex="-1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Confirmar a alteração dos pesos</h3>
                                </div>
                                <div class="modal-body">
                                    <p>Você tem certeza que deseja confirmar que os pesos dos <span class="text-warning">produtos baseados em peso</span> foram revisados e alterados corretamente? Após a confirmação o preço final será gerado ao cliente e você não poderá mais alterar o peso.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button class="btn btn-success"><i class="fas fa-check-square"></i> Confirmar alteração dos pesos</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row d-print-none">
                    <!-- Back Field -->
                    <div class="form-group col-12">
                        {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete', 'class' => 'd-inline']) !!}
                            {!! Form::button('<i class="fa fa-trash"></i> Cancelar pedido', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger',
                            'onclick' => "return confirm('Você tem certeza que deseja excluir este pedido?')"
                            ]) !!}
                        {!! Form::close() !!}
                        <div class="float-right">
                            @if($order->order_status_id == 2)
                                <a href="#!" data-toggle="modal" data-target="#modal-prepare-order" class="btn btn-info"><i class="fa fa-check"></i> {{trans('lang.prepare_order')}}</a>
                                <a href="{!! route('orders.list',['pending']) !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.back')}}</a>
                            @endif
                            @if($order->order_status_id > 2)
                                @if($order->productOrders->where('final_quantity_defined', 0)->count() > 0)
                                    <button type="button" data-toggle="modal" data-target="#modal-confirm-weight-changes" class="btn btn-success"><i class="fa fa-check-square"></i> Confirmar pesos e quantidades</button>
                                @endif
                                @if($order->payment->status != 'Pago')
                                    <a href="{!! route('orders.mark_as_paid', [$order->id]) !!}" class="btn btn-success"><i class="fa fa-money"></i> {{trans('lang.mark_as_paid')}}</a>
                                @endif
                            @endif
                            @if($order->order_status_id == 3)
                                @if($order->payment->method == 'Pagar na retirada')
                                    <a href="{!! route('orders.change_status',[$order->id, 4]) !!}" class="btn btn-info"><i class="fa fa-check"></i> {{trans('lang.ready_to_collect')}}</a>
                                @else
                                    <a href="{!! route('orders.change_status',[$order->id, 4]) !!}" class="btn btn-info"><i class="fa fa-check"></i> {{trans('lang.ready_to_deliver')}}</a>
                                @endif
                                <a href="{!! route('orders.list',['processing']) !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.back')}}</a>
                            @endif
                            @if($order->order_status_id == 4)
                                @if($order->payment->method == 'Pagar na retirada')
                                    <a href="{!! route('orders.list',['ready_to_collect']) !!}" class="btn btn-info"><i class="fa fa-check"></i> {{trans('lang.goto_ready_to_collect_list')}}</a>
                                @else
                                    <a href="{!! route('orders.list',['ready_to_delivery']) !!}" class="btn btn-info"><i class="fa fa-check"></i> {{trans('lang.goto_ready_to_delivery_list')}}</a>
                                @endif
                            @endif
                            @if($order->order_status_id == 5)
                                <a href="{!! route('orders.list',['sent']) !!}" class="btn btn-info"><i class="fa fa-check"></i> {{trans('lang.goto_sent_list')}}</a>
                            @endif
                            @if($order->order_status_id == 6)
                                <a href="{!! route('orders.list',['delivered']) !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.back')}}</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-prepare-order" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="GET" action="{!! route('orders.change_status',[$order->id, 3]) !!}">
                    <div class="modal-header">
                        <h3 class="modal-title">Estimativa de tempo para a entrega</h3>
                    </div>
                    <div class="modal-body">
                        <p>Escolha a estimativa de tempo que o pedido demorará para ser ENTREGUE ao cliente final. Esta estimativa será notificada ao cliente.</p>
                        <select class="form-control" name="delivery_minutes" id="delivery_minutes">
                            <option value="10">10 Minutos</option>
                            <option value="20">20 Minutos</option>
                            <option value="30">30 Minutos</option>
                            <option value="40">40 Minutos</option>
                            <option value="50">50 Minutos</option>
                            <option value="60">60 Minutos</option>
                            <option value="70">70 Minutos</option>
                            <option value="80">80 Minutos</option>
                            <option value="90">90 Minutos</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button class="btn btn-success"><i class="fas fa-check"></i> Iniciar preparo do pedido</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if($order->order_status_id == 3)
    <div class="modal fade" id="modal-add-product" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form method="POST" action="/admin/orders/{{ $order->id }}/add_product" id="add-product-form">
                    @method('POST')
                    @csrf
                    <div class="modal-header">
                        <h3 class="modal-title">Adicionar produto ao pedido </h3>
                    </div>
                    <div class="modal-body">
                        <p>Escolha um produto para ser adicionado ao pedido. O valor do pedido será atualizado para a nova listagem de produtos. A negociação de valores adicionais no pedido não poderá ser realizada através do iMarket e deverá ser realizada diretamente com o cliente final.</p>
                        <div class="row">
                            <div class="col-lg-10 col-8">
                                <div class="form-group">
                                    <label class="control-label">Escolha um produto</label>
                                    <select name="product_id" class="select_products form-control" name="product_id">
                                        <option value="">Selecione um produto</option>
                                        <option value="1">Teste</option>
                                        <option value="2">Teste2</option>
                                        <option value="3">Teste3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-4">
                                <div class="form-group">
                                    <label class="control-label" id="add-product-quantity-label">Qtd</label>
                                    <input type="number" class="form-control" name="quantity" id="add-product-quantity-input" placeholder="Qtd" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button class="btn btn-success"><i class="fas fa-plus"></i> Adicionar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-remove-product" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="/admin/orders/{{ $order->id }}/remove_product/" id="remove-product-form">
                    @csrf
                    <div class="modal-header">
                        <h3 class="modal-title">Remover produto do pedido</h3>
                    </div>
                    <div class="modal-body">
                        <p>Você tem certeza que deseja remover o produto deste pedido? O valor do pedido será atualizado para a nova listagem de produtos caso este produto seja removido. A negociação da mudança de valores no pedido não poderá ser realizada através do iMarket e deverá ser realizada diretamente com o cliente final.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button class="btn btn-danger"><i class="fas fa-times"></i> Remover</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
@endsection

@push('scripts')
    @if($order->order_status_id == 3)
        <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js" integrity="sha512-rmZcZsyhe0/MAjquhTgiUcb4d9knaFc7b5xAfju483gbEXTkeJRUMIPk6s3ySZMYUHEcjKbjLjyddGWMrNEvZg==" crossorigin="anonymous"></script>
        <script>
            var preparing_start_date = moment('{{ $order->preparing_start_date }}');

            window.setInterval(function(){
                let now = moment();

                const diff = now.diff(preparing_start_date);
                const diffDuration = moment.duration(diff);

                let hours = diffDuration.hours().toString();
                let minutes = diffDuration.minutes().toString();
                let seconds = diffDuration.seconds().toString();

                $("#clock").html(hours.padStart(2,'0')+':'+minutes.padStart(2,'0')+':'+seconds.padStart(2,'0'));
            }, 1000);

            var market_id = {!! $order->productOrders[0]->product->market_id !!};

            $('.select_products').select2({
                dropdownParent: $('#modal-add-product'),
                ajax: {
                    url: '/api/products',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            market_id,
                            search: 'gtin_ean:'+params.term+';name:'+params.term,
                            searchFields: 'gtin_ean:like;name:like',
                            searchJoin: 'or',
                            orderBy: 'name',
                            limit: 20
                        }

                        return query;
                    },
                    processResults: function (response) {
                        let data = [];
                        $.each(response.data, function(index, product){
                            data.push({
                                id : product.id,
                                text : product.name
                            });
                        });

                        return {
                            results: data
                        };
                    }
                }
            });

            function addProductOrder(){
                $("#add-product-form").attr('action', '/admin/orders/'+order_id+'/add_product');
            }

            function removeProductOrder(obj){
                let product_order_id = $(obj).attr('product_order_id');

                $("#remove-product-form").attr('action', '/admin/orders/'+order_id+'/remove_product/'+product_order_id);
            }

            $('#modal-add-product').on("change", ".select_products", function(){
                let product_id = this.value;

                $.ajax({
                    url: '/api/products/'+product_id,
                    method: 'GET',
                    success: function(response){
                        let product = response.data;
                        let quantity = $("add-product-quantity-input").val();

                        if(product.weight_based == 1){
                            $("#add-product-quantity-label").text('Peso (g)');
                            $("#add-product-quantity-input").attr('placeholder', 'Peso (g)');
                            $("#add-product-quantity-input").val(100);
                        }else{
                            $("#add-product-quantity-label").text('Qtd');
                            $("#add-product-quantity-input").attr('placeholder', 'Qtd');
                            $("#add-product-quantity-input").val(1);
                        }
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });
        </script>
    @endif

    <script type="text/javascript">
        $("#printOrder").on("click",function () {
            window.print();
        });

        $.fn.focusNextInputField = function() {
            return this.each(function() {
                var fields = $(this).parents('form:eq(0),body').find(':input').not('[type=hidden]');
                var index = fields.index( this );
                if ( index > -1 && ( index + 1 ) < fields.length ) {
                    fields.eq( index + 1 ).focus();
                }
                return false;
            });
        };

        window.setTimeout(function(){
            $('[tooltip="true"]').tooltip({boundary: 'window'});
        }, 1000)

        var order_id = {!! $order->id !!};

        function gtinEanKeyPress(event, obj){
            if(event.keyCode == 13){
                checkGtinEan($(obj));
            }
        }

        function gtinEanButtonClick(obj){
            let input = $(obj).parent().parent().find('input:first');

            checkGtinEan($(input));
        }

        function checkGtinEan(obj){
            let product_id = parseInt($(obj).attr('product_id'));
            let gtin_ean = $(obj).val();
            let button = $(obj).parent().find('button');

            $.ajax({
                url: '{{ url("api/orders/check_gtin_ean") }}' + '/{!! $order->id !!}',
                method: 'PUT',
                data: {product_id : product_id, gtin_ean : gtin_ean},
                beforeSend: function(){
                    $(button).html('<i class="fa fa-spin fa-spinner"></i>');
                    $(button).attr('disabled', true);
                },
                success: function(response){
                    $(button).removeAttr('disabled');
                    $(button).html('<i class="fa fa-check"></i>');

                    if(response.success){
                        if(response.data.quantity){
                            var input = $(obj).closest('tr').find('input:first');
                            if(response.data.product.weight_based == 1){
                                $(input).val(response.data.quantity * 1000);
                            }else{
                                $(input).val(response.data.quantity);
                            }

                            autoUpdatePrice($(input));
                        }
                        $(obj).attr('readonly', true);
                        $(obj).removeClass('bg-danger');
                        $(obj).addClass('bg-success text-white');
                        $(obj).parent().find('button').removeClass('btn-danger');
                        $(obj).parent().find('button').addClass('btn-success');
                        $(obj).removeAttr('onkeyup');
                        $(obj).parent().find('.input-group-append').remove();
                        $(obj).focusNextInputField();
                    }else{
                        $(obj).select();
                        $(obj).addClass('bg-danger text-white');
                        $(obj).parent().find('button').removeClass('btn-success');
                        $(obj).parent().find('button').addClass('btn-danger');
                    }
                },
                error: function(response){
                    console.log(response)
                    $(button).removeAttr('disabled');
                    $(button).html('<i class="fa fa-check"></i>');

                    if(response.responseJSON.success){
                        if(response.data.quantity){
                            var input = $(obj).closest('tr').find('input:first');
                            if(response.data.product.weight_based == 1){
                                $(input).val(response.data.quantity * 1000);
                            }else{
                                $(input).val(response.data.quantity);
                            }

                            autoUpdatePrice($(input));
                        }
                        $(obj).attr('readonly', true);
                        $(obj).removeClass('bg-danger');
                        $(obj).addClass('bg-success text-white');
                        $(obj).parent().find('button').removeClass('btn-danger');
                        $(obj).parent().find('button').addClass('btn-success');
                        $(obj).removeAttr('onkeyup');
                        $(obj).parent().find('.input-group-append').remove();
                        $(obj).focusNextInputField();
                    }else{
                        $(obj).select();
                        $(obj).addClass('bg-danger text-white');
                        $(obj).parent().find('button').removeClass('btn-success');
                        $(obj).parent().find('button').addClass('btn-danger');
                    }
                }
            });
        }

        function autoUpdatePrice(input){
            let quantity_type = $(input).attr('qty_type');

            let price = $(input).attr('price');
            let quantity = (quantity_type == 'weight') ? ($(input).val() / 1000) : $(input).val();

            let new_price = price * quantity;

            $(input).closest('tr').find('.product-price-div').html(new_price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));
        }
    </script>
@endpush
