@php
if(request()->segment(4) == 'ready_to_delivery'){
    $route = url('admin/orders/mark_as_sent');
}else{
    $route = url('admin/orders/mark_as_delivered');
}
@endphp
<form method="POST" action="{{ $route }}">
    @csrf
    <table class="table">
        <thead>
            <tr>
                @if(request()->segment(4) != 'ready_to_collect')
                    <th><input type="checkbox" id="mark_all"></th>
                @endif
                <th>ID</th>
                <th>Agendamento</th>
                <th>Cliente</th>
                <th>Status</th>
                <th>Método</th>
                <th>Pagamento</th>
                @if(request()->segment(4) == 'sent')
                    <th>Entregador</th>
                @endif
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @forelse($orders as $order)
                <tr>
                    @if(request()->segment(4) != 'ready_to_collect')
                        <td><input type="checkbox" class="order_checkbox" name="orders[]" value="{{ $order->id }}"></td>
                    @endif
                    <td>{{ $order->id }}</td>
                    <td>{{ ($order->scheduled_delivery_data) ? date('d/m/Y H:i', strtotime($order->scheduled_delivery_data)) : 'Agora' }}</td>
                    <td>{{ $order->user->name }}</td>
                    <td>{{ $order->orderStatus->status }}</td>
                    <td>{{ $order->payment->method }}</td>
                    <td>{!! getPayment($order->payment,'status') !!}</td>
                    @if(request()->segment(4) == 'sent')
                        <td>{{ $order->driver ? $order->driver->name : '' }}</td>
                    @endif
                    <td>
                        <a href="{{ route('orders.show', $order->id) }}" class="btn bnt-primary btn-sm" tooltip="true" title="Detalhes"><i class="fas fa-eye"></i></a>
                        @if(request()->segment(4) == 'ready_to_collect')
                            <a href="{{ route('orders.deliver', $order->id) }}" class="btn bnt-primary btn-sm" tooltip="true" title="Confirmar entrega"><i class="fas fa-box"></i></a>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    @if(request()->segment(4) == 'sent')
                        <td colspan="8">Nenhum pedido disponível.</td>
                    @else
                        <td colspan="7">Nenhum pedido disponível.</td>
                    @endif
                </tr>
            @endforelse
        </tbody>
    </table>

    <div class="modal fade" id="choose-driver-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Escolher o entregador</h3>
                </div>
                <div class="modal-body">
                    @if($drivers->count() <= 0)
                        <p>Você não tem nenhum entregador cadastrado ainda. Efetue o cadastro de um entregador para confirmar o envio dos pedidos.</p>
                    @else
                        <p>Selecione abaixo qual o entregador que fará esta entrega.</p>
                        <select name="driver_id" class="form-control" required>
                            @foreach($drivers as $driver)
                                <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bnt-secondary" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    @if($orders->count() > 0)
        @if(request()->segment(4) == 'ready_to_delivery')
            <button type="button" class="btn btn-primary mt-4 float-right" data-toggle="modal" data-target="#choose-driver-modal">
                Confirmar envio dos pedidos marcados
            </button>
        @elseif(request()->segment(4) == 'sent')
            <button class="btn btn-primary mt-4 float-right">
                Confirmar entrega dos pedidos marcados
            </button>
        @endif
    @endif
</form>

@prepend('scripts')
    <script>
        $("#mark_all").on('change', function(){
           if($(this).prop('checked')){
               $(".order_checkbox").prop('checked', true);
           }else{
               $(".order_checkbox").prop('checked', false);
           }
        });

        $('[tooltip="true"]').tooltip({boundary: 'window'});
    </script>
@endprepend