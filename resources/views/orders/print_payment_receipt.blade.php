<html>
    <head>
        <title>Comprovante de pagamento</title>
        <style>
            .text-center {
                text-align: center;
            }
            .ttu {
                text-transform: uppercase;
            }
            .printer-ticket {
                display: table !important;
                width: 100%;
                max-width: 400px;
                font-weight: light;
                line-height: 1.3em;
            }
            .printer-ticket,
            .printer-ticket * {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 10px;
            }
            .printer-ticket th:nth-child(2),
            .printer-ticket td:nth-child(2) {
                width: 50px;
            }
            .printer-ticket th:nth-child(3),
            .printer-ticket td:nth-child(3) {
                width: 90px;
                text-align: right;
            }
            .printer-ticket th {
                font-weight: inherit;
                padding: 10px 0;
                text-align: center;
                border-bottom: 1px dashed #bcbcbc;
            }
            .printer-ticket tbody tr:last-child td {
                padding-bottom: 10px;
            }
            .printer-ticket tfoot .sup td {
                padding: 10px 0;
                border-top: 1px dashed #bcbcbc;
            }
            .printer-ticket tfoot .sup.p--0 td {
                padding-bottom: 0;
            }
            .printer-ticket .title {
                font-size: 1.5em;
                padding: 15px 0;
            }
            .printer-ticket .top td {
                padding-top: 10px;
            }
            .printer-ticket .last td {
                padding-bottom: 10px;
            }

        </style>
    </head>
    <body>
    @php
        function mask($val, $mask) {
            $maskared = '';
            $k = 0;
            for($i = 0; $i<=strlen($mask)-1; $i++) {
                if($mask[$i] == '#') {
                    if(isset($val[$k])) $maskared .= $val[$k++];
                } else {
                    if(isset($mask[$i])) $maskared .= $mask[$i];
                }
            }
            return $maskared;
        }

        $cpf = $order->user->document ? mask($order->user->document, '###.###.###-##') : '';
    @endphp
    <table class="printer-ticket">
        <thead>
        <tr>
            <th class="title" colspan="3">{{ $order->productOrders[0]->product->market->name }}</th>
        </tr>
        <tr>
            <th colspan="3">{{ date('d/m/Y - H:i:s', strtotime($order->payment->payment_date)) }}</th>
        </tr>
        <tr>
            <th colspan="3">
                {{ $order->user->name }} <br />
                {{ $cpf }}
            </th>
        </tr>
        <tr>
            <th class="ttu" colspan="3">
                <b>Comprovante de Pagamento</b>
            </th>
        </tr>
        </thead>
        {{--<tbody>
        <tr class="top">
            <td colspan="3">Doce de brigadeiro</td>
        </tr>
        <tr>
            <td>R$7,99</td>
            <td>2.0</td>
            <td>R$15,98</td>
        </tr>
        <tr>
            <td colspan="3">Opcional Adicicional: grande</td>
        </tr>
        <tr>
            <td>R$0,33</td>
            <td>2.0</td>
            <td>R$0,66</td>
        </tr>
        </tbody>--}}
        <tfoot>
        <tr class="ttu p--0">
            <td colspan="3">
                <b>Totais</b>
            </td>
        </tr>
        <tr class="ttu">
            <td colspan="2">Sub-total</td>
            <td align="right">R${{ number_format($order->payment->price - $order->delivery_fee,2,',','.') }}</td>
        </tr>
        <tr class="ttu">
            <td colspan="2">Frete</td>
            <td align="right">R${{ number_format($order->delivery_fee,2,',','.') }}</td>
        </tr>
        <tr class="ttu">
            <td colspan="2">Total</td>
            <td align="right">R${{ number_format($order->payment->price,2,',','.') }}</td>
        </tr>
        <tr class="sup ttu p--0">
            <td colspan="3">
                <b>Pagamentos</b>
            </td>
        </tr>
        <tr class="ttu">
            <td colspan="2">Saldo FR</td>
            <td align="right">R${{ number_format($order->payment->price,2,',','.') }}</td>
        </tr>
        <tr class="sup">
            <td colspan="3" align="center">
                <b>Pedido: #{{ str_pad($order->id, 10, "0", STR_PAD_LEFT) }}</b>
            </td>
        </tr>
        {{--<tr class="sup">
            <td colspan="3" align="center">
                imarket.digital
            </td>
        </tr>--}}
        </tfoot>
    </table>

    <script>
        window.print();
    </script>
    </body>
</html>