<div class="card {{ Request::is('admin/users*') || Request::is('admin/settings/permissions*') || Request::is('admin/settings/roles*') ? '' : 'collapsed-card' }}">
    <div class="card-header">
        <h3 class="card-title">{{trans('lang.permission_menu')}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa {{ Request::is('admin/settings/users*') || Request::is('admin/settings/permissions*') || Request::is('admin/settings/roles*') ? 'fa-minus' : 'fa-plus' }}"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="{!! route('permissions.index') !!}" class="nav-link {{  Request::is('admin/settings/permissions*') ? 'selected' : '' }}">
                    <i class="fa fa-inbox"></i> {{trans('lang.permission_plural')}}
                </a>
            </li>
            <li class="nav-item">
                <a href="{!! route('roles.index') !!}" class="nav-link {{  Request::is('admin/settings/roles*') ? 'selected' : '' }}">
                    <i class="fa fa-inbox"></i> {{trans('lang.role_plural')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! route('users.index') !!}" class="nav-link {{  Request::is('admin/users*') ? 'selected' : '' }}">
                    <i class="fa fa-users"></i> {{trans('lang.user_plural')}}
                </a>
            </li>

        </ul>
    </div>
</div>

<div class="card {{
             Request::is('admin/settings/app/*') ||
             Request::is('admin/settings/mail*') ||
             Request::is('admin/settings/translation*') ||
             Request::is('admin/settings/payment*') ||
             Request::is('admin/settings/currencies*') ||
             Request::is('admin/settings/customFields*')
 ? '' : 'collapsed-card' }}">
    <div class="card-header">
        <h3 class="card-title">{{trans('lang.app_setting_globals')}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa {{
             Request::is('admin/settings/app/*') ||
             Request::is('admin/settings/mail*') ||
             Request::is('admin/settings/translation*') ||
             Request::is('admin/settings/payment*') ||
             Request::is('admin/settings/currencies*') ||
             Request::is('admin/settings/customFields*')
             ? 'fa-minus' : 'fa-plus' }}"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="{!! url('admin/settings/app/globals') !!}" class="nav-link {{  Request::is('admin/settings/app/globals*') ? 'selected' : '' }}">
                    <i class="fa fa-cog"></i> {{trans('lang.app_setting_globals')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/app/localisation') !!}" class="nav-link {{  Request::is('admin/settings/app/localisation*') ? 'selected' : '' }}">
                    <i class="fa fa-language"></i> {{trans('lang.app_setting_localisation')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/app/social') !!}" class="nav-link {{  Request::is('admin/settings/app/social*') ? 'selected' : '' }}">
                    <i class="fa fa-globe"></i> {{trans('lang.app_setting_social')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/payment/payment') !!}" class="nav-link {{  Request::is('admin/settings/payment*') ? 'selected' : '' }}">
                    <i class="fa fa-credit-card"></i> {{trans('lang.app_setting_payment')}}
                </a>
            </li>

            @can('currencies.index')
                <li class="nav-item">
                    <a href="{!! route('currencies.index') !!}" class="nav-link {{ Request::is('admin/settings/currencies*') ? 'selected' : '' }}" ><i class="nav-icon fa fa-dollar ml-1"></i> {{trans('lang.currency_plural')}}</a>
                </li>
            @endcan

            <li class="nav-item">
                <a href="{!! url('admin/settings/app/notifications') !!}" class="nav-link {{  Request::is('admin/settings/app/notifications*') || Request::is('admin/notificationTypes*') ? 'selected' : '' }}">
                    <i class="fa fa-bell"></i> {{trans('lang.app_setting_notifications')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/mail/smtp') !!}" class="nav-link {{ Request::is('admin/settings/mail*') ? 'selected' : '' }}">
                    <i class="fa fa-envelope"></i> {{trans('lang.app_setting_mail')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/translation/en') !!}" class="nav-link {{ Request::is('admin/settings/translation*') ? 'selected' : '' }}">
                    <i class="fa fa-language"></i> {{trans('lang.app_setting_translation')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! route('customFields.index') !!}" class="nav-link {{ Request::is('admin/settings/customFields*') ? 'selected' : '' }}">
                    <i class="fa fa-list"></i> {{trans('lang.custom_field_plural')}}
                </a>
            </li>

        </ul>
    </div>
</div>


<div class="card {{ Request::is('admin/settings/mobile*') ? '' : 'collapsed-card' }}">
    <div class="card-header">
        <h3 class="card-title">{{trans('lang.mobile_menu')}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa {{ Request::is('admin/settings/mobile*') ? 'fa-minus' : 'fa-plus' }}"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="{!! url('admin/settings/mobile/globals') !!}" class="nav-link {{  Request::is('admin/settings/mobile/globals*') ? 'selected' : '' }}">
                    <i class="fa fa-inbox"></i> {{trans('lang.mobile_globals')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/mobile/colors') !!}" class="nav-link {{  Request::is('admin/settings/mobile/colors*') ? 'selected' : '' }}">
                    <i class="fa fa-inbox"></i> {{trans('lang.mobile_colors')}}
                </a>
            </li>

        </ul>
    </div>
</div>
