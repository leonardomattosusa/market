@can('dashboard')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/dashboard*') ? 'active' : '' }}" href="{!! url('admin/dashboard') !!}">
            @if($icons)<i class="nav-icon fa fa-dashboard"></i>@endif
            <p>{{trans('lang.dashboard')}}</p>
        </a>
    </li>
@endcan

@can('favorites.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/favorites*') ? 'active' : '' }}" href="{!! route('favorites.index') !!}">
            @if($icons)<i class="nav-icon fa fa-heart"></i>@endif
            <p>{{trans('lang.favorite_plural')}}</p>
        </a>
    </li>
@endcan

@can('fields.index', 'categories.index', 'proucts.index')
    <li class="nav-header">{{trans('lang.app_management')}}</li>
@endcan

@can('fields.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/fields*') ? 'active' : '' }}" href="{!! route('fields.index') !!}">
            @if($icons)<i class="nav-icon fa fa-tasks"></i>@endif
            <p>{{trans('lang.field_plural')}}</p>
        </a>
    </li>
@endcan

@can('categories.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/categories*') ? 'active' : '' }}" href="{!! route('categories.index') !!}">
            @if($icons)<i class="nav-icon fa fa-folder"></i>@endif
            <p>{{trans('lang.category_plural')}}</p>
        </a>
    </li>
@endcan

{{--@can('products.index')
    @can('optionGroups.index')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/optionGroups*') ? 'active' : '' }}" href="{!! route('optionGroups.index') !!}">@if($icons)<i class="nav-icon fa fa-plus-square"></i>@endif<p>{{trans('lang.option_group_plural')}}</p></a>
        </li>
    @endcan
@endcan--}}

<li class="nav-header">{{trans('lang.market_plural')}}</li>

@can('markets.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/markets*') ? 'active' : '' }}" href="{!! route('markets.index') !!}">
            @if($icons)<i class="nav-icon fa fa-shopping-basket"></i>@endif
            <p>{{trans('lang.market_plural')}}</p>
        </a>
    </li>
@endcan

@if(auth()->user()->hasRole('manager'))
    @can('users.index')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('settings/users*') ? 'active' : '' }}" href="{!! route('users.index') !!}">
                @if($icons)<i class="nav-icon fa fa-star"></i>@endif
                <p>{{trans('lang.manager_plural')}}</p>
            </a>
        </li>
    @endcan
@endif

{{--@can('markets.index')
    <li class="nav-item has-treeview {{ (Request::is('admin/markets*') || Request::is('admin/galleries*') || Request::is('admin/marketReviews*')) && !Request::is('admin/marketsPayouts*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ (Request::is('admin/markets*') || Request::is('admin/galleries*') || Request::is('admin/marketReviews*')) && !Request::is('admin/marketsPayouts*')? 'active' : '' }}"> @if($icons)
                <i class="nav-icon fa fa-shopping-basket"></i>@endif
            <p>{{trans('lang.market_plural')}} <i class="right fa fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('markets.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/markets*') ? 'active' : '' }}" href="{!! route('markets.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-reorder"></i>@endif<p>{{trans('lang.market_plural')}}</p></a>
                </li>
            @endcan
            @can('galleries.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/galleries*') ? 'active' : '' }}" href="{!! route('galleries.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-image"></i>@endif<p>{{trans('lang.gallery_plural')}}</p></a>
                </li>
            @endcan--}}
            {{--@can('marketReviews.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/marketReviews*') ? 'active' : '' }}" href="{!! route('marketReviews.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-comments"></i>@endif<p>{{trans('lang.market_review_plural')}}</p></a>
                </li>
            @endcan
        </ul>
    </li>
@endcan--}}

@can('orders.index')
    <li class="nav-item has-treeview {{ Request::is('admin/orders*') || Request::is('admin/orderStatuses*') || Request::is('admin/deliveryAddresses*')? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/orders*') || Request::is('admin/orderStatuses*') || Request::is('admin/deliveryAddresses*')? 'active' : '' }}"> @if($icons)
                <i class="nav-icon fa fa-shopping-bag"></i>@endif
            <p>{{trans('lang.order_plural')}} <i class="right fa fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/pending*') ? 'active' : '' }}" href="{!! route('orders.list', ['pending']) !!}">
                        @if($icons)<i class="nav-icon fa fa-boxes"></i>@endif
                        <p>{{trans('lang.pending_orders')}}</p>
                    </a>
                </li>
            @endcan
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/processing*') ? 'active' : '' }}" href="{!! route('orders.list', ['processing']) !!}">
                        @if($icons)<i class="nav-icon fa fa-box-open"></i>@endif
                        <p>{{trans('lang.processing_orders')}}</p>
                    </a>
                </li>
            @endcan
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/ready_to_collect*') ? 'active' : '' }}" href="{!! route('orders.list', ['ready_to_collect']) !!}">
                        @if($icons)<i class="nav-icon fa fa-box"></i>@endif
                        <p>{{trans('lang.ready_to_collect_orders')}}</p>
                    </a>
                </li>
            @endcan
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/ready_to_delivery*') ? 'active' : '' }}" href="{!! route('orders.list', ['ready_to_delivery']) !!}">
                        @if($icons)<i class="nav-icon fa fa-dolly"></i>@endif
                        <p>{{trans('lang.ready_to_delivery_orders')}}</p>
                    </a>
                </li>
            @endcan
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/sent*') ? 'active' : '' }}" href="{!! route('orders.list', ['sent']) !!}">
                        @if($icons)<i class="nav-icon fas fa-shipping-fast"></i>@endif
                        <p>{{trans('lang.sent_orders')}}</p>
                    </a>
                </li>
            @endcan
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/delivered*') ? 'active' : '' }}" href="{!! route('orders.list', ['delivered']) !!}">
                        @if($icons)<i class="nav-icon fa fa-check"></i>@endif
                        <p>{{trans('lang.delivered_orders')}}</p>
                    </a>
                </li>
            @endcan
            @can('orders.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/orders/list/canceled*') ? 'active' : '' }}" href="{!! route('orders.list', ['canceled']) !!}">
                        @if($icons)<i class="nav-icon fa fa-times"></i>@endif
                        <p>{{trans('lang.canceled_orders')}}</p>
                    </a>
                </li>
            @endcan
            @if(auth()->user()->hasRole('admin'))
                @can('deliveryAddresses.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/deliveryAddresses*') ? 'active' : '' }}" href="{!! route('deliveryAddresses.index') !!}">@if($icons)<i class="nav-icon fa fa-map"></i>@endif<p>{{trans('lang.delivery_address_plural')}}</p></a>
                </li>
                @endcan
            @endif
        </ul>
    </li>
@endcan

@if(auth()->user()->hasRole('admin'))
    @can('users.index')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/users*') ? 'active' : '' }}" href="{!! route('users.index') !!}">
                @if($icons)<i class="nav-icon fa fa-star"></i>@endif<p>{{trans('lang.user_plural')}} administrativos</p>
            </a>
        </li>
    @endcan
@endif

@can('drivers.index')
    <li class="nav-item has-treeview {{ Request::is('admin/drivers*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/drivers*') ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-truck"></i>@endif
            <p>{{trans('lang.driver_plural')}} <i class="right fa fa-angle-left"></i></p>
        </a>
        <ul class="nav nav-treeview">
            @can('drivers.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/drivers*') && !Request::is('admin/drivers/reports*') ? 'active' : '' }}" href="{!! route('drivers.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-truck"></i>@endif
                        <p>{{trans('lang.driver_plural')}}</p>
                    </a>
                </li>
            @endcan
            @can('drivers.reports')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/drivers/reports*') ? 'active' : '' }}" href="{!! route('drivers.reports') !!}">
                        @if($icons)<i class="nav-icon fa fa-line-chart"></i>@endif
                        <p>{{trans('lang.reports')}}</p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan

@can('order_pickers.index')
    <li class="nav-item has-treeview {{ Request::is('admin/order_pickers*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/order_pickers*') ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-users-cog"></i>@endif
            <p>{{trans('lang.order_picker_plural')}} <i class="right fa fa-angle-left"></i></p>
        </a>
        <ul class="nav nav-treeview">
            @can('order_pickers.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/order_pickers*') && !Request::is('admin/order_pickers/reports*') ? 'active' : '' }}" href="{!! route('order_pickers.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-users-cog"></i>@endif
                        <p>{{trans('lang.order_picker_plural')}}</p>
                    </a>
                </li>
            @endcan
            @can('order_pickers.reports')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/order_pickers/reports*') ? 'active' : '' }}" href="{!! route('order_pickers.reports') !!}">
                        @if($icons)<i class="nav-icon fa fa-line-chart"></i>@endif
                        <p>{{trans('lang.reports')}}</p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan

@can('products.index')
    <li class="nav-item has-treeview {{ Request::is('admin/products*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/products*') ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-bookmark"></i>@endif
            <p>{{trans('lang.product_plural')}} <i class="right fa fa-angle-left"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/products*') && (!request()->validated || request()->validated != 'no') ? 'active' : '' }}" href="{!! route('products.index') !!}">
                    @if($icons)<i class="nav-icon fa fa-circle text-success"></i>@endif
                    <p>{{trans('lang.product_plural')}} revisados</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (request()->validated && request()->validated == 'no') ? 'active' : '' }}" href="{!! route('products.index', ['validated' => 'no']) !!}">
                    @if($icons)<i class="nav-icon fa fa-circle text-warning"></i>@endif
                    <p>Produtos não revisados</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (request()->manually_disabled && request()->manually_disabled == 'yes') ? 'active' : '' }}" href="{!! route('products.index', ['manually_disabled' => 'yes']) !!}">
                    @if($icons)<i class="nav-icon fa fa-circle text-danger"></i>@endif
                    <p>Produtos desativados</p>
                </a>
            </li>
        </ul>
    </li>
@endcan

{{--@can('products.index')
    <li class="nav-item has-treeview {{ Request::is('admin/products*') || Request::is('admin/options*') || Request::is('admin/productReviews*') || Request::is('admin/nutrition*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{  Request::is('admin/products*') || Request::is('admin/options*') || Request::is('admin/productReviews*') || Request::is('admin/nutrition*') ? 'active' : '' }}"> @if($icons)
                <i class="nav-icon fa fa-bookmark"></i>@endif
            <p>{{trans('lang.product_plural')}} <i class="right fa fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('products.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('products*') ? 'active' : '' }}" href="{!! route('products.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-bookmark"></i>@endif
                        <p>{{trans('lang.product_plural')}}</p></a>
                </li>
            @endcan
            @can('optionGroups.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/optionGroups*') ? 'active' : '' }}" href="{!! route('optionGroups.index') !!}">@if($icons)<i class="nav-icon fa fa-plus-square"></i>@endif<p>{{trans('lang.option_group_plural')}}</p></a>
                </li>
            @endcan--}}
            {{--@can('options.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/options*') ? 'active' : '' }}" href="{!! route('options.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-plus-square-o"></i>@endif<p>{{trans('lang.option_plural')}}</p></a>
                </li>
            @endcan
            @can('productReviews.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/productReviews*') ? 'active' : '' }}" href="{!! route('productReviews.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-comments"></i>@endif<p>{{trans('lang.product_review_plural')}}</p></a>
                </li>
            @endcan
        </ul>
    </li>
@endcan--}}

@can('customers.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/customers*') ? 'active' : '' }}" href="{!! route('customers.index') !!}">
            @if($icons)<i class="nav-icon fa fa-group"></i>@endif
            <p>{{trans('lang.customer_plural')}}</p>
        </a>
    </li>
@endcan

@can('cash_register.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/cash_register*') ? 'active' : '' }}" href="{!! url('admin/cash_register') !!}">
            @if($icons)<i class="nav-icon fa fa-cash-register"></i>@endif
            <p>{{trans('lang.cash_register')}}</p>
        </a>
    </li>
@endcan

@can('faqs.index')
    <li class="nav-item has-treeview {{ Request::is('admin/faqCategories*') || Request::is('admin/faqs*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/faqs*') || Request::is('admin/faqCategories*') ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-support"></i>@endif
            <p>{{trans('lang.faq_plural')}} <i class="right fa fa-angle-left"></i></p>
        </a>
        <ul class="nav nav-treeview">
            @can('faqCategories.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/faqCategories*') ? 'active' : '' }}" href="{!! route('faqCategories.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-folder"></i>@endif
                        <p>{{trans('lang.faq_category_plural')}}</p>
                    </a>
                </li>
            @endcan

            @can('faqs.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/faqs*') ? 'active' : '' }}" href="{!! route('faqs.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-question-circle"></i>@endif
                        <p>{{trans('lang.faq_plural')}}</p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan

<li class="nav-header">{{trans('lang.app_setting')}}</li>

@can('account.index')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/account*') ? 'active' : '' }}" href="{!! route('account.index') !!}">
            @if($icons)<i class="nav-icon fa fa-cogs"></i>@endif
            <p>{{trans('lang.my_account')}}</p>
        </a>
    </li>
@endcan

@can('medias')
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/medias*') ? 'active' : '' }}" href="{!! url('admin/medias') !!}">
            @if($icons)<i class="nav-icon fa fa-picture-o"></i>@endif
            <p>{{trans('lang.media_plural')}}</p>
        </a>
    </li>
@endcan

@can('payments.index')
    <li class="nav-item has-treeview {{ Request::is('admin/drivers*') || Request::is('admin/earnings*') || Request::is('admin/driversPayouts*') || Request::is('admin/marketsPayouts*') || Request::is('admin/payments*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/drivers*') || Request::is('admin/earnings*') || Request::is('admin/driversPayouts*') || Request::is('admin/marketsPayouts*') || Request::is('admin/payments*') ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-credit-card"></i>@endif
            <p>{{trans('lang.payment_plural')}}<i class="right fa fa-angle-left"></i></p>
        </a>
        <ul class="nav nav-treeview">
            @can('payments.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/payments*') ? 'active' : '' }}" href="{!! route('payments.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-money"></i>@endif
                        <p>{{trans('lang.payment_plural')}}</p>
                    </a>
                </li>
            @endcan

            @can('earnings.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/earnings*') ? 'active' : '' }}" href="{!! route('earnings.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-money"></i>@endif
                        <p>{{trans('lang.earning_plural')}} <span class="right badge badge-danger">New</span> </p>
                    </a>
                </li>
            @endcan

            @can('driversPayouts.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/driversPayouts*') ? 'active' : '' }}" href="{!! route('driversPayouts.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-dollar"></i>@endif
                        <p>{{trans('lang.drivers_payout_plural')}}</p>
                    </a>
                </li>
            @endcan

            @can('marketsPayouts.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/marketsPayouts*') ? 'active' : '' }}" href="{!! route('marketsPayouts.index') !!}">
                        @if($icons)<i class="nav-icon fa fa-dollar"></i>@endif
                        <p>{{trans('lang.markets_payout_plural')}}</p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan

@can('app-settings')
    {{--<li class="nav-item has-treeview {{ Request::is('admin/settings/mobile*') ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{ Request::is('admin/settings/mobile*') ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-mobile"></i>@endif
            <p>
                {{trans('lang.mobile_menu')}}
                <i class="right fa fa-angle-left"></i>
            </p></a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{!! url('admin/settings/mobile/globals') !!}" class="nav-link {{  Request::is('admin/settings/mobile/globals*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-cog"></i> @endif <p>{{trans('lang.app_setting_globals')}} <span class="right badge badge-danger">New</span> </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/mobile/colors') !!}" class="nav-link {{  Request::is('admin/settings/mobile/colors*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-pencil"></i> @endif <p>{{trans('lang.mobile_colors')}} <span class="right badge badge-danger">New</span> </p>
                </a>
            </li>
        </ul>

    </li>--}}
    <li class="nav-item">
        <a href="{!! url('admin/settings/app/globals') !!}" class="nav-link {{ (Request::is('admin/settings*') || Request::is('admin/menu*') || Request::is('admin/notificationTypes*')) ? 'active' : '' }}">
            @if($icons)<i class="nav-icon fa fa-cog"></i> @endif
            <p>{{trans('lang.app_setting')}}</p>
        </a>
    </li>
    {{--<li class="nav-item has-treeview {{
    (Request::is('admin/settings*') ||
     Request::is('admin/users*')) && !Request::is('admin/settings/mobile*')
        ? 'menu-open' : '' }}">
        <a href="#" class="nav-link {{
        (Request::is('admin/settings*') ||
         Request::is('admin/users*')) && !Request::is('admin/settings/mobile*')
          ? 'active' : '' }}"> @if($icons)<i class="nav-icon fa fa-cogs"></i>@endif
            <p>{{trans('lang.app_setting')}} <i class="right fa fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{!! url('admin/settings/app/globals') !!}" class="nav-link {{  Request::is('admin/settings/app/globals*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-cog"></i> @endif <p>{{trans('lang.app_setting_globals')}}</p>
                </a>
            </li>

            @can('users.index')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/users*') ? 'active' : '' }}" href="{!! route('users.index') !!}">@if($icons)
                            <i class="nav-icon fa fa-users"></i>@endif
                        <p>{{trans('lang.user_plural')}}</p></a>
                </li>
            @endcan

            <li class="nav-item has-treeview {{ Request::is('admin/settings/permissions*') || Request::is('admin/settings/roles*') ? 'menu-open' : '' }}">
                <a href="#" class="nav-link {{ Request::is('admin/settings/permissions*') || Request::is('admin/settings/roles*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-user-secret"></i>@endif
                    <p>
                        {{trans('lang.permission_menu')}}
                        <i class="right fa fa-angle-left"></i>
                    </p></a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('admin/settings/permissions') ? 'active' : '' }}" href="{!! route('permissions.index') !!}">
                            @if($icons)<i class="nav-icon fa fa-circle-o"></i>@endif
                            <p>{{trans('lang.permission_table')}}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('admin/settings/permissions/create') ? 'active' : '' }}" href="{!! route('permissions.create') !!}">
                            @if($icons)<i class="nav-icon fa fa-circle-o"></i>@endif
                            <p>{{trans('lang.permission_create')}}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('admin/settings/roles') ? 'active' : '' }}" href="{!! route('roles.index') !!}">
                            @if($icons)<i class="nav-icon fa fa-circle-o"></i>@endif
                            <p>{{trans('lang.role_table')}}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('admin/settings/roles/create') ? 'active' : '' }}" href="{!! route('roles.create') !!}">
                            @if($icons)<i class="nav-icon fa fa-circle-o"></i>@endif
                            <p>{{trans('lang.role_create')}}</p>
                        </a>
                    </li>
                </ul>

            </li>

            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/settings/customFields*') ? 'active' : '' }}" href="{!! route('customFields.index') !!}">@if($icons)
                        <i class="nav-icon fa fa-list"></i>@endif<p>{{trans('lang.custom_field_plural')}}</p></a>
            </li>


            <li class="nav-item">
                <a href="{!! url('admin/settings/app/localisation') !!}" class="nav-link {{  Request::is('admin/settings/app/localisation*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-language"></i> @endif <p>{{trans('lang.app_setting_localisation')}}</p></a>
            </li>
            <li class="nav-item">
                <a href="{!! url('admin/settings/translation/en') !!}" class="nav-link {{ Request::is('admin/settings/translation*') ? 'active' : '' }}">
                    @if($icons) <i class="nav-icon fa fa-language"></i> @endif <p>{{trans('lang.app_setting_translation')}}</p></a>
            </li>
            @can('currencies.index')
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/settings/currencies*') ? 'active' : '' }}" href="{!! route('currencies.index') !!}">@if($icons)<i class="nav-icon fa fa-dollar"></i>@endif<p>{{trans('lang.currency_plural')}}</p></a>
            </li>
            @endcan

            <li class="nav-item">
                <a href="{!! url('admin/settings/payment/payment') !!}" class="nav-link {{  Request::is('admin/settings/payment*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-credit-card"></i> @endif <p>{{trans('lang.app_setting_payment')}}</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/app/social') !!}" class="nav-link {{  Request::is('admin/settings/app/social*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-globe"></i> @endif <p>{{trans('lang.app_setting_social')}}</p>
                </a>
            </li>


            <li class="nav-item">
                <a href="{!! url('admin/settings/app/notifications') !!}" class="nav-link {{  Request::is('admin/settings/app/notifications*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-bell"></i> @endif <p>{{trans('lang.app_setting_notifications')}}</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{!! url('admin/settings/mail/smtp') !!}" class="nav-link {{ Request::is('admin/settings/mail*') ? 'active' : '' }}">
                    @if($icons)<i class="nav-icon fa fa-envelope"></i> @endif <p>{{trans('lang.app_setting_mail')}}</p>
                </a>
            </li>

        </ul>
    </li>--}}
@endcan


