@push('css_lib')
@include('layouts.datatables_css')
@endpush

{!! $dataTable->table(['width' => '100%']) !!}

@push('scripts_lib')
@include('layouts.datatables_js')
{!! $dataTable->scripts() !!}
@endpush

@push('scripts')
    <script>
        function clearInputBG(button){
            $(button).closest('.input-group').find('input').removeClass('bg-warning').removeAttr('style');
            $(button).remove();
        }
    </script>
@endpush