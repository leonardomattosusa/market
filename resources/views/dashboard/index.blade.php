@extends('layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header content-header{{setting('fixed_header')}}">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{trans('lang.dashboard')}}</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a href="/admin?period=month" class="btn {{ (request()->period == 'month' ? 'btn-primary' : 'btn-secondary') }} btn-sm">Mês</a>
                        <a href="/admin?period=year" class="btn {{ (!request()->period || request()->period == 'year' ? 'btn-primary' : 'btn-secondary') }} btn-sm">Ano</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$ordersCount}}</h3>

                        <p>{{trans('lang.dashboard_total_orders')}}</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-bag"></i>
                    </div>
                    <a href="{!! route('orders.list', ['pending']) !!}" class="small-box-footer">{{trans('lang.dashboard_more_info')}}
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{$membersCount}}</h3>

                        <p>{{trans('lang.dashboard_total_clients')}}</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-group"></i>
                    </div>
                    <a href="{!! route('users.index') !!}" class="small-box-footer">{{trans('lang.dashboard_more_info')}}
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        @if(setting('currency_right', false) != false)
                            <h3>{{ number_format($earning,2,',','.') }}{{setting('default_currency')}}</h3>
                        @else
                            <h3>{{setting('default_currency')}}{{number_format($earning,2,',','.')}}</h3>
                        @endif

                        <p>{{trans('lang.dashboard_total_earnings')}} <span style="font-size: 10px">{{trans('lang.dashboard_after taxes')}}</span></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-money"></i>
                    </div>
                    <a href="#!" class="small-box-footer">{{trans('lang.dashboard_more_info')}}
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>R$ {{ number_format($averageTicket,2,',','.') }}</h3>
                        <p>Ticket médio por cliente</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cutlery"></i>
                    </div>
                    <a href="{!! route('markets.index') !!}" class="small-box-footer">{{trans('lang.dashboard_more_info')}}
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header no-border">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title">{{ trans('lang.earning_plural') }} por mês</h3>
                            <a href="#!">{{trans('lang.dashboard_view_all_payments')}}</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex">
                            <p class="d-flex flex-column">
                                <span class="text-bold text-lg" id="total-earning-graph">{{setting('default_currency')}}{{ number_format($earning,2,',','.')}}</span>
                                <span>{{trans('lang.dashboard_earning_over_time')}}</span>
                            </p>
                        </div>
                        <!-- /.d-flex -->

                        <div class="position-relative mb-4">
                            <canvas id="sales-chart" height="200"></canvas>
                        </div>

                        <div class="d-flex flex-row justify-content-end">
                            <span class="mr-2"> <i class="fa fa-square text-primary"></i> {{trans('lang.dashboard_this_year')}} </span>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header no-border">
                        <h3 class="card-title">Nossos aplicativos</h3>
                    </div>
                    <div class="card-body">
                        <div class="media">
                            <img src="{{ asset('images/imarket_app_logo.png') }}" style="width: 80px" alt="App do iMarket">
                            <div class="media-body ml-2">
                                <h5>App do iMarket</h5>
                                <small class="d-block" style="color: #777777">
                                    <div class="d-flex">
                                        <b class="mr-1">Google Play</b>
                                        <a href="https://play.google.com/store/apps/details?id=digital.imarket" class="flex-grow-1" target="_blank">https://play.google.com/store/apps/details?id=digital.imarket</a>
                                        <div class="align-items-center">
                                            <button class="btn btn-sm btn-success" data-sharer="whatsapp" data-url="https://play.google.com/store/apps/details?id=digital.imarket"><i class="fa fa-whatsapp"></i></button>
                                        </div>
                                    </div>
                                </small>
                                <small class="d-block mt-2" style="color: #777777">
                                    <div class="d-flex">
                                        <b class="mr-1">App Store</b>
                                        <a href="https://apps.apple.com/us/app/id1523712241" class="flex-grow-1" target="_blank">https://apps.apple.com/us/app/id1523712241</a>
                                        <div class="align-items-center">
                                            <button class="btn btn-sm btn-success" data-sharer="whatsapp" data-url="https://apps.apple.com/us/app/id1523712241"><i class="fa fa-whatsapp"></i></button>
                                        </div>
                                    </div>
                                </small>
                            </div>
                        </div>
                        <div class="media mt-5">
                            <img src="{{ asset('images/imarket_drivers_logo.png') }}" style="width: 80px;" alt="App do iMarket">
                            <div class="media-body ml-2">
                                <h5>App dos Entregadores</h5>
                                <small class="d-block" style="color: #777777">
                                    <div class="d-flex">
                                        <b class="mr-1">Google Play</b>
                                        <a href="https://play.google.com/store/apps/details?id=digital.imarket.delivery" class="flex-grow-1" target="_blank">https://play.google.com/store/apps/details?id=digital.imarket.delivery</a>
                                        <div class="align-items-center">
                                            <button class="btn btn-sm btn-success" data-sharer="whatsapp" data-url="https://play.google.com/store/apps/details?id=digital.imarket.delivery"><i class="fa fa-whatsapp"></i></button>
                                        </div>
                                    </div>
                                </small>
                                <small class="d-block mt-2" style="color: #777777">
                                    <div class="d-flex">
                                        <b class="mr-1">App Store</b>
                                        <a href="https://apps.apple.com/us/app/id1526682586" class="flex-grow-1" target="_blank">https://apps.apple.com/us/app/id1526682586</a>
                                        <div class="align-items-center">
                                            <button class="btn btn-sm btn-success" data-sharer="whatsapp" data-url="https://apps.apple.com/us/app/id1526682586"><i class="fa fa-whatsapp"></i></button>
                                        </div>
                                    </div>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header no-border">
                        <h3 class="card-title">{{trans('lang.market_plural')}}</h3>
                        <div class="card-tools">
                            <a href="{{route('markets.index')}}" class="btn btn-tool btn-sm"><i class="fa fa-bars"></i> </a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped table-valign-middle">
                            <thead>
                            <tr>
                                <th>{{trans('lang.market_image')}}</th>
                                <th>{{trans('lang.market')}}</th>
                                <th>{{trans('lang.market_address')}}</th>
                                <th>{{trans('lang.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($markets as $market)

                                <tr>
                                    <td>
                                        {!! getMediaColumn($market, 'image','img-circle img-size-32 mr-2') !!}
                                    </td>
                                    <td>{!! $market->name !!}</td>
                                    <td>
                                        {!! $market->address !!}
                                    </td>
                                    <td class="text-center">
                                        <a href="{!! route('markets.edit',$market->id) !!}" class="text-muted"> <i class="fa fa-edit"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts_lib')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sharer.js@latest/sharer.min.js"></script>
@endpush
@push('scripts')
    <script type="text/javascript">
        var data = [];
        var labels = [];

        function renderChart(chartNode, data, labels) {
            var ticksStyle = {
                fontColor: '#495057',
                fontStyle: 'bold'
            };

            var mode = 'index';
            var intersect = true;
            return new Chart(chartNode, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            backgroundColor: '#007bff',
                            borderColor: '#007bff',
                            data: data
                        }
                    ]
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        mode: mode,
                        intersect: intersect
                    },
                    hover: {
                        mode: mode,
                        intersect: intersect
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            // display: false,
                            gridLines: {
                                display: true,
                                lineWidth: '4px',
                                color: 'rgba(0, 0, 0, .2)',
                                zeroLineColor: 'transparent'
                            },
                            ticks: $.extend({
                                beginAtZero: true,

                                // Include a dollar sign in the ticks
                                callback: function (value, index, values) {
                                    if (value >= 1000) {
                                        value /= 1000
                                        value += 'k'
                                    }
                                    return '$' + value
                                }
                            }, ticksStyle)
                        }],
                        xAxes: [{
                            display: true,
                            gridLines: {
                                display: false
                            },
                            ticks: ticksStyle
                        }]
                    }
                }
            })
        }

        $(function () {
            'use strict'

            var $salesChart = $('#sales-chart')
            $.ajax({
                url: "{!! $ajaxEarningUrl !!}",
                success: function (result) {
                    $("#loadingMessage").html("");

                    var total = 0;

                    $.each(result.data[0], function(index, value){
                       total += value;
                    });

                    $("#total-earning-graph").html(total.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));

                    var data = result.data[0];
                    var labels = result.data[1];
                    renderChart($salesChart, data, labels)
                },
                error: function (err) {
                    $("#loadingMessage").html("Error");
                }
            });
            //var salesChart = renderChart($salesChart, data, labels);
        })

    </script>
@endpush