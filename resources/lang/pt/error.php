<?php 

return [
    'back' => 'Voltar',
    'back_to_home' => 'Voltar para o perfil',
    'page' => [
        'not_found' => 'Página não encontrada',
    ],
    'permission' => 'Usuário não tem essa permissão',
    'store_disabled' => 'Loja desativada',
];