<?php

return [
    'agree' => 'Eu concordo com os termos',
    'already_member' => 'Já sou cadastrado',
    'email' => 'E-mail',
    'failed' => 'As credenciais indicadas não coincidem com as registadas no sistema.',
    'forgot_password' => 'Esqueci minha senha',
    'login' => 'Login',
    'login_facebook' => 'Login com o Facebook',
    'login_google' => 'Login com o Google',
    'login_title' => 'Efetuar login',
    'login_twitter' => 'Login com o Twitter',
    'logout' => 'Logout',
    'name' => 'Nome completo',
    'password' => 'Senha',
    'password_confirmation' => 'Confirme sua senha',
    'register' => 'Cadastrar',
    'register_new_member' => 'Cadastrar um novo usuário',
    'remember_me' => 'Lembrar-me',
    'remember_password' => 'Retornar para login',
    'reset_password' => 'Redefinir senha',
    'reset_password_title' => 'Digite sua nova senha',
    'reset_title' => 'Digite seu e-mail para redefinir sua senha',
    'send_password' => 'Enviar link de redefinição de senha',
    'throttle' => 'O número limite de tentativas de login foi atingido. Por favor tente novamente dentro de :seconds segundos.'
];