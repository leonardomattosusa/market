function createElementFromTemplate(main_template, data, main_div){
    var template = $(main_template).clone();
    var data = data;

    template.find('[data-attributes]').each(function(index, div){
        let attributes = $(div).attr('data-attributes').split(',');
        let variables = $(div).attr('data-variables').split(',');

        for ($i = 0; $i < attributes.length; $i++){
            let current_attribute = attributes[$i];
            let current_variable = variables[$i];

            updateElementAttribute(div, current_attribute, data[current_variable]);
        }
    })

    $(template).appendTo(main_div);
}

function updateElementAttribute(div, attribute, value){
    var default_image = 'https://imarket.digital/images/image_default.png';

    switch (attribute) {
        case 'text':
            $(div).text(value);
            break
        case 'time-text':
            $(div).text(value);
            break
        case 'text_not_null':
            if(value != null && parseFloat(value.replace('R$', '')) > 0){
                $(div).text(value);
            }else{
                $(div).text('');
            }
            break
        case 'html':
            $(div).html(value);
            break
        case 'src':
            $(div).attr('src', value != undefined ? value : default_image);
            break
        /*case 'class':
            $(div).addClass(value);
            break*/
        case 'style-image':
            $(div).attr('style', value != undefined ? 'background: url(' + value + '); background-size:contain !important' : 'background: url(' + default_image + '); background-size:contain !important');
            break
        default:
            $(div).attr(attribute, value);
            break
    }
}
